//
//  CarritoDeliveryPriceTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 10/12/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

protocol CarritoDeliveryPickupDelegate {
    func didSwitchPickup (forOrder: MOrder, wantsPickup: Bool,cell:CarritoDeliveryPriceTableViewCell)
}

class CarritoDeliveryPriceTableViewCell: UITableViewCell {

    @IBOutlet weak var deliveryImage: UIImageView!
    @IBOutlet weak var deliveryPriceLbl: UILabel!
    @IBOutlet weak var deliveryTitleLbl: UILabel!

    var order: MOrder!
    var customDelegate: CarritoDeliveryPickupDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func pickupSwitchDidChange(sender: UISwitch) {
        customDelegate?.didSwitchPickup(self.order, wantsPickup: sender.on,cell:self)
    }
}
