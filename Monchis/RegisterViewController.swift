//
//  RegisterViewController.swift
//  Monchis
//
//  Created by jrivarola on 6/22/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import DynamicBlurView
import KeychainSwift

class RegisterViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var nombres: UITextField!
    @IBOutlet weak var apellidos: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var pass1: UITextField!
    @IBOutlet weak var pass2: UITextField!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    let api = MAPI.SharedInstance
    let user = MUser.sharedInstance
    
    struct Segues {
        static let unwindToLogin = "unwindFromRegistrarseToLoginSegue"
        static let showMap = "showMapFromRegister"
        static let showMainView = "showMainViewFromRegister"
    }
    @IBAction func togglePasswordVisibility(sender: UIButton) {
        pass1.secureTextEntry = !pass1.secureTextEntry
    }
    @IBOutlet weak var registerBtn: UIButton!
    var datePicker = UIDatePicker()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Register observers
        let center = NSNotificationCenter.defaultCenter()
        
        //Notifications for keyboard
        center.addObserver(self, selector: #selector(RegisterViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(RegisterViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        datePicker.datePickerMode = .Date
        
        
        pass2.inputView = datePicker
        datePicker.addTarget(self, action: #selector(RegisterViewController.datePickerValueChanged(_:)), forControlEvents: .ValueChanged)
        datePicker.locale = user.locale
        
        // Do any additional setup after loading the view.
        
        //        registerBtn.imageView?.contentMode = .ScaleAspectFill
        //        registerBtn.setImage(UIImage(named: "registrarme"), forState: .Normal)
        //        registerBtn.setImage(UIImage(named: "registrarme"), forState: .Highlighted)
        //
        
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        pass2.text = dateFormatter.stringFromDate(sender.date)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: Keyboards notifications
    
    func keyboardWillShow(notification: NSNotification){
        _ = notification.userInfo
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            _ = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
            
            if selectedTextField == email || selectedTextField == pass1 || selectedTextField == pass2 {
                self.topConstraint.constant = -keyboardSize.height + 40
                
                self.view.layoutIfNeeded()
            }
            
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification){
        self.topConstraint.constant = 0
        self.view.layoutIfNeeded()
    }
    
    var selectedTextField: UITextField?
    var blurView: DynamicBlurView!
    var dismissTapGesture: UITapGestureRecognizer!
    
    func dismissWaitScreen(animated: Bool = true){
        MBProgressHUD.hideHUDForView(self.view, animated: animated)
    }
    
    @IBAction func backToLoginPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion:nil)
    }
    
    @IBAction func registrarsePressed(sender: UIButton) {
        
        if  pass1.text != "" && nombres.text != "" && apellidos.text != "" && email.text != ""{ //add animation
            self.view.endEditing(true)
            
            
            let spinner = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
//            let dateFormatter = NSDateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd"
//            
//            
//            let birthDate = dateFormatter.stringFromDate(datePicker.date)
            spinner.mode = MBProgressHUDMode.Indeterminate
            spinner.label.text = NSLocalizedString("Registrando",comment: "Registrando nuevo usuario")
            spinner.label.font = UIFont(name: "OpenSans-Semibold", size: 14.0)!
            
            let parameters =    [  "first_name":nombres.text ?? "",
                "last_name":apellidos.text ?? "",
                "email": email.text ?? "",
                "password":pass1.text ?? "",
                "device_token":MUser.sharedInstance.pushToken,
                "type":"1",
                "model":MUser.sharedInstance.modelName,
                "system":"iOS",
                "version":MUser.sharedInstance.systemVersion
            ]
            
            api.jsonRequest(.REGISTER_MAIL, parameters: parameters) { json, error in
                if debugModeEnabled {
                    print(#function,"parameters: \(parameters)",json,error)
                }
                
                if error == nil {
                    
                    
                    let success = json?["success"].bool ?? false
                    if let data = json?["data"] {
                        if success {
                            self.user.unparseUserInfoFromApi(data)
                            let monchis_token = data["monchis_token"].string ?? ""
                            self.api.token = monchis_token
                            self.api.mgr.session.configuration.HTTPAdditionalHeaders = ["token": "\(monchis_token)"]
                            print("Monchis token : \(monchis_token)")
                            
                            //login user after registration succesfull
                            self.user.authenticationMethod = .Monchis
                            self.user.password = self.pass1.text!
                            
                            self.loadUserAdresses()
                        }else{ // show error
                            MBProgressHUD.hideHUDForView(self.view, animated: true)
                            
                            let message = data.string ?? ""
                            let titleLocalizable = NSLocalizedString("Error",comment: "error")
                            let alert = UIAlertController(title: titleLocalizable, message: message, preferredStyle: .Alert)
                            let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                            alert.addAction(ok)
                            self.presentViewController(alert, animated: true, completion: nil)
                            
                        }
                    }else{
                        print("bad response")
                        
                    }
                }else{ //network error
                    MBProgressHUD.hideHUDForView(self.view, animated: true)
                    
                    print("network error")
                    
                    let titleLocalizable = NSLocalizedString("Error",comment: "error")
                    let alert = UIAlertController(title: titleLocalizable, message: error, preferredStyle: .Alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                    alert.addAction(ok)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
            
        }else{ // passwords dont match
            let titleLocalizable = NSLocalizedString("Lo sentimos!",comment: "Lo sentimos!")
            let messageLocalizable = NSLocalizedString("Debes completar todos los datos.",comment: "Debes completar todos los datos.")
            let alert = UIAlertController(title: titleLocalizable, message: messageLocalizable, preferredStyle: .Alert)
            let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
            alert.addAction(ok)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    
    func dismissRequest(){
        api.currentRequest?.cancel()
        dispatch_async(dispatch_get_main_queue()) {
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            self.blurView?.removeFromSuperview()
            self.blurView?.removeGestureRecognizer(self.dismissTapGesture)
        }
    }
    
    // MARK: UITextField Delegate
    
    func textFieldDidEndEditing(textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        selectedTextField = textField
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    func loadUserAdresses(){
        
        api.jsonRequest(.USER_ADDRESSES, parameters: nil) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters nil",json,error)
            }
            
            if error == nil {
                if let success = json?["success"].bool {
                    if success {
                        self.user.logged = true
                        print("Loggued IN")
                        let addresses = json?["data"].array ?? []
                        self.user.addresses = []
                        for address in addresses {
                            let testAddr = MAddress(json: address)

                            self.user.addresses.append(testAddr)
                        }
                        //set automatic login for user
                        NSUserDefaults.standardUserDefaults().setBool(true, forKey: kAutomaticLogin)
                        let keychain = KeychainSwift()
                        keychain.set(self.user.email, forKey: kMonchisUserEmail)
                        keychain.set(self.user.password, forKey: kMonchisUserPassword)
                        KeychainSwift().set("monchis", forKey: kMonchisUserAuthenticationMethod)
                        
                        self.user.authenticationMethod = .Monchis
                        self.registerPushToken()
                        self.dismissWaitScreen()
                        if self.user.addresses.count == 0 { //if user has no addresses we show the map
                            self.performSegueWithIdentifier(Segues.showMap, sender: self)
                        }else{
                            self.performSegueWithIdentifier(Segues.showMainView, sender: self)
                        }
                        self.user.addresses.sortInPlace({ (addr1, addr2) -> Bool in
                            return addr1.is_default
                        })
                    }else{
                        self.dismissWaitScreen()
                        let comment = json?["data"].string ?? "No hay mensaje"
                        let alert = UIAlertController(title: NSLocalizedString("ERROR", comment: "Titulo de error cuando se trato de conseguir las direcciones del usuario MONCHIS"), message: comment, preferredStyle: .Alert)
                        let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar mensaje de error para continuar"), style: .Default, handler: nil)
                        alert.addAction(ok)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
            }else{ // network error
                self.dismissWaitScreen()
                
                let alert = UIAlertController(title: NSLocalizedString("Error al conectarse con el servidor", comment: "Titulo de error cuando se trato de conseguir las direcciones del usuario MONCHIS"), message: error, preferredStyle: .Alert)
                let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar mensaje de error para continuar"), style: .Default, handler: nil)
                alert.addAction(ok)
                self.presentViewController(alert, animated: true, completion: nil)
                
                
            }
        }
        
    }
    
    func registerPushToken(){
        let parameters = [  "device_token":MUser.sharedInstance.pushToken,
            "type":"1",
            "model":MUser.sharedInstance.modelName,
            "system":"iOS",
            "version":MUser.sharedInstance.systemVersion
        ]
        MAPI.SharedInstance.jsonRequest(.REGISTER_USER_DEVICE, parameters: parameters) { (json, error) -> Void in
            print(#function,"called with parameters \(parameters)",json,error)
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    
                }else{
                    let message = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Error al registrar dispositivo.", message: message, preferredStyle: .Alert)
                    let cancel = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(cancel)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }else{
                let message = json?["data"].string ?? "Error desconocido"
                let alert = UIAlertController(title: "Error al registrar dispositivo.", message: message, preferredStyle: .Alert)
                let cancel = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(cancel)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
        }
    }
    
    
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
}
