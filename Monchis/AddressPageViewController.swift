//
//  AddressPageViewController.swift
//  Monchis
//
//  Created by Monchis Mac on 3/8/17.
//  Copyright © 2017 cibersons. All rights reserved.
//

import UIKit
import TextFieldEffects
import GoogleMaps
import AMPopTip
import MBProgressHUD
import SwiftyJSON

class AddressPageViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var titleLbl: UILabel!

    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var nextBtn: UIButton!
    var index: Int = 0
    var address: MAddress = MAddress()
    var tip: AMPopTip!
    var tipText: String = "Ingresa la primera calle de tu dirección"
    override func viewDidLoad() {
        super.viewDidLoad()
        inputTextField.delegate = self
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextBtnPressed(sender: UIButton) {
        self.performSegueWithIdentifier("showMapSegue", sender: self)
    }
    
    
    func getLocationFromAddress(address : String) -> CLLocationCoordinate2D {
        var lat : Double = 0.0
        var lon : Double = 0.0
        let url = "https://maps.google.com/maps/api/geocode/json?sensor=false&components=country:PY&address=" + address.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        if let result = NSData(contentsOfURL: NSURL(string: url)!) {
            let json = JSON(data: result)
            lat = json["results"][0]["geometry"]["location"]["lat"].doubleValue
            lon = json["results"][0]["geometry"]["location"]["lng"].doubleValue
        }
        
        
        
        
        return CLLocationCoordinate2D(latitude: lat, longitude: lon)
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddressPageViewController.keyboardWillShow(_:)) , name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddressPageViewController.keyboardWillHide(_:)) , name: UIKeyboardWillHideNotification, object: nil)
//        if self.index == 0 {
//            self.inputTextField.text = self.address.street1
//        }
//        if self.index == 1{
//            self.inputTextField.text = self.address.number
//        }
//        if self.index == 2{
//            self.inputTextField.text = self.address.street2
//        }
//        if self.index == 3 {
//            self.inputTextField.text = self.address.reference
//        }
//        if self.index == 5{
//            self.inputTextField.text = self.address.name
//        }


    }
    
    var keyboardHeight: CGFloat!
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        dispatch_async(dispatch_get_main_queue()) {
            self.inputTextField?.becomeFirstResponder()
            
            if self.inputTextField?.text != "" {
                self.showBtn(self.nextBtn)
            }
            let delay = 0.1 * Double(NSEC_PER_SEC)
            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
            dispatch_after(time, dispatch_get_main_queue()) {
                self.tip?.hide()
                self.configureTip()
                self.tip.showText(self.tipText, direction: .Down, maxWidth: self.view.bounds.width*0.75, inView: self.view, fromFrame: self.inputTextField.frame)
            }
            
            
        }
    }
    
    func keyboardWillShow(notification: NSNotification){
        let userInfo:NSDictionary = notification.userInfo!
        let keyboardFrame:NSValue = userInfo.valueForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.CGRectValue()
        keyboardHeight = keyboardRectangle.height
    }
    func keyboardWillHide(notification: NSNotification){
        self.tip?.hide()
    }
    
    
    var blockSwipe: Bool = false
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        
        
        if (range.length == 1 && string.isEmpty  && textField.text?.characters.count == 1){
            hideBtn(nextBtn)
            NSNotificationCenter.defaultCenter().postNotificationName("blockPageViewScrollingNotification", object: nil)
            textField.becomeFirstResponder()
            return true
        }else{
            showBtn(nextBtn)
            if textField.text == "" {
                NSNotificationCenter.defaultCenter().postNotificationName("enablePageViewScrollingNotification", object: nil)
                textField.becomeFirstResponder()
            }
        }
        dispatch_async(dispatch_get_main_queue()) {
        }
        
        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            self.address.coordinate = self.getLocationFromAddress(textField.text! + string)
            print(self.address.coordinate)
            dispatch_async(dispatch_get_main_queue(), { () -> Void in

            })
        })
        

        
//        if textField.text == "" && string == "" {
//            hideBtn(nextBtn)
//            NSNotificationCenter.defaultCenter().postNotificationName("blockPageViewScrollingNotification", object: nil)
//        }else if textField.text?.characters.count == 1 && string == "" {
//            hideBtn(nextBtn)
////            NSNotificationCenter.defaultCenter().postNotificationName("blockPageViewScrollingNotification", object: nil)
//        }else{
//            showBtn(nextBtn)
//            NSNotificationCenter.defaultCenter().postNotificationName("enablePageViewScrollingNotification", object: nil)
//        }
//        if index == 0 {
//            address.street1 = (textField.text ?? "") + string
//        }
//        if index == 1{
//            address.number = (textField.text ?? "") + string
//        }
//        if index == 2{
//            address.street2 = (textField.text ?? "") + string
//        }
//        if index == 3 {
//            address.reference = (textField.text ?? "") + string
//        }
//        if index == 5{
//            address.name = (textField.text ?? "") + string
//        }
        //        fullAddressLbl.text = address.formattedAddress
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
//        if index == 0 {
//            address.street1 = (textField.text ?? "")
//        }
//        if index == 1{
//            address.number = (textField.text ?? "") 
//        }
//        if index == 2{
//            address.street2 = (textField.text ?? "")
//        }
//        if index == 3 {
//            address.reference = (textField.text ?? "")
//        }
//        if index == 5{
//            address.name = (textField.text ?? "")
//        }
        address.street1 = (textField.text ?? "")
        return true
    }
    
    
    func configureTip(){
        self.tip = AMPopTip()
        self.tip.popoverColor = UIColor(hexString: "#353333")!
        self.tip.shouldDismissOnTap = true
    }
    
    
    func hideBtn(button: UIButton) {
        self.tip?.hide()
        configureTip()
        self.tip.showText(self.tipText, direction: .Down, maxWidth: self.view.bounds.width*0.75, inView: self.view, fromFrame: self.inputTextField.frame)
        UIView.animateWithDuration(0.5, delay: 0, options: [.CurveEaseOut, .BeginFromCurrentState], animations: {
            button.alpha = 0
            button.transform = CGAffineTransformMakeScale(0.001, 0.001)
        }) { (fin) in
            button.hidden = true
            self.showingBtn = false
        }
    }
    
    var showingBtn: Bool = false
    func showBtn(button: UIButton) {
        self.tip?.hide()
        if showingBtn {
            return
        }
        showingBtn = true
        button.alpha = 0
        button.transform = CGAffineTransformMakeScale(0.001, 0.001)
        button.hidden = false
        UIView.animateWithDuration(0.5, delay: 0, options: [.CurveEaseOut,.BeginFromCurrentState], animations: {
            button.alpha = 1
            button.transform = CGAffineTransformIdentity
        }) { (fin) in
            
        }
    }
    
    
    
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        if segue.identifier == "showMapSegue" {
            let dvc = segue.destinationViewController as! AddressMapV3ViewController
            dvc.address = self.address
        }
     }
    
}
