//
//  MUser.swift
//  Monchis
//
//  Created by jrivarola on 6/24/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftyJSON
import KeychainSwift
import FacebookLogin
import FacebookCore
import SCLAlertView
import MBProgressHUD
import Mixpanel


let kUserLoggedInNotification = "userLoggedInNotification"

class MUser {
    
    // shared user object instance
    static let sharedInstance = MUser()
    var debugMode: Bool = false //for api debug
    var authenticationMethod: Authentication = .Unregistered
    var logged: Bool = false {
        didSet {
            if logged {
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "userLoggedKey")
                NSUserDefaults.standardUserDefaults().synchronize()
                NSNotificationCenter.defaultCenter().postNotificationName(kUserLoggedInNotification, object: nil)
            }else{
                resetUser()
            }
        }
    }
    let facebookLoginManager = LoginManager()
    var invoice_name: String?
    var invoice_ruc: String?
    var addresses:[MAddress] = []
    var preferences = MPreferences()
    var first_name = ""
    var last_name = ""
    var ruc: String?
    var rucSocialName: String?
    var birthDate:NSDate?
    var pushToken: String = ""
    var email = ""
    var id: Int = 0
    var password: String = ""
    var monchis_token: String =  ""
    var api_password: String = ""
    var facebook_id: String =  ""
    var facebook_token: String =  ""
    var twitter_id: String =  ""
    var twitter_token: String =  ""
    var document_number: String = ""
    var favorites: [MFavorite] = []
    var gender: String? = ""
    var birth_date: String? = ""
    var phone: String? = ""
    var locale: NSLocale  {
        return NSLocale(localeIdentifier: "es_PY")
    }
    
    let systemVersion = UIDevice.currentDevice().systemVersion
    let modelName = UIDevice.currentDevice().modelName
    
    var country: String {
        return self.locale.localeIdentifier
    }
    
    var defaultAddress: MAddress {
        return self.addresses.filter{$0.is_default == true}.first ?? MAddress()
    }
    
    init(){
        
    }
    
    var productFavorites: [MProduct] = []
    var branchesFavorites: [MRestaurant] = []
    var orderHistory:[MOrder] = []
    var orderHistoryForCentralController: [MOrder] = []
    
    var orderHistoryByProducts: [MProduct] {
        var tmp : [MProduct] = []
        for order in self.orderHistoryForCentralController {
            for product in order.products {
                product.branch_products = [order.branch]
                tmp.append(product)
            }
        }
        return tmp
    }
    
    func resetUser(){
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "userLoggedKey")
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: kAutomaticLogin)
        addresses = []
        first_name = ""
        last_name = ""
        email = ""
        user_id = nil
        password = ""
        monchis_token =  ""
        phone = ""
        api_password = ""
        facebook_id =  ""
        facebook_token =  ""
        twitter_id =  ""
        user_id = nil
        twitter_token =  ""
        invoice_ruc = ""
        invoice_name = ""
        gender = ""
        birth_date = ""
        MAPI.SharedInstance.token = ""
        cacheFormats = [:]
        mainviewImageCacheFormtas = [:]
        NSUserDefaults.standardUserDefaults().setValue(cacheFormats, forKey: "cacheFormats")
        NSUserDefaults.standardUserDefaults().setValue(mainviewImageCacheFormtas, forKey: "mainviewImageCacheFormtas")
        document_number = ""
        deliveryType = .Delivery
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: kAutomaticLogin)
        NSUserDefaults.standardUserDefaults().synchronize()
        if authenticationMethod == .Facebook {
            facebookLoginManager.logOut()
        }
        MCart.sharedInstance.clear()
        self.productFavorites = []
        self.orderHistory = []
        self.branchesFavorites = []
        authenticationMethod = .Unregistered
    }
    
    
    var isTemporal: Bool {
        if self.email.containsString("aaxx-") && self.email.containsString("@temporal.monchis") {
            return true
        }
        return false
    }
    
    var user_id:Int?
    
    enum Authentication: String {
        case Facebook = "FACEBOOK"
        case Twitter = "TWITTER"
        case Monchis = "MONCHIS"
        case Unregistered = "No Registrado"
    }
    
    enum DeliveryType: Int {
        case Delivery = 1
        case Pickup = 2
    }
    
    
    
    var deliveryType: DeliveryType = .Delivery  {
        didSet{
            NSUserDefaults.standardUserDefaults().setValue(deliveryType.rawValue, forKey: kUserDeliveryType)
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    func unparseUserInfoFromApi(json: JSON){
        self.monchis_token = json["monchis_token"].string ?? ""
        self.first_name = json["first_name"].string ?? ""
        self.last_name = json["last_name"].string ?? ""
        self.email = json["email"].string ?? ""
        self.facebook_id = json["facebook_id"].string ?? ""
        self.facebook_token = json["facebook_token"].string ?? ""
        self.twitter_id = json["twitter_id"].string ?? ""
        self.twitter_token = json["twitter_token"].string ?? ""
        self.document_number = json["document_number"].string ?? ""
        self.invoice_name = json["invoice_name"].string
        self.invoice_ruc = json["invoice_ruc"].string
        self.birth_date = json["birth_date"].string
        self.phone = json["phone"].string ?? ""
        self.user_id = json["user_id"].int
        self.id = json["id"].int ?? 0
        self.gender = json["gender"].string
        Mixpanel.mainInstance().people.set(property: "$first_name",
                                           to: self.first_name)
        Mixpanel.mainInstance().people.set(property: "$last_name",
                                           to: self.last_name)
        Mixpanel.mainInstance().people.set(property: "Fecha de Nacimiento" ,
                                           to: self.birth_date!)
        Mixpanel.mainInstance().people.set(property: "Razon Social",
                                           to: self.invoice_name ?? "")
        Mixpanel.mainInstance().people.set(property: "RUC",
                                           to: self.invoice_ruc ?? "")
        
        Mixpanel.mainInstance().people.set(property: "Sexo",
                                           to: self.gender ?? "")
        Mixpanel.mainInstance().people.set(property: "Numero de Cedula",
                                           to: self.document_number)
        if phone != "" {
            Mixpanel.mainInstance().people.set(properties: ["$phone":self.phone!])
        }
        
        if let addrs = json["addresses"].array {
            var tmp :[MAddress] = []
            for adr in addrs {
                tmp.append(MAddress(json: adr))
            }
            self.addresses = tmp
            Mixpanel.mainInstance().people.set(property: "Direccion Default Latitud", to: self.defaultAddress.latitude ?? "")
            Mixpanel.mainInstance().people.set(property: "Direccion Default Longitud",to: self.defaultAddress.longitude ?? "")
            
        }
    }
    
    
    
    
    func updateUserData(parameters:[String: AnyObject],completion: (success: Bool, erorrMsg: String?) -> Void){
        
        MAPI.SharedInstance.jsonRequest(.UPDATE_USER, parameters: parameters) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters\(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    self.unparseUserInfoFromApi(json!["data"])
                    completion(success: true, erorrMsg: nil)
                }else{
                    completion(success: true, erorrMsg: json?["data"].string ?? "Error desconocido.")
                }
            }else{
                completion(success: true,erorrMsg: "Error desconocido.")
            }
        }
    }
    
    
    
    
    
    func refreshToken(){
        if self.logged {
            MAPI.SharedInstance.jsonRequest(.REFRESH_TOKEN, parameters: nil, cacheRequest: false, returnCachedDataIfPossible: false, checkNetworkConnectivity: false) { (json, error) in
                if debugModeEnabled {
                    print("Refresh token called", "response \(json,error)")
                }
                if let success = json?["success"].bool {
                    if success {
                        let monchis_token = json?["data"]["monchis_token"].string ?? ""
                        MAPI.SharedInstance.token = monchis_token
                    }else{
                        self.loginErrorHandler()
                        
                    }
                }else{
                    self.refreshToken()
                }
            }
        }
    }
    
    
    
    func automaticUserLogin(completion: (success: Bool) -> Void){
        let userAuthentication = KeychainSwift().get(kMonchisUserAuthenticationMethod)
        print(userAuthentication)
        if let userAuthentication = userAuthentication {
            if userAuthentication == "facebook" {
                
                //We try with facebook
                // User is already logged in
                self.authenticationMethod = .Facebook
                getUserFacebookData { success, registerParameters in
                    
                    if success {
                        self.registerFacebookUser(registerParameters!) {success in
                            if success {
                                completion(success: true)
                            }else{
                                completion(success: false)
                                
                            }
                        }
                    }else{
                        completion(success: success)
                    }
                    
                }
            }else if userAuthentication == "monchis"{
                self.authenticationMethod = .Monchis
                loginMonchis(nil,password: nil) { success, error in
                    completion(success: success)
                    
                    //                    if success {
                    //                        WaitScreen?.dismissViewControllerAnimated(true, completion: nil)
                    //                    }else{
                    //                        self.loginErrorHandler()
                    //                    }
                }
            }
        }else{
            completion(success: false)
            
        }
    }
    
    func loginMonchis(mail: String?, password: String?,completion: (success: Bool,error:String?) -> Void){
        let keychain = KeychainSwift()
        let mail = mail ?? keychain.get(kMonchisUserEmail)
        let password = password ?? keychain.get(kMonchisUserPassword)
        
        let parameters = ["email":mail ?? "","password":password ?? "", "device_token":MUser.sharedInstance.pushToken,
                          "type":"1",
                          "model":MUser.sharedInstance.modelName,
                          "system":"iOS",
                          "version":MUser.sharedInstance.systemVersion]
        MAPI.SharedInstance.jsonRequest(.LOGIN_EMAIL, parameters: parameters, completion: { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters \(parameters)",json,error)
            }
            if error == nil {
                
                let success = json?["success"].bool ?? false
                if let data = json?["data"] {
                    if success {
                        //we save the user data
                        self.email = mail ?? ""
                        self.password = password ?? ""
                        self.unparseUserInfoFromApi(data)
                        self.logged = true
                        //set the monchis token
                        let monchis_token = data["monchis_token"].string ?? ""
                        MAPI.SharedInstance.token = monchis_token
                        print("Monchis token : \(monchis_token)")
                        
                        //redirect user?? show animation? show alert?
                        self.authenticationMethod = .Monchis
                        
                        //we load the user addresses
                        //set automatic login for user
                        let keychain = KeychainSwift()
                        NSUserDefaults.standardUserDefaults().setBool(true, forKey: kAutomaticLogin)
                        keychain.set(self.email, forKey: kMonchisUserEmail)
                        keychain.set(self.password, forKey: kMonchisUserPassword)
                        if self.authenticationMethod == .Facebook {
                            keychain.set("facebook", forKey: kMonchisUserAuthenticationMethod)
                        }
                        if self.authenticationMethod == .Monchis {
                            keychain.set("monchis", forKey: kMonchisUserAuthenticationMethod)
                        }
                        //WE LOAD ALL USER DATA HERE
                        self.logged = true
                        NSNotificationCenter.defaultCenter().postNotificationName("userAutomatedLogin", object: nil)
                        
                        self.addresses.sortInPlace({ (addr1, addr2) -> Bool in
                            return addr1.is_default
                        })
                        //                        self.registerPushToken()
                        completion(success: true, error: nil)
                    }else{ // show error
                        completion(success: false, error: data.string)
                    }
                    
                }
                
            }else{ // net error
                completion(success: false, error: "Error de conexión.")
                
            }
        })
    }
    
    func loginErrorHandler(){
        self.logged = false
        if  UIApplication.sharedApplication().keyWindow != nil {
            SCLAlertView().showError("Lo sentimos", subTitle: "Ocurrio un error inesperado. Porfavor intente nuevamente.")
        }
        NSNotificationCenter.defaultCenter().postNotificationName("showLoginScreenAfterFailedLogin", object: nil)
    }
    
    
    func registerFacebookUser(registerParameters:[String:AnyObject]?, completion:(success: Bool) -> Void) {
        var parameters: [String:AnyObject]
        if registerParameters == nil {
            let first_name = KeychainSwift().get("userFirstName") ?? ""
            let last_name = KeychainSwift().get("userLastName") ?? ""
            let id = KeychainSwift().get(kMonchisUserFacebookID) ?? ""
            
            let email =  KeychainSwift().get(kMonchisUserEmail) ?? ""
            
            var app_version:String = ""
            if let version = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String {
                app_version = version
            }
            
            parameters = ["first_name":"\(first_name)","last_name":"\(last_name)","email":"\(email)","facebook_id":"\(id)", "device_token":MUser.sharedInstance.pushToken,
                          "type":"1",
                          "model":MUser.sharedInstance.modelName,
                          "system":"iOS",
                          "version":MUser.sharedInstance.systemVersion,
                          "facebook_token": facebook_token,
                          "app_version":app_version ]
            
        }else{
            parameters = registerParameters!
        }
        
        MAPI.SharedInstance.jsonRequest(.REGISTER_FACEBOOK, parameters: parameters, completion: { (json, error) -> Void in
            if debugModeEnabled {
                print("registerFacebok()","called with parameters \(parameters)",json,error)
            }
            
            if error == nil {
                if let json = json {
                    let success = json["success"].bool ?? false
                    
                    if success {
                        let monchis_token = json["data"]["monchis_token"].string ?? ""
                        MAPI.SharedInstance.token = monchis_token
                        MAPI.SharedInstance.mgr.session.configuration.HTTPAdditionalHeaders = ["token": "\(monchis_token)"]
                        print("Monchis token : \(monchis_token)")
                        /* ok
                         self.api.mgr.session.configuration.HTTPAdditionalHeaders = ["api_password": "\(api_password)"]
                         */
                        self.unparseUserInfoFromApi(json["data"])
                        
                        
                        
                        self.authenticationMethod = .Facebook
                        
                        let keychain = KeychainSwift()
                        keychain.set(self.facebook_id as String, forKey: kMonchisUserFacebookID)
                        keychain.set(self.first_name , forKey: "userFirstName")
                        keychain.set(self.last_name , forKey: "userLastName")
                        // LOAD ALL USER DATA HERE BEFORE PROCEEDING
                        //set automatic login for user
                        NSUserDefaults.standardUserDefaults().setBool(true, forKey: kAutomaticLogin)
                        keychain.set(self.email, forKey: kMonchisUserEmail)
                        keychain.set(self.password, forKey: kMonchisUserPassword)
                        if self.authenticationMethod == .Facebook {
                            keychain.set("facebook", forKey: kMonchisUserAuthenticationMethod)
                        }
                        if self.authenticationMethod == .Monchis {
                            keychain.set("monchis", forKey: kMonchisUserAuthenticationMethod)
                        }
                        //WE LOAD ALL USER DATA HERE
                        self.logged = true
                        NSNotificationCenter.defaultCenter().postNotificationName("userAutomatedLogin", object: nil)
                        
                        self.addresses.sortInPlace({ (addr1, addr2) -> Bool in
                            return addr1.is_default
                        })
                        
                        //                        self.registerPushToken()
                        completion(success:true)
                    }else{ //request responded success false
                        completion(success:false)
                        
                    }
                }
            }else{// handle error
                completion(success:false)
                
            }
            
            
        })
    }
    
    func getUserFacebookData(completion: (success: Bool, registerParameters: [String:AnyObject]?) -> Void)
    {
        
        
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me",parameters: ["fields":"email,first_name,last_name,id"])) { httpResponse, result in
            switch result {
            case .Success(let response):
                print("Graph Request Succeeded: \(response)")
                
                let first_name : NSString = response.dictionaryValue!["first_name"] as? NSString ?? ""
                let last_name : NSString = response.dictionaryValue!["last_name"] as? NSString ?? ""
                let id = response.dictionaryValue!["id"] as? NSString ?? ""
                let email : NSString = response.dictionaryValue!["email"] as? NSString ?? "\(id)@facebook.com"
                let facebook_token =  self.facebook_token
                var app_version:String = ""
                if let version = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String {
                    app_version = version
                }
                let registerParameters:[String:AnyObject] = ["first_name":"\(first_name)","last_name":"\(last_name)","email":"\(email)","facebook_id":"\(id)", "device_token":MUser.sharedInstance.pushToken,
                                                             "type":"1",
                                                             "model":MUser.sharedInstance.modelName,
                                                             "system":"iOS",
                                                             "version":MUser.sharedInstance.systemVersion,
                                                             "facebook_token": facebook_token,
                                                             "app_version":app_version ]
                
                completion(success: true,registerParameters: registerParameters)
                
                
                
                
            case .Failed(let error):
                print("Graph Request Failed: \(error)")
                completion(success: false, registerParameters: nil)
            }
        }
        connection.start()
        
        
    }
    
    func loadUserAdresses(){
        
        MAPI.SharedInstance.jsonRequest(.USER_ADDRESSES, parameters: nil) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters nil",json,error)
            }
            
            if error == nil {
                if let success = json?["success"].bool {
                    if success {
                        print("Loggued IN")
                        let addresses = json?["data"].array ?? []
                        self.addresses = []
                        for address in addresses {
                            let testAddr = MAddress(json: address)
                            self.addresses.append(testAddr)
                        }
                    }else{
                        
                    }
                }
            }else{ // network error
                
            }
        }
        
    }
    
    func registerPushToken(){
        var app_version:String = ""
        if let version = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String {
            app_version = version
        }
        let parameters = [  "device_token":MUser.sharedInstance.pushToken,
                            "type":"1",
                            "model":MUser.sharedInstance.modelName,
                            "system":"iOS",
                            "version":MUser.sharedInstance.systemVersion,
                            "app_version":app_version
        ]
        MAPI.SharedInstance.jsonRequest(.REGISTER_USER_DEVICE, parameters: parameters) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    print("success at registering device data")
                }else{
                }
            }else{
                
            }
            
        }
    }
    
    let temporalPassword = "notapassword"
    
    func registerAsTemporalUser(completion: (success: Bool,error:String?) -> Void){

                    
                    let parameters =    ["first_name":"Temporal","last_name":"Monchis",
                                           "email": "aaxx-\(UIDevice.currentDevice().identifierForVendor!.UUIDString)\(NSDate().timeIntervalSince1970 * 1000)@temporal.monchis",
                                           "password":temporalPassword,
                                           "device_token":MUser.sharedInstance.pushToken,
                                           "type":"1",
                                           "model":MUser.sharedInstance.modelName,
                                           "system":"iOS",
                                           "version":MUser.sharedInstance.systemVersion
                    ]
                    
                     MAPI.SharedInstance.jsonRequest(.REGISTER_MAIL, parameters: parameters) { json, error in
                        if debugModeEnabled {
                            print(#function,"parameters: \(parameters)",json,error)
                        }
                        
                        if error == nil {
                            
                            
                            let success = json?["success"].bool ?? false
                            if let data = json?["data"] {
                                if success {
                                    self.unparseUserInfoFromApi(data)
                                    let monchis_token = data["monchis_token"].string ?? ""
                                    MAPI.SharedInstance.token = monchis_token
                                    MAPI.SharedInstance.mgr.session.configuration.HTTPAdditionalHeaders = ["token": "\(monchis_token)"]
                                    print("Monchis token : \(monchis_token)")
                                    /* ok
                                     self.api.mgr.session.configuration.HTTPAdditionalHeaders = ["api_password": "\(api_password)"]
                                     */
                                    
                                    
                                    
                                    self.authenticationMethod = .Monchis
                                    
                                    let keychain = KeychainSwift()
                                    // LOAD ALL USER DATA HERE BEFORE PROCEEDING
                                    //set automatic login for user
                                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: kAutomaticLogin)
                                    keychain.set(self.email, forKey: kMonchisUserEmail)
                                    keychain.set(self.temporalPassword, forKey: kMonchisUserPassword)
                                    keychain.set("monchis", forKey: kMonchisUserAuthenticationMethod)
                                    //WE LOAD ALL USER DATA HERE
                                    self.logged = true
                                    NSNotificationCenter.defaultCenter().postNotificationName("userAutomatedLogin", object: nil)
                                    
                                    self.addresses.sortInPlace({ (addr1, addr2) -> Bool in
                                        return addr1.is_default
                                    })
                                    
                                    //                        self.registerPushToken()
                                    completion(success:true, error: nil)
                                }else{ // show error
                                   completion(success:false,error: data.string)
                                    
                                }
                            }else{
                                print("bad response")
                                
                            }
                        }else{ //network error
                            completion(success:false,error: "No hay conexión")
                        }
                    }
                    
            }
    
    
}





