//
//  SelectCustomizeTableViewController.swift
//  Monchis
//
//  Created by jrivarola on 9/15/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class SelectCustomizeTableViewController: UITableViewController {
    
    var field: MCustomizableField!
    struct Constants {
        static let customFieldReuseID = "customFieldReuseID"
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = field.name
        tableView.tableFooterView = UIView(frame: CGRectZero)
        
   
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return  1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return field.addons.count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title = NSLocalizedString("HASTA ", comment: "hasta cuantas opciones se pueden elegir")
        return title + "\(self.field.limit!)"
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.customFieldReuseID, forIndexPath: indexPath) 
        cell.tintColor = UIColor.redColor()
        cell.textLabel?.text = field.addons[indexPath.row].name
        if field.userSelectedIndexes.contains(indexPath.row) {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark

        }else{
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        // Configure the cell...
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        if !field.userSelectedIndexes.contains(indexPath.row) { // selection not in model
            if field.userSelectedIndexes.count == field.limit! {
                //show alert
                    print("error, solo se pueden elegir \(field.limit) opciones")
            }else{
            field.userSelectedIndexes.append(indexPath.row)
            //add checkmark
            cell?.accessoryType = .Checkmark
            }
            
        }else{ //already in model, deselect
          let index =  field.userSelectedIndexes.indexOf(indexPath.row)
            field.userSelectedIndexes.removeAtIndex(index!)

            cell?.accessoryType = .None

        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: false)

    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
  

}
