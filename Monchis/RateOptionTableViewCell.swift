//
//  RateOptionTableViewCell.swift
//  Monchis
//
//  Created by Javier Rivarola on 8/29/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit

class RateOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var rateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
