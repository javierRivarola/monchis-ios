//
//  LoginViewController.swift
//  Monchis
//
//  Created by jrivarola on 6/22/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import DynamicBlurView
import KeychainSwift

import TwitterKit


class LoginViewController: UIViewController,FBSDKLoginButtonDelegate {
    
    
    // MARK: Properties
    
    var user = MUser.sharedInstance
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var logoIV: UIImageView!
    @IBOutlet weak var facebookLogin: UIButton!
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var instaBtn: UIButton!
    @IBOutlet weak var registerMonchisBtn: UIButton!
    @IBOutlet weak var ingresarSinRegistrarteBtn: UIButton!
    var blurView: DynamicBlurView!
    var api  = MAPI.SharedInstance
    var dismissTapGesture: UITapGestureRecognizer!
    struct Segues {
        static let ShowMap = "ShowMap"
        static let MainViewFromLogin = "MainViewFromLogin"
    }
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        welcomeAnimation()
        if let automaticLogin = NSUserDefaults.standardUserDefaults().valueForKey(kAutomaticLogin) as? Bool {
            if automaticLogin {
                automaticUserLogin()
            }
        }
    }
    
    
    
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func automaticUserLogin(){
        
        
        
        let userAuthentication = KeychainSwift().get(kMonchisUserAuthenticationMethod)
        print(userAuthentication)
        if userAuthentication == "facebook" {
            
            FBSDKProfile.enableUpdatesOnAccessTokenChange(true)
            //We try with facebook
            if (FBSDKAccessToken.currentAccessToken() != nil){
                // User is already logged in
                user.authenticationMethod = .Facebook
                let profilePicture = FBSDKProfilePictureView(frame: CGRectMake(0, 0, 70, 70))
                profilePicture.profileID = "me"
                user.facebookProfile.profilePicture = profilePicture
                user.facebookProfile.accessToken = FBSDKAccessToken.currentAccessToken()
                
                
                //            UIView.animateWithDuration(0.2, animations: {
                //                self.blurView.blurRadius = 8
                //            })
                
                
                
                createWaitScreen("Iniciando Sesión..", comment: "Iniciando sesion con facebook")
                
                getUserFacebookData()
                
                
            }else{ // We try with twitter
                
                
                
                
                
                
                
                
                
            }
        }else if userAuthentication == "monchis"{
            user.authenticationMethod = .Monchis
            loginMonchis()
        }
    }
    
    func loginMonchis(){
        createWaitScreen("Iniciando Sesión..", comment: "Iniciando sesion con facebook")
        let keychain = KeychainSwift()
        let mail = keychain.get(kMonchisUserEmail)
        let password = keychain.get(kMonchisUserPassword)
        
        let parameters = ["email":mail ?? "","password":password ?? "", "device_token":MUser.sharedInstance.pushToken,
            "type":"1",
            "model":MUser.sharedInstance.modelName,
            "system":"iOS",
            "version":MUser.sharedInstance.systemVersion]
        api.jsonRequest(.LOGIN_EMAIL, parameters: parameters, completion: { (json, error) -> Void in
            if debugModeEnabled {
                print(__FUNCTION__,"called with parameters \(parameters)",json,error)
            }
            
            if error == nil {
                
                let success = json?["success"].bool ?? false
                if let data = json?["data"] {
                    if success {
                        //we save the user data
                        self.user.email = mail ?? ""
                        self.user.password = password ?? ""
                        self.user.unparseUserInfoFromApi(data)
                        self.user.logged = true
                        //set the monchis token
                        let monchis_token = data["monchis_token"].string ?? ""
                        self.api.token = monchis_token
                        self.api.mgr.session.configuration.HTTPAdditionalHeaders = ["token": "\(monchis_token)"]
                        print("Monchis token : \(monchis_token)")
                        
                        //redirect user?? show animation? show alert?
                        self.user.authenticationMethod = .Monchis
                        
                        //we load the user addresses
                        self.loadUserAdresses()
                        // self.registerPushToken()
                    }else{ // show error
                        self.dismissWaitScreen()
                        let message = data.string ?? ""
                        let titleLocalizable = NSLocalizedString("Error",comment: "error")
                        let alert = UIAlertController(title: titleLocalizable, message: message, preferredStyle: .Alert)
                        let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                        alert.addAction(ok)
                        self.presentViewController(alert, animated: true, completion: nil)
                        
                    }
                    
                }
                
            }else{ // net error
                self.dismissWaitScreen()
                let titleLocalizable = NSLocalizedString("Error",comment: "error")
                let alert = UIAlertController(title: titleLocalizable, message: error, preferredStyle: .Alert)
                let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                alert.addAction(ok)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        })
    }
    
    func dismissRequest(){
        dispatch_async(dispatch_get_main_queue()) {
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            self.blurView?.removeFromSuperview()
            self.blurView?.removeGestureRecognizer(self.dismissTapGesture)
            self.api.currentRequest?.cancel()
        }
    }
    
    
    func twitterLogin(){
        
    }
    
    /* CURRENTYL DISABLED
    func login(){
    let parameters = ["":""]
    api.jsonRequest(.LOGIN_EMAIL, parameters: parameters) { (json, error) -> Void in
    
    if debugModeEnabled {
    print(__FUNCTION__,"called with parameters \(parameters)",json,error)
    }
    
    if error == nil {
    if let json = json {
    let first_name = json[""].string
    let last_name = json[""].string
    let email = json["email"].string
    let monchis_token = json["monchis_token"].string
    let facebook_id = json["facebook_id"].string
    let facebook_token = json["facebook_token"].string
    let twitter_id = json["twitter_id"].string
    let twitter_token = json["twitter_token"].string
    let document_number = json["document_number"].string
    
    }else{ //json nil
    
    }
    }else{ // error not nil
    
    }
    }
    }
    
    */
    
    func colorize(imageView: UIImageView, color: CGColorRef) {
        
        // Set the image's shadowColor, radius, offset, and
        // set masks to bounds to false
        imageView.layer.shadowColor = color
        imageView.layer.shadowRadius = 20.0
        imageView.layer.shadowOffset = CGSizeZero
        imageView.layer.masksToBounds = false
        
        // Animate the shadow opacity to "fade it in"
        let shadowAnim = CABasicAnimation()
        shadowAnim.keyPath = "shadowOpacity"
        shadowAnim.fromValue = NSNumber(float: 0.0)
        shadowAnim.toValue = NSNumber(float: 0.9)
        shadowAnim.duration = 0.13
        shadowAnim.repeatCount = 4
        shadowAnim.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        shadowAnim.autoreverses = true
        imageView.layer.addAnimation(shadowAnim, forKey: "shadowOpacity")
        imageView.layer.shadowOpacity = 1
    }
    
    func glowMonchis(){
        colorize(self.logoIV, color:UIColor(hexString: "#950505")!.CGColor)
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
    }
    
    
    
    
    
    
    
    
    @IBAction func loginFacebookPressed(sender: UIButton) {
        
        print("login facebook pressed")
        createWaitScreen("Iniciando Sesión..", comment: "Iniciando sesion con facebook")
        
        let fbLoginManager = FBSDKLoginManager()
        let permissions = user.facebookProfile.permissions
        fbLoginManager.logInWithReadPermissions(permissions, handler: { token in
            
            
            
            let error = token.1
            let result = token.0
            
            
            if ((error) != nil)
            {
                // Process error
                self.dismissWaitScreen()
                print(error)
                fbLoginManager.logOut()
            }else if result.isCancelled {
                self.dismissWaitScreen()
                fbLoginManager.logOut()
                
                // Handle cancellations
                print("Canceled")
            }else {
                self.user.facebookProfile.accessToken = FBSDKAccessToken.currentAccessToken()
                // If you ask for multiple permissions at once, you
                // should check if specific permissions missing
                if result.grantedPermissions.contains("email")
                {
                    // do work
                    /*
                    if let user_birth = KeychainSwift().get("user_birthday") {
                        self.user.birth_date = user_birth
                    }else{
                        let alert = UIAlertController(title: NSLocalizedString("Ingresa tu fecha de nacimiento.", comment: "Ingresa tu fecha de nacimiento."), message: "", preferredStyle: .Alert)
                        let datePicker = UIDatePicker()

                        alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
                            datePicker.datePickerMode = .Date
                            textField.placeholder = NSLocalizedString("Fecha de nacimiento..",comment:"Fecha de nacimiento..")
                            textField.inputView = datePicker
                        })
                        
                        let continuar = UIAlertAction(title: NSLocalizedString("Continuar", comment: "Continuar"), style: .Default, handler: { (action) -> Void in
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "yyy-mm-dd"
                            let birth_date = dateFormatter.stringFromDate(datePicker.date)
                            self.user.birth_date = birth_date
                            self.getUserFacebookData()

                        })
                        
                        let cancelar = UIAlertAction(title: NSLocalizedString("Cancelar", comment: "Cancelar"), style: .Cancel, handler: { (cancelAction) -> Void in
                            
                        })
                        alert.addAction(cancelar)
                        alert.addAction(continuar)
                        
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    */
                    self.getUserFacebookData()
                }else{
                    self.dismissWaitScreen()
                    
                    let alert = UIAlertController(title: NSLocalizedString("Error al iniciar sesión con Facebook", comment: "Titulo de error cuando se trato de registrar en el sistema MONCHIS los datos del usuario conseguidos de Facebook"), message: NSLocalizedString("Necesitamos los permisos necesarios para continuar.", comment: "Necesitamos los permisos necesarios para continuar."), preferredStyle: .Alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar mensaje de error para continuar"), style: .Default, handler: nil)
                    alert.addAction(ok)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
                /*
                if result.grantedPermissions.contains("user_birthday")
                {
                    //register user location
                    print("user location is:")
                    self.getUserFacebookData()
                    
                    //                                    self.userLocationGranted = true
                }else{
                    self.dismissWaitScreen()

                    let alert = UIAlertController(title: NSLocalizedString("Error al iniciar sesión con Facebook", comment: "Titulo de error cuando se trato de registrar en el sistema MONCHIS los datos del usuario conseguidos de Facebook"), message: NSLocalizedString("Necesitamos los permisos necesarios para continuar.", comment: "Necesitamos los permisos necesarios para continuar."), preferredStyle: .Alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar mensaje de error para continuar"), style: .Default, handler: nil)
                    alert.addAction(ok)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
                */
            }
            
            
        })
        
        
    }
    
    func registerFacebook(){
        
        
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Segues.ShowMap {
            //customization
            let destinationNavigationController = segue.destinationViewController as? UINavigationController
            let targetController = destinationNavigationController?.topViewController as? MapViewController
            targetController?.saveBtnTag = 0
            
        }
        if segue.identifier == Segues.MainViewFromLogin {
            //customize view for user
            
        }
    }
    
    //UNWIND FROM MAIL LOGIN
    
    @IBAction func unwindFromMailLogin(segue: UIStoryboardSegue) {
        
        let spinner = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        spinner.mode = MBProgressHUDMode.Indeterminate
        spinner.labelText = NSLocalizedString("Iniciando Sesión",comment: "Iniciando Sesion")
        spinner.labelFont = UIFont(name: "OpenSans-Semibold", size: 14.0)
        loadUserAdresses()
        
    }
    
    @IBAction func ingresaSinRegistartePressed(sender: UIButton) {
        self.performSegueWithIdentifier(Segues.ShowMap, sender: self)
    }
    
    
    
    
    var userLocationGranted: Bool = false
    // Facebook Delegate Methods
    
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("User Logged Out", terminator: "")
        user.logged = false
    }
    
    func createWaitScreen(title: String, comment: String) {
        let spinner = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        spinner.mode = MBProgressHUDMode.Indeterminate
        spinner.labelText = NSLocalizedString(title,comment: comment)
        spinner.labelFont = UIFont(name: "OpenSans-Semibold", size: 14.0)
        
    }
    
    func dismissWaitScreen(animated: Bool = true){
        MBProgressHUD.hideAllHUDsForView(self.view, animated: animated)
    }
    
    func getUserFacebookData()
    {
        let fbParameters = ["fields":"email,first_name,last_name,id"]
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: fbParameters)
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            if debugModeEnabled {
                print(__FUNCTION__,"called with parameters \(fbParameters)")
            }
            
            if ((error) != nil)
            {
                FBSDKLoginManager().logOut()
                self.dismissWaitScreen()
                // Process error
                if debugModeEnabled {
                    print("Error at getting user facebook data: \(error)")
                }
                let alert = UIAlertController(title: NSLocalizedString("Error al iniciar sesión con Facebook", comment: "Titulo de error cuando se trato de registrar en el sistema MONCHIS los datos del usuario conseguidos de Facebook"), message: error.localizedDescription, preferredStyle: .Alert)
                let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar mensaje de error para continuar"), style: .Default, handler: nil)
                alert.addAction(ok)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
                print("fetched user: \(result)", terminator: "")
                let first_name : NSString = result.valueForKey("first_name") as? NSString ?? ""
                let last_name : NSString = result.valueForKey("last_name") as? NSString ?? ""
                let id = result.valueForKey("id") as? NSString ?? ""
                let email : NSString = result.valueForKey("email") as? NSString ?? "userid@facebook.com"
                let birthDate = self.user.birth_date ?? ""
                let registerParameters = ["first_name":"\(first_name)","last_name":"\(last_name)","email":"\(email)","facebook_id":"\(id)", "device_token":MUser.sharedInstance.pushToken,
                    "type":"1",
                    "model":MUser.sharedInstance.modelName,
                    "system":"iOS",
                    "version":MUser.sharedInstance.systemVersion]
                
                self.api.jsonRequest(.REGISTER_FACEBOOK, parameters: registerParameters, completion: { (json, error) -> Void in
                    if debugModeEnabled {
                        print("registerFacebok()","called with parameters \(registerParameters)",json,error)
                    }
                    if error == nil {
                        if let json = json {
                            let success = json["success"].bool ?? false
                            
                            if success {
                                let monchis_token = json["data"]["monchis_token"].string ?? ""
                                self.api.token = monchis_token
                                self.api.mgr.session.configuration.HTTPAdditionalHeaders = ["token": "\(monchis_token)"]
                                print("Monchis token : \(monchis_token)")
                                /* ok
                                self.api.mgr.session.configuration.HTTPAdditionalHeaders = ["api_password": "\(api_password)"]
                                */
                                self.user.unparseUserInfoFromApi(json["data"])
                                
                                
                                
                                self.user.authenticationMethod = .Facebook
                                
                                let keychain = KeychainSwift()
                                keychain.set(id as String, forKey: kMonchisUserFacebookID)
                                
                                // LOAD ALL USER DATA HERE BEFORE PROCEEDING
                                self.loadUserAdresses()
                                //self.registerPushToken()
                                
                            }else{ //request responded success false
                                self.dismissWaitScreen()
                                let message = json["message"].string ?? ""
                                let alert = UIAlertController(title: NSLocalizedString("Error al iniciar sesión con Facebook", comment: "Titulo de error cuando se trato de registrar en el sistema MONCHIS los datos del usuario conseguidos de Facebook"), message: message, preferredStyle: .Alert)
                                let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar mensaje de error para continuar"), style: .Default, handler: nil)
                                alert.addAction(ok)
                                self.presentViewController(alert, animated: true, completion: nil)
                                
                            }
                        }
                    }else{// handle error
                        self.dismissWaitScreen()
                        
                        let alert = UIAlertController(title: NSLocalizedString("Error al iniciar sesión con Facebook", comment: "Titulo de error cuando se trato de registrar en el sistema MONCHIS los datos del usuario conseguidos de Facebook"), message: error, preferredStyle: .Alert)
                        let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar mensaje de error para continuar"), style: .Default, handler: nil)
                        alert.addAction(ok)
                        self.presentViewController(alert, animated: true, completion: nil)
                        
                    }
                    
                    
                })
                
                
                
                
                //                if self.userLocationGranted {
                //                    let uLoc: AnyObject? = result.valueForKey("location")
                //                    let location : NSString = uLoc!.valueForKey("name") as! NSString
                //                    let id : NSString = uLoc!.valueForKey("id") as! NSString
                //                    println("User location: \(location) with id: \(id)")
                //                    self.user.facebookProfile.locationName = location as String
                //                    self.user.facebookProfile.locationID = id as String
                //                    self.performSegueWithIdentifier(Segues.ShowMap, sender: self)
                //
                //                }
                
                
            }
        })
    }
    
    
    func loadUserAdresses(){
        
        MCart.sharedInstance.getOrders { (success) -> Void in
            print("cart orders success: \(success)")
            
        }
        
        api.jsonRequest(.USER_ADDRESSES, parameters: nil) { (json, error) -> Void in
            if debugModeEnabled {
                print(__FUNCTION__,"called with parameters nil",json,error)
            }
            
            if error == nil {
                if let success = json?["success"].bool {
                    if success {
                        self.user.logged = true
                        print("Loggued IN")
                        let addresses = json?["data"].array ?? []
                        self.user.addresses = []
                        for address in addresses {
                            let testAddr = MAddress()
                            testAddr.automaticJSONParse(address)
                            testAddr.id = address["id"].int!
                            self.user.addresses.append(testAddr)
                        }
                        //set automatic login for user
                        NSUserDefaults.standardUserDefaults().setBool(true, forKey: kAutomaticLogin)
                        let keychain = KeychainSwift()
                        keychain.set(self.user.email, forKey: kMonchisUserEmail)
                        keychain.set(self.user.password, forKey: kMonchisUserPassword)
                        if self.user.authenticationMethod == .Facebook {
                            keychain.set("facebook", forKey: kMonchisUserAuthenticationMethod)
                        }
                        if self.user.authenticationMethod == .Monchis {
                            keychain.set("monchis", forKey: kMonchisUserAuthenticationMethod)
                        }
                        self.dismissWaitScreen()
                        //WE LOAD ALL USER DATA HERE
                        self.getFavorites()
                        if self.user.addresses.count == 0 { //if user has no addresses we show the map
                            self.performSegueWithIdentifier(Segues.ShowMap, sender: self)
                        }else{
                            self.performSegueWithIdentifier(Segues.MainViewFromLogin, sender: self)
                        }
                        self.user.addresses.sortInPlace({ (addr1, addr2) -> Bool in
                            return addr1.is_default
                        })
                    }else{
                        FBSDKLoginManager().logOut()
                        self.dismissWaitScreen()
                        let comment = json?["data"].string ?? "No hay mensaje"
                        let alert = UIAlertController(title: NSLocalizedString("ERROR", comment: "Titulo de error cuando se trato de conseguir las direcciones del usuario MONCHIS"), message: comment, preferredStyle: .Alert)
                        let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar mensaje de error para continuar"), style: .Default, handler: nil)
                        alert.addAction(ok)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
            }else{ // network error
                FBSDKLoginManager().logOut()
                self.dismissWaitScreen()
                
                let alert = UIAlertController(title: NSLocalizedString("Error al conectarse con el servidor", comment: "Titulo de error cuando se trato de conseguir las direcciones del usuario MONCHIS"), message: error, preferredStyle: .Alert)
                let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar mensaje de error para continuar"), style: .Default, handler: nil)
                alert.addAction(ok)
                self.presentViewController(alert, animated: true, completion: nil)
                
                
            }
        }
        
    }
    
    
    
    
    
    @IBAction func skipPressed(sender: UIButton) {
        self.user.authenticationMethod = .Unregistered
        self.performSegueWithIdentifier(Segues.ShowMap, sender: self)
    }
    
    
    func welcomeAnimation(){
        
        logoIV.transform = CGAffineTransformMakeTranslation(0,-logoIV.bounds.height)
        logoIV.alpha = 0
        //logo animation
        
        UIView.animateWithDuration(1, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.6, options: .CurveEaseInOut, animations: {
            self.logoIV.transform = CGAffineTransformMakeTranslation(0, 0)
            self.logoIV.alpha = 1
            
            }, completion: {fin in
                
        })
        
        
        glowMonchis()
        
        //login button animations
        facebookLogin.transform = CGAffineTransformMakeTranslation(-self.view.bounds.width, 0)
        instaBtn.transform = CGAffineTransformMakeTranslation(-self.view.bounds.width, 0)
        twitterBtn.transform = CGAffineTransformMakeTranslation(self.view.bounds.width, 0)
        
        
        UIView.animateWithDuration(0.7, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.6, options: .CurveEaseInOut, animations: {
            self.facebookLogin.transform = CGAffineTransformMakeTranslation(0, 0)
            self.twitterBtn.transform = CGAffineTransformMakeTranslation(0,0)
            
            
            }, completion: {fin in
                
        })
        
        UIView.animateWithDuration(0.7, delay: 0.3, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.6, options: .CurveEaseInOut, animations: {
            self.instaBtn.transform = CGAffineTransformMakeTranslation(0, 0)
            
            
            }, completion: {fin in
                
        })
        
        
        
    }
    
    // MARK: undiwnd from any view controller to login screen
    @IBAction func unwindToLogin(segue: UIStoryboardSegue){
        
    }
    
    func registerPushToken(){
        let parameters = [  "device_token":MUser.sharedInstance.pushToken,
            "type":"1",
            "model":MUser.sharedInstance.modelName,
            "system":"iOS",
            "version":MUser.sharedInstance.systemVersion
        ]
        MAPI.SharedInstance.jsonRequest(.REGISTER_USER_DEVICE, parameters: parameters) { (json, error) -> Void in
            if debugModeEnabled {
                print(__FUNCTION__,"called with parameters \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    print("success at registering device data")
                }else{
                    let message = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Error al registrar dispositivo.", message: message, preferredStyle: .Alert)
                    let cancel = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(cancel)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }else{
                let message = json?["data"].string ?? "Error desconocido"
                let alert = UIAlertController(title: "Error al registrar dispositivo.", message: message, preferredStyle: .Alert)
                let cancel = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(cancel)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
        }
    }
    
    
    // USER DATA LOAD
    
    func getFavorites(){
        api.jsonRequest(.PRODUCT_FAVORITE_INFO, parameters: nil, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(__FUNCTION__,"called with parameters: nil)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    if let branchesFavorites = json?["data"]["branches"].array {
                        MUser.sharedInstance.branchesFavorites = []
                        
                        for branch in branchesFavorites {
                            let newBranch = MRestaurant(data: branch["branch"])
                            let newFranchise = MFranchise(json: branch["branch"]["franchise"])
                            newBranch.franchise = newFranchise
                            MUser.sharedInstance.branchesFavorites.append(newBranch)
                        }
                    }
                    if let productFavorites = json?["data"]["products"].array {
                        MUser.sharedInstance.productFavorites = []
                        for product in productFavorites {
                            let newProduct = MProduct(json: product["product"])
                            for att in product["product"]["attachments"].array! {
                                let newAtt = MAttachment(json: att)
                                newProduct.attachments = [newAtt]
                            }
                            let newBranch = MRestaurant(data: product["branch"])
                            let newFranchise = MFranchise(json: product["branch"]["franchise"])
                            newBranch.franchise = newFranchise
                            newProduct.branch_products = [newBranch]
                            MUser.sharedInstance.productFavorites.append(newProduct)
                        }
                    }
                }else{
                    //something went wrong
                }
            }else{
                print(error)
            }
        }
    }
    
    func loadHistory(){
        
        let parameters:[String : AnyObject] =   [
            "limit":10,
            "state": ["finished","delivered"]
        ]
        
        api.jsonRequest(.ORDER_HISTORY, parameters: parameters, cacheRequest: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(__FUNCTION__,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                if let json = json {
                    if let success = json["success"].bool {
                        if success {
                            if let jsonOrders = json["data"]["orders"].array {
                                self.user.orderHistory = []
                                var tmpOrders:[MOrder] = []
                                
                                for order in jsonOrders {
                                    let newOrder:MOrder = MOrder()
                                    newOrder.products = []
                                    newOrder.automaticJSONParse(order)
                                    newOrder.delivery_type = order["delivery_type"].int
                                    
                                    let newBranch = MRestaurant(data: order["branch"])
                                    newOrder.branch = newBranch
                                    newOrder.created_at_date = order["created_at"].string
                                    let newFranchise = MFranchise(json: order["branch"]["franchise"])
                                    newOrder.branch.franchise = newFranchise
                                    newOrder.order_id = order["id"].int
                                    if order["payment_type"] == "CASH" {
                                        newOrder.payMethod = .CASH
                                    }else{
                                        newOrder.payMethod = .CREDIT
                                    }
                                    if let status = order["state"].int {
                                        
                                        if status == MOrder.Status.CANCELED.rawValue {
                                            newOrder.status = .CANCELED
                                        }
                                        if status == MOrder.Status.CONFIRMED.rawValue {
                                            newOrder.status = .CONFIRMED
                                        }
                                        if status == MOrder.Status.DELIVERED.rawValue {
                                            newOrder.status = .DELIVERED
                                        }
                                        if status == MOrder.Status.DELIVERY.rawValue {
                                            newOrder.status = .DELIVERY
                                        }
                                        if status == MOrder.Status.FINISHED.rawValue {
                                            newOrder.status = .FINISHED
                                        }
                                        if status == MOrder.Status.IN_PROGRESS.rawValue {
                                            newOrder.status = .IN_PROGRESS
                                        }
                                        
                                    }
                                    if let orderProductsArray = order["order_products"].array {
                                        for orderProduct in orderProductsArray {
                                            let newProduct = MProduct(json: orderProduct["product"])
                                            newProduct.addons = []
                                            if let attachmentArray = orderProduct["product"]["attachments"].array {
                                                for att in attachmentArray {
                                                    let newAtt = MAttachment(json: att)
                                                    newAtt.imageSize = "500x500/"
                                                    newProduct.attachments = [newAtt]
                                                }
                                            }
                                            
                                            if let addonArray = orderProduct["product"]["order_product_addons"].array {
                                                for addon in addonArray {
                                                    let newAddon = MAddon()
                                                    newAddon.name = addon["addon"]["addon_name"]["name"].string
                                                    newAddon.price = addon["addon"]["price"].string
                                                    newAddon.id = addon["addon"]["id"].int
                                                    newProduct.addons.append(newAddon)
                                                }
                                                
                                            }
                                            
                                            newOrder.products?.append(newProduct)
                                        }
                                        
                                    }
                                    
                                    tmpOrders.append(newOrder)
                                }
                                self.user.orderHistory = tmpOrders
                                
                            }
                            
                        }else{
                            
                        }
                    }
                    
                }
                
            }else{
                
            }
        }
        
        
    }
    
    @IBAction func didPressLoginWithTwitter(sender: UIButton) {
        Twitter.sharedInstance().logInWithCompletion { session, error in
            if (session != nil) {
                print("signed in as \(session?.userName)");
                
                
                
                
                
                let shareEmailViewController = TWTRShareEmailViewController() { email, error in
                    print("Email \(email), Error: \(error)")
                    
                    if error == nil {
                        let userName = session?.userName ?? ""
                        let lastName = ""
                        let registerParameters = ["first_name":userName,"last_name":lastName,"email":"\(email)","twitter_id":"", "device_token":MUser.sharedInstance.pushToken,
                            "type":"1",
                            "model":MUser.sharedInstance.modelName,
                            "system":"iOS",
                            "version":MUser.sharedInstance.systemVersion]
                        self.user.first_name = userName
                        self.user.last_name = ""
                        self.user.email = email ?? ""
                        self.registerTwitterLogin(registerParameters)
                        
                        //                            let keychain = KeychainSwift()
                        //                            keychain.set(id as String, forKey: kMonchisUserTwitterID)
                    }else{
                        
                        
                        //ask for email
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                    
                }
                self.presentViewController(shareEmailViewController, animated: true, completion: nil)
                
                
                
                
                
                
                
                
                
            } else {
                print("error: \(error?.localizedDescription)");
            }
        }
    }
    
    
    func registerTwitterLogin(parameters: [String:AnyObject]){
        
        
        
        self.api.jsonRequest(.REGISTER_TWITTER, parameters: parameters, completion: { (json, error) -> Void in
            if debugModeEnabled {
                print(__FUNCTION__,"called with parameters \(parameters)",json,error)
            }
            if error == nil {
                if let json = json {
                    let success = json["success"].bool ?? false
                    
                    if success {
                        let monchis_token = json["data"]["monchis_token"].string ?? ""
                        self.api.token = monchis_token
                        self.api.mgr.session.configuration.HTTPAdditionalHeaders = ["token": "\(monchis_token)"]
                        print("Monchis token : \(monchis_token)")
                        /* ok
                        self.api.mgr.session.configuration.HTTPAdditionalHeaders = ["api_password": "\(api_password)"]
                        */
                        
                        
                        
                        
                        self.user.authenticationMethod = .Twitter
                        
                        
                        
                        // LOAD ALL USER DATA HERE BEFORE PROCEEDING
                        self.loadUserAdresses()
                        //self.registerPushToken()
                        
                    }else{ //request responded success false
                        self.dismissWaitScreen()
                        let message = json["message"].string ?? ""
                        let alert = UIAlertController(title: NSLocalizedString("Error al iniciar sesión con Facebook", comment: "Titulo de error cuando se trato de registrar en el sistema MONCHIS los datos del usuario conseguidos de Facebook"), message: message, preferredStyle: .Alert)
                        let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar mensaje de error para continuar"), style: .Default, handler: nil)
                        alert.addAction(ok)
                        self.presentViewController(alert, animated: true, completion: nil)
                        
                    }
                }
            }else{// handle error
                self.dismissWaitScreen()
                
                let alert = UIAlertController(title: NSLocalizedString("Error al iniciar sesión con Facebook", comment: "Titulo de error cuando se trato de registrar en el sistema MONCHIS los datos del usuario conseguidos de Facebook"), message: error, preferredStyle: .Alert)
                let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar mensaje de error para continuar"), style: .Default, handler: nil)
                alert.addAction(ok)
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            
            
        })
    }
    
}
