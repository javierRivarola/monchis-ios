//
//  MMenu.swift
//  Monchis
//
//  Created by jrivarola on 9/9/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
import SwiftyJSON

class MMenu: NSObject {
    var name: String = ""
    var items:[MProduct] = []
    var id: Int?
    var franchise_id: Int?
    override init (){
        
    }
    
    init(json: SwiftyJSON.JSON) {
        self.name = json["name"].string ?? ""
        self.id = json["id"].int
        self.franchise_id = json["franchise_id"].int
    }
    
    init (name: String) {
        self.name = name
    }
    
    class func compareMenus(prod1: [MMenu], prod2: [MMenu]) -> Bool {
        for c in prod1 {
            if !prod2.contains( {cat in
                return cat.id == c.id
            }) {
                return false
            }
        }
        return true
    }

}