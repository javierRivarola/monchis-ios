//
//  CategoriaTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 8/27/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class CategoriaTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.layoutIfNeeded()
    }

}

class TableCellCollectionView: UICollectionView {
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        if let hitView = super.hitTest(point, withEvent: event) {
            if hitView.isKindOfClass(TableCellCollectionView) {
                return nil
            } else {
                return hitView
            }
        } else {
            return nil
        }
    }
    
}