//
//  BranchSelectionTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 25/1/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
protocol BranchSelectionMapDelegate {
    func didSelectBranch(branch: MRestaurant)
}

class BranchSelectionTableViewCell: UITableViewCell {

    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var branchNameLbl: UILabel!
    var delegate: BranchSelectionMapDelegate?
    var branch: MRestaurant!
    @IBOutlet weak var branchAddressLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func didPressSeeOnMap(sender: UIButton) {
        delegate?.didSelectBranch(branch)
    }
}
