//
//  ValesViewController.swift
//  Monchis
//
//  Created by Javier Rivarola on 6/16/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import Haneke
import MBProgressHUD
import SCLAlertView
import Mixpanel

class ValesViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    let api = MAPI.SharedInstance
    var product: MProduct!
    @IBOutlet weak var qrBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        qrBtn.imageView?.contentMode = .ScaleAspectFit
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
     // MARK: - Navigation
    var pop: VoucherProductTableViewController!
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        if segue.identifier == "showProduct" {
            
            pop = segue.destinationViewController as! VoucherProductTableViewController
            pop.product = self.product
            pop.branch = self.branch
            pop.navController = self.navigationController
            pop.tableView.reloadData()
            
            pop.modalPresentationStyle = .Popover
            pop.popoverPresentationController?.delegate = self
            pop.popoverPresentationController?.sourceRect = CGRectMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds),0,0)
            
            pop.popoverPresentationController?.sourceView = self.view
            pop.preferredContentSize = CGSizeMake(self.view.bounds.width , self.view.bounds.height)

        }
     }
    
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }

    var lastVoucherScanned: String!
    func getProduct(voucherCode:String) {
            lastVoucherScanned = voucherCode
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            MProduct.getProductForVoucher(voucherCode) {error, product, branch in
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                Mixpanel.mainInstance().track(event: "Usuario ingreso codigo de voucher por teclado", properties: ["Codigo Voucher":voucherCode])
                if error == nil {
                    self.tabBarController?.setTabBarVisible(false, animated: true)
                    self.product = product
                    self.product.voucherCode = voucherCode
                    self.branch = branch

                    self.performSegueWithIdentifier("showProduct", sender: self)
//                    self.tableView.reloadData()
//                    self.showPop()
                }else{
                    //handle error
                    SCLAlertView().showError("Lo sentimos", subTitle: error!)
                }
            }
    }
    
    var branch: MRestaurant!
    
    @IBAction func enviarPressed(sender: UIButton) {
        self.view.endEditing(true)
        if voucherCodeTxt.text != nil && voucherCodeTxt.text != "" {
            getProduct(voucherCodeTxt.text!)
        }
    }

    
    
    
    @IBOutlet weak var voucherCodeTxt: UITextField!

    

    }
