//
//  HistorialPedidosTableViewController.swift
//  Monchis
//
//  Created by jrivarola on 7/3/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import DZNEmptyDataSet
import ReachabilitySwift
import Crashlytics
import Mixpanel


var globalOrderStatusController:HistorialPedidosTableViewController!

class HistorialPedidosTableViewController: UITableViewController, UISearchBarDelegate,callRestaurantDelegate,UIPopoverPresentationControllerDelegate,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource {
    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    lazy var searchBar = UISearchBar(frame: CGRectMake(0, 0, 200, 20))
    var offset: Int = 0
    
    struct Constants {
        static let reuseIdentifier = "reuseIdentifier"
        static let reuseEnCaminoIdentifier = "reuseIdentifierEnCamino"
    }
    
    struct Segues {
        static let detallePedido = "showOrderDetailsSegue"
    }
    var totalProducts:Int = 0
    let limitResultsTo = 25 //limit the number of results
    var history: [MOrder] = []
    let api = MAPI.SharedInstance
    let user = MUser.sharedInstance
    var headerMonchisImage: UIImageView!
    var loadingBeforeEmptyDataSet:Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        globalOrderStatusController = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        self.tableView.estimatedRowHeight = 200
        let img = UIImage(named: "texto-monchis-header")
        headerMonchisImage = UIImageView(frame: CGRectMake(0, 0, 50, 25))
        headerMonchisImage.image = img
        let bg = UIImageView(image: UIImage(named: "background-completoChico"))
        bg.contentMode = .ScaleAspectFill
        bg.clipsToBounds = true
        self.tableView.backgroundView = bg
        bg.layer.zPosition = -1
        
        headerMonchisImage.contentMode = .ScaleAspectFit
        self.navigationItem.titleView = headerMonchisImage
        searchBar.delegate = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        configurePullToRefresh()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HistorialPedidosTableViewController.refreshPendingOrders), name: "updateOrderStateNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HistorialPedidosTableViewController.reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HistorialPedidosTableViewController.pullToRefresh(_:)), name: kUserLoggedInNotification, object: nil)
        
 NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HistorialPedidosTableViewController.pullToRefresh(_:)), name: "refreshOrderStatusNotification", object: nil)
        
    }
    
    
    func refreshPendingOrders(){
        getPendingOrders()
    }
    
    
    func reachabilityChanged(notification: NSNotification) {
        
        let reachability = notification.object as! Reachability
        
        if reachability.isReachable() {
            self.tableView.reloadEmptyDataSet()
            if user.logged {
                self.getPendingOrders()
            }
            if reachability.isReachableViaWiFi() {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            print("Network not reachable")
            
        }
    }
    
    var notificationOrder: MOrder! {
        didSet {
            self.navigationController?.popToRootViewControllerAnimated(false)
            seeMapPressed(notificationOrder)
        }
    }
    
    func seeMapPressed(order: MOrder) {
        let map = self.storyboard?.instantiateViewControllerWithIdentifier("BranchesOnMapViewController") as! BranchesOnMapViewController
        map.branches = [order.branch]
        Answers.logCustomEventWithName("Usuario vio mapa de sucursal",
                                       customAttributes: ["User ID":self.user.id,"Sucursal": order.branch?.name ?? ""])

        Mixpanel.mainInstance().track(event: kUserPressedSeeOnMapEvent, properties: ["Pantalla":"Pedidos Pendientes", "Local":order.branch?.name ?? "","Local ID":order.branch?.id ?? 0])

        self.navigationController?.pushViewController(map, animated: true)
    }
    
    func configurePullToRefresh(){
        refreshControl = UIRefreshControl()
        refreshControl = self.refreshControl
        refreshControl?.tintColor = UIColor.whiteColor()
        let title = NSMutableAttributedString()
        let tira = NSAttributedString(string: "Tira para actualizar", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        title.appendAttributedString(tira)
        refreshControl?.attributedTitle = title
        refreshControl?.addTarget(self, action: #selector(HistorialPedidosTableViewController.pullToRefresh(_:)), forControlEvents: .ValueChanged)
        self.tableView?.alwaysBounceVertical = true
        refreshControl?.layer.zPosition += 1
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillAppear(animated)
        self.refreshTimer?.invalidate()
    }
    
    func pullToRefresh(sender: UIRefreshControl){
        cameFromViewWillAppear = true
        getPendingOrders()
        
    }
    
    func pullToRefreshTimer(){
        self.cameFromViewWillAppear = true
        self.getPendingOrders()
        
    }
    
    func createWaitScreen(){
        spinner.startAnimating()
    }
    
    
    
    func dismissWaitScreen(){
        spinner.stopAnimating()
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        refreshControl?.endRefreshing()
    }
    
    
    var refreshTimer: NSTimer!
    
    
    var loadingPendingOrders: Bool = false
    func getPendingOrders(){
        loadingBeforeEmptyDataSet = true
        let parameters =   [
            "limit":limitResultsTo,
            "state": ["confirmed","in_progress","delivery","pickup","canceled","finished","delivered"],
            "offset": cameFromViewWillAppear ? 0 : self.history.count
            //            "updated_at":updated_at
        ]
        
//        if !loadingPendingOrders {
            createWaitScreen()
            loadingPendingOrders = true
            api.jsonRequest(.ORDER_HISTORY, parameters: parameters as? [String : AnyObject], cacheRequest: false) { (json, error) -> Void in
                if debugModeEnabled {
                    print(#function,"called with parameters \(parameters)",json,error)
                }
                if error == nil {
                    if let json = json {
                        var tmpOrders:[MOrder] = []
                        if let jsonOrders = json["data"]["orders"].array {
                            self.totalProducts = json["data"]["count"].int ?? 0
                            // let count = json["data"]["count"].int
                            for order in jsonOrders {
                                let newOrder:MOrder = MOrder(json: order)
                                if newOrder.status == .FINISHED || newOrder.status == .CANCELED || newOrder.status == .DELIVERED{
                                    if let confirmedAt = newOrder.confirmed_at {
                                        let dateFormatter = NSDateFormatter()
                                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                        if let date = dateFormatter.dateFromString(confirmedAt){
                                            let dateIn4hours = date.dateByAddingTimeInterval(14400)
                                            let now = NSDate()
                                            if dateIn4hours.compare(now) == .OrderedDescending || dateIn4hours.compare(now) == .OrderedSame {
                                                tmpOrders.append(newOrder)
                                            }
                                        }
                                    }
                                }else{
                                    tmpOrders.append(newOrder)
                                }
                            }
                            if tmpOrders.count > 0 {
                                self.refreshTimer?.invalidate()
                                self.refreshTimer = NSTimer.scheduledTimerWithTimeInterval(20.0, target: self, selector: #selector(HistorialPedidosTableViewController.getPendingOrders), userInfo: nil, repeats: true)
                            }else{
                                self.refreshTimer?.invalidate()
                            }
//                            if self.cameFromViewWillAppear {
//                                self.cameFromViewWillAppear = false
//                                
                                self.history = tmpOrders
//                            }else{
//                                self.history += tmpOrders
//                            }
//                            self.history.removeDuplicates()
                        }
                    }
                    self.loadingPendingOrders = false
                    self.loadingBeforeEmptyDataSet = false
                    self.dismissWaitScreen()
                    self.tableView.reloadData()
                }else{
                    self.dismissWaitScreen()
                }
            }
//        }
        
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    var cameFromViewWillAppear: Bool = false
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if user.logged {
            cameFromViewWillAppear = true
            getPendingOrders()
        }else{
            loadingBeforeEmptyDataSet = false
            tableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return self.history.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == self.history.count - 1 {
            return CGFloat.min
        }
        return 25
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    
    func calificarPressedForOrder(order: MOrder) {
        selectedOrder = order
        self.performSegueWithIdentifier("calificarSegue", sender: self)
    }
    
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let historyOrder = history[indexPath.section]
        if historyOrder.status == .DELIVERY  || historyOrder.status == .DELIVERED {
            let cell = tableView.dequeueReusableCellWithIdentifier(Constants.reuseIdentifier, forIndexPath: indexPath) as! HistorialPedidosTableViewCell
            cell.cancelView.hidden = true
            if historyOrder.feedback_reasons.count > 0 || historyOrder.feedback_comments != nil || historyOrder.feedback_stars != nil {
                    cell.ratingView.hidden = false
                cell.calificaPedidoBtn.hidden = true
                for i in 1...5 {
                    let button = cell.viewWithTag(i) as! UIButton
                    button.setImage(UIImage(named: "estrella-off"), forState: .Normal)
                }
                
                let tag = historyOrder.feedback_stars  ?? 1
                
                for i in 1...tag {
                    let button = cell.viewWithTag(i) as! UIButton
                    button.setImage(UIImage(named: "estrella-on"), forState: .Normal)
                }
            }else{
                cell.calificaPedidoBtn.hidden = false
                cell.ratingView.hidden = true
            }
            // Configure the cell...
            cell.recibidoImgView.image = UIImage(named: "icono-recibido-activo")
            cell.preparandoImgView.image = UIImage(named: "icono-preparando-activo")
            cell.enCaminoImgView.image = UIImage(named: "icono-delivery-activo")
            cell.deliveryOrPickupLbl.text = NSLocalizedString("EN CAMINO",comment: "EN CAMINO")
            let attributedTitle = NSMutableAttributedString(string: "LLAMAR A ", attributes: [NSFontAttributeName : UIFont.systemFontOfSize(16), NSForegroundColorAttributeName: UIColor.whiteColor()])
            
            var attributedName: NSAttributedString!
            if historyOrder.delivery_type == 1 {
                if let franchiseName = historyOrder.branch?.franchise?.name {
                    attributedName = NSAttributedString(string: franchiseName.uppercaseString,attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor()])
                    attributedTitle.appendAttributedString(attributedName)
                }
                cell.mapBtnHeightConstraint.constant = 0
            }else{
                if let branchName = historyOrder.branch?.name {
                    attributedName = NSAttributedString(string: branchName.uppercaseString,attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor()])
                    attributedTitle.appendAttributedString(attributedName)
                }
                
                let str = NSMutableAttributedString(attributedString: NSAttributedString(string: "COMO LLEGAR A ",attributes: [NSFontAttributeName:UIFont.systemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()]))
                    str.appendAttributedString(NSAttributedString(string: historyOrder.branch?.name ?? "", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()]))
                cell.mapBtnHeightConstraint.constant = 55
                cell.mapBtn.setAttributedTitle(str, forState: .Normal)
            }
            let extraString = NSAttributedString(string:"\nSi desea modificar o cancelar su pedido", attributes:  [NSFontAttributeName: UIFont.systemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()])
            attributedTitle.appendAttributedString(extraString)
            cell.callBtn.setAttributedTitle(attributedTitle, forState: .Normal)
            cell.order = historyOrder
            cell.branchImgView.image = NO_BRANCH_PLACEHOLDER
            if let urlString = historyOrder.branch.franchise?.icon_attachment?.imageComputedURL {
                cell.branchImgView.hnk_setImageFromURL(urlString, placeholder: NO_BRANCH_PLACEHOLDER)
                if !imageCacheKeys.contains(urlString.URLString) {
                    imageCacheKeys.append(urlString.URLString)
                }
            }

            cell.branchNameLbl.text = attributedName.string
            /*
             let gpsTracking = historyOrder.branch?.delivery_gps_tracked ?? false
             if !gpsTracking {
             cell.seeOnMapBtn.hidden = true
             }
             */
            cell.headView.backgroundColor = UIColor(hexString: "#F5E519")
            cell.headGreenView.backgroundColor = UIColor(hexString: "#6ECE1A")
            
            cell.delegate = self
            return cell
        }
        
        if historyOrder.status == .PICKUP {
            let cell = tableView.dequeueReusableCellWithIdentifier(Constants.reuseIdentifier, forIndexPath: indexPath) as! HistorialPedidosTableViewCell
            cell.cancelView.hidden = true
            if historyOrder.feedback_reasons.count > 0 || historyOrder.feedback_comments != nil || historyOrder.feedback_stars != nil {
                cell.ratingView.hidden = false
                cell.calificaPedidoBtn.hidden = true

                for i in 1...5 {
                    let button = cell.viewWithTag(i) as! UIButton
                    button.setImage(UIImage(named: "estrella-off"), forState: .Normal)
                }
                
                let tag = historyOrder.feedback_stars ?? 1
                
                for i in 1...tag {
                    let button = cell.viewWithTag(i) as! UIButton
                    button.setImage(UIImage(named: "estrella-on"), forState: .Normal)
                }

            }else{
                cell.ratingView.hidden = true
                cell.calificaPedidoBtn.hidden = false
            }
            // Configure the cell...
            cell.recibidoImgView.image = UIImage(named: "icono-recibido-activo")
            cell.preparandoImgView.image = UIImage(named: "icono-preparando-activo")
            cell.enCaminoImgView.image = UIImage(named: "icono-pick-up-activo")
            cell.deliveryOrPickupLbl.text = NSLocalizedString("PARA BUSCAR",comment: "PARA BUSCAR")
            
            let attributedTitle = NSMutableAttributedString(string: "LLAMAR A ", attributes: [NSFontAttributeName : UIFont.systemFontOfSize(16), NSForegroundColorAttributeName: UIColor.whiteColor()])
            var attributedName: NSAttributedString!
            if historyOrder.delivery_type == 1 {
                if let franchiseName = historyOrder.branch?.franchise?.name {
                    attributedName = NSAttributedString(string: franchiseName.uppercaseString,attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor()])
                    attributedTitle.appendAttributedString(attributedName)
                }
                cell.mapBtnHeightConstraint.constant = 0

            }else{
                if let branchName = historyOrder.branch?.name {
                    attributedName = NSAttributedString(string: branchName.uppercaseString,attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor()])
                    attributedTitle.appendAttributedString(attributedName)
                }
                cell.mapBtnHeightConstraint.constant = 55

                let str = NSMutableAttributedString(attributedString: NSAttributedString(string: "COMO LLEGAR A ",attributes: [NSFontAttributeName:UIFont.systemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()]))
                str.appendAttributedString(NSAttributedString(string: historyOrder.branch?.name ?? "", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()]))
                cell.mapBtn.setAttributedTitle(str, forState: .Normal)
            }
            let extraString = NSAttributedString(string:"\nSi desea modificar o cancelar su pedido", attributes:  [NSFontAttributeName: UIFont.systemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()])
            attributedTitle.appendAttributedString(extraString)
            cell.callBtn.setAttributedTitle(attributedTitle, forState: .Normal)
            cell.order = historyOrder
            cell.branchImgView.image = NO_BRANCH_PLACEHOLDER
            
            if let urlString = historyOrder.branch.franchise?.icon_attachment?.imageComputedURL {
                cell.branchImgView.hnk_setImageFromURL(urlString, placeholder: NO_BRANCH_PLACEHOLDER)
                if !imageCacheKeys.contains(urlString.URLString) {
                    imageCacheKeys.append(urlString.URLString)
                }
            }
            cell.branchNameLbl.text = attributedName.string
            /*
             let gpsTracking = historyOrder.branch?.delivery_gps_tracked ?? false
             if !gpsTracking {
             cell.seeOnMapBtn.hidden = true
             }
             */
            cell.headView.backgroundColor = UIColor(hexString: "#F5E519")
            cell.headGreenView.backgroundColor = UIColor(hexString: "#6ECE1A")
            cell.delegate = self
            return cell
        }
        
        if historyOrder.status == .OPEN {
            let cell = tableView.dequeueReusableCellWithIdentifier(Constants.reuseIdentifier, forIndexPath: indexPath) as! HistorialPedidosTableViewCell
            cell.cancelView.hidden = true
            if historyOrder.feedback_reasons.count > 0 || historyOrder.feedback_comments != nil || historyOrder.feedback_stars != nil {
                cell.ratingView.hidden = false
                cell.calificaPedidoBtn.hidden = true
                for i in 1...5 {
                    let button = cell.viewWithTag(i) as! UIButton
                    button.setImage(UIImage(named: "estrella-off"), forState: .Normal)
                }

                let tag = historyOrder.feedback_stars ?? 1
                
                for i in 1...tag {
                    let button = cell.viewWithTag(i) as! UIButton
                    button.setImage(UIImage(named: "estrella-on"), forState: .Normal)
                }
            }else{
                cell.ratingView.hidden = true
                cell.calificaPedidoBtn.hidden = false
            }
            // Configure the cell...
            cell.recibidoImgView.image = UIImage(named: "icono-recibido-inactivo")
            cell.preparandoImgView.image = UIImage(named: "icono-preparando-inactivo")
            if historyOrder.delivery_type == 1 {
                cell.enCaminoImgView.image = UIImage(named: "icono-delivery-inactivo")
                cell.deliveryOrPickupLbl.text = NSLocalizedString("EN CAMINO",comment: "EN CAMINO")
                
            }else{
                cell.enCaminoImgView.image = UIImage(named: "icono-pick-up-inactivo")
                cell.deliveryOrPickupLbl.text = NSLocalizedString("PARA BUSCAR",comment: "PARA BUSCAR")
                
            }
            let attributedTitle = NSMutableAttributedString(string: "LLAMAR A ", attributes: [NSFontAttributeName : UIFont.systemFontOfSize(16), NSForegroundColorAttributeName: UIColor.whiteColor()])
            var attributedName: NSAttributedString!
            if historyOrder.delivery_type == 1 {
                if let franchiseName = historyOrder.branch?.franchise?.name {
                    attributedName = NSAttributedString(string: franchiseName.uppercaseString,attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor()])
                    attributedTitle.appendAttributedString(attributedName)
                }
                cell.mapBtnHeightConstraint.constant = 0

            }else{
                cell.mapBtnHeightConstraint.constant = 55
                if let branchName = historyOrder.branch?.name {
                    attributedName = NSAttributedString(string: branchName.uppercaseString,attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor()])
                    attributedTitle.appendAttributedString(attributedName)
                }
                let str = NSMutableAttributedString(attributedString: NSAttributedString(string: "COMO LLEGAR A ",attributes: [NSFontAttributeName:UIFont.systemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()]))
                str.appendAttributedString(NSAttributedString(string: historyOrder.branch?.name ?? "", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()]))
                cell.mapBtn.setAttributedTitle(str, forState: .Normal)
            }
            let extraString = NSAttributedString(string:"\nSi desea modificar o cancelar su pedido", attributes:  [NSFontAttributeName: UIFont.systemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()])
            attributedTitle.appendAttributedString(extraString)
            cell.callBtn.setAttributedTitle(attributedTitle, forState: .Normal)
            cell.order = historyOrder
            cell.branchImgView.image = NO_BRANCH_PLACEHOLDER
            
            if let urlString = historyOrder.branch.franchise?.icon_attachment?.imageComputedURL {
                cell.branchImgView.hnk_setImageFromURL(urlString, placeholder: NO_BRANCH_PLACEHOLDER)
                if !imageCacheKeys.contains(urlString.URLString) {
                    imageCacheKeys.append(urlString.URLString)
                }
            }
            cell.branchNameLbl.text = attributedName.string
            cell.delegate = self
            
            cell.headView.backgroundColor = UIColor.darkGrayColor()
            cell.headGreenView.backgroundColor = UIColor.darkGrayColor()
            return cell
        }
        if historyOrder.status == .IN_PROGRESS {
            let cell = tableView.dequeueReusableCellWithIdentifier(Constants.reuseIdentifier, forIndexPath: indexPath) as! HistorialPedidosTableViewCell
            cell.cancelView.hidden = true
            
            if historyOrder.feedback_reasons.count > 0 || historyOrder.feedback_comments != nil || historyOrder.feedback_stars != nil {
                cell.ratingView.hidden = false
                cell.calificaPedidoBtn.hidden = true

                for i in 1...5{
                    let button = cell.viewWithTag(i) as! UIButton
                    button.setImage(UIImage(named: "estrella-off"), forState: .Normal)
                }

                let tag = historyOrder.feedback_stars ??  1
 
                for i in 1...tag {
                    let button = cell.viewWithTag(i) as! UIButton
                    button.setImage(UIImage(named: "estrella-on"), forState: .Normal)
                }

            }else{
                cell.ratingView.hidden = true
                cell.calificaPedidoBtn.hidden = false
            }
            // Configure the cell...
            cell.recibidoImgView.image = UIImage(named: "icono-recibido-activo")
            cell.preparandoImgView.image = UIImage(named: "icono-preparando-activo")
            if historyOrder.delivery_type == 1 {
                cell.enCaminoImgView.image = UIImage(named: "icono-delivery-inactivo")
                cell.deliveryOrPickupLbl.text = NSLocalizedString("EN CAMINO",comment: "EN CAMINO")
            }else{
                cell.enCaminoImgView.image = UIImage(named: "icono-pick-up-inactivo")
                cell.deliveryOrPickupLbl.text = NSLocalizedString("PARA BUSCAR",comment: "PARA BUSCAR")
            }
            let attributedTitle = NSMutableAttributedString(string: "LLAMAR A ", attributes: [NSFontAttributeName : UIFont.systemFontOfSize(16), NSForegroundColorAttributeName: UIColor.whiteColor()])
            var attributedName: NSAttributedString!
            if historyOrder.delivery_type == 1 {
                if let franchiseName = historyOrder.branch?.franchise?.name {
                    attributedName = NSAttributedString(string: franchiseName.uppercaseString,attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor()])
                    attributedTitle.appendAttributedString(attributedName)
                }
                cell.mapBtnHeightConstraint.constant = 0

            }else{
                cell.mapBtnHeightConstraint.constant = 55
                if let branchName = historyOrder.branch?.name {
                    attributedName = NSAttributedString(string: branchName.uppercaseString,attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor()])
                    attributedTitle.appendAttributedString(attributedName)
                }
                let str = NSMutableAttributedString(attributedString: NSAttributedString(string: "COMO LLEGAR A ",attributes: [NSFontAttributeName:UIFont.systemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()]))
                str.appendAttributedString(NSAttributedString(string: historyOrder.branch?.name ?? "", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()]))
                cell.mapBtn.setAttributedTitle(str, forState: .Normal)
            }
            let extraString = NSAttributedString(string:"\nSi desea modificar o cancelar su pedido", attributes:  [NSFontAttributeName: UIFont.systemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()])
            attributedTitle.appendAttributedString(extraString)
            cell.callBtn.setAttributedTitle(attributedTitle, forState: .Normal)
            cell.order = historyOrder
            cell.delegate = self
            cell.branchImgView.image = NO_BRANCH_PLACEHOLDER
            cell.headView.backgroundColor = UIColor(hexString: "#F5E519")
            
            cell.headGreenView.backgroundColor = UIColor.darkGrayColor()
            
            if let urlString = historyOrder.branch.franchise?.icon_attachment?.imageComputedURL {
                cell.branchImgView.hnk_setImageFromURL(urlString, placeholder: NO_BRANCH_PLACEHOLDER)
                if !imageCacheKeys.contains(urlString.URLString) {
                    imageCacheKeys.append(urlString.URLString)
                }
            }
            cell.branchNameLbl.text = attributedName.string
            return cell
        }
        
        if historyOrder.status == .CONFIRMED {
            let cell = tableView.dequeueReusableCellWithIdentifier(Constants.reuseIdentifier, forIndexPath: indexPath) as! HistorialPedidosTableViewCell
            cell.cancelView.hidden = true
            if historyOrder.feedback_reasons.count > 0 || historyOrder.feedback_comments != nil || historyOrder.feedback_stars != nil {
                cell.ratingView.hidden = false
                cell.calificaPedidoBtn.hidden = true

                for i in 1...5{
                    let button = cell.viewWithTag(i) as! UIButton
                    button.setImage(UIImage(named: "estrella-off"), forState: .Normal)
                }

                let tag = historyOrder.feedback_stars ?? 1
                
                for i in 1...tag {
                    let button = cell.viewWithTag(i) as! UIButton
                    button.setImage(UIImage(named: "estrella-on"), forState: .Normal)
                }
            }else{
                cell.ratingView.hidden = true
                cell.calificaPedidoBtn.hidden = false

            }
            // Configure the cell...
            cell.recibidoImgView.image = UIImage(named: "icono-recibido-activo")
            cell.preparandoImgView.image = UIImage(named: "icono-preparando-inactivo")
            if historyOrder.delivery_type == 1 {
                cell.enCaminoImgView.image = UIImage(named: "icono-delivery-inactivo")
                cell.deliveryOrPickupLbl.text = NSLocalizedString("EN CAMINO",comment: "EN CAMINO")
                
            }else{
                cell.enCaminoImgView.image = UIImage(named: "icono-pick-up-inactivo")
                cell.deliveryOrPickupLbl.text = NSLocalizedString("PARA BUSCAR",comment: "PARA BUSCAR")
                
            }
            let attributedTitle = NSMutableAttributedString(string: "LLAMAR A ", attributes: [NSFontAttributeName : UIFont.systemFontOfSize(16), NSForegroundColorAttributeName: UIColor.whiteColor()])
            var attributedName: NSAttributedString!
            if historyOrder.delivery_type == 1 {
                cell.mapBtnHeightConstraint.constant = 0

                if let franchiseName = historyOrder.branch?.franchise?.name {
                    attributedName = NSAttributedString(string: franchiseName.uppercaseString,attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor()])
                    attributedTitle.appendAttributedString(attributedName)
                }
            }else{
                cell.mapBtnHeightConstraint.constant = 55
                if let branchName = historyOrder.branch?.name {
                    attributedName = NSAttributedString(string: branchName.uppercaseString,attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor()])
                    attributedTitle.appendAttributedString(attributedName)
                }
                let str = NSMutableAttributedString(attributedString: NSAttributedString(string: "COMO LLEGAR A ",attributes: [NSFontAttributeName:UIFont.systemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()]))
                str.appendAttributedString(NSAttributedString(string: historyOrder.branch?.name ?? "", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()]))
                cell.mapBtn.setAttributedTitle(str, forState: .Normal)
            }
            let extraString = NSAttributedString(string:"\nSi desea modificar o cancelar su pedido", attributes:  [NSFontAttributeName: UIFont.systemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()])
            attributedTitle.appendAttributedString(extraString)
            cell.callBtn.setAttributedTitle(attributedTitle, forState: .Normal)
            cell.order = historyOrder
            cell.delegate = self
            cell.branchImgView.image = NO_BRANCH_PLACEHOLDER
            cell.headGreenView.backgroundColor = UIColor.darkGrayColor()
            cell.headView.backgroundColor = UIColor.darkGrayColor()
            
            if let urlString = historyOrder.branch.franchise?.icon_attachment?.imageComputedURL {
                cell.branchImgView.hnk_setImageFromURL(urlString, placeholder: NO_BRANCH_PLACEHOLDER)
                if !imageCacheKeys.contains(urlString.URLString) {
                    imageCacheKeys.append(urlString.URLString)
                }
            }
            cell.branchNameLbl.text = attributedName.string
            return cell
        }
        
        if historyOrder.status == .CANCELED {
            let cell = tableView.dequeueReusableCellWithIdentifier(Constants.reuseIdentifier, forIndexPath: indexPath) as! HistorialPedidosTableViewCell
            cell.cancelView.hidden = false
            if historyOrder.feedback_reasons.count > 0 || historyOrder.feedback_comments != nil || historyOrder.feedback_stars != nil {
                cell.ratingView.hidden = false
                cell.calificaPedidoBtn.hidden = true

                for i in 1...5{
                    let button = cell.viewWithTag(i) as! UIButton
                    button.setImage(UIImage(named: "estrella-off"), forState: .Normal)
                }

                let tag = historyOrder.feedback_stars ?? 1
                
                for i in 1...tag {
                    let button = cell.viewWithTag(i) as! UIButton
                    button.setImage(UIImage(named: "estrella-on"), forState: .Normal)
                }
            }else{
                cell.ratingView.hidden = true
                cell.calificaPedidoBtn.hidden = false
            }
            // Configure the cell...
            cell.recibidoImgView.image = UIImage(named: "icono-recibido-inactivo")
            cell.preparandoImgView.image = UIImage(named: "icono-preparando-inactivo")
            if historyOrder.delivery_type == 1 {
                cell.enCaminoImgView.image = UIImage(named: "icono-delivery-inactivo")
                cell.deliveryOrPickupLbl.text = NSLocalizedString("EN CAMINO",comment: "EN CAMINO")
                
            }else{
                cell.enCaminoImgView.image = UIImage(named: "icono-pick-up-inactivo")
                cell.deliveryOrPickupLbl.text = NSLocalizedString("PARA BUSCAR",comment: "PARA BUSCAR")
                
            }
            let attributedTitle = NSMutableAttributedString(string: "LLAMAR A ", attributes: [NSFontAttributeName : UIFont.systemFontOfSize(16), NSForegroundColorAttributeName: UIColor.whiteColor()])
            var attributedName: NSAttributedString!
            if historyOrder.delivery_type == 1{
                if let franchiseName = historyOrder.branch?.franchise?.name {
                    attributedName = NSAttributedString(string: franchiseName.uppercaseString,attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor()])
                    attributedTitle.appendAttributedString(attributedName)
                }
                cell.mapBtnHeightConstraint.constant = 0

            }else{
                cell.mapBtnHeightConstraint.constant = 55

                if let branchName = historyOrder.branch?.name {
                    attributedName = NSAttributedString(string: branchName.uppercaseString,attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor()])
                    attributedTitle.appendAttributedString(attributedName)
                }
                let str = NSMutableAttributedString(attributedString: NSAttributedString(string: "COMO LLEGAR A ",attributes: [NSFontAttributeName:UIFont.systemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()]))
                str.appendAttributedString(NSAttributedString(string: historyOrder.branch?.name ?? "", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()]))
                cell.mapBtn.setAttributedTitle(str, forState: .Normal)
            }
            let extraString = NSAttributedString(string:"\nSi desea modificar o cancelar su pedido", attributes:  [NSFontAttributeName: UIFont.systemFontOfSize(11), NSForegroundColorAttributeName: UIColor.whiteColor()])
            attributedTitle.appendAttributedString(extraString)
            cell.callBtn.setAttributedTitle(attributedTitle, forState: .Normal)
            cell.order = historyOrder
            cell.branchImgView.image = NO_BRANCH_PLACEHOLDER
            
            if let urlString = historyOrder.branch.franchise?.icon_attachment?.imageComputedURL {
                cell.branchImgView.hnk_setImageFromURL(urlString, placeholder: NO_BRANCH_PLACEHOLDER)
                if !imageCacheKeys.contains(urlString.URLString) {
                    imageCacheKeys.append(urlString.URLString)
                }
            }
            cell.branchNameLbl.text = attributedName.string
            cell.delegate = self
            
            cell.headView.backgroundColor = UIColor.darkGrayColor()
            cell.headGreenView.backgroundColor = UIColor.darkGrayColor()
            return cell
        }
        
        return UITableViewCell()
        
    }
    

    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == self.history.count - 1 {
            return nil
        }
        let view = UIView()
        view.backgroundColor = UIColor.clearColor()
        return view
    }
    

    
    var selectedOrder: MOrder!
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedOrder = history[indexPath.section]
        //performSegueWithIdentifier(Segues.detallePedido, sender: self)
        
        /* POP OVER STYLE */
        /*
         let orderDetailsPopOver = self.storyboard?.instantiateViewControllerWithIdentifier("CarritoViewController") as! CarritoViewController
         orderDetailsPopOver.modalPresentationStyle = .Popover
         orderDetailsPopOver.preferredContentSize = CGSizeMake(self.tableView.bounds.width - 100, self.tableView.bounds.height - 100)
         orderDetailsPopOver.orderHistory = [selectedOrder]
         orderDetailsPopOver.showingProductDetails = true
         let popoverMenuViewController = orderDetailsPopOver.popoverPresentationController
         popoverMenuViewController?.permittedArrowDirections = .Any
         popoverMenuViewController?.delegate = self
         if let cell = tableView.cellForRowAtIndexPath(indexPath) as? HistorialPedidosTableViewCell {
         popoverMenuViewController?.sourceView = cell.ordersLbl
         popoverMenuViewController?.sourceRect = cell.ordersLbl.bounds
         }
         if let cell = tableView.cellForRowAtIndexPath(indexPath) as? HistorialPedidosEnCaminoTableViewCell {
         popoverMenuViewController?.sourceView = cell.ordersLbl
         popoverMenuViewController?.sourceRect = cell.ordersLbl.bounds
         }
         
         popoverMenuViewController?.backgroundColor = UIColor.whiteColor()
         
         presentViewController(
         orderDetailsPopOver,
         animated: true,
         completion: nil)
         */
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == Segues.detallePedido {
            let dvc = segue.destinationViewController as! OrderDetailsViewController
            dvc.order = selectedOrder
        }
        if segue.identifier == "calificarSegue" {
            let dvc = segue.destinationViewController as! CalificarPedidoTableViewController
            dvc.order = selectedOrder
            
        }
    }
    
    
    // MARK: CallRestaurantDelegate
    func callPressed(sender: UIButton, order: MOrder) {
        var phone = order.branch?.phone ?? ""
        if phone == ""  {
            phone = order.branch.house_number ?? ""
        }
        var editedPhoneNumber = ""
        for i in phone.characters {
            
            switch (i){
            case "0","1","2","3","4","5","6","7","8","9" : editedPhoneNumber = editedPhoneNumber + String(i)
            default : print("Removed invalid character.")
            }
        }
        
        let alert = UIAlertController(title: "Llamar a \(order.branch?.franchise?.name ?? "")", message: phone, preferredStyle: .Alert)
        let llamar = UIAlertAction(title: NSLocalizedString("LLamar", comment: "LLamar a restaurante"), style: .Default) { (action) -> Void in
            if let phoneCallURL:NSURL = NSURL(string: "tel://\(editedPhoneNumber)") {
                let application:UIApplication = UIApplication.sharedApplication()
                if (application.canOpenURL(phoneCallURL)) {
                    application.openURL(phoneCallURL)
                    Answers.logCustomEventWithName("Usuario llamo a sucursal",
                                                   customAttributes: ["User ID":self.user.id,"Sucursal": order.branch?.franchise?.name ?? ""])
                    Mixpanel.mainInstance().track(event: kUserCalledBranchEvent, properties: ["Local": order.branch?.name ?? "", "Local ID": order.branch?.id ?? 0])
                    

                }else{
                    print("cant make call")
                }
            }else{
                print("cant make url")
            }
            
        }
        let cancelar = UIAlertAction(title: NSLocalizedString("Cancelar", comment: "Cancelar"), style: .Cancel, handler: nil)
        alert.addAction(cancelar)
        alert.addAction(llamar)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    func seeOrderDetailsPressed(order: MOrder)
    {
        Answers.logCustomEventWithName("Usuario presiono Ver Detalles de orden Desde Historial",
                                       customAttributes:["Order ID": order.order_id ?? order.id ?? 0])
    Mixpanel.mainInstance().track(event: kUserPressedOrderDetailsEvent, properties: ["Pantalla": "Pedidos Pendientes"])
        selectedOrder = order
        
        self.performSegueWithIdentifier(Segues.detallePedido, sender: self)
    }
    
    // MARK: PopOverDelegate
    
    func adaptivePresentationStyleForPresentationController(
        controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    // MARK: EmptyDataSetSource
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        if connectedToNetwork() {
            let image = UIImage(named: "historial-vacio")
            return image
        }else{
            let image = UIImage(named: "icono-error-red")
            return image
        }
    }
    
    func imageAnimationForEmptyDataSet(scrollView: UIScrollView!) -> CAAnimation! {
        
        let animation = CABasicAnimation(keyPath: "transform")
        
        animation.fromValue = NSValue(CATransform3D: CATransform3DIdentity)
        animation.toValue = NSValue(CATransform3D:CATransform3DMakeRotation(CGFloat(M_PI_2), 0.0, 0.0, 1.0))
        
        animation.duration = 0.25
        animation.cumulative = true
        animation.repeatCount = 1
        return animation
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        if loadingBeforeEmptyDataSet {
            if connectedToNetwork() {
                let text = NSAttributedString(string: NSLocalizedString("Buscando pedidos pendientes..", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
                
                return text
            }else{
                let noConection = NSAttributedString(string: NSLocalizedString("No hay conexión.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
                
                return noConection
                
            }
            
        }else{
            if connectedToNetwork() {
                let text = NSAttributedString(string: NSLocalizedString("No tienes pedidos pendientes.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
                
                return text
            }else{
                let noConection = NSAttributedString(string: NSLocalizedString("No hay conexión.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
                
                return noConection
            }
            
        }
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        if connectedToNetwork() {
            let subTitle = NSAttributedString(string: NSLocalizedString("Todos los pedidos que realices aparecen aqui.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(15),NSForegroundColorAttributeName:UIColor.whiteColor()])
            return subTitle
        }else{
            return nil
        }
    }
    
    func emptyDataSetShouldAnimateImageView(scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    var cellHeight: CGFloat = 0
    override func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cellHeight = cell.bounds.height
    }
    
    // loading on demand
    
    var footerView: UIView = UIView(frame: CGRectZero)
    
    func createFooterViewForLoading(){
        footerView.frame = CGRectMake(0, 0, self.view.bounds.width, 40)
        let activity = UIActivityIndicatorView()
        activity.hidesWhenStopped = true
        activity.color = UIColor.whiteColor()
        activity.center = footerView.center
        activity.tag = 10
        footerView.addSubview(activity)
    }
    
    
    override func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let endOfTable: Bool = (scrollView.contentOffset.y >= (CGFloat(totalProducts * Int(cellHeight)) - scrollView.frame.size.height))
        if self.history.count < totalProducts && totalProducts > 0 {
            if endOfTable && !scrollView.dragging && !scrollView.decelerating {
                createFooterViewForLoading()
                if self.history.count >= 15 {
                    self.getPendingOrders()
                    self.tableView.tableFooterView = footerView
                    
                    let activity = footerView.viewWithTag(10) as! UIActivityIndicatorView
                    activity.startAnimating()
                }
                
            }
        }else{
            tableView.tableFooterView = UIView(frame: CGRectZero)
        }
        
        
    }
    
    func getCurrentViewController(vc: UIViewController) -> UIViewController? {
        if let pvc = vc.presentedViewController {
            return getCurrentViewController(pvc)
        }
        else if let svc = vc as? UISplitViewController where svc.viewControllers.count > 0 {
            return getCurrentViewController(svc.viewControllers.last!)
        }
        else if let nc = vc as? UINavigationController where nc.viewControllers.count > 0 {
            return getCurrentViewController(nc.topViewController!)
        }
        else if let tbc = vc as? UITabBarController {
            if let svc = tbc.selectedViewController {
                return getCurrentViewController(svc)
            }
        }
        return vc
    }
    
    
}
