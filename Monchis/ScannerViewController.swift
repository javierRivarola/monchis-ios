//
//  ScannerViewController.swift
//  Monchis
//
//  Created by Javier Rivarola on 6/10/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import AVFoundation
import Haneke
import MBProgressHUD
import SCLAlertView
import Mixpanel

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, UIPopoverPresentationControllerDelegate {
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    @IBOutlet weak var scanView: UIView!
    let api = MAPI.SharedInstance
    let notification = CWStatusBarNotification()
    var product: MProduct?
    override func viewDidLoad() {
        super.viewDidLoad()
        configureScan()


        notification.notificationStyle = .NavigationBarNotification
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        if segue.identifier == "showProduct" {
            pop = segue.destinationViewController as! VoucherProductTableViewController
            pop.product = self.product
            pop.branch = self.branch
            pop.navController = self.navigationController
            pop.parent = self
            pop.tableView.reloadData()
            
            pop.modalPresentationStyle = .Popover
            pop.popoverPresentationController?.delegate = self
            pop.popoverPresentationController?.sourceRect = CGRectMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds),0,0)
            
            pop.popoverPresentationController?.sourceView = self.view
            pop.preferredContentSize = CGSizeMake(self.view.bounds.width, self.view.bounds.height)
            
        }
     }
    var pop: VoucherProductTableViewController!


    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        captureSession?.startRunning()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        captureSession?.stopRunning()
    }
    
    
    func configureScan(){
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        // Get an instance of the AVCaptureDeviceInput class using the previous device object.
        
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
                                                         AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
                                                         AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode]
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            scanView.layer.addSublayer(videoPreviewLayer!)
            
            
            // Start video capture.
            captureSession?.startRunning()
            
            
            
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            qrCodeFrameView?.layer.borderColor = UIColor.greenColor().CGColor
            qrCodeFrameView?.layer.borderWidth = 2
            scanView.addSubview(qrCodeFrameView!)
            scanView.bringSubviewToFront(qrCodeFrameView!)
        } catch {
            print("error in sesion")
        }
        
        
    }
    
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRectZero
            print( "No barcode is detected")
            return
        }
        
        let barcodeTypes = [AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
                            AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
                            AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode]
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if barcodeTypes.contains(metadataObj.type){
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
            qrCodeFrameView?.frame = barCodeObject.bounds;
            
            if metadataObj.stringValue != nil {
                print(metadataObj.stringValue )
                //getProductData(metadataObj.stringValue)
                getProduct(metadataObj.stringValue)
            }
        }
    }
    
    var lastVoucherScanned: String!
    func getProduct(voucherCode:String) {
            if lastVoucherScanned != voucherCode {
                lastVoucherScanned = voucherCode
                MBProgressHUD.showHUDAddedTo(self.view, animated: true)

                MProduct.getProductForVoucher(voucherCode) {error, product, branch in
                    MBProgressHUD.hideHUDForView(self.view, animated: true)
                    Mixpanel.mainInstance().track(event: "Usuario utilizo camara para voucher", properties: ["Codigo Voucher":voucherCode])
                    if error == nil {
                        self.product = product
                        self.product?.voucherCode = voucherCode
                        self.branch = branch
                        self.tabBarController?.setTabBarVisible(false, animated: true)
                        self.performSegueWithIdentifier("showProduct", sender: self)
                    }else{
                        //handle error
                        //handle error
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let alert = SCLAlertView(appearance: appearance)
                        alert.addButton("Listo", action: {
                            self.lastVoucherScanned = ""
                        })
                        alert.showError("Lo sentimos", subTitle: error ?? "Ocurrio un error inesperado.")

                    }
                }
            }
        
    }
    

    
    
    var productImageView: UIImageView!
    var branch: MRestaurant!
    func animationForSuccessfulAddedOrder() {
        if productImageView != nil {
            let imgViewForAnimation = UIImageView()
            imgViewForAnimation.image = productImageView.image
            imgViewForAnimation.frame = CGRectMake(0, 0, 150, 150)
            imgViewForAnimation.contentMode = .ScaleAspectFit
            imgViewForAnimation.layer.cornerRadius = imgViewForAnimation.bounds.width/2
            imgViewForAnimation.clipsToBounds = true
            imgViewForAnimation.center = self.productImageView.center
            
            var viewOrigin = imgViewForAnimation.frame.origin
            viewOrigin.y = viewOrigin.y + imgViewForAnimation.frame.size.height / 2.0
            viewOrigin.x = viewOrigin.x + imgViewForAnimation.frame.size.width / 2.0
            
            
            imgViewForAnimation.layer.position = viewOrigin
            
            // Set up fade out effect
            let fadeOutAnimation = CABasicAnimation(keyPath: "opacity")
            //        fadeOutAnimation setToValue:[NSNumber numberWithFloat:0.3]];
            fadeOutAnimation.fromValue = NSNumber(float: 1.0)
            fadeOutAnimation.toValue = NSNumber(float: 0.2)
            fadeOutAnimation.fillMode = kCAFillModeForwards
            fadeOutAnimation.removedOnCompletion = false
            self.view.addSubview(imgViewForAnimation)
            
            // Set up scaling
            let resizeAnimation = CABasicAnimation(keyPath: "bounds.size")
            
            resizeAnimation.fromValue = NSValue(CGSize: imgViewForAnimation.bounds.size)
            resizeAnimation.toValue = NSValue(CGSize: CGSizeMake(10,10))
            
            
            resizeAnimation.fillMode = kCAFillModeForwards
            resizeAnimation.removedOnCompletion = false
            
            // Set up path movement
            let pathAnimation = CAKeyframeAnimation(keyPath: "position")
            pathAnimation.calculationMode = kCAAnimationPaced
            pathAnimation.fillMode = kCAFillModeForwards
            pathAnimation.removedOnCompletion = false
            //Setting Endpoint of the animation
            let endPoint = CGPointMake(self.view.bounds.width - 50, self.tabBarController!.tabBar.frame.origin.y + self.tabBarController!.tabBar.frame.size.height/2.0)
            
            //to end animation in last tab use
            
            let curvedPath = CGPathCreateMutable()
            CGPathMoveToPoint(curvedPath, nil, viewOrigin.x, viewOrigin.y)
            CGPathAddCurveToPoint(curvedPath, nil, endPoint.x, viewOrigin.y, endPoint.x, viewOrigin.y, endPoint.x, endPoint.y)
            pathAnimation.path = curvedPath
            
            let group = CAAnimationGroup()
            group.animations = [fadeOutAnimation,pathAnimation,resizeAnimation]
            group.fillMode = kCAFillModeForwards
            group.removedOnCompletion = false
            
            group.duration = 1.2
            group.delegate = self
            imgViewForAnimation.layer.addAnimation(group, forKey: nil)
            
            
        }
        
    }
    
}
