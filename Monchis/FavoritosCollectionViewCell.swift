//
//  FavoritosCollectionViewCell.swift
//  Monchis
//
//  Created by jrivarola on 6/22/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
protocol DeleteProductFavoriteDelegate {
    func didPressDeleteProductFavorite(product: MProduct)
    func didPressRepeatProductFavorite(product: MProduct)

}
class FavoritosCollectionViewCell: UICollectionViewCell {
    var product: MProduct!
    var branch: MRestaurant!
    var delegate: DeleteProductFavoriteDelegate?
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descripcion: UILabel!
    @IBOutlet weak var precio: UILabel!
    @IBOutlet weak var branchNameLbl: UILabel!
    
    override func awakeFromNib() {
        
    }
    @IBAction func didPressDeleteProductFavorite(sender: UIButton) {
        delegate?.didPressDeleteProductFavorite(product)
    }
    @IBAction func didPressRepeatProduct(sender: UIButton) {
        delegate?.didPressRepeatProductFavorite(product)
    }
}
