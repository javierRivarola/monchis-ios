//
//  MAddress.swift
//  Monchis
//
//  Created by jrivarola on 10/6/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftyJSON
import Mixpanel

class MAddress: NSObject, NSCopying {
    var name: String = ""
    var street1:String = ""
    var street2:String = ""
    var reference:String = ""
    var coordinate: CLLocationCoordinate2D? {
        didSet {
            if let coor = coordinate {
                self.latitude = "\(coor.latitude)"
                self.longitude = "\(coor.longitude)"

            }
        }
    }
    var computedCoordinate: CLLocationCoordinate2D {
        let latitudeInDegrees:Double =  Double(self.latitude ?? "") ?? -25.33585
        let longitudeInDegrees:Double = Double(self.longitude ?? "") ?? -57.505556
        return CLLocationCoordinate2D(latitude: latitudeInDegrees, longitude: longitudeInDegrees)
    }
    var number: String = ""
    var id: Int = 0
    var formattedAddress:String {
        var responseStreet1 = street1
        var responseStreet2 = street2
        var responseNum = number
        var responseReference = reference
        if responseStreet1 == "" {
            responseStreet1 = "(Calle 1)"
        }
        if responseStreet2 == "" {
            responseStreet2 = "(Calle 2)"
        }
        if responseNum == "" {
            responseNum = "(Numero)"
        }
        if responseReference == "" {
            responseReference = "(Referencias)"
        }
        return (responseStreet1 + " " + responseNum + ", " + responseStreet2 + ", " + responseReference)
    }
    var is_default: Bool = false
    var latitude: String = ""
    var longitude: String = ""
    
//    func copy() -> MAddress {
//        let copy = MAddress(addr: self)
//        return copy
//    }
    
    required override init (){
        
    }
    
    func parseAddressForAPI () -> [String:AnyObject]? {
        var parameters: [String:AnyObject] = [:]
        parameters["id"] = self.id
        parameters["name"] = self.name
        parameters["street1"] = self.street1
        parameters["street2"] = self.street2
        parameters["number"] = self.number
        parameters["reference"] = self.reference
        parameters["latitude"] = self.latitude
        parameters["longitude"] = self.longitude 
        parameters["is_default"] = self.is_default
        return parameters
        
    }
    
    let street1DefaultString = "Sin Calle Principal"
   let street2DefaultString = "Sin Calle Secundaria"
    let referenceDefaultString = "Sin Referencias"
    let numberDefaultString = "Sin Numero de Casa"
    
    
    func needsToFillDetails() -> Bool {
        return (self.street2 == self.street2DefaultString) || (number == numberDefaultString) || (reference == referenceDefaultString) || (street1 == street1DefaultString)
    }
    
    
    
    func saveBackend(editMode: Bool, completion: (success: Bool, errorMessage: String?)->Void){
        
        if street2 == "" {
            street2 = street2DefaultString
        }
        if number == "" {
            number = numberDefaultString
        }
        if reference == "" {
            reference = referenceDefaultString
        }
        if street1 == "" {
            street1 = street1DefaultString
        }
        
        
        if name == "" {
            var i = 1
            var toName = "Dirección \(MUser.sharedInstance.addresses.count + i)"
            
            var filt = MUser.sharedInstance.addresses.filter({ (addr) -> Bool in
                return addr.name == toName
            })
            while filt.count > 0 {
                i += 1
                toName = "Dirección \(MUser.sharedInstance.addresses.count + i)"
                filt = MUser.sharedInstance.addresses.filter({ (addr) -> Bool in
                    return addr.name == toName
                })
            }
                
            
            name = toName
        }
        
            if street1 != "" && street2 != "" && reference != "" && number != "" && name != "" {

     
                    
                    //we always make default the address
                    is_default = true

                        var command: MAPI.Command!
                        
                        if editMode {
                            command = .USER_ADDRESSES_UPDATE
                        }else{
                            command = .USER_ADDRESSES_CREATE
                            
                        }
                        let addrPrmt = self.parseAddressForAPI()
                        MAPI.SharedInstance.jsonRequest(command, parameters: addrPrmt) { (json, error) -> Void in
//                            if debugModeEnabled {
                                print(#function,"called with parameters: \(addrPrmt)",json, error)
                                
//                            }
                            if error == nil {
                                if let json = json {
                                    let success = json["success"].bool ?? false
                                    if success {
                                        for  addr in MUser.sharedInstance.addresses {
                                            addr.is_default = false
                                        }
                                        self.is_default = true
                                        self.id = json["data"]["id"].int ?? 0
                                        if  editMode {
                                            let findOld = MUser.sharedInstance.addresses.filter({ (addr) -> Bool in
                                                return addr.id == self.id
                                            })
                                            if let index = MUser.sharedInstance.addresses.indexOf(findOld.first!) {
                                                MUser.sharedInstance.addresses.removeAtIndex(index)
                                            }
                                            MUser.sharedInstance.addresses.append(self)
                                            Mixpanel.mainInstance().track(event: "Usuario Edito Direccion",properties: addrPrmt)

                                        }else{
                                            MUser.sharedInstance.addresses.append(self)
                                            Mixpanel.mainInstance().track(event: "Nueva Direccion Creada", properties: ["Latitud":self.latitude ?? "", "Longitud":self.longitude ?? "", "Nombre":self.name])
                                            
                                        }
                                        MUser.sharedInstance.addresses.sortInPlace({ (addr1, addr2) -> Bool in
                                            return addr1.is_default
                                        })
                                        NSNotificationCenter.defaultCenter().postNotificationName("updateAddressUINotification", object: nil)
                                        NSNotificationCenter.defaultCenter().postNotificationName(kUserDidChangeDefaultAddressNotification, object: nil)
                                        completion(success: true, errorMessage: nil)
                                    }else{//handle error in request
                                        let message = json["data"].string ?? ""

                                        completion(success: false, errorMessage: message)

                                    }
                                }else{ //json nil
                                    completion(success: false, errorMessage: "Ocurrion un error inesperado!")

                                }
                            }else{ //mayor error not nil
                                completion(success: false, errorMessage: "Ocurrion un error inesperado!")

                            }
                        }
            }else{
                return completion(success: false, errorMessage: "Faltan completar datos!")
        }
    }
    
    var locationAsCoordinate: CLLocation {
        let resto1Coordinates = CLLocation(latitude: (self.latitude as NSString).doubleValue, longitude: (self.longitude as NSString).doubleValue)
        return resto1Coordinates
    }
    
 
    
    init (json: SwiftyJSON.JSON) {
        self.name = json["name"].string ?? ""
        self.street1 = json["street1"].string ?? ""
        self.street2 = json["street2"].string ?? ""
        self.number = json["number"].string ?? ""
        self.reference = json["reference"].string ?? ""
        self.is_default = json["is_default"].bool ?? false
        self.latitude = json["latitude"].string ?? ""
        self.longitude = json["longitude"].string ?? ""
        self.id = json["id"].int ?? 0

    }
    
     required init (addr: MAddress) {
        self.id = addr.id
        self.name = addr.name
        self.street1 = addr.street1
        self.number = addr.number
        self.street2 = addr.street2
        self.reference = addr.reference
        self.latitude = addr.latitude
        self.longitude = addr.longitude
        self.coordinate = addr.coordinate
    }
    
    func unparseAddressFromAPI(json: JSON) -> MAddress {
        self.name = json["name"].string ?? ""
        self.street1 = json["street1"].string ?? ""
        self.street2 = json["street2"].string ?? ""
        self.number = json["number"].string ?? ""
        self.reference = json["reference"].string ?? ""
        self.is_default = json["is_default"].bool ?? false
        self.latitude = json["latitude"].string ?? ""
        self.longitude = json["longitude"].string ?? ""
        self.id = json["id"].int ?? 0
        
        return self
    }
    
    func copyWithZone(zone: NSZone) -> AnyObject {
        return self.dynamicType.init(addr: self)
    }
    
}

func == (lhs: MAddress, rhs: MAddress) -> Bool {
    return lhs.id == rhs.id
}



