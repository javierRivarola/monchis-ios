//
//  MFranchise.swift
//  Monchis
//
//  Created by jrivarola on 7/30/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
import SwiftyJSON

class MFranchise: NSObject {
    var id: Int?
    var name: String?
    var legal_name: String?
    var ruc: String?
    var contact_name: String?
    var street1: String?
    var street2: String?
    var house_number: String?
    var email: String?
    var phone: String?
    var franchise_info: String?
    var franchise_category_id:Int?
    var promotion_available: Int?
    var icon_attachment: MAttachment?
    var header_attachment: MAttachment?
    var branches: [MRestaurant] = []
    var branches_with_pickup: Bool!
    var branches_with_delivery: Bool!
    var list_order:Int?
    init (json: JSON) {
        self.id = json["id"].int
        self.name = json["name"].string
        self.legal_name = json["legal_name"].string
        self.ruc = json["ruc"].string
        self.contact_name = json["contact_name"].string
        self.street1 = json["street1"].string
        self.street2 = json["street2"].string
        self.house_number = json["house_number"].string
        self.email = json["email"].string
        self.phone = json["phone"].string
        self.franchise_info = json["franchise_info"].string
        self.franchise_category_id = json["franchise_category_id"].int
        self.promotion_available = json["promotion_available"].int
        self.header_attachment = MAttachment(json: json["header_attachment"])
        self.icon_attachment = MAttachment(json: json["icon_attachment"])
        self.branches_with_pickup = json["branches_with_pickup"].bool
        self.branches_with_delivery = json["branches_with_delivery"].bool
        self.list_order = json["list_order"].int

    }
    override init (){
        
    }
}

func == (lhs: MFranchise, rhs: MFranchise) -> Bool {
    return lhs.id == rhs.id
}