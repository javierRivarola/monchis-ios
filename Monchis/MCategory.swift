//
//  MCategory.swift
//  Monchis
//
//  Created by jrivarola on 9/8/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
import SwiftyJSON

class MCategory {
    var restaurants: [MRestaurant] = []
    var name:String?
    var id: Int?
    var image: UIImage?
    var description:String?
    var franchises: [MFranchise] = []
    var attachment_id: Int?
    var attachment: MAttachment?
    
    class func compareCategories(cat1: [MCategory], cat2: [MCategory]) -> Bool {
        for c in cat1 {
            if !cat2.contains( {cat in
                return cat.id == c.id
            }) {
                return false
            }
        }
        return true
    }
    
    init (){
        
    }
    
    
    init (json: JSON) {
        self.name = json["name"].string
        self.id = json["id"].int
        self.description = json["description"].string
        self.attachment = MAttachment(json: json["attachment"])
    }
    
}

