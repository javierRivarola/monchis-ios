//
//  AddressWalktroughViewController.swift
//  Monchis
//
//  Created by Monchis Mac on 3/7/17.
//  Copyright © 2017 cibersons. All rights reserved.
//

import UIKit
import SCLAlertView
import MBProgressHUD

class AddressWalktroughViewController: UIViewController,UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    @IBOutlet weak var backBtn: UIButton!
    var editMode: Bool = false
    var firstStreetVC: UIViewController!
    var secondStreetVC: UIViewController!
    var numberStreetVC: UIViewController!
    var referencesStreetVC: UIViewController!
    var addressNameVC: UIViewController!
    var mapVC: UIViewController!
    var vcs:[UIViewController] = []
    var showBackBtn: Bool = false
    var currentIndex = 0
    var pageViewController : UIPageViewController!
    var address: MAddress!
    var pageContainer: UIPageViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        if address == nil {
            address = MAddress()
        }
        self.backBtn.hidden = !showBackBtn
        let id = "AddressPageViewControllerID"
        mapVC = self.storyboard?.instantiateViewControllerWithIdentifier("AddressViewController")
        (mapVC as? AddressViewController)?.addressToEdit = self.address
        print(address.number, " is the number")
        if editMode {
            (mapVC as? AddressViewController)?.editingMode = true
            (mapVC as? AddressViewController)?.addressToEdit = self.address
            
        }
        
        mapVC.loadViewIfNeeded()
        
        (mapVC as? AddressViewController)?.nextBtn.addTarget(self, action: #selector(AddressWalktroughViewController.scrollPageViewToNext), forControlEvents: .TouchUpInside)
        firstStreetVC = self.storyboard?.instantiateViewControllerWithIdentifier(id)
        firstStreetVC.loadViewIfNeeded()
        (firstStreetVC as? AddressPageViewController)?.nextBtn.addTarget(self, action: #selector(AddressWalktroughViewController.scrollPageViewToNext), forControlEvents: .TouchUpInside)
        (firstStreetVC as? AddressPageViewController)?.address = self.address
        (firstStreetVC as? AddressPageViewController)?.index = 0
        (firstStreetVC as? AddressPageViewController)?.inputTextField?.placeholder = "Calle 1"
        (firstStreetVC as? AddressPageViewController)?.tipText = "Ingresa la Calle 1 de tu dirección"
        (firstStreetVC as? AddressPageViewController)?.titleLbl?.text = "Paso 1/6"
        
        
        numberStreetVC = self.storyboard?.instantiateViewControllerWithIdentifier(id)
        numberStreetVC.loadViewIfNeeded()
        (numberStreetVC as? AddressPageViewController)?.address = self.address
        (numberStreetVC as? AddressPageViewController)?.index = 1
        
        
        (numberStreetVC as? AddressPageViewController)?.nextBtn.addTarget(self, action: #selector(AddressWalktroughViewController.scrollPageViewToNext), forControlEvents: .TouchUpInside)
        (numberStreetVC as? AddressPageViewController)?.tipText = "Ingresa el numero de Casa, Departamento, Edificio etc.."
        (numberStreetVC as? AddressPageViewController)?.inputTextField?.placeholder = "Numero de Casa o Departamento"
        (numberStreetVC as? AddressPageViewController)?.titleLbl?.text = "Paso 2/6"
        
        
        
        secondStreetVC = self.storyboard?.instantiateViewControllerWithIdentifier(id)
        secondStreetVC.loadViewIfNeeded()
        (secondStreetVC as? AddressPageViewController)?.address = self.address
        
        (secondStreetVC as? AddressPageViewController)?.index = 2
        (secondStreetVC as? AddressPageViewController)?.nextBtn.addTarget(self, action: #selector(AddressWalktroughViewController.scrollPageViewToNext), forControlEvents: .TouchUpInside)
        
        (secondStreetVC as? AddressPageViewController)?.tipText = "Ingresa la Calle 2 de tu dirección"
        (secondStreetVC as? AddressPageViewController)?.inputTextField?.placeholder = "Calle 2"
        (secondStreetVC as? AddressPageViewController)?.titleLbl?.text = "Paso 3/6"
        
        
        referencesStreetVC = self.storyboard?.instantiateViewControllerWithIdentifier(id)
        referencesStreetVC.loadViewIfNeeded()
        (referencesStreetVC as? AddressPageViewController)?.address = self.address
        
        (referencesStreetVC as? AddressPageViewController)?.nextBtn.addTarget(self, action: #selector(AddressWalktroughViewController.scrollPageViewToNext), forControlEvents: .TouchUpInside)
        (referencesStreetVC as? AddressPageViewController)?.index = 3
        
        (referencesStreetVC as? AddressPageViewController)?.tipText = "Ingresa alguna referencia que nos ayude aún mas a encontrarte"
        (referencesStreetVC as? AddressPageViewController)?.titleLbl?.text = "Paso 4/6"
        
        (referencesStreetVC as? AddressPageViewController)?.inputTextField?.placeholder = "Referencias"
        addressNameVC = self.storyboard?.instantiateViewControllerWithIdentifier(id)
        addressNameVC.loadViewIfNeeded()
        (addressNameVC as? AddressPageViewController)?.index = 5
        (addressNameVC as? AddressPageViewController)?.address = self.address
        
        (addressNameVC as? AddressPageViewController)?.nextBtn.addTarget(self, action: #selector(AddressWalktroughViewController.endSavingAddress), forControlEvents: .TouchUpInside)
        (addressNameVC as? AddressPageViewController)?.tipText = "Dale un Nombre a tu nueva Dirección!"
        (addressNameVC as? AddressPageViewController)?.titleLbl?.text = "Paso 6/6"
        
        (addressNameVC as? AddressPageViewController)?.inputTextField?.placeholder = "Nombre de Dirección"
        
        
        
        vcs = [firstStreetVC,numberStreetVC,secondStreetVC,referencesStreetVC,mapVC, addressNameVC]
        pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageViewControllerID") as! UIPageViewController
        
        configurePageController()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddressWalktroughViewController.keyboardWillShow(_:)) , name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddressWalktroughViewController.keyboardWillHide(_:)) , name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddressWalktroughViewController.blockScrolling) , name: "blockPageViewScrollingNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddressWalktroughViewController.enableScrolling) , name: "enablePageViewScrollingNotification", object: nil)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func blockScrolling(){
        self.pageViewController.dataSource = nil
    }
    
    func enableScrolling(){
        self.pageViewController.dataSource = self
    }
    
    
    func endSavingAddress(){
        (addressNameVC as? AddressPageViewController)?.view.endEditing(true)
        let hud = MBProgressHUD.showHUDAddedTo((addressNameVC as! AddressPageViewController).view, animated: true)
        hud.label.text = "Guardando..."
        
        self.address.saveBackend(self.editMode) { (success, errorMessage) in
            hud.hideAnimated(true)
            if success {
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false
                )
                let alert = SCLAlertView(appearance: appearance)
                alert.addButton("Listo!", action: {

                        self.dismissViewControllerAnimated(true, completion: nil)
                })
                alert.showSuccess("Excelente!", subTitle: "Empezá a hacer pedidos ya!")
            }else{
                SCLAlertView().showError("Lo sentimos!", subTitle: errorMessage!)
            }
        }
    }
    
    @IBAction func backBtnPressed(sender: UIButton) {
        dispatch_async(dispatch_get_main_queue()) {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    
    func keyboardWillShow(notification: NSNotification) {
        let userInfo:NSDictionary = notification.userInfo!
        let keyboardFrame:NSValue = userInfo.valueForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.CGRectValue()
        pageViewController.view.frame = CGRectMake(0, pageViewController.view.frame.origin.y, self.view.bounds.width, self.view.frame.height - keyboardRectangle.height - bottomDistance)
        
        
        
    }
    
    let bottomDistance:CGFloat = 60.0
    
    func keyboardWillHide(notification: NSNotification){
        pageViewController.view.frame = CGRectMake(0, pageViewController.view.frame.origin.y, self.view.bounds.width, self.view.frame.height - bottomDistance)
    }
    var bottomConstraint: NSLayoutConstraint!
    func configurePageController(){
        self.pageViewController.dataSource = self
        self.pageViewController.delegate = self
        dispatch_async(dispatch_get_main_queue()) {
            self.pageViewController.setViewControllers([self.vcs.first!], direction: .Forward, animated: true, completion: nil)
            
            self.pageViewController.view.frame = CGRectMake(0, self.bottomDistance, self.view.frame.width, self.view.frame.height)
            self.addChildViewController(self.pageViewController)
            self.view.addSubview(self.pageViewController.view)
            self.pageViewController.didMoveToParentViewController(self)
            
            
        }
        
        
        
        
        
    }
    
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        if let index = vcs.indexOf(viewController) {
            let newIndex = index+1
            if newIndex == vcs.count {
                return nil
            }
            
            
            let vc = vcs[newIndex]
            return vc
        }else{
            return nil
        }
    }
    
    var vcIndexToScroll: Int = 1
    
    func scrollPageViewToNext(){
        if vcIndexToScroll < 0 || vcIndexToScroll >= vcs.count {
            return
        }
        currentIndex = vcIndexToScroll
        
        self.pageViewController.setViewControllers([self.vcs[vcIndexToScroll]], direction: .Forward, animated: true, completion: nil)
        vcIndexToScroll += 1
        
    }
    
    func scrollPageToBack(){
        self.currentIndex = vcIndexToScroll - 2
        self.pageViewController.setViewControllers([self.vcs[vcIndexToScroll - 2]], direction: .Forward, animated: true, completion: nil)
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool){
        if let pageContentViewController = pageViewController.viewControllers![0] as? AddressPageViewController {
            vcIndexToScroll = pageContentViewController.index + 1
        }else{
            currentIndex = 3
            vcIndexToScroll = 4
        }
        
    }
    
    
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        if let index = vcs.indexOf(viewController) {
            let newIndex = index-1
            if newIndex < 0{
                return nil
            }
            
            let vc = vcs[newIndex]
            
            return vc
        }else{
            return nil
        }
        
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return self.vcs.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return currentIndex
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
