//
//  RecomendacionesTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 8/28/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class RecomendacionesTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.layoutIfNeeded()
    }
    
}
