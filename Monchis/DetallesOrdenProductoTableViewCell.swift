//
//  DetallesOrdenProductoTableViewCell.swift
//  Monchis
//
//  Created by Javier Rivarola on 10/25/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit

class DetallesOrdenProductoTableViewCell: UITableViewCell {

    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productAddons: UILabel!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productQuantityLbl: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productComments: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
