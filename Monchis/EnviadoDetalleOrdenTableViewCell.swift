//
//  EnviadoDetalleOrdenTableViewCell.swift
//  Monchis
//
//  Created by Javier Rivarola on 10/25/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit

class EnviadoDetalleOrdenTableViewCell: UITableViewCell {

    @IBOutlet weak var addressNameLbl: UILabel!
    @IBOutlet weak var addressDescrLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
