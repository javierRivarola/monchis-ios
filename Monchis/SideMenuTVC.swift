//
//  SideMenuTVC.swift
//  Monchis
//
//  Created by jrivarola on 8/11/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class SideMenuTVC: UITableViewCell {

    @IBOutlet weak var noAddressesLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var addAddressBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func addAddressPressed(sender: UIButton) {
        NSNotificationCenter.defaultCenter().postNotificationName("addNewAddressBtnPressed", object: nil)
    }
}
