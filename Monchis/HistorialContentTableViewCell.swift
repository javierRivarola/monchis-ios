//
//  HistorialContentTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 1/2/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
protocol HistorialContentTVCDelegate {
    func didPressOrderAgainProduct(product: MProduct, order: MOrder)
    func didPressOrderAgainOrder(order: MOrder)
    func didPressSeeDetails(order: MOrder)
}
class HistorialContentTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, HistorialContentCellDelegate {
    var delegate: HistorialContentTVCDelegate?
    @IBOutlet weak var branchNameLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var orderTotalLbl: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var cancelView: UIView!
    var order: MOrder!
    var parentController: UITableViewController!
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        calificarBtn.imageView?.contentMode = .ScaleAspectFit
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    @IBOutlet weak var calificarBtn: UIButton!
    @IBAction func calificarPressed(sender: UIButton) {
        (parentController as? TodoHistorialTableViewController)?.selectedOrder = order
        parentController?.performSegueWithIdentifier("calificarSegue", sender: parentController)
    }
    
    @IBOutlet weak var ratingView: UIView!
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("historialContentCollectionCell", forIndexPath: indexPath) as! HistorialContentCollectionViewCell

        let product = order.products[indexPath.row]
        cell.imageView.image = PLACE_HOLDER_IMAGE
        product.attachments.first?.width = cell.imageView.bounds.width
        product.attachments.first?.width = cell.imageView.bounds.height

        if let imgUrl = product.attachments.first?.imageComputedURL {
            cell.imageView.hnk_setImageFromURL(imgUrl, placeholder: NO_ITEM_PLACEHOLDER)
            cacheFormats[imgUrl.URLString] = cell.imageView.hnk_format.name
            if !imageCacheKeys.contains(imgUrl.URLString) {
                imageCacheKeys.append(imgUrl.URLString)
            }
        }

        cell.product = product
        cell.productNameLbl.text = product.name
        cell.productPriceLbl.text = product.price?.asLocaleCurrency
        cell.quantityLbl.text = "\(product.quantity ?? 1)"
       cell.quantityView.layer.cornerRadius = 25/2
        cell.quantityView.clipsToBounds = true
        cell.customDelegate = self
        
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return order.products.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func didPressOrderAgain(product: MProduct)
    {
        delegate?.didPressOrderAgainProduct(product, order: order)
    }
    
    @IBAction func didPressRepeatOrder(sender: UIButton) {
        delegate?.didPressOrderAgainOrder(order)
    }
    
  
    @IBAction func seeDetailsPressed(sender: UIButton) {
        delegate?.didPressSeeDetails(order)

    }

    
}
