//
//  PickupDeliveryViewController.swift
//  Monchis
//
//  Created by jrivarola on 5/1/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit

class PickupDeliveryViewController: UIViewController {
    
    @IBOutlet weak var deliveryView: UIView!
    @IBOutlet weak var pickupView: UIView!
    var franchise: MFranchise!
    struct Segues {
        static let unwindFromSelection = "unwindFromSelection"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }

    @IBAction func backBtnPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func setupUI(){
        let tapGestureDelivery = UITapGestureRecognizer(target: self, action: #selector(PickupDeliveryViewController.deliveryViewPressed))
        let tapGesturePickup = UITapGestureRecognizer(target: self, action: #selector(PickupDeliveryViewController.pickupViewPressed))
        
        deliveryView.addGestureRecognizer(tapGestureDelivery)
        pickupView.addGestureRecognizer(tapGesturePickup)

        
    }
    
    @IBAction func deliveryBtnPressed(sender: UIButton) {
        deliveryViewPressed()
    }
    
    @IBAction func pickupBtnPressed(sender: UIButton) {
        pickupViewPressed()
    }
    
    var wantsDelivery: Bool!
    
    func deliveryViewPressed(){
        wantsDelivery = true
        self.performSegueWithIdentifier(Segues.unwindFromSelection, sender: self)
    }
    
    func pickupViewPressed(){
        wantsDelivery = false
        self.performSegueWithIdentifier(Segues.unwindFromSelection, sender: self)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Segues.unwindFromSelection {
            let dvc = segue.destinationViewController as! MainViewPagingMenuViewController
            dvc.wantsDelivery = wantsDelivery
        }
    }
    

}
