//
//  MWaitScreen.swift
//  Monchis
//
//  Created by jrivarola on 8/20/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
import MBProgressHUD
import DynamicBlurView
import UIKit

func customWaitScreen(text: String,comment: String, view: UIView, blurView: DynamicBlurView?){
    if blurView != nil {
        view.addSubview(blurView!)
        UIView.animateWithDuration(0.2, animations: {
            blurView!.blurRadius = 8
        })
    }
    
    let spinner = MBProgressHUD.showHUDAddedTo(view, animated: true)
    spinner.mode = MBProgressHUDMode.Indeterminate
    spinner.label.text = NSLocalizedString(text,comment: comment)
    spinner.label.font = UIFont(name: "OpenSans-Semibold", size: 14.0)!
    
}

func dismissCustomWaitScreen(view: UIView, blurView: DynamicBlurView?){
    dispatch_async(dispatch_get_main_queue()) {
        blurView?.removeFromSuperview()
        MBProgressHUD.hideHUDForView(view, animated: true)
    }
}


func customAlertWithOK(title: String , message: String) -> UIAlertController{
    let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
    
    let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar mensaje de error"), style: .Default, handler: nil)
    alert.addAction(ok)
    return alert
}
