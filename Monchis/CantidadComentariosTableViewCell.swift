//
//  CantidadComentariosTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 5/11/15.
//  Copyright © 2015 cibersons. All rights reserved.
//

import UIKit
import GMStepper

protocol didChangeQuantityComments {
    func quantityDidChange(sender: GMStepper)
}
class CantidadComentariosTableViewCell: UITableViewCell {

    @IBOutlet weak var commentsDescLbl: UILabel!
    @IBOutlet weak var commentsLbl: UILabel!
    @IBOutlet weak var quantityDescLbl: UILabel!
    @IBOutlet weak var quanityLbl: UILabel!
    @IBOutlet weak var cantidadStepper: GMStepper!
    @IBOutlet weak var cometariosTextView: UITextView!
    var product: MProduct!
    var delegate: didChangeQuantityComments?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func didChangeQuantityForOrder(sender: GMStepper) {
        if product != nil {
            delegate?.quantityDidChange(sender)
        }
    }

}
