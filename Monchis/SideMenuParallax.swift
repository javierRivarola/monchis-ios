//
//  SideMenuParallax.swift
//  Monchis
//
//  Created by jrivarola on 7/13/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
import RESideMenu
import Crashlytics

class SideMenuParallax: RESideMenu, RESideMenuDelegate {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.menuPreferredStatusBarStyle = .LightContent
        self.contentViewShadowColor = UIColor.blackColor()
        self.contentViewShadowOffset = CGSizeMake(0, 0)
        self.contentViewShadowOpacity = 0.6
        self.contentViewShadowRadius = 12
        self.contentViewInPortraitOffsetCenterX = 80
        self.interactivePopGestureRecognizerEnabled = true
        self.bouncesHorizontally = true
        self.scaleMenuView = false
        self.interactivePopGestureRecognizerEnabled = false
        
        self.contentViewShadowEnabled = true
        let contentNC = self.storyboard?.instantiateViewControllerWithIdentifier("InicioTC")

        vCs["Inicio"] = contentNC

        self.contentViewController = contentNC
        self.leftMenuViewController = self.storyboard?.instantiateViewControllerWithIdentifier("menuVC") as! SideMenuViewController
        self.rightMenuViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RightSideMenuViewController")
        
        self.backgroundImage = UIImage(named: "background-completoChico")
        self.delegate = self
        
        
    }
    
    
    
    func sideMenu(sideMenu: RESideMenu!, willShowMenuViewController menuViewController: UIViewController!) {
        let center = NSNotificationCenter.defaultCenter()
        center.postNotificationName("willShowMenuViewController", object: nil)
        if menuViewController == self.rightMenuViewController {
            Answers.logCustomEventWithName("Usuario abrio menu Derecho",
                                           customAttributes:[:])
        }
        if menuViewController == self.leftMenuViewController {
            Answers.logCustomEventWithName("Usuario abrio menu Izquierdo",
                                           customAttributes:[:])
        }
    }
    
    func sideMenu(sideMenu: RESideMenu!, didRecognizePanGesture recognizer: UIPanGestureRecognizer!) {
        let center = NSNotificationCenter.defaultCenter()
        center.postNotificationName("didRecognizePanGesture", object: recognizer)

    }
    
    func sideMenu(sideMenu: RESideMenu!, willHideMenuViewController menuViewController: UIViewController!) {
        let center = NSNotificationCenter.defaultCenter()
        center.postNotificationName("willHideMenuViewController", object: nil)
    }
    

}