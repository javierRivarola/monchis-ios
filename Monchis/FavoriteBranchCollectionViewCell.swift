//
//  FavoriteBranchCollectionViewCell.swift
//  Monchis
//
//  Created by jrivarola on 12/1/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit

protocol DeleteBranchFavoriteDelegate {
    func didPressDeleteFavorite(branch: MRestaurant)
}

class FavoriteBranchCollectionViewCell: UICollectionViewCell {
    
    var delegate: DeleteBranchFavoriteDelegate?
    var branch: MRestaurant!

    @IBOutlet weak var branchNameLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var deleteBranchBtn: UIButton!
    
    @IBAction func deleteBranchPressed(sender: UIButton) {
        delegate?.didPressDeleteFavorite(branch)
    }
    
}
