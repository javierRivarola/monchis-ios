//
//  FavoritosTableViewController.swift
//  Monchis
//
//  Created by jrivarola on 8/13/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import QuartzCore
import DZNEmptyDataSet
import MBProgressHUD
import Mixpanel

class FavoritosTableViewController: UITableViewController,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource,DeleteBranchFavoriteDelegate,DeleteProductFavoriteDelegate, BranchSelectionDelegate{
    
    // MARK: Properties
    var collectionView: UICollectionView!
    var shouldAnimateCells: Bool = false
    var selectedFavorites:[MFavorite] = []
    let api = MAPI.SharedInstance
    let user = MUser.sharedInstance
    
    struct Constants {
        static let reuseFavoritosTableViewCellID = "reuseFavoritosIdentifier"
        static let reuseFavoritosCVID = "reuseCVID"
        static let favoritosCRVID = "FavoritosCRVID"
        static let reuseFavoritosBranchesCVID = "FavoriteBranchReuseID"
        static let reuseFavoriteBranchTV = "reuseFavoritosBranchesIdentifier"
    }
    
    // MARK: LifeCycle
    var productCollectionView: UICollectionView!
    var branchesCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let bg = UIImageView(image: UIImage(named: "background-completoChico"))
        bg.contentMode = .ScaleAspectFill
        bg.clipsToBounds = true
        self.tableView.backgroundView = bg
        bg.layer.zPosition = -1
        registerForNotifications()
        let img = UIImage(named: "texto-monchis-header")
        let imgView = UIImageView(frame: CGRectMake(0, 0, 50, 25))
        imgView.image = img
        imgView.contentMode = .ScaleAspectFit
        self.navigationItem.titleView = imgView
//        tableView.rowHeight = UITableViewAutomaticDimension
//        tableView.estimatedRowHeight = 200

        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.emptyDataSetDelegate = self
        tableView.emptyDataSetSource = self
        getFavorites()
        configurePullToRefresh()
    }
    
    func configurePullToRefresh(){
        
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.whiteColor()
        let title = NSMutableAttributedString()
        let tira = NSAttributedString(string: "Tira para actualizar", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        title.appendAttributedString(tira)
        refreshControl.attributedTitle = title
        refreshControl.addTarget(self, action: #selector(FavoritosTableViewController.pullToRefresh(_:)), forControlEvents: .ValueChanged)
        refreshControl.layer.zPosition += 1
        self.refreshControl = refreshControl
    }
    
    func pullToRefresh(sender: UIRefreshControl){
        getFavorites()
    }

    
    func registerForNotifications(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(FavoritosTableViewController.userDefaultAddressChanged), name: kUserDidChangeDefaultAddressNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(FavoritosTableViewController.getFavorites), name: kUserLoggedInNotification, object: nil)

    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if user.logged {
            getFavorites()
        }
    }
    
    func userDefaultAddressChanged(){
        //handle address change
        
    }
    var selectedFavoriteProduct: MProduct!

    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        if user.branchesFavorites.count == 0 && user.productFavorites.count == 0 {
//            return 0
//        }
//        if user.branchesFavorites.count > 0 && user.productFavorites.count > 0 {
//            return 2
//        }
//        return 1
        return user.productFavorites.count > 0 ? 1 : 0
        
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return 1
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPaht: NSIndexPath) -> CGFloat {
        return tableView.bounds.height
    }
    
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 && user.productFavorites.count > 0{
            let cell = tableView.dequeueReusableCellWithIdentifier(Constants.reuseFavoritosTableViewCellID, forIndexPath: indexPath) as! FavoritosTableViewCell
            cell.collectionView.tag = 10
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.reloadData()

            return cell

        }
        
        if (indexPath.section == 0 && user.productFavorites.count == 0) || indexPath.section == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier(Constants.reuseFavoriteBranchTV, forIndexPath: indexPath) as! FavoriteBranchTableViewCell
            cell.collectionView.tag = 20
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.reloadData()
            return cell

        }
        return UITableViewCell()
    }
 
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 && user.productFavorites.count > 0{
            let lbl = UILabel(frame: CGRectMake(10, 0, tableView.bounds.width - 10, 55))
            
            //lbl.textAlignment = .Left

            
            
            lbl.text = NSLocalizedString("PRODUCTOS FAVORITOS", comment: "productos favoritos")
           // view.addSubview(lbl)
            lbl.font = UIFont.boldSystemFontOfSize(17)
            lbl.textColor = UIColor.whiteColor()

            return lbl
        }
        if (section == 0 && user.productFavorites.count == 0) || section == 1 {
            let lbl = UILabel(frame: CGRectMake(10, 0, tableView.bounds.width, 55))
  

            lbl.text = NSLocalizedString("LOCALES FAVORITOS", comment: "productos favoritos")
            lbl.font = UIFont.boldSystemFontOfSize(17)
            lbl.textColor = UIColor.whiteColor()
          
        

            return lbl
        }
        return nil
    }
    
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func editPressed(sender: UIBarButtonItem) {
        if sender.tag == 0 {
            sender.title = "Listo"
            sender.tag = 1
            shouldAnimateCells = true
        }else{
            sender.tag = 0
            sender.title = "Editar"
            //handle deleting
        }
        
    }
    
    func colorize(cell: UICollectionViewCell, color: CGColorRef) {
        
        // Set the image's shadowColor, radius, offset, and
        // set masks to bounds to false
        cell.layer.shadowColor = color
        cell.layer.shadowRadius = 20.0
        cell.layer.shadowOffset = CGSizeZero
        cell.layer.masksToBounds = false
        
        // Animate the shadow opacity to "fade it in"
        let shadowAnim = CABasicAnimation()
        shadowAnim.keyPath = "shadowOpacity"
        shadowAnim.fromValue = NSNumber(float: 0.0)
        shadowAnim.toValue = NSNumber(float: 0.9)
        shadowAnim.duration = 0.13
        
        shadowAnim.repeatCount = 4
        shadowAnim.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        shadowAnim.autoreverses = true
        cell.layer.addAnimation(shadowAnim, forKey: "shadowOpacity")
        cell.layer.shadowOpacity = 1
    }
    
    
    
}

extension FavoritosTableViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if collectionView.tag == 10 {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Constants.reuseFavoritosCVID, forIndexPath: indexPath) as! FavoritosCollectionViewCell
        let product = user.productFavorites[indexPath.row]
            cell.imageView.image = NO_ITEM_PLACEHOLDER
        if let imgUrl = product.attachments.first?.imageComputedURL {
            cell.imageView.hnk_setImageFromURL(imgUrl, placeholder: NO_ITEM_PLACEHOLDER)
        }
            if let franchiseName = user.productFavorites[indexPath.row].branch_products.first?.franchise?.name {
                cell.branchNameLbl.text = franchiseName
            }
        cell.titulo.text = product.name
        cell.descripcion.text = product.product_description
        cell.precio.text = product.price?.asLocaleCurrency
            cell.product = product
            cell.delegate = self
        return cell
        }else{
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Constants.reuseFavoritosBranchesCVID, forIndexPath: indexPath) as! FavoriteBranchCollectionViewCell
            cell.imgView.image = NO_BRANCH_PLACEHOLDER
            let branch = user.branchesFavorites[indexPath.row]
                        if let imgUrl = branch.franchise?.icon_attachment?.imageComputedURL {
                cell.imgView.hnk_setImageFromURL(imgUrl, placeholder: NO_BRANCH_PLACEHOLDER)
            }
            cell.delegate = self
            cell.branch = branch
            cell.branchNameLbl.text = branch.franchise?.name
            return cell
        }
    }
    
    
    func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {

    }
    
//    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
//        let view = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: Constants.favoritosCRVID, forIndexPath: indexPath) as! FavoritosCollectionReusableView
//        //add image
//        return view
//    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 10 {
            return user.productFavorites.count
        }else{
            
            return user.branchesFavorites.count
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 250)
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
//        cell.transform = CGAffineTransformMakeTranslation(0,-cell.bounds.height)
//
//        UIView.animateWithDuration(0.6, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.7, options: .CurveEaseInOut, animations: {
//            cell.transform = CGAffineTransformIdentity
//            }) { (fin) -> Void in
//                
//        }
    }
    func didSelectBranch(branch: MRestaurant,order: MOrder?)
    {
        if selectedFavoriteProduct != nil {
            let customizeProductVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
            customizeProductVC.restaurant = branch
            customizeProductVC.product = selectedFavoriteProduct
            self.navigationController?.pushViewController(customizeProductVC, animated: true)
        }else{
            let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
            restaurantVC.restaurant = branch
            self.navigationController?.pushViewController(restaurantVC, animated: true)
        }

    }
    
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if collectionView.tag == 10 {
            
             selectedFavoriteProduct = user.productFavorites[indexPath.row]
            let customizeProductVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
    
            Mixpanel.mainInstance().track(event: "Usuario Eligio un Producto", properties: ["Producto":selectedFavoriteProduct.name!,"Producto ID": selectedFavoriteProduct.id!, "Pantalla": "Favoritos"])

            customizeProductVC.product = selectedFavoriteProduct
            deliveryOrPickupHistorySearch(selectedFavoriteProduct.branch_products.first!, completion: { (wantsDelivery,newBranch) in
                customizeProductVC.restaurant = newBranch
                if wantsDelivery == nil || wantsDelivery == true {
                    customizeProductVC.restaurant.mode = 0
                    self.navigationController?.pushViewController(customizeProductVC, animated: true)
                }else{
                    customizeProductVC.restaurant.mode = 1
                    let branchSelection = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                    branchSelection.franchise = newBranch.franchise
                    branchSelection.delegate = self
                    self.navigationController?.pushViewController(branchSelection, animated: true)
                    
                }

            })
            
 
           
            
        }else{
            let selectedBranch = user.branchesFavorites[indexPath.row]
            
            
            selectedFavoriteProduct = nil
            deliveryOrPickupHistorySearch(selectedBranch, completion: { (wantsDelivery,newBranch) in
                let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
                restaurantVC.restaurant = newBranch
                if wantsDelivery == nil || wantsDelivery == true  {
                    restaurantVC.restaurant.mode = 0
                    self.navigationController?.pushViewController(restaurantVC, animated: true)
                }else{
                    
                    let branchSelection = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                    branchSelection.franchise = newBranch.franchise
                    branchSelection.delegate = self
                    self.navigationController?.pushViewController(branchSelection, animated: true)
                    
                    
                    restaurantVC.restaurant.mode = 1
                }
                
            })
            
        }
       
    }
    
    func collectionView(collectionView: UICollectionView,
        didDeselectItemAtIndexPath indexPath: NSIndexPath) {
              }
    
    // MARK: EmptyDataSetSource
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        let image = UIImage(named: "favoritos-vacio")
        return image
    }
    
    func imageAnimationForEmptyDataSet(scrollView: UIScrollView!) -> CAAnimation! {
        
        let animation = CABasicAnimation(keyPath: "transform")
        
        animation.fromValue = NSValue(CATransform3D: CATransform3DIdentity)
        animation.toValue = NSValue(CATransform3D:CATransform3DMakeRotation(CGFloat(M_PI_2), 0.0, 0.0, 1.0))
        
        
        
        animation.duration = 0.25
        animation.cumulative = true
        animation.repeatCount = 1
        
        return animation
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let text = NSMutableAttributedString()
        let emptyCart = NSAttributedString(string: NSLocalizedString("Todavía no agregaste nada a tu lista de favoritos", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
        
        text.appendAttributedString(emptyCart)
        return text
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = NSMutableAttributedString()
        text.appendAttributedString(NSAttributedString(string: NSLocalizedString("Agregá ", comment: "platos"), attributes: [NSFontAttributeName:UIFont.systemFontOfSize(14),NSForegroundColorAttributeName: UIColor.whiteColor()]))
        
        
        let platos = NSAttributedString(string: NSLocalizedString("platos", comment: "platos"), attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSForegroundColorAttributeName: UIColor.whiteColor()])
        let locales = NSAttributedString(string: NSLocalizedString("locales", comment: "locales"), attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSForegroundColorAttributeName: UIColor.whiteColor()])
        text.appendAttributedString(platos)
        text.appendAttributedString(NSAttributedString(string: NSLocalizedString(" o ", comment: "o"), attributes: [NSFontAttributeName:UIFont.systemFontOfSize(14),NSForegroundColorAttributeName: UIColor.whiteColor()]))
        text.appendAttributedString(locales)
        text.appendAttributedString(NSAttributedString(string: NSLocalizedString(" que te gusten a esta lista y accedé a ellos directamente desde acá", comment: "descripcion de no favoritos"), attributes: [NSFontAttributeName:UIFont.systemFontOfSize(14),NSForegroundColorAttributeName: UIColor.whiteColor()]))
        return text
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAnimateImageView(scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func getFavorites(){
        api.jsonRequest(.PRODUCT_FAVORITE_INFO, parameters: nil, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: nil)",json,error)
            }
            if error == nil {
                    if let branchesFavorites = json?["data"]["branches"].array {
                        self.user.branchesFavorites = []
                        
                        for branch in branchesFavorites {
                            let newBranch = MRestaurant(data: branch["branch"])
                            let newFranchise = MFranchise(json: branch["branch"]["franchise"])
                            newBranch.franchise = newFranchise
                            self.user.branchesFavorites.append(newBranch)
                        }
                    }
                    if let productFavorites = json?["data"]["products"].array {
                        self.user.productFavorites = []
                        for product in productFavorites {
                            let newProduct = MProduct(json: product["product"])
                            
                            for att in product["product"]["attachments"].array! {
                                let newAtt = MAttachment(json: att)
                                newProduct.attachments = [newAtt]
                            }
                            let newBranch = MRestaurant(data: product["branch"])
                            let newFranchise = MFranchise(json: product["branch"]["franchise"])
                            newBranch.franchise = newFranchise
                            newProduct.branch_products = [newBranch]
                            self.user.productFavorites.append(newProduct)
                        }
                    }
                    self.refreshControl?.endRefreshing()
                    self.tableView.reloadData()

            }else{
                print(error)
            }
        }
    }

    
    // MARK: DeleteBranchFavoriteDelegate 
    
    func didPressDeleteFavorite(branch: MRestaurant) {
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        hud.label.text = NSLocalizedString("Eliminando..", comment: "")
        let parameters = ["branch_id":branch.id!]
        api.jsonRequest(.DELETE_PRODUCT_FAVORITE, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
              
                    /* handle favorite removal */
                    
                    if let index = self.user.branchesFavorites.indexOf({$0.id == branch.id}) {
                        self.user.branchesFavorites.removeAtIndex(index)
                    }
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.tableView.reloadData()
                        
                    })
                }else{
                    let error = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Lo sentimos!", message: error, preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }else{
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }

    }
    
    // MARK: DeleteProductFavoriteDelegate

    func didPressDeleteProductFavorite(product: MProduct){
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        let branchId = product.branch_products.first?.id ?? 0
        hud.label.text = NSLocalizedString("Eliminando..", comment: "")
        let parameters = ["product_id":product.id ?? 0,"branch_id":branchId]
        api.jsonRequest(.DELETE_PRODUCT_FAVORITE, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    /* handle favorite removal */
                    
                    Mixpanel.mainInstance().track(event: "Usuario Elimino Producto de Favoritos ", properties: ["Producto":product.name!,"Producto ID": product.id!, "Pantalla": "Favoritos"])

                    if let index = self.user.productFavorites.indexOf({$0.id == product.id}) {
                        self.user.productFavorites.removeAtIndex(index)
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.tableView.reloadData()
                        
                    })
                }else{
                    let error = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Lo sentimos!", message: error, preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }else{
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }

    }
    
    func didPressRepeatProductFavorite(product: MProduct) {
        selectedFavoriteProduct = product
        let customizeProductVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
        
        customizeProductVC.product = selectedFavoriteProduct
        deliveryOrPickupHistorySearch(selectedFavoriteProduct.branch_products.first!, completion: { (wantsDelivery,newBranch) in
            customizeProductVC.restaurant = newBranch
            if wantsDelivery == nil || wantsDelivery == true  {
                customizeProductVC.restaurant.mode = 0
                self.navigationController?.pushViewController(customizeProductVC, animated: true)
            }else{
                customizeProductVC.restaurant.mode = 1
                let branchSelection = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                branchSelection.franchise = newBranch.franchise
                branchSelection.delegate = self
                self.navigationController?.pushViewController(branchSelection, animated: true)
                
            }
            
        })

    }
    
}
