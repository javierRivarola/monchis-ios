//
//  rightSideImageButton.swift
//  Monchis
//
//  Created by jrivarola on 10/2/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class rightSideImageButton: UIButton {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    override func layoutSubviews(){
        super.layoutSubviews()
       
        let imageFrame = self.imageView?.frame;
        let labelFrame = self.titleLabel?.frame;
        
        let inset: CGFloat = 5
        
        if var imageFrame = imageFrame
        {
            if var labelFrame = labelFrame
            {
                let cumulativeWidth = imageFrame.width + labelFrame.width + inset
                let excessiveWidth = self.bounds.width - cumulativeWidth
                labelFrame.origin.x = excessiveWidth / 2
                imageFrame.origin.x = labelFrame.origin.x + labelFrame.width + inset
                
                self.imageView?.frame = imageFrame
                self.titleLabel?.frame = labelFrame
            }
        }
    }

}
