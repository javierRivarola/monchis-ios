//
//  UIViewExtension.swift
//  Feed Me
//
//  Created by Ron Kliffer on 8/30/14.
//  Copyright (c) 2014 Ron Kliffer. All rights reserved.
//

import UIKit
import SwiftyJSON
import Haneke
import SystemConfiguration
import pop
import AudioToolbox

extension SystemSoundID {
    static func playFileNamed(fileName: String, withExtenstion fileExtension: String) {
        var sound: SystemSoundID = 0
        if let soundURL = NSBundle.mainBundle().URLForResource(fileName, withExtension: fileExtension) {
            AudioServicesCreateSystemSoundID(soundURL, &sound)
            AudioServicesPlaySystemSound(sound)
        }
    }
}

extension UIView {
    
    func lock() {
        if let _ = viewWithTag(10) {
            //View is already locked
        }
        else {
            let lockView = UIView(frame: bounds)
            lockView.backgroundColor = UIColor(white: 0.0, alpha: 0.75)
            lockView.tag = 10
            lockView.alpha = 0.0
            let activity = UIActivityIndicatorView(activityIndicatorStyle: .White)
            activity.hidesWhenStopped = true
            activity.center = lockView.center
            lockView.addSubview(activity)
            activity.startAnimating()
            addSubview(lockView)
            
            UIView.animateWithDuration(0.2) {
                lockView.alpha = 1.0
            }
        }
    }
    
    func unlock() {
        if let lockView = self.viewWithTag(10) {
            UIView.animateWithDuration(0.2, animations: {
                lockView.alpha = 0.0
                }) { finished in
                    lockView.removeFromSuperview()
            }
        }
    }
    
    func fadeOut(duration: NSTimeInterval) {
        UIView.animateWithDuration(duration) {
            self.alpha = 0.0
        }
    }
    
    func fadeIn(duration: NSTimeInterval) {
        UIView.animateWithDuration(duration) {
            self.alpha = 1.0
        }
    }
    
    class func viewFromNibName(name: String) -> UIView? {
        let views = NSBundle.mainBundle().loadNibNamed(name, owner: nil, options: nil)
        return views.first as? UIView
    }
}

extension NSNull {
    func length() -> Int { return 0 }
    
    func integerValue() -> Int { return 0 }
    
    func floatValue() -> Float { return 0 };
    
    func componentsSeparatedByString(separator: String) -> [AnyObject] { return [AnyObject]() }
    
    func objectForKey(key: AnyObject) -> AnyObject? { return nil }
    
    func boolValue() -> Bool { return false }
}

// Create a string with currency formatting based on the device locale
//
extension Float {
    var asLocaleCurrency:String {
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyStyle
        formatter.locale = MUser.sharedInstance.locale
        if formatter.locale?.localeIdentifier == "en_PY" || formatter.locale?.localeIdentifier == "es_PY" {
            formatter.numberStyle = .DecimalStyle
            formatter.groupingSeparator = "."
            formatter.groupingSize = 3
            formatter.currencySymbol = "Gs. "
            return formatter.stringFromNumber(self)! + " Gs."
        }
        
        
        return formatter.stringFromNumber(self)!
    }
}

extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
}

//Present alert on any screen
extension UIViewController {
    
    func presentViewControllerFromVisibleViewController(viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)?) {
        if self is UINavigationController {
            let navigationController = self as! UINavigationController
            navigationController.topViewController?.presentViewControllerFromVisibleViewController(viewControllerToPresent, animated: true, completion: nil)
        }else if self is UITabBarController {
            let tabBarController = self as! UITabBarController
            tabBarController.selectedViewController?.presentViewControllerFromVisibleViewController(viewControllerToPresent, animated: true, completion: nil)
        } else if (presentedViewController != nil) {
            presentedViewController!.presentViewControllerFromVisibleViewController(viewControllerToPresent, animated: true, completion: nil)
        } else {
            presentViewController(viewControllerToPresent, animated: true, completion: nil)
        }
    }
}

extension UIImageView {
    func downloadImageFrom(link link:String, contentMode: UIViewContentMode) {
        NSURLSession.sharedSession().dataTaskWithURL( NSURL(string:link)!, completionHandler: {
            (data, response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}

extension UITabBarController {
    
    func setTabBarVisible(visible:Bool, animated:Bool) {
        
        // bail if the current state matches the desired state
        if (tabBarIsVisible() == visible) { return }
        
        // get a frame calculation ready
        let frame = self.tabBar.frame
        let height = frame.size.height
        let offsetY = (visible ? -height : height)
        
        // animate the tabBar
        UIView.animateWithDuration(animated ? 0.3 : 0.0) {
            self.tabBar.frame = CGRectOffset(frame, 0, offsetY)
            self.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height + offsetY)
            self.view.setNeedsDisplay()
            self.view.layoutIfNeeded()
        }
    }
    
    func tabBarIsVisible() ->Bool {
        return self.tabBar.frame.origin.y < CGRectGetMaxY(self.view.frame)
    }
}



// global functions

func getDayOfWeek()->Int? {
    let todayDate = NSDate()
    let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
    let myComponents = myCalendar?.components(.Weekday, fromDate: todayDate)
    let weekDay = myComponents?.weekday
    if weekDay! - 1 > 0 {
        return weekDay! - 1
    }else{
        return 7
    }
}


protocol CheckboxDelegate {
    // #1
    func didSelectCheckbox(state: Bool, identifier: Int, title: String);
}

class Checkbox : UIButton {
    var mDelegate: CheckboxDelegate?;
    // #1
    required init?(coder: NSCoder) {

        super.init(coder: coder)
    
        self.adjustEdgeInsets();
        self.applyStyle();
   
        self.addTarget(self, action: #selector(Checkbox.onTouchUpInside(_:)), forControlEvents: UIControlEvents.TouchUpInside);
    }
    override var highlighted: Bool {
        didSet { if highlighted { highlighted = false } }
    }
    // #2
    init(frame: CGRect, title: String, selected: Bool) {
        super.init(frame: frame);
        self.adjustEdgeInsets();
        self.applyStyle();
        
        self.setTitle(title, forState: UIControlState.Normal);
      //  self.addTarget(self, action: "onTouchUpInside:", forControlEvents: UIControlEvents.TouchUpInside);
    }
    
    func adjustEdgeInsets() {
        self.adjustsImageWhenHighlighted = false
        let lLeftInset: CGFloat = 8.0;
        // #3
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left;
        // #4
        self.imageEdgeInsets = UIEdgeInsetsMake(0.0 as CGFloat, lLeftInset, 0.0 as CGFloat, 0.0 as CGFloat);
        // #5
        self.titleEdgeInsets = UIEdgeInsetsMake(0.0 as CGFloat, (lLeftInset * 2), 0.0 as CGFloat, 0.0 as CGFloat);
    }
    
    // #6
    func applyStyle() {
//        self.setImage(UIImage(named: "checkbox-marcado"), forState: UIControlState.Selected)
//        self.setImage(UIImage(named: "checkbox-marcado"), forState: UIControlState.Highlighted)
//        self.setImage(UIImage(named: "checkbox-vacio"), forState: UIControlState.Normal)
        self.imageView?.contentMode = .ScaleAspectFit
        self.setTitleColor(UIColor.blackColor(), forState: .Normal)
        
    }
    
    func onTouchUpInside(sender: UIButton) {
        
        //self.selected = !self.selected;
        mDelegate?.didSelectCheckbox(self.selected, identifier: self.tag, title: self.currentTitle!);
    }
}

extension UIView {
    func addDashedBorder() {
        let color = UIColor.lightGrayColor().CGColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clearColor().CGColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).CGPath
        
        self.layer.addSublayer(shapeLayer)
        
    }
}

extension SwiftyJSON.JSON: DataConvertible, DataRepresentable {
    
    public typealias Result = SwiftyJSON.JSON
    
    public static func convertFromData(data:NSData) -> Result? {
        return  SwiftyJSON.JSON(data: data)
    }
    
    public func asData() -> NSData! {
        do {
            return try self.rawData()
        }catch
        {
            return nil
        }
    }
    
}

import UIKit

public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 where value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}



func connectedToNetwork() -> Bool {
    
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(&zeroAddress, {
        SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
    }) else {
        return false
    }
    
    var flags : SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
        return false
    }
    
    let isReachable = flags.contains(.Reachable)
    let needsConnection = flags.contains(.ConnectionRequired)
    return (isReachable && !needsConnection)
}


extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.Top:
            border.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), thickness)
            break
        case UIRectEdge.Bottom:
            border.frame = CGRectMake(0, CGRectGetHeight(self.frame) - thickness, CGRectGetWidth(self.frame), thickness)
            break
        case UIRectEdge.Left:
            border.frame = CGRectMake(0, 0, thickness, CGRectGetHeight(self.frame))
            break
        case UIRectEdge.Right:
            border.frame = CGRectMake(CGRectGetWidth(self.frame) - thickness, 0, thickness, CGRectGetHeight(self.frame))
            break
        default:
            break
        }
        
        border.backgroundColor = color.CGColor;
        
        self.addSublayer(border)
    }
}

extension NSDate {
    func isBetween(date date1: NSDate, andDate date2: NSDate) -> Bool {
        return date1.compare(self).rawValue * self.compare(date2).rawValue >= 0
    }
}

extension UITextField {
    
    var borderColor : UIColor? {
        get {
            if let cgcolor = layer.borderColor {
                return UIColor(CGColor: cgcolor)
            } else {
                return nil
            }
        }
        set {
            layer.borderColor = newValue?.CGColor
            
            // width must be at least 1.0
            if layer.borderWidth < 1.0 {
                layer.borderWidth = 1.0
            }
        }
    }
}

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
}



func cacheImagesForUrls(urls: [NSURL]) {
    for url in urls {
        let tmpImgView = UIImageView(frame: CGRectMake(0, 0, 1024, 1024))
        tmpImgView.hnk_setImageFromURL(url)
    }
}

extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.addAnimation(animation, forKey: "shake")
    }
    
    func pop(){
        let popAnimation = POPBasicAnimation(propertyNamed: kPOPViewScaleXY)
        popAnimation.fromValue = NSValue(CGPoint: CGPoint(x: 0.9, y: 0.9))
        popAnimation.toValue = NSValue(CGPoint: CGPoint(x: 1.3, y: 1.3))
        popAnimation.autoreverses = true
        popAnimation.duration = 0.2
        self.pop_addAnimation(popAnimation, forKey: "scalingUp")
    }
    
}

extension UITextField {
    func addMissingInputAnimation(){
        
        let colorAnimation = CABasicAnimation(keyPath: "borderColor")
        colorAnimation.fromValue = UIColor.lightGrayColor().CGColor
        colorAnimation.toValue = UIColor.redColor().CGColor
        self.layer.borderColor = UIColor.redColor().CGColor
        
        let widthAnimation = CABasicAnimation(keyPath: "borderWidth")
        widthAnimation.fromValue = 1
        widthAnimation.toValue = 1
        widthAnimation.duration = 1
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        
        let bothAnimations = CAAnimationGroup()
        bothAnimations.duration = 0.25
        bothAnimations.animations = [colorAnimation, widthAnimation]
        bothAnimations.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        self.layer.addAnimation(bothAnimations, forKey: "color and width")
        self.shake()
        self.becomeFirstResponder()
    }
}

extension String {
    func replace(string:String, replacement:String) -> String {
        return self.stringByReplacingOccurrencesOfString(string, withString: replacement, options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(" ", replacement: "")
    }
}

