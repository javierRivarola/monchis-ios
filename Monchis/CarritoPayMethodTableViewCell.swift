//
//  CarritoPayMethodTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 10/12/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

protocol CarritoPayMethodDelegate {
    func payCashPressed(cell: CarritoPayMethodTableViewCell)
    func payCreditPressed(cell: CarritoPayMethodTableViewCell)
    func wantsTicketPressed(cell: CarritoPayMethodTableViewCell)
}

class CarritoPayMethodTableViewCell: UITableViewCell {

    @IBOutlet weak var wantsTicketBtn: UIButton!
    @IBOutlet weak var payCreditButton: UIButton!
    @IBOutlet weak var payCashButton: UIButton!
    var order: MOrder!
    var delegate: CarritoPayMethodDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func payCashPressed(sender: UIButton) {
        delegate?.payCashPressed(self)
    }
    @IBAction func payCreditPressed(sender: UIButton) {
        delegate?.payCreditPressed(self)

    }
    @IBAction func wantsTicketPressed(sender: UIButton) {
        delegate?.wantsTicketPressed(self)

    }
}
