//
//  VoucherProductTableViewController.swift
//  Monchis
//
//  Created by Javier Rivarola on 12/3/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import Haneke
import MBProgressHUD
import SCLAlertView
import Mixpanel

class VoucherProductTableViewController: UITableViewController, BranchSelectionDelegate {
    var product : MProduct!
    var branch: MRestaurant!
    
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productDescLbl: UILabel!
    
    @IBOutlet weak var restoImgView: UIImageView!
    @IBOutlet weak var restoBtn: UIButton!
    @IBOutlet weak var productImgView: UIImageView!
    
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var validityLbl: UILabel!
    @IBOutlet weak var deliveryBtn: UIButton!
    @IBOutlet weak var pickupBtn: UIButton!
    var parent: ScannerViewController!
    @IBAction func backPressed(sender: UIButton) {

        self.navController?.tabBarController?.setTabBarVisible(true, animated: true)
        self.dismissViewControllerAnimated(true, completion: { fin in
            self.parent?.lastVoucherScanned = ""
        })
    }
    @IBOutlet weak var backBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 200
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        self.setupProduct()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func setupProduct(){
        if let product = self.product {
            //configure cell
            productDescLbl.text = product.product_description
            productNameLbl.text = product.name
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.locale = MUser.sharedInstance.locale
            dateFormatter.dateFormat = "yyyy-MM-dd"
            print(product.voucher_validTo)

            if let date = dateFormatter.dateFromString(product.voucher_validTo ?? "") {
                let mut = NSMutableAttributedString(string: "Oferta válida hasta el ",attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont.systemFontOfSize(13)])
                dateFormatter.dateFormat = "d MMMM y"
                let string = dateFormatter.stringFromDate(date)
                mut.appendAttributedString(NSAttributedString(string: string, attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName : UIFont.boldSystemFontOfSize(13)] ))
                validityLbl.attributedText = mut
            }
            
            
            
            
            
            //            cell.pickupBtn.addTarget(self, action: #selector(ValesViewController.pickupPressed), forControlEvents: .TouchUpInside)
            //            cell.deliveryBtn.addTarget(self, action: #selector(ValesViewController.deliveryPressed), forControlEvents: .TouchUpInside)
            if branch.franchise!.branches_with_pickup != true {
                pickupBtn.backgroundColor = UIColor.lightGrayColor()
                pickupBtn.enabled = false
            }else{
                pickupBtn.backgroundColor = UIColor.redColor()
                pickupBtn.enabled = true
            }
            if branch.franchise?.branches_with_delivery == true {
                deliveryBtn.backgroundColor = UIColor.redColor()
                deliveryBtn.enabled = true
                
            }else{
                deliveryBtn.backgroundColor = UIColor.lightGrayColor()
                deliveryBtn.enabled = false
            }
            deliveryBtn?.imageView?.contentMode = .ScaleAspectFit
            pickupBtn?.imageView?.contentMode = .ScaleAspectFit
            //            cell.backBtn.addTarget(self, action: #selector(ValesViewController.backPressed), forControlEvents: .TouchUpInside)
            if let restoUrl = branch.attachment?.resizedImageUrl2 {
                restoImgView.hnk_setImageFromURL(restoUrl)
            }
            restoBtn.setTitle(branch.franchise?.name, forState: .Normal)
            product.attachments.first?.width = self.view.bounds.width
            product.attachments.first?.height = self.productImgView.bounds.height
            if let imgUrlString = product.attachments.first?.resizedImageUrl2 {
                let format = Format<UIImage>(name: "product_big")
                print("product image url", imgUrlString)
                
                productImgView?.hnk_setImageFromURL(imgUrlString,placeholder:NO_ITEM_PLACEHOLDER,format: format, success: { (img) in
                    self.productImgViewHeightConstraint.constant = img.size.height*img.scale
                    self.productImgView?.image = img
                    if !self.loadedImage {
//                        self.tableView.beginUpdates()
                        self.tableView.reloadData()
//                        self.tableView.endUpdates()
                        self.loadedImage = true
                    }
                })
                if !imageCacheKeys.contains(imgUrlString.URLString) {
                    imageCacheKeys.append(imgUrlString.URLString)
                }
                cacheFormats[imgUrlString.URLString] = productImgView.hnk_format.name
                
                
            }else{
                productImgView?.image = NO_ITEM_PLACEHOLDER
                
            }
            
            
            if product.price != 0 {
                priceLbl?.text = product.price?.asLocaleCurrency
            }else{
                priceLbl.text = "Gratis!"
            }
        }

    }

    @IBOutlet weak var productImgViewHeightConstraint: NSLayoutConstraint!
    var loadedImage: Bool = false
    
    func deliveryPressed(){
        addToCartPressed(1, branch_id: nil)
    }
    
    @IBAction func deliveryBtnPressed(sender: UIButton) {
        print(#function)
        deliveryPressed()
          Mixpanel.mainInstance().track(event: "Usuario agrego voucher a carrito", properties: ["Producto":product.name ?? "", "Codigo Voucher": product.voucherCode ?? "", "Metodo": "Pickup"])
    }
    @IBAction func pickupBtnPressed(sender: UIButton) {
        print(#function)
       
        pickupPressed()
    }
    var navController: UINavigationController!
    func pickupPressed(){
        self.navController?.tabBarController?.setTabBarVisible(true, animated: true)
        self.dismissViewControllerAnimated(true, completion: {fin in
            let dvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
            dvc.franchise = self.branch.franchise
            dvc.delegate = self
            self.navController?.pushViewController(dvc, animated: true)
            })
    }
    
    func didSelectBranch(branch: MRestaurant,order: MOrder?) {
         Mixpanel.mainInstance().track(event: "Usuario agrego voucher a carrito", properties: ["Producto":product.name ?? "", "Codigo Voucher": product.voucherCode ?? "", "Metodo": "Pickup", "Local": branch.name])
        addToCartPressed(2, branch_id: branch.id!)
    }
    
    
    func addToCartPressed(deliveryType: Int, branch_id: Int?){
        if let vCode = self.product?.voucherCode {
            MBProgressHUD.showHUDAddedTo(UIApplication.sharedApplication().keyWindow!, animated: true)
            MProduct.redeemVoucher(vCode,deliveryType: deliveryType, branch_id: branch_id, completion: { (success, error) in
                MBProgressHUD.hideHUDForView(UIApplication.sharedApplication().keyWindow!, animated: true)
                if success {

                    self.navController?.tabBarController?.setTabBarVisible(true, animated: true)

                    self.dismissViewControllerAnimated(true, completion: {fin in
                        self.parent?.lastVoucherScanned = ""


                        })
                    self.navController?.tabBarController?.selectedIndex = 3
                }else{
                    SCLAlertView().showError("Lo sentimos", subTitle: error ?? "Ocurrio un error inesperado.")
                }
            })
            
        }
        
    }
    


}
