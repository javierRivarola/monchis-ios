//
//  CustomizarProductoViewController.swift
//  Monchis
//
//  Created by jrivarola on 4/11/15.
//  Copyright © 2015 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import Haneke
import GMStepper
import QuartzCore
import SwiftyJSON
import ReachabilitySwift
import DZNEmptyDataSet
import SCLAlertView
import Crashlytics
import Mixpanel

class CustomizarProductoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,AddToCartDelegate, UICollectionViewDelegateFlowLayout,didChangeQuantityComments,UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate,SearchResultSelectedDelegate,ProductFavoriteDelegate, UIPopoverPresentationControllerDelegate, UITextViewDelegate, DZNEmptyDataSetSource,BranchSelectionDelegate, UIDocumentInteractionControllerDelegate {
    
    var selectedProductSearch: MProduct!
    var selectedBranchSearch: MRestaurant!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var restoInfoBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var pickupDeliveryView: UIView!
    // MARK: Properties
    var dimm: UIView!
    var restaurant: MRestaurant!
    var currentItemPrice = "140.000 Gs."
    var selectedCF: MCustomizableField!
    var userSelections: [MCustomizableField] = []
    var customFieldCounter: Int = 0
    var searchTableViewController:SearchTableViewController!
    var searchController: UISearchController!
    var selectedScope: Int = 0
    var cellPriceLabel: UILabel?
    var commentsIndexPath: NSIndexPath!
    @IBOutlet weak var leadingHeaderImgViewConstraint: NSLayoutConstraint!
    var customFieldSingleSelectionIndexes:[Int] = []
    let api = MAPI.SharedInstance
    let cart = MCart.sharedInstance
    var product:MProduct! = MProduct()
    var selectedIP: NSIndexPath!
    var descriptionLblHeight:CGFloat = 20.0
    let maximumItemCountForHorizontalFlow = 4
    var headerMonchisImage:UIImageView!
    var doneLoadingProductInfo: Bool = false
    var editingProduct: Bool = false
    var productImageView: UIImageView!
    var refreshControl: UIRefreshControl!
    var editingProductAddons: [MAddon] = []
    var loadedImage: Bool = false
    @IBOutlet weak var restoHeaderImageView: UIImageView!
    @IBOutlet weak var branchFavoriteBtn: UIButton!
    
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var pickupDeliveryHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblPickupDelivery: UILabel!
    @IBOutlet weak var iconoPickupDelivery: UIImageView!
    @IBOutlet weak var statesSpinner: UIActivityIndicatorView!
    @IBOutlet weak var closedRestoIconImgView: UIImageView!
    @IBOutlet weak var noCoverRestoIconImgView: UIImageView!
    var productFavoriteBtn: UIButton!
    struct Constants {
        static let customizeItemMultipleSelectionReuseID = "customizeItemMultipleSelectionReuseID"
        static let customizeItemTopReuseID = "customizeItemTopReuseID"
        static let customizeOptionsTableCell = "reuseOptions"
        static let customizeItemNavigationReuseID = "customizeItemNavigationReuseID"
        static let singleSelectionItemReuseID = "singleSelectionItemReuseID"
        static let cantidadComentariosReuseCell = "cantidadComentariosReuseCell"
        static let customizeItemSelections = "customizeItemSelections"
    }
    
    struct Segues {
        static let customizeFieldSegue = "customizeFieldSegue"
        static let unwindFromCustomizeFieldsSegue = "unwindFromCustomizeFieldsSegue"
        static let infoResto = "infoRestoSegueFromCustomization"
        static let showBranchSelectionSegue = "showBranchSelectionSegue"
        static let unwindFromBranchSelection = "unwindFromBranchSelection"
        static let gotoPerfilSegue = "gotoPerfilSegue"
        static let gotoRegisterSegue = "gotoRegisterSegue"
    }
    
    var branchMode: Int = 0
    @IBAction func editModePressed(sender: UIButton) {
        if editingProduct {
            SCLAlertView().showNotice("Lo sentimos", subTitle: "Solo puedes cambiar atributos del producto.")
            return
        }
        selectedProductSearch = nil
        selectedBranchSearch = nil
        changeBranchMode(self.restaurant) { (branch) in
            self.branchMode = branch.mode
            if branch.mode == 1 || branch.mode == 2{
                let bsvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                bsvc.delegate = self
                bsvc.franchise = self.restaurant.franchise
                self.navigationController?.pushViewController(bsvc, animated: true)
            }else{
                self.statesSpinner.startAnimating()
                NSNotificationCenter.defaultCenter().postNotificationName("refreshBranchForModeChangeNotification", object: nil)
                self.refreshBranchUI()
                self.refreshBranch()
            }

        }

    }
    
    
    
    func didSelectBranch(branch: MRestaurant, order: MOrder?) {
        if selectedProductSearch != nil && selectedBranchSearch == nil {
            let customizeProductVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
            customizeProductVC.restaurant = branch
            customizeProductVC.product = selectedProductSearch
            self.navigationController?.pushViewController(customizeProductVC, animated: true)
        }else if selectedBranchSearch != nil && selectedProductSearch == nil{
            let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
            branch.mode = 1
            restaurantVC.restaurant = branch
            self.navigationController?.pushViewController(restaurantVC, animated: true)
        }else{
            self.restaurant = branch
            self.refreshBranchUI()
        }
    }
    
    
    let deliveryBackgroundColor = UIColor(hexString: "#DA1214")
    let deliveryFontColor = UIColor.whiteColor()

    let pickupBackgroundColor = UIColor(hexString: "#F3F3F3")
    let pickupFontColor = UIColor.darkGrayColor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBtn?.imageView?.contentMode = .ScaleAspectFit
        self.branchMode = restaurant.mode
        self.tableView.scrollsToTop = true
        if restaurant.mode == 0 {
            self.tableView.backgroundColor = deliveryBackgroundColor
        }else{
            self.tableView.backgroundColor = pickupBackgroundColor
        }

        refreshBranchUIForModeChange()
        tableView.panGestureRecognizer.delaysTouchesBegan = tableView.delaysContentTouches
        tableView.emptyDataSetSource = self
        shadowView.layer.shadowColor = UIColor.blackColor().CGColor
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shadowOffset = CGSizeZero
        shadowView.layer.shadowRadius = 10
        restoHeaderImageView.layer.cornerRadius = restoHeaderImageView.bounds.width/2
        restoHeaderImageView.clipsToBounds = true

        tableView.estimatedRowHeight = 1500
        tableView.tableFooterView = UIView(frame: CGRectZero)
        self.title = product.name
        for customField in product.customizableFields {
            if customField.multiple {
                let index = product.customizableFields.indexOf(customField)!
                customFieldSingleSelectionIndexes.append(index + 1)
            }
        }
        if restaurant?.mode == 1 {
            restoInfoBtn.setTitle(restaurant?.name.uppercaseString, forState: .Normal)
        }else{
            restoInfoBtn.setTitle(restaurant?.franchise?.name?.uppercaseString, forState: .Normal)
        }
        self.restaurant?.franchise?.header_attachment?.width = 45
        self.restaurant?.franchise?.header_attachment?.height = 45
        if let imageURL = self.restaurant?.franchise?.header_attachment?.imageComputedURL {
            restoHeaderImageView.hnk_setImageFromURL(imageURL,placeholder: NO_BRANCH_PLACEHOLDER)
        }
        
        animationsForStates()
        Answers.logContentViewWithName("Usuario vio producto \(self.product.name ?? "").",
                                       contentType: "Productos",
                                       contentId: "\(self.product.id!)",
                                       customAttributes: ["Local":self.restaurant.name ?? ""])
        
        loadProductData()
        //configurePullToRefresh()
        configureSearch()
        
        //favorites
        
//        if let _ = MUser.sharedInstance.branchesFavorites.indexOf({$0.id == restaurant.id}) {            self.branchFavoriteBtn.setImage(favorited, forState: .Normal)
//        }else{
//            self.branchFavoriteBtn.setImage(unFavorited, forState: .Normal)
//            
//        }
        registerNotifications()
        
    }
    
    func registerNotifications(){
        // Keyboard stuff.
        let center: NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.addObserver(self, selector: #selector(CustomizarProductoViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(CustomizarProductoViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CustomizarProductoViewController.reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CustomizarProductoViewController.defaultAddressChanged), name: kUserDidChangeDefaultAddressNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CustomizarProductoViewController.refreshBranchUIForModeChange), name: "refreshBranchForModeChangeNotification", object: nil)
    }
    
    
    func refreshBranchUIForModeChangeNotificationHandler(notification: NSNotification){
        if let branch = notification.object?["branch"] as? MRestaurant {
            self.restaurant = branch
        }
        refreshBranchUIForModeChange()
    }
    func defaultAddressChanged(){
        refreshBranch()
    }
    
    func reachabilityChanged(notification: NSNotification) {
        
        if let reachability = notification.object as? Reachability {
        
        if reachability.isReachable() {
            self.tableView?.reloadEmptyDataSet()
            if let _ = MUser.sharedInstance.branchesFavorites.indexOf({$0.id == restaurant.id}) {
                self.branchFavoriteBtn?.setImage(favorited, forState: .Normal)
            }else{
                self.branchFavoriteBtn?.setImage(unFavorited, forState: .Normal)
            }
            if branchMode == 0 {
                refreshBranch()
            }else{
                self.refreshBranchUI()
            }
            if !loadedImage {
                loadProductData()
            }
            if reachability.isReachableViaWiFi() {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            print("Network not reachable")
            
        }
        }
    }
    
    @IBAction func shareBranchPressed(sender: UIButton) {
        
            var array:[AnyObject] = []
            let text = "Mira este Local en Monchis! - " + self.restaurant.name
            let urlString = "https://admin.monchis.com.py/?q="
            let query = "franchiseid=\(self.restaurant.franchise!.id!)&type=branch"
            let hash = query.toBase64() ?? ""
            
            let link = NSURL(string: urlString + hash)!
            array.append(text)
            array.append(link)
            if let img = self.restoHeaderImageView.image {
                array.insert(img, atIndex: 0)
            }
            let activityViewController = UIActivityViewController(activityItems: array, applicationActivities: nil)
            activityViewController.setValue("Mira este Local en Monchis!", forKey: "subject")
            
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                    activityViewController.completionWithItemsHandler = { activityType, success, items, error in
                        if !success {
                            print("cancelled")
                            return
                        }
            
                        Answers.logShareWithMethod(activityType ?? "",
                                                   contentName: self.restaurant.name,
                                                   contentType: "Restaurante",
                                                   contentId: "\(self.restaurant.id!)",
                                                   customAttributes: nil)
                        Mixpanel.mainInstance().track(event: "Usuario compartio una Franquicia",
                                                      properties: ["Franquicia" : self.restaurant.franchise?.name ?? "","Pantalla":"Customizacion de Producto","Mientras veia producto":self.product.name!, "Producto ID": self.product.id!, "Método": activityType ?? ""])
                    }
            self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        if connectedToNetwork() {
            if restaurant.mode == 0 {
                let image = UIImage(named: "carrito-vacio")
                return image
            }else{
                let image = UIImage(named: "carrito-vacio-rojo")
                return image
            }
        }else{
            if restaurant.mode == 0 {
                
                return UIImage(named: "icono-error-red")
            }else{
                return UIImage(named: "icono-error-red-rojo")
            }
        }

        
    }
    var loadingBeforeEmptyDataSet: Bool = true
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        var color = UIColor.whiteColor()
        if restaurant.mode == 1 {
            color = UIColor.redColor()
        }
        if connectedToNetwork() {
            if loadingBeforeEmptyDataSet {
                let text = NSAttributedString(string: NSLocalizedString("Obteniendo Producto...", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(18),NSForegroundColorAttributeName:color])
                
                return text
            }else{
                let text = NSAttributedString(string: NSLocalizedString("Producto no encontrado.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(18),NSForegroundColorAttributeName:color])
                
                return text
            }
        }else{
            let text = NSAttributedString(string: NSLocalizedString("No tienes conexión a internet.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(18),NSForegroundColorAttributeName:color])
            
            return text
        }
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
        self.tableView.reloadData()
        //favorites
//        if let _ = MUser.sharedInstance.branchesFavorites.indexOf({$0.id == restaurant.id}) {            self.branchFavoriteBtn.setImage(favorited, forState: .Normal)
//        }else{
//            self.branchFavoriteBtn.setImage(unFavorited, forState: .Normal)
//            
//        }
        animationsForStates()
//        refreshBranch()
    }
    
    
    func refreshBranchUIForModeChange(){
        

        if restaurant?.mode == 0 {
            iconoPickupDelivery?.image = UIImage(named: "icono-delivery")
            lblPickupDelivery?.text = "Delivery a: \(MUser.sharedInstance.defaultAddress.name)"
            restoInfoBtn?.setTitle(restaurant?.franchise?.name?.uppercaseString, forState: .Normal)
            noCoverRestoIconImgView?.hidden = false

            UIView.animateWithDuration(1.0, animations: {
                self.pickupDeliveryView?.backgroundColor = UIColor(hexString: "#363A40")
                self.tableView?.backgroundColor = self.deliveryBackgroundColor
            })
        }else{
            noCoverRestoIconImgView?.hidden = true

            iconoPickupDelivery?.image = UIImage(named: "icono-pickup-activo-sobre-activo")
            lblPickupDelivery?.text = "Para buscar de sucursal"
            restoInfoBtn?.setTitle(restaurant?.name.uppercaseString, forState: .Normal)
            UIView.animateWithDuration(1.0, animations: {
                self.pickupDeliveryView?.backgroundColor = UIColor(hexString: "#CF1B1B")
//                self.pickupDeliveryView?.backgroundColor = UIColor.clearColor()
                self.tableView?.backgroundColor = self.pickupBackgroundColor
                
            })
        }
        self.tableView?.reloadData()
        self.tableView?.reloadEmptyDataSet()
    }
    
    func refreshBranchUI(){

        refreshBranchUIForModeChange()

        animationsForStates()
   
        self.tableView?.beginUpdates()
        if self.tableView.numberOfSections  > 0 {
            self.tableView?.reloadSections(NSIndexSet(index: (self.tableView.numberOfSections - 1)), withRowAnimation: .Automatic)
        }
        self.tableView?.endUpdates()
        if let imageURL = self.restaurant?.franchise?.header_attachment?.imageComputedURL {
            restoHeaderImageView.hnk_setImageFromURL(imageURL,placeholder: NO_BRANCH_PLACEHOLDER)
            
        }
        
    }
    
    
    
    func animatonForClosedState() {
        
        closedRestoIconImgView?.hidden = false
        UIView.animateWithDuration(0.5, delay: 0, options: [.Autoreverse,.Repeat], animations: { () -> Void in
            self.closedRestoIconImgView?.alpha = 0.1
            }, completion: { (fin) -> Void in
        })
        
    }
    
    func animationForOutOfCoverState(){
        if self.restaurant?.mode == 0 {
            noCoverRestoIconImgView?.hidden = false
        }else{
            noCoverRestoIconImgView?.hidden = true
        }
        UIView.animateWithDuration(0.5, delay: 0, options: [.Autoreverse,.Repeat], animations: { () -> Void in
            self.noCoverRestoIconImgView?.alpha = 0.1
            }, completion: { (fin) -> Void in
                
        })
        
    }
    
    func animationsForStates(){
        closedRestoIconImgView?.alpha = 1
        noCoverRestoIconImgView?.alpha = 1
        noCoverRestoIconImgView?.hidden = true
        closedRestoIconImgView?.hidden = true
        leadingHeaderImgViewConstraint?.constant = 8
        if restaurant != nil {
            if restaurant.mode == 0 {
                if restaurant.hasDelivery() && (restaurant.isOpen()) { // open an delivery available
                    leadingHeaderImgViewConstraint?.constant = 8
                }
                if restaurant.hasDelivery() &&  (!restaurant.isOpen()) {
                    leadingHeaderImgViewConstraint?.constant = 31
                    animatonForClosedState()
                }
                if !restaurant.hasDelivery() && (restaurant.isOpen()) {
                        leadingHeaderImgViewConstraint?.constant = 31
                        animationForOutOfCoverState()
                }
                if !restaurant.hasDelivery() && (!restaurant.isOpen()) {
                    leadingHeaderImgViewConstraint?.constant = 31
                    animationForOutOfCoverState()
                    animatonForClosedState()
                }
            }else{
                if restaurant.isOpen() {
                    leadingHeaderImgViewConstraint?.constant = 8
                }else{
                    leadingHeaderImgViewConstraint?.constant = 31
                    animatonForClosedState()
                }
            }
        }
        UIView.animateWithDuration(0.2) {
            self.view.setNeedsLayout()
        }
        
    }
    
    func configureSearch(){
        //SearchBar Controller
        searchTableViewController = storyboard?.instantiateViewControllerWithIdentifier("SearchTableViewController") as! SearchTableViewController
        searchController = UISearchController(searchResultsController: searchTableViewController)
        
        searchController.hidesNavigationBarDuringPresentation = false
        //searchController.searchBar.searchBarStyle = UISearchBarStyle.Minimal
        
        searchController.searchBar.sizeToFit()
        let searchBar = searchController.searchBar
        searchBar.tintColor = UIColor(hexString: "#FF8000")
        
        searchBar.translucent = false
        
        searchBar.delegate = self
        for subView in searchBar.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.attributedPlaceholder =  NSAttributedString(string:"Buscar.. (min 3 caracteres)",
                                                                          attributes:[NSFontAttributeName: UIFont(name: "OpenSans-Semibold", size: 13)!])
                }
            }
        }
        searchBar.scopeButtonTitles = ["Productos","Restaurantes"]
        
        
        
        searchTableViewController.definesPresentationContext = true
        searchTableViewController.searchDelegate = self
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = true // important
        //self.navigationItem.titleView = searchBar
        searchController.delegate = self
        
    }
    
    func didDismissSearchController(searchController: UISearchController) {
        searchTableViewController.products = []
        searchTableViewController.restaurants = []
        searchTableViewController.tableView.reloadData()
    }
    
    
    func configurePullToRefresh(){
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.whiteColor()
        let title = NSMutableAttributedString()
        let tira = NSAttributedString(string: "Tira para", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        let refrescar = NSAttributedString(string: " actualizar", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSForegroundColorAttributeName: UIColor(hexString: "#6BBA3B")!])
        title.appendAttributedString(tira)
        title.appendAttributedString(refrescar)
        refreshControl.attributedTitle = title
        refreshControl.addTarget(self, action: #selector(CustomizarProductoViewController.pullToRefresh(_:)), forControlEvents: .ValueChanged)
        self.tableView?.addSubview(refreshControl)
    }
    
    func pullToRefresh(sender: UIRefreshControl){
        loadProductData()
    }
    
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    override func viewWillLayoutSubviews() {
        if let navContr = self.navigationController {
            if navContr.respondsToSelector(Selector("interactivePopGestureRecognizer")) {
                navContr.interactivePopGestureRecognizer?.enabled = false
            }
            
        }
    }
    
    
    
    
    func keyboardWillShow(notification: NSNotification) {
        print(#function)
        let info:NSDictionary = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        
        let _: CGFloat = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber as CGFloat
        
        var contentInsets:UIEdgeInsets
        
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height - 30, 0.0);
        
        print(contentInsets)
        
        self.tableView.contentInset = contentInsets
        self.tableView.scrollIndicatorInsets = self.tableView.contentInset
        if commentsIndexPath != nil {
            tableView.scrollToRowAtIndexPath(commentsIndexPath, atScrollPosition: .Bottom, animated: true)
            
        }
        //            selectedIP = NSIndexPath(forItem: product.customizableFields.count + 1, inSection: 0)
        //
        //            tableView.scrollToRowAtIndexPath(selectedIP, atScrollPosition: .Top, animated: true)
        
        
        
    }
    
    
    
    
    func keyboardWillHide(notification: NSNotification) {
        let info: NSDictionary = notification.userInfo!
        _ = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        
        
        let _: CGFloat = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber as CGFloat
        
        UIView.animateWithDuration(0.25, delay: 0.25, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            
            }, completion: nil)
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
    
    
    
    //    func createWaitScreen(){
    //
    //        let spinner = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    //        spinner.mode = MBProgressHUDMode.Indeterminate
    //        spinner.labelText = NSLocalizedString("Obteniendo Información de producto..",comment: "Buscando datos de producto")
    //        spinner.labelFont = UIFont(name: "OpenSans-Semibold", size: 14.0)
    //
    //    }
    //
    //    func dismissWaitScreen(){
    //        dispatch_async(dispatch_get_main_queue()) {
    //            MBProgressHUD.hideHUDForView(self.view, animated: true)
    //        }
    //    }
    
    
    func createWaitScreen(){
        spinner.startAnimating()
        // glowMonchis()
    }
    
    func dismissWaitScreen(){
        spinner.stopAnimating()
        refreshControl?.endRefreshing()
        //        unGlowMonchis()
        MBProgressHUD.hideHUDForView(self.view, animated: true)
    }
    
    func unGlowMonchis(){
        headerMonchisImage.layer.shadowColor = UIColor.clearColor().CGColor
        headerMonchisImage.layer.shadowRadius = 0
        headerMonchisImage.layer.shadowOpacity = 0
        headerMonchisImage.layer.removeAllAnimations()
    }
    
    
    
    
    func glowMonchis(){
        colorize(self.headerMonchisImage, color:UIColor(hexString: "#950505")!.CGColor)
    }
    
    func loadProductData() {
        loadingBeforeEmptyDataSet = true
        let parameters = ["branch_id":"\(self.restaurant?.id ?? 0)","product_id":"\(self.product?.id ?? 0)"]
        createWaitScreen()
        api.returnCachedData(.SHOW_PRODUCT, parameters: parameters, completion: { (json) in
            if debugModeEnabled {
                print("returnCachedData for list products","called with parameters: nil",json)
            }
            if let jsonArray = json?["data"] {
                self.updateUIForProduct(jsonArray)
            }
            
        })
        
        
        //self.collectionView?.reloadEmptyDataSet()
        
        api.postQueueRequest(.SHOW_PRODUCT, parameters: parameters) { (json, error) in
            if debugModeEnabled {
                print(#function,"called with parameters: nil",json,error)
            }
            if error == nil {
                if let jsonArray = json?["data"] {
                    self.updateUIForProduct(jsonArray)
                }
            }else{
                
            }
            
        }
        
    }
    
    func updateUIForProduct(jsonData: SwiftyJSON.JSON){
        loadingBeforeEmptyDataSet = false
        let newProduct = MProduct(json: jsonData)
        if let attachmentsArray = jsonData["attachments"].array {
            for att in attachmentsArray {
                let newAtt = MAttachment(json: att)
                newProduct.attachments = [newAtt]
            }
        }
        if let productAddonsArray = jsonData["product_addons"].array {
            for addon in productAddonsArray {
                let customField = MCustomizableField()
                
                customField.id = addon["id"].int ?? 0
                customField.name = addon["addon_category"]["label"].string ?? ""
                customField.limit = addon["limit"].int ?? 1
                /* HARD CODED PARA QUE FUNCIONE AHORA*/
                if customField.limit == 0 {
                    customField.limit = 1
                }
                if addon["required"].int == 0 {
                    customField.required = false
                }else{
                    customField.required = true
                }
                
                if let actionType = addon["addon_category"]["action_type"].int {
                    if actionType == 0 {
                        customField.action = .addition
                    }
                    if actionType == 1 {
                        customField.action = .subtraction
                    }
                    if actionType == 2 {
                        customField.action = .maximum
                    }
                    if actionType == 3 {
                        customField.action = .minimum
                    }
                    if actionType == 4 {
                        customField.action = .average
                    }
                }
                /* FIN HARD CODED */
                if let optionsArray = addon["addon_category"]["addons"].array {
                    for option in optionsArray {
                        let addon = MAddon(json: option)
                        customField.addons.append(addon)
                    }
                }
                newProduct.customizableFields.append(customField)
            }
        }
        
        if !self.editingProduct {
            self.product = newProduct
        }else{
            self.product.customizableFields = newProduct.customizableFields
            for customization in self.product.customizableFields {
                for addon in self.product!.addons {
                    for adCust in customization.addons {
                        if adCust.id == addon.id {
                            if let index = customization.addons.indexOf(adCust) {
                                customization.userSelectedIndexes.append(index)
                            }
                        }
                    }
                }
            }
        }
        self.doneLoadingProductInfo = true
        self.product.customizableFields.sortInPlace({$0.limit! <= $1.limit!})
        self.tableView.reloadData()
        self.dismissWaitScreen()
        
    }
    
    
    //    func loadProductData(useCache useCache: Bool = true){
    //        createWaitScreen()
    //        let parameters = ["branch_id":"\(self.restaurant?.id ?? 0)","product_id":"\(self.product?.id ?? 0)"]
    //        api.jsonRequest(.SHOW_PRODUCT, parameters: parameters, returnCachedDataIfPossible: useCache) { (json, error) -> Void in
    //
    //            if debugModeEnabled {
    //                print(#function,"called with parameter: \(parameters)",json,error)
    //            }
    //            if error == nil{
    //
    //                if let jsonData = json?["data"] {
    //                    let newProduct = MProduct(json: jsonData)
    //                    if let attachmentsArray = jsonData["attachments"].array {
    //                        for att in attachmentsArray {
    //                            let newAtt = MAttachment(json: att)
    //                            newProduct.attachments = [newAtt]
    //                        }
    //                    }
    //
    //
    //
    //                    if let productAddonsArray = jsonData["product_addons"].array {
    //
    //
    //
    //
    //                        for addon in productAddonsArray {
    //                            let customField = MCustomizableField()
    //                            customField.automaticJSONParse(addon)
    //                            customField.name = addon["addon_category"]["label"].string ?? ""
    //                            customField.limit = addon["limit"].int ?? 1
    //
    //                            /* HARD CODED PARA QUE FUNCIONE AHORA*/
    //                            if customField.limit == 0 {
    //                                customField.limit = 1
    //                            }
    //                            if addon["required"].int == 0 {
    //                                customField.required = false
    //                            }else{
    //                                customField.required = true
    //                            }
    //
    //                            if let actionType = addon["addon_category"]["action_type"].int {
    //                                if actionType == 0 {
    //                                    customField.action = .addition
    //                                }
    //                                if actionType == 1 {
    //                                    customField.action = .subtraction
    //                                }
    //                                if actionType == 2 {
    //                                    customField.action = .maximum
    //                                }
    //                                if actionType == 3 {
    //                                    customField.action = .minimum
    //                                }
    //                                if actionType == 4 {
    //                                    customField.action = .average
    //                                }
    //                            }
    //                            print("action type",addon["addon_category"]["action_type"].int)
    //
    //
    //                            /* FIN HARD CODED */
    //                            if let optionsArray = addon["addon_category"]["addons"].array {
    //                                for option in optionsArray {
    //                                    let addon = MAddon(json: option)
    //                                    customField.addons.append(addon)
    //                                }
    //                            }
    //                            newProduct.customizableFields.append(customField)
    //                        }
    //                    }
    //
    //                    if !self.editingProduct {
    //                        self.product = newProduct
    //                    }else{
    //                        self.product.customizableFields = newProduct.customizableFields
    //                        for customization in self.product.customizableFields {
    //                            for addon in self.product!.addons {
    //                                for adCust in customization.addons {
    //                                    if adCust.id == addon.id {
    //                                        if let index = customization.addons.indexOf(adCust) {
    //                                            customization.userSelectedIndexes.append(index)
    //                                        }
    //                                    }
    //                                }
    //                            }
    //                        }
    //                    }
    //                    self.doneLoadingProductInfo = true
    //                    self.product.customizableFields.sortInPlace({$0.limit! <= $1.limit!})
    //                    self.tableView.reloadData()
    //                    self.dismissWaitScreen()
    //
    //                }
    //            }else{ // network error
    //                self.dismissWaitScreen()
    //
    //                let titleLocalizable = NSLocalizedString("Error",comment: "error")
    //                let alert = UIAlertController(title: titleLocalizable, message: error, preferredStyle: .Alert)
    //                let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
    //                alert.addAction(ok)
    //                self.presentViewController(alert, animated: true, completion: nil)
    //
    //
    //            }
    //        }
    //    }
    //
    
    
    
    
    // MARK: - Navigation
    
    
    
    
    var selectedBranchForPickup: MRestaurant!
    
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return doneLoadingProductInfo ? (product.customizableFields.count + 3) : 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section > 0 && section < (product.customizableFields.count + 1) {
            let customItem = product.customizableFields[section - 1]
            if !customItem.multiple && customItem.addons.count <= maximumItemCountForHorizontalFlow {
                return 1
            }else{
                return 3
            }
        }else{
            return 1
        }
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier(Constants.customizeItemTopReuseID, forIndexPath: indexPath) as! CustomizeItemTopTableViewCell
            cell.shareBtn.imageView?.contentMode = .ScaleAspectFit
            product.attachments.first?.width = self.view.bounds.width
            if let imgUrlString = product.attachments.first?.resizedImageUrl2 {
                cell.itemImageView.hnk_setImageFromURL(imgUrlString,placeholder:NO_ITEM_PLACEHOLDER, success: { (img) in
                    cell.heightConstraint.constant = img.size.height*img.scale
                    cell.itemImageView?.image = img
                    self.product.downloadedImageForShare = img
                    if !self.loadedImage {
                        self.tableView.beginUpdates()
                        self.tableView.endUpdates()
                        self.loadedImage = true
                    }
                })
                
                
                
            }else{
                cell.itemImageView.image = NO_ITEM_PLACEHOLDER
                
            }
            productImageView = cell.itemImageView
            self.cellPriceLabel = cell.itemCurrentPrice
            let amount = (((product.price ?? 0) + product.userCustomizationsTotalPrice)*Float((product.quantity ?? 0)))
            if amount == 0 {
                cell.itemCurrentPrice.hidden = true
            }else{
                cell.itemCurrentPrice.hidden = false
            }
            cell.itemCurrentPrice.text = calculateProductPriceAsString()
            cell.productNameLbl.text = product.name
            cell.descripcionProducto.text = product.product_description
            cell.descripcionProducto.sizeToFit()
            descriptionLblHeight = cell.descripcionProducto.frame.height
            productFavoriteBtn = cell.favoriteBtn
            cell.delegate = self
            if let _ = MUser.sharedInstance.productFavorites.indexOf({$0.id == product.id}) {
                productFavoriteBtn.setImage(favorited, forState: .Normal)
            }else{
                productFavoriteBtn.setImage(unFavorited, forState: .Normal)
            }
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsetsZero
            cell.layoutMargins = UIEdgeInsetsZero
            cell.layoutIfNeeded()
            return cell
        }
        
        // QUANTITY AND COMMENTS CELL
        
        if indexPath.section == product.customizableFields.count + 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier(Constants.cantidadComentariosReuseCell, forIndexPath: indexPath) as! CantidadComentariosTableViewCell
            commentsIndexPath = indexPath
            cell.cantidadStepper.value = Double(self.product?.quantity ?? 1)
            cell.cantidadStepper.maximumValue = 100
            cell.delegate = self
            
            
            if restaurant.mode == 0 {
                cell.contentView.backgroundColor = deliveryBackgroundColor
                cell.quanityLbl.textColor = deliveryFontColor
                cell.quantityDescLbl.textColor = deliveryFontColor
                cell.commentsLbl.textColor = deliveryFontColor
                cell.commentsDescLbl.textColor = deliveryFontColor
            }else{
                cell.contentView.backgroundColor = pickupBackgroundColor
                cell.quanityLbl.textColor = pickupFontColor
                cell.quantityDescLbl.textColor = pickupFontColor
                cell.commentsLbl.textColor = pickupFontColor
                cell.commentsDescLbl.textColor = pickupFontColor
            }
            cell.product = product
            cell.cometariosTextView.layer.cornerRadius = 5
            cell.cometariosTextView.layer.borderWidth = 1
            cell.cometariosTextView.layer.borderColor = UIColor.lightGrayColor().CGColor
            cell.cometariosTextView.clipsToBounds = true
            cell.cometariosTextView.text = self.product?.comments
            cell.cometariosTextView.delegate = self
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsetsZero
            cell.layoutMargins = UIEdgeInsetsZero
            return cell
        }
        
        
        // ADD TO CART CELL
        if indexPath.section == product.customizableFields.count + 2 {
            let cell = tableView.dequeueReusableCellWithIdentifier(Constants.customizeItemNavigationReuseID, forIndexPath: indexPath) as! CustomizeItemNavigationTableViewCell
            cell.addToCartBtn.setBackgroundImage(UIImage(named: "boton-textura-verde"), forState: .Normal)
            cell.addToCartBtn.enabled = true
            if editingProduct {
                cell.addToCartBtn.setTitle(NSLocalizedString("GUARDAR CAMBIOS", comment: "Guardar cambios al editar un producto"), forState: .Normal)
            }else{
                cell.addToCartBtn.setTitle("AGREGAR AL CARRITO", forState: .Normal)
                cell.addToCartBtn.layer.cornerRadius = 3
                cell.addToCartBtn.clipsToBounds = true
                if self.restaurant.isClosed() {
                    cell.addToCartBtn.setTitle("LOCAL CERRADO", forState: .Normal)
                    cell.addToCartBtn.enabled = false
                    cell.addToCartBtn.setBackgroundImage(nil, forState: .Normal)
                    cell.addToCartBtn.backgroundColor = UIColor(hexString: "#b30000")
                }else{
                    if self.restaurant.mode == 0 && self.restaurant.delivery_available == false {
                    cell.addToCartBtn.setTitle("LOCAL FUERA DE COBERTURA", forState: .Normal)
                    cell.addToCartBtn.enabled = false
                    cell.addToCartBtn.setBackgroundImage(nil, forState: .Normal)
                    cell.addToCartBtn.backgroundColor = UIColor(hexString: "#b30000")
                    }
                    
                }

            }
            
            if restaurant.mode == 0 {
                cell.contentView.backgroundColor = deliveryBackgroundColor
            }else{
                cell.contentView.backgroundColor = pickupBackgroundColor
            }

            
            
            cell.delegate = self
            //            cell.preservesSuperviewLayoutMargins = false
            //            cell.separatorInset = UIEdgeInsetsZero
            //            cell.layoutMargins = UIEdgeInsetsZero
            if statesSpinner.isAnimating() {
                cell.addToCartBtn.setTitle("CARGANDO...", forState: .Normal)
            }
            return cell
        }
        
        //PRODUCT CUSTOMIZATIONS
        if indexPath.section >= 1 && indexPath.section <= product.customizableFields.count  {
            let customItem = product.customizableFields[indexPath.section - 1]
            if !customItem.multiple && customItem.addons.count <= maximumItemCountForHorizontalFlow {
                let cell = tableView.dequeueReusableCellWithIdentifier(Constants.singleSelectionItemReuseID, forIndexPath: indexPath) as! CustomizeItemSingleSelectionTableViewCell
                cell.collectionView.delegate = self
                cell.collectionView.dataSource = self
                cell.collectionView.tag = indexPath.section - 1
                cell.collectionView.reloadData()
                cell.addonTitle.text = customItem.name.uppercaseString
                if restaurant.mode == 0 {
                    cell.contentView.backgroundColor = deliveryBackgroundColor

                    cell.addonTitle.textColor = deliveryFontColor
                }else{
                    cell.addonTitle.textColor = pickupFontColor
                    cell.contentView.backgroundColor = pickupBackgroundColor
                }
                
        
                cell.preservesSuperviewLayoutMargins = false
                cell.separatorInset = UIEdgeInsetsZero
                cell.layoutMargins = UIEdgeInsetsZero
                return cell
                
            }else{
                if indexPath.row == 0 {
                    
                    let cell = tableView.dequeueReusableCellWithIdentifier(Constants.customizeItemMultipleSelectionReuseID, forIndexPath: indexPath) as! CustomizarItemMultipleSelectionTableViewCell
                    
                    let ip = NSIndexPath(forRow: 1, inSection: indexPath.section)
                    if self.expandedIndexPaths.contains(ip) {
                        cell.dropImageView.transform = CGAffineTransformMakeRotation(CGFloat(M_PI/2))
                    
                    }else{
                        cell.dropImageView.transform = CGAffineTransformIdentity
                    }

                    let localizedMax = NSLocalizedString("Max: ", comment: "Maximas opciones a marcar")
                    let localizedMin = NSLocalizedString("Min: ", comment: "Minimas opciones a marcar")
                    
                    
                    var minimumAmmount = 0
                    if customItem.required! {
                        minimumAmmount = 1
                    }
                    
                    let minMax = "  (" + localizedMin + " \(minimumAmmount) | " + localizedMax + " \(customItem.limit!))"
                    
                    var color: UIColor!
                    
                    if restaurant.mode == 0 {
                        color = deliveryFontColor
                        cell.contentView.backgroundColor = deliveryBackgroundColor
                    }else{
                        color = pickupFontColor
                        cell.contentView.backgroundColor = pickupBackgroundColor

                    }
                    
                    let attributedTitle: NSMutableAttributedString = NSMutableAttributedString(string: customItem.name.uppercaseString, attributes:  [NSFontAttributeName:UIFont.boldSystemFontOfSize(15), NSForegroundColorAttributeName: color])
                    
                    let attributedString = NSAttributedString(string: minMax, attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName: color])
                    
                    attributedTitle.appendAttributedString(attributedString)
                    
                    cell.customizarTitulo.attributedText = attributedTitle
                    cell.layoutIfNeeded()
                    cell.preservesSuperviewLayoutMargins = false
                    cell.separatorInset = UIEdgeInsetsZero
                    cell.layoutMargins = UIEdgeInsetsZero
                    return cell
                }else if indexPath.row == 2{
                    let cell = tableView.dequeueReusableCellWithIdentifier(Constants.customizeItemSelections, forIndexPath: indexPath) as! CustomizeItemSelectionsTableViewCell
                    if restaurant.mode == 0 {
                        cell.customizationLbl.textColor = deliveryFontColor
                        cell.contentView.backgroundColor = UIColor(hexString: "#AB1B1D")
                    }else{
                        cell.contentView.backgroundColor = UIColor.darkGrayColor()
                    }
                    cell.customizationLbl.text = customItem.selectedText
                    cell.preservesSuperviewLayoutMargins = false
                    cell.separatorInset = UIEdgeInsetsZero
                    cell.layoutMargins = UIEdgeInsetsZero
                    return cell
                }
                if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCellWithIdentifier(Constants.customizeOptionsTableCell, forIndexPath: indexPath) as! MultipleOptionsTableViewCell
                    
              
                    cell.customField = customItem
                    cell.tableView.reloadData()
                    cell.tableView.layoutIfNeeded()
                    cell.parentController = self
                    cell.itemIndexPath = indexPath
                    cell.tableView.tag = indexPath.section
                    cell.layoutIfNeeded()
                    cell.preservesSuperviewLayoutMargins = false
                    cell.separatorInset = UIEdgeInsetsZero
                    cell.layoutMargins = UIEdgeInsetsZero
                    return cell
                }
            }
        }
        return UITableViewCell()
        
    }
    
    /*
     func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
     
     
     
     if !tableView.cellForRowAtIndexPath(indexPath)!.isKindOfClass(CustomizeItemSingleSelectionTableViewCell) {
     if indexPath.row >= 1 && indexPath.row <= product.customizableFields.count {
     selectedCF = product.customizableFields[indexPath.row - 1]
     let ip = NSIndexPath(forRow: 2, inSection: 0)
     tableView.insertRowsAtIndexPaths([ip], withRowAnimation: .Fade)
     
     }else if indexPath.row == 1{
     let ip = NSIndexPath(forRow: 2, inSection: 0)
     tableView.deleteRowsAtIndexPaths([ip], withRowAnimation: .Fade)
     tableView.deselectRowAtIndexPath(indexPath, animated: true)
     }
     
     
     }
     
     
     
     }
     */
    
    //    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    //        if indexPath.row == 0 {
    //            return 200
    //        }
    //        if indexPath.row == item.customizableFields.count + 1 {
    //            return 80
    //        }
    //        return 50
    //    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return NO if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return NO if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == Segues.customizeFieldSegue {
            let dvc = segue.destinationViewController as! SelectCustomizeTableViewController
            dvc.field = selectedCF
        }
        if segue.identifier == Segues.infoResto {
            let dvc = segue.destinationViewController as! InfoRestoViewController
            dvc.restaurant = self.restaurant
        }
        if segue.identifier == Segues.showBranchSelectionSegue {
            let dvc = segue.destinationViewController as! BranchSelectionViewController
            dvc.franchise = self.restaurant.franchise
            dvc.cameFromCustomizeProduct = true
        }
        
        
    }
    
    // MARK: animation
    
    func animationForSuccessfulAddedOrder() {
        if productImageView != nil {
            let imgViewForAnimation = UIImageView()
            imgViewForAnimation.image = productImageView.image
            imgViewForAnimation.frame = CGRectMake(0, 0, 150, 150)
            imgViewForAnimation.contentMode = .ScaleAspectFill
            imgViewForAnimation.layer.cornerRadius = 75
            imgViewForAnimation.clipsToBounds = true
            imgViewForAnimation.center = self.productImageView.center
            
            var viewOrigin = imgViewForAnimation.frame.origin
            viewOrigin.y = viewOrigin.y + imgViewForAnimation.frame.size.height / 2.0
            viewOrigin.x = viewOrigin.x + imgViewForAnimation.frame.size.width / 2.0
            
            
            imgViewForAnimation.layer.position = viewOrigin
            
            // Set up fade out effect
            let fadeOutAnimation = CABasicAnimation(keyPath: "opacity")
            //        fadeOutAnimation setToValue:[NSNumber numberWithFloat:0.3]];
            fadeOutAnimation.fromValue = NSNumber(float: 1.0)
            fadeOutAnimation.toValue = NSNumber(float: 0.2)
            fadeOutAnimation.fillMode = kCAFillModeForwards
            fadeOutAnimation.removedOnCompletion = false
            self.view.addSubview(imgViewForAnimation)
            
            // Set up scaling
            let resizeAnimation = CABasicAnimation(keyPath: "bounds.size")
            
            resizeAnimation.fromValue = NSValue(CGSize: imgViewForAnimation.bounds.size)
            resizeAnimation.toValue = NSValue(CGSize: CGSizeMake(10,10))
            
            let cornerAnimation = CABasicAnimation(keyPath: "cornerRadius")
            cornerAnimation.fromValue = 75
            cornerAnimation.toValue = 5
            
            
            resizeAnimation.fillMode = kCAFillModeForwards
            resizeAnimation.removedOnCompletion = false
            
            // Set up path movement
            let pathAnimation = CAKeyframeAnimation(keyPath: "position")
            pathAnimation.calculationMode = kCAAnimationPaced
            pathAnimation.fillMode = kCAFillModeForwards
            pathAnimation.removedOnCompletion = false
            //Setting Endpoint of the animation
            let endPoint = CGPointMake(self.view.bounds.width - 100, self.tabBarController!.tabBar.frame.origin.y + self.tabBarController!.tabBar.frame.size.height/2.0)
            
            //to end animation in last tab use
            
            let curvedPath = CGPathCreateMutable()
            CGPathMoveToPoint(curvedPath, nil, viewOrigin.x, viewOrigin.y)
            CGPathAddCurveToPoint(curvedPath, nil, endPoint.x, viewOrigin.y, endPoint.x, viewOrigin.y, endPoint.x, endPoint.y)
            pathAnimation.path = curvedPath
            
            let group = CAAnimationGroup()
            group.animations = [fadeOutAnimation,pathAnimation,resizeAnimation,cornerAnimation]
            group.fillMode = kCAFillModeForwards
            group.removedOnCompletion = false
            
            group.duration = 1.2
            group.delegate = self
            imgViewForAnimation.layer.addAnimation(group, forKey: nil)
            
            
        }
        
    }
    
    
    // MARK: UnwindSegue
    
    @IBAction func unwindFromCustomizeFields(segue: UIStoryboardSegue) {
        self.tableView.beginUpdates()
        self.tableView.reloadData()
        self.tableView.endUpdates()
    }
    
    // MARK: AddToCartDelegate
    
    func addToCart() {
//        if MUser.sharedInstance.logged {
//        if !self.editingProduct {
////                    if MUser.sharedInstance.phone != "" && MUser.sharedInstance.birth_date != "" {
//            
//                            
//                            addToBackEndCart()
//                            
//
//                        
//                    }else{ // user needs missing data for making order
//                        
//                        let alert = UIAlertController(title: NSLocalizedString("Atención!", comment: "Atención!"), message: NSLocalizedString("Necesitamos que completes tu fecha de nacimiento y numero de teléfono para poder hacer pedidos.", comment: "Necesitamos que completes tu fecha de nacimiento y numero de teléfono para poder hacer pedidos."), preferredStyle: .Alert)
//                        let cancelar = UIAlertAction(title: NSLocalizedString("Cancelar", comment: "Cancelar"), style: .Cancel, handler: nil)
//                        
//                        let completarDatos = UIAlertAction(title: NSLocalizedString("Completar Datos", comment: "Cancelar"), style: .Default, handler: { (action) in
//                            self.performSegueWithIdentifier(Segues.gotoPerfilSegue, sender: self)
//                            
//                        })
//                        alert.addAction(cancelar)
//                        alert.addAction(completarDatos)
//                        self.presentViewController(alert, animated: true, completion: nil)
//                        
//                        
//                    }
//                    
//            
//        }else{
//         addToBackEndCart()
//        }
//        }
        
        addToBackEndCart()
    }
    
    func addToBackEndCart(){
        var missingAddons: String = ""
        var canAdd: Bool = true
        self.view.endEditing(true)
        for cF in self.product.customizableFields {
            if cF.required == true {
                if cF.userSelectedIndexes.count < 1 {
                    canAdd = false
                    missingAddons += cF.name + ","
                }
            }
        }
        if missingAddons != "" {
            missingAddons.removeAtIndex(missingAddons.endIndex.predecessor())
        }
        //check for requiremnts
        if canAdd {

            var selectionsIDs:[Int] = []
            for customField in self.product.customizableFields {
                for index in customField.userSelectedIndexes {
                    selectionsIDs.append(customField.addons[index].id!)
                }
            }
            
            let newOrder = MOrder()
            newOrder.products = [self.product]
            newOrder.branch_id = self.restaurant?.id
            newOrder.branch = self.restaurant
            let quantityCell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow:0, inSection: product.customizableFields.count + 1)) as! CantidadComentariosTableViewCell
            let quantity = quantityCell.cantidadStepper.value
            
            var parameters:[String:AnyObject]!
            var code: MAPI.Command!
            let comments = self.product?.comments ?? ""
            
            if editingProduct {
                let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                hud.label.text = "Editando Producto..."
                code = .EDIT_PRODUCT_CART
                parameters = ["product_id":self.product.id!,"addons": selectionsIDs,"quantity":quantity,"comments":comments,"order_product_id":Int(self.product?.order_product_id ?? 0),"order_id":Int(self.product?.order_id ?? 0)]
                
                api.jsonRequest(code, parameters: parameters) { (json, error) -> Void in
                    if debugModeEnabled {
                        print(#function,"called with parameters: \(parameters)",json,error)
                    }
                    hud.hideAnimated(true)
                    if error == nil {
                        if let json = json {
                            let success = json["success"].bool!
                            
                            if (success){ // OK
                                Answers.logCustomEventWithName("Usuario edito producto",
                                                               customAttributes: ["User ID":MUser.sharedInstance.id ?? 0,"order_product_id": Int(self.product?.order_product_id ?? 0)])
                                Mixpanel.mainInstance().track(event: "Usuario Edito un Producto del Carrito",
                                                              properties: ["Producto" : self.product.name!,"Pantalla":"Customizacion de Producto", "Franquicia": self.restaurant.name ?? "", "Producto ID": self.product.id!])
                                self.navigationController?.popViewControllerAnimated(true)
                            }else{ // missing data for adding to cart
                                self.dismissWaitScreen()
                                let titleLocalizable = NSLocalizedString("Lo sentimos!",comment: "error")
                                let alert = UIAlertController(title: titleLocalizable, message: json["data"].string, preferredStyle: .Alert)
                                let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                                alert.addAction(ok)
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                        }
                    }else{ //network error
                        self.dismissWaitScreen()
                        
                        let titleLocalizable = NSLocalizedString("Error",comment: "error")
                        let alert = UIAlertController(title: titleLocalizable, message: error, preferredStyle: .Alert)
                        let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                        alert.addAction(ok)
                        self.presentViewController(alert, animated: true, completion: nil)
                        
                    }
                }
                
                
            }else{
                
                code = .ADD_TO_CART
                if self.restaurant.mode == 1 {
                    parameters = ["product_id":self.product.id!,"addons": selectionsIDs,"quantity":quantity,"comments":comments,"branch_id":self.restaurant?.id ?? 0,"delivery_type":2]
                }else{
                    parameters = ["product_id":self.product.id!,"addons": selectionsIDs,"quantity":quantity,"comments":comments,"branch_id":self.restaurant?.id ?? 0,"delivery_type":1]
                }
                if connectedToNetwork(){
                    self.animationForSuccessfulAddedOrder()
                }
                self.product.branch_products  = [self.restaurant]
                Answers.logAddToCartWithPrice(NSDecimalNumber(float: (((product.price ?? 0) + product.userCustomizationsTotalPrice)*Float((product.quantity ?? 0)))),
                                              currency: "PYG",
                                              itemName: self.product.name!,
                                              itemType:"Producto",
                                              itemId: "\(self.product.id!)",
                                              customAttributes: ["Cantidad":quantity,"TipoDelivery": parameters["delivery_type"] as! Int])
                
                
          
                
                
                cart.addProductToCart(parameters, product: self.product) { success, parameters, product in
                    
                    
                    
                    
                    
                    
                    
                    print("PRODUCT ADDED \(self.product.name)",success)
                    if success {
                        self.reflectBadge()
                        Mixpanel.mainInstance().track(event: "Usuario Agrego un Producto al Carrito",
                                                      properties: ["Producto" : self.product.name!,"Pantalla":"Customizacion de Producto", "Franquicia": self.restaurant.name ?? "", "Producto ID": self.product.id!, "Cantidad": self.product.quantity!])
                        globalCartTableView?.reloadData()
                    }else{
                        
                    }
//                            let sections = globalCartTableView?.numberOfSections ?? 0
                            //                        globalCartTableView?.reloadSections(NSIndexSet(index: 0), withRowAnimation: .Automatic)
                            
                            
//                            globalCartTableView?.beginUpdates()
//                            if sections - 1 == self.cart.orders.count{
//                                var ip: NSIndexSet!
//                                var index = 0
//                                for p in self.cart.orders {
//                                    for prod in p.products {
//                                        if prod.id == product.id {
//                                            ip = NSIndexSet(index: index)
//                                        }
//                                    }
//                                    index += 1
//                                    
//                                }
//                                if sections == 2{
//                                    globalCartTableView?.deleteRowsAtIndexPaths([NSIndexPath(forRow: 2, inSection: 0)], withRowAnimation: .Fade)
//                                }
//                                globalCartTableView?.reloadSections(ip, withRowAnimation: .Automatic)
//                            }else{
//                                
//                                if sections == 0 {
//                                    globalCartTableView?.insertSections(NSIndexSet(index: 0), withRowAnimation: .Fade)
//                                    globalCartTableView?.insertSections(NSIndexSet(index: 1), withRowAnimation: .Fade)
//                                    
//                                }else{
//                                    if sections == 2 {
//                                        globalCartTableView?.deleteRowsAtIndexPaths([NSIndexPath(forRow: 2, inSection: 0)], withRowAnimation: .Fade)
//                                    }
//                                    globalCartTableView?.insertSections(NSIndexSet(index: sections ), withRowAnimation: .Fade)
//                                }
//                            }
//                            
//                            
//                            globalCartTableView?.endUpdates()
                            
                            
                            
                
                }
                
            }

        
        
        
        }else{
            SCLAlertView().showNotice("Mmmm...", subTitle: "Al parecer te faltan estas opciones: \(missingAddons.uppercaseString)") // Notice
        }
        
        
           }
    
    func reflectBadge(){
        if let carritoItem = tabBarController?.tabBar.items?[3] {
            carritoItem.badgeValue = "\(self.cart.orders.count)"
        }
    }
    
    
    // MARK: UICollectionViewDataSource
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CustomizeItemSingleSelectionID", forIndexPath: indexPath) as! CustomizeItemSingleSelectionCollectionViewCell
        let addon = product.customizableFields[collectionView.tag].addons[indexPath.item]
        cell.optionLbl.text = addon.etiqueta
        cell.layer.cornerRadius = 5
        cell.clipsToBounds = true
        cell.layer.masksToBounds = true
        if addon.price == 0 {
            cell.priceLbl.text = "Sin costo."
        }else{
            cell.priceLbl.text = addon.price?.asLocaleCurrency ?? ""
        }
        
        if indexPath.row ==  product.customizableFields[collectionView.tag].userSelectedIndexes.first {
            //cell.contentView.backgroundColor = UIColor(hexString: "#7ED321")
            cell.imagenFondo.hidden = false
            cell.imagenFondo.alpha = 1
            cell.backgroundColor = UIColor(hexString: "#7ED321")
            
            cell.transform = CGAffineTransformMakeScale(1.1, 1.1)
            
        }else{
            //cell.contentView.backgroundColor = UIColor(hexString: "#EFFBE4")
            cell.imagenFondo.hidden = true
            cell.imagenFondo.alpha = 0
            cell.backgroundColor = UIColor(hexString: "#EFFBE4")
            
            cell.transform = CGAffineTransformIdentity
            
            
        }
        
        return cell
    }
    
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return product.customizableFields[collectionView.tag].addons.count
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! CustomizeItemSingleSelectionCollectionViewCell
        
        if let index = product.customizableFields[collectionView.tag].userSelectedIndexes.indexOf(indexPath.row) {// if selected deselect
            product.customizableFields[collectionView.tag].userSelectedIndexes.removeAtIndex(index)
            cell.backgroundColor = UIColor(hexString: "#EFFBE4")
            
            UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: [.CurveEaseInOut,.AllowUserInteraction], animations: { () -> Void in
                cell.transform = CGAffineTransformIdentity
                
                }, completion: { (fin) -> Void in
                    
                    collectionView.reloadData()
            })
            
            
        }else{
            
            if let lastCellIndex = product.customizableFields[collectionView.tag].userSelectedIndexes.first {
                if let cellToDeselect = collectionView.cellForItemAtIndexPath(NSIndexPath(forItem:lastCellIndex , inSection: 0)) as? CustomizeItemSingleSelectionCollectionViewCell {
                    cell.backgroundColor = UIColor(hexString: "#7ED321")
                    
                    UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: [.CurveEaseInOut,.AllowUserInteraction], animations: { () -> Void in
                        cellToDeselect.transform = CGAffineTransformIdentity
                        
                        }, completion: { (fin) -> Void in
                            
                    })
                }
            }
            product.customizableFields[collectionView.tag].userSelectedIndexes = [indexPath.row]
            cell.imagenFondo.alpha = 0
            cell.imagenFondo.hidden = false
            cell.backgroundColor = UIColor(hexString: "#7ED321")
            
            UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: [.CurveEaseInOut,.AllowUserInteraction], animations: { () -> Void in
                cell.transform = CGAffineTransformMakeScale(1.1, 1.1)
                
                
                }, completion: { (fin) -> Void in
                    collectionView.reloadData()
            })
            
        }
        cellPriceLabel?.text = calculateProductPriceAsString()
    }
    
    func calculateProductPriceAsString()->String {
        let amount = (((product.price ?? 0) + product.userCustomizationsTotalPrice)*Float((product.quantity ?? 0)))
        if amount > 0 {
            cellPriceLabel?.hidden = false
        }else{
            cellPriceLabel?.hidden = true
            
        }
        let str = Float(amount).asLocaleCurrency
        return str
    }
    
    func updatePriceLabel(){
        cellPriceLabel?.text = calculateProductPriceAsString()
        let amount = (((product.price ?? 0) + product.userCustomizationsTotalPrice)*Float((product.quantity ?? 0)))
        if amount > 0 {
            cellPriceLabel?.hidden = false
        }else{
            cellPriceLabel?.hidden = true
            
        }
        //self.tableView.reloadData()
    }
    
    
    // MARK: UICollectionViewFlowLayoutDelegate
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        if (product.customizableFields[collectionView.tag].addons.count == 1) {
            return UIEdgeInsetsMake(0, (collectionView.bounds.width/2 - 135/2), 0,  0);
        } else {
            return UIEdgeInsetsMake(0,30, 0, 30); // top, left, bottom, right
        }
    }
    
    
    
    // MARK: - Properties
    
    /// Array of `NSIndexPath` objects for all of the expanded cells.
    var expandedIndexPaths = [NSIndexPath]()
    
    // MARK: - Actions
    
    /**
     Expand or collapse the cell.
     
     :param: cell Cell that should be expanded or collapsed.
     :param: animated If `true` action should be animated.
     */
//    func toggleCell(cell: CustomizarItemMultipleSelectionTableViewCell, animated: Bool) {
//        UIView.animateWithDuration(0.4) { () -> Void in
//            cell.dropImageView.transform = cell.expanded ? CGAffineTransformIdentity : CGAffineTransformMakeRotation(CGFloat(M_PI/2))
//            
//        }
//        if !cell.expanded {
//            expandCell(cell, animated: animated)
//        } else {
//            collapseCell(cell, animated: animated)
//        }
//    }
    
    
    
    
    // MARK: - UITableViewDelegate
    
    /**
     `AEAccordionTableViewController` will set cell to be expanded or collapsed without animation.
     
     Tells the delegate the table view is about to draw a cell for a particular row.
     
     :param: tableView The table-view object informing the delegate of this impending event.
     :param: cell A table-view cell object that tableView is going to use when drawing the row.
     :param: indexPath An index path locating the row in tableView.
     */
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        expandedIPHeights[indexPath] = cell.frame.size.height

        
    }
    
    /**
     `AEAccordionTableViewController` will animate cell to be expanded or collapsed.
     
     Tells the delegate that the specified row is now deselected.
     
     :param: tableView A table-view object informing the delegate about the row deselection.
     :param: indexPath An index path locating the deselected row in tableView.
     */
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
     
      

        selectedIP = indexPath
        if let cell = tableView.cellForRowAtIndexPath(indexPath) as? CustomizarItemMultipleSelectionTableViewCell {
            let ip = NSIndexPath(forRow: 1, inSection: indexPath.section)
            if self.expandedIndexPaths.contains(ip) {
                self.removeFromExpandedIndexPaths(ip)
            }else{
                self.addToExpandedIndexPaths(ip)
            }
            selectedCF = product.customizableFields[indexPath.section - 1]
            
            CATransaction.begin()
            CATransaction.setCompletionBlock { () -> Void in
                // your code here
            }
            tableView.beginUpdates()
            tableView.reloadRowsAtIndexPaths([ip], withRowAnimation: .Fade)
            tableView.endUpdates()
      
            CATransaction.commit()
            UIView.animateWithDuration(0.3) { () -> Void in
                
                if !self.expandedIndexPaths.contains(ip) {
                    cell.dropImageView.transform =  CGAffineTransformIdentity
                }else{
                    cell.dropImageView.transform = CGAffineTransformMakeRotation(CGFloat(M_PI/2))
                }
            }
//            toggleCell(cell, animated: true)
        }
        self.view.endEditing(true)
    }
    
    
    private func addToExpandedIndexPaths(indexPath: NSIndexPath) {
        expandedIndexPaths.append(indexPath)
    }
    
    private func removeFromExpandedIndexPaths(indexPath: NSIndexPath) {
        if let index = self.expandedIndexPaths.indexOf(indexPath) {
            self.expandedIndexPaths.removeAtIndex(index)
        }
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return expandedIPHeights[indexPath] ?? 1000
    }
    
    var expandedIPHeights: [NSIndexPath: CGFloat] = [:]
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableViewAutomaticDimension
        }
        if indexPath.section == product.customizableFields.count + 1 {
            return 270
        }
        if indexPath.section >= 1 && indexPath.section <= product.customizableFields.count  {
            let customItem = product.customizableFields[indexPath.section - 1 ]
            if customItem.multiple || (!customItem.multiple && customItem.addons.count > maximumItemCountForHorizontalFlow) {
                if indexPath.row == 0 {
                    //                    heightForSelections = 60 + CGFloat(customItem.addons.count*50)
                    //                    heightForSelections = 20 + CGFloat(customItem.addons.count*50)
                    
//                    if let cell = tableView.cellForRowAtIndexPath(indexPath) as? CustomizarItemMultipleSelectionTableViewCell {
//                        //                        heightForSelections += CGFloat(cell.customField.userSelectedIndexes.count*18)
//                        heightForSelections += cell.tableView.contentSize.height
//                    }
//                    if expandedIndexPaths.contains(indexPath) {
//                        return UITableViewAutomaticDimension
//                    }else{
//                        return 60
//                    }
                    UITableViewAutomaticDimension
                }

                if indexPath.row == 1 {
                    var h: CGFloat = 0
                    
                  
                    
                    let height = CGFloat(customItem.addons.count*46)
                    let ip = NSIndexPath(forRow: 1, inSection: indexPath.section)
                    if customItem.userSelectedIndexes.count == 0 {
                        return expandedIndexPaths.contains(ip) ? height : 0
                    }
                    
                    for ad in customItem.addons {
                        h += ad.cellDescriptionHeight ?? 0
                    }
                    if editingProduct {
                        if h == 0 {
                            
                            return expandedIndexPaths.contains(ip) ? (height+CGFloat(customItem.userSelectedIndexes.count)*10) : 0
                        }
                    }
                    return expandedIndexPaths.contains(ip) ? h : 0
                }
                
                if indexPath.row == 2{
                    return customItem.userSelectedIndexes.count > 0 ? UITableViewAutomaticDimension : 0
                }
            }else{
                return 180
            }
        }
        
        if indexPath.section == product.customizableFields.count + 2 {
            return UITableViewAutomaticDimension
        }
        
        return  60.0
        
    }
    
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: QuantityComments
    
    func quantityDidChange(sender: GMStepper) {
        Answers.logCustomEventWithName("Usuario cambió cantidad desde Pantalla de Producto", customAttributes: [:])

        self.product?.quantity = Int(sender.value ?? 1)
        cellPriceLabel?.text = calculateProductPriceAsString()
    }
    
    // MARK: UISearchBarDelegate
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            if searchBar.selectedScopeButtonIndex == 0 {
                searchTableViewController.lastProductSearched = ""
                searchTableViewController.loadingBeforeEmptyDataSet = false
            }else{
                searchTableViewController.loadingBeforeEmptyDataSet = false
                searchTableViewController.lastFranchiseSearched = ""
            }
        }
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        print(#function)
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        searchTableViewController.scopeIndex = selectedScope
        if selectedScope == 0 {
            searchBar.text = searchTableViewController.lastProductSearched
        }else{
            searchBar.text = searchTableViewController.lastFranchiseSearched
        }
    }
    
    // MARK: UISearchResultsUpdating
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if let text = searchController.searchBar.text {
            if text.characters.count >= 3 {
                self.searchTableViewController.treshold = true
                
                //                let delay = 0.5 * Double(NSEC_PER_SEC)
                //                let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                //                dispatch_after(time, dispatch_get_main_queue()) {
                if searchController.searchBar.selectedScopeButtonIndex == 0 {
                    self.searchTableViewController.searchProductWithText(text)
                }else{
                    self.searchTableViewController.searchFranchisesWithText(text)
                }
                //                }
            }else{
                self.searchTableViewController.loadingBeforeEmptyDataSet = false
                self.searchTableViewController.treshold = false
                self.searchTableViewController.products = []
                self.searchTableViewController.restaurants = []
                self.searchTableViewController.tableView.reloadData()
            }
        }
        print(#function)
    }
    
    // MARK: UISearchControllerDelegate
    
    func willPresentSearchController(searchController: UISearchController) {
        print(#function)
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
    }
    
    
    
    func willDismissSearchController(searchController: UISearchController) {
        self.tabBarController!.setTabBarVisible(true, animated: true)
    }
    
    
    @IBAction func searchPressed(sender: UIButton) {
        if searchController.active {
            self.tabBarController!.setTabBarVisible(true, animated: true)
            self.searchController.dismissViewControllerAnimated(true, completion: nil)
        }else{
            self.tabBarController!.setTabBarVisible(false, animated: true)
            self.presentViewController(self.searchController, animated: true){
            }
        }
    }
    
    
    // MARK: SearchResultSelectedDelegate
    func didSelectSearchResult(product: MProduct?, branch: MRestaurant?) {
        selectedProductSearch = nil
        selectedBranchSearch = nil
        if product != nil {
            
            deliveryOrPickupHistorySearch(product!.branch_products.first!, completion: { (isDelivery, newBranch) in
                let productoVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
                if isDelivery == nil || isDelivery == true {
                    productoVC.product = product
                    let branch = newBranch
                    productoVC.restaurant = branch
                    branch.mode = 0
                    productoVC.restaurant?.franchise = product?.franchise
                    self.searchController.active = false
                    self.navigationController?.pushViewController(productoVC, animated: true)
                    
                }else{//pickup
                    self.searchController.active = false
                    self.selectedProductSearch = product
                    let bvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                    bvc.franchise = newBranch.franchise
                    bvc.delegate = self
                    self.navigationController?.pushViewController(bvc, animated: true)
                    
                }
            })
            
            
        }else{
            deliveryOrPickupHistorySearch(branch!, completion: { (isDelivery, newBranch) in
                
                if isDelivery == nil || isDelivery == true {
                    
                    let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
                    newBranch.mode = 0
                    restaurantVC.restaurant = newBranch
                    
                    //            controllers?.append(categoriasVC!)
                    self.searchController.active = false
                    var controllers = self.navigationController!.viewControllers
                    controllers.removeLast()
                    controllers.removeLast()
                    controllers.removeLast()
                    controllers.append(restaurantVC)
                    self.navigationController?.setViewControllers(controllers, animated: true)

                    
                }else{//pickup
                    self.searchController.active = false
                    self.selectedBranchSearch = newBranch
                    let bvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                    bvc.franchise = newBranch.franchise
                    bvc.delegate = self
                    self.navigationController?.pushViewController(bvc, animated: true)
                    
                }
            })
            
            
        }
        
    }

    
    
    // MARK: Product Favorite
    
    func didPressFavoriteBtn(){
        if MUser.sharedInstance.logged {
            
            if MUser.sharedInstance.productFavorites.contains(product){
                productFavoriteBtn.setImage(unFavorited, forState: .Normal)
                productFavoriteBtn?.pop()
                deleteProductFavorite()
            }else{
                productFavoriteBtn.setImage(favorited, forState: .Normal)
                productFavoriteBtn?.pop()
                addProductFavorite()
            }
        }
    }
    
    func didPressShareBtn() {
        var array:[AnyObject] = []
        let text = "Mira este Producto en Monchis! - " + self.product.name!
        let urlString = "https://admin.monchis.com.py/?q="
        let query = "productid=\(self.product.id!)&franchiseid=\(self.restaurant.franchise!.id!)&type=product"
        let hash = query.toBase64() ?? ""
        
        let link = NSURL(string: urlString + hash)!
        array.append(text)
        array.append(link)
        if let img = self.product.downloadedImageForShare {
            array.insert(img, atIndex: 0)
        }
        let activityViewController = UIActivityViewController(activityItems: array, applicationActivities: nil)
        activityViewController.setValue("Mira este producto en Monchis!", forKey: "subject")

        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        activityViewController.completionWithItemsHandler = { activityType, success, items, error in
            if !success {
                print("cancelled")
                return
            }
            Answers.logShareWithMethod(activityType ?? "",
                                       contentName: self.product.name!,
                                       contentType: "Producto",
                                       contentId: "\(self.product.id!)",
                                       customAttributes: nil)
            Mixpanel.mainInstance().track(event: "Usuario Compartio un Producto",
                                          properties: ["Franquicia" : self.restaurant.franchise?.name ?? "","Pantalla":"Customizacion de Producto","Producto":self.product.name!, "Producto ID": self.product.id!, "Método": activityType ?? ""])
            if activityType!.rangeOfString("whatsapp") != nil{
                
            }
        }
        self.presentViewController(activityViewController, animated: true, completion: nil)

    }
    func documentInteractionControllerViewControllerForPreview(controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    var docController: UIDocumentInteractionController!
    
    func addProductFavorite(){
        
        let parameters = ["product_id":product.id ?? 0,"branch_id":restaurant.id ?? 0]
        api.jsonRequest(.FAVORITE_PRODUCT, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    MUser.sharedInstance.productFavorites.append(self.product)
                    Mixpanel.mainInstance().track(event: "Usuario Agrego Producto a Favoritos ", properties: ["Producto":self.product.name!,"Producto ID": self.product.id!, "Pantalla": "Customizacion de Producto"])
                }else{
                    self.productFavoriteBtn.setImage(unFavorited, forState: .Normal)
                    let error = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Lo sentimos!", message: error, preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }else{
                self.productFavoriteBtn.setImage(unFavorited, forState: .Normal)
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func deleteProductFavorite(){
        let parameters = ["product_id":product.id ?? 0,"branch_id":restaurant.id ?? 0]
        api.jsonRequest(.DELETE_PRODUCT_FAVORITE, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            Mixpanel.mainInstance().track(event: "Usuario Elimino Producto de Favoritos ", properties: ["Producto":self.product.name!,"Producto ID": self.product.id!, "Pantalla": "Customizacion de Producto"])
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    /* handle favorite removal */
                    
                    if let index = MUser.sharedInstance.productFavorites.indexOf({$0.id == self.product.id}) {
                        MUser.sharedInstance.productFavorites.removeAtIndex(index)
                        self.productFavoriteBtn.setImage(unFavorited, forState: .Normal)
                        
                    }
                    
                    
                }else{
                    let error = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Lo sentimos!", message: error, preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }else{
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    // MARK: Branch Favorite
    
    @IBAction func favoritePressed(sender: UIButton) {
            if MUser.sharedInstance.branchesFavorites.contains(restaurant!){
                branchFavoriteBtn.setImage(unFavorited, forState: .Normal)
                deleteFavorite()
                self.branchFavoriteBtn.pop()

            }else{
                branchFavoriteBtn.setImage(favorited, forState: .Normal)
                addFavorite()
                self.branchFavoriteBtn.pop()

            }
    }
    
    
    func addFavorite(){
        
        let parameters = ["branch_id":restaurant.id!]
        api.jsonRequest(.FAVORITE_PRODUCT, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    MUser.sharedInstance.branchesFavorites.append(self.restaurant!)
                }else{
                    self.branchFavoriteBtn.setImage(unFavorited, forState: .Normal)
                    let error = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Lo sentimos!", message: error, preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }else{
                self.branchFavoriteBtn.setImage(unFavorited, forState: .Normal)
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func deleteFavorite(){
        let parameters = ["branch_id":restaurant.id!]
        api.jsonRequest(.DELETE_PRODUCT_FAVORITE, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    /* handle favorite removal */
                    
                    if let index = MUser.sharedInstance.branchesFavorites.indexOf({$0.id == self.restaurant.id}) {
                        MUser.sharedInstance.branchesFavorites.removeAtIndex(index)
                        self.branchFavoriteBtn.setImage(unFavorited, forState: .Normal)
                        
                    }
                    
                    
                }else{
                    let error = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Lo sentimos!", message: error, preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }else{
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func refreshBranch(){
        self.refreshBranchUI()

        self.leadingHeaderImgViewConstraint.constant = 31
        UIView.animateWithDuration(0.2) {
            self.view.setNeedsLayout()
        }
        noCoverRestoIconImgView.hidden = true
        closedRestoIconImgView.hidden = true
        statesSpinner.startAnimating()
        let currentDay = getDayOfWeek()!
        let latitude = MUser.sharedInstance.defaultAddress.latitude ?? ""
        let longitude = MUser.sharedInstance.defaultAddress.longitude ?? ""
        let parameters: [String:AnyObject] = ["franchise_id":self.restaurant.franchise_id!,"day":currentDay,"latitude":latitude,"longitude":longitude,"group_by_franchise":true]
        api.jsonRequest(.SEARCH_BRANCHES, parameters: parameters, returnCachedDataIfPossible: false ) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            self.statesSpinner.stopAnimating()
            if error == nil {
                if let restaurantsArray = json?["data"].array {
                    for data in restaurantsArray {
                        let newBranch = MRestaurant(data: data["branch"])
                        self.restaurant = newBranch
                    }
//                    self.restaurant.mode = self.branchMode
                    self.refreshBranchUI()
                    self.dismissWaitScreen()
                    
                }else{// network error
                    
                }
                
            }
        }
        
    }
    
    
    @IBAction func unwindFromBranchSelection(segue: UIStoryboardSegue) {
        let dvc = segue.sourceViewController as! BranchSelectionViewController
        selectedBranchForPickup = dvc.selectedBranch
        self.restaurant = selectedBranchForPickup
        addToBackEndCart()
    }
    
    @IBAction func unwindFromRegistrarse(segue: UIStoryboardSegue) {
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.dimm?.alpha = 0
            
        }) { (fin) -> Void in
            self.dimm?.removeFromSuperview()
            
        }
        addToBackEndCart()
    }
    
    
    func showRegisterPopOver(){
        //dim the background
        
        dimm = UIView(frame: (UIApplication.sharedApplication().keyWindow!.frame))
        
        dimm.backgroundColor = UIColor(hexString: "#000000")!
        UIApplication.sharedApplication().keyWindow?.addSubview(dimm)
        dimm.alpha = 0
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.dimm.alpha = 0.7
        })
        
        let addressPopover = self.storyboard?.instantiateViewControllerWithIdentifier("RegisterTableViewController") as! RegisterTableViewController
        addressPopover.modalPresentationStyle = .Popover
        addressPopover.preferredContentSize = CGSizeMake(self.view.bounds.width - 50, self.view.bounds.height - 100)
        
        let popoverMenuViewController = addressPopover.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = .Down
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = self.view
        popoverMenuViewController?.sourceRect = CGRectMake(CGRectGetMidX(self.view.bounds), self.view.bounds.height - 50,0,0)
        
        self.presentViewController(
            addressPopover,
            animated: true,
            completion: nil)
        
    }
    
    // MARK: PopOverDelegate
    
    func adaptivePresentationStyleForPresentationController(
        controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.dimm?.alpha = 0
            
        }) { (fin) -> Void in
            self.dimm?.removeFromSuperview()
            
        }
    }
    
    // MARK: UITextViewDelegate 
    
    
    
    func textViewDidChange(textView: UITextView) {
        self.product.comments = textView.text
        
    }
    
}
