//
//  CashAmountSelectionTableViewController.swift
//  Monchis
//
//  Created by Javier Rivarola on 12/3/15.
//  Copyright © 2015 cibersons. All rights reserved.
//

import UIKit

class MCash {
    var image: UIImage?
    var amount: Float?
    var type: String?
    var code: Int?
}

protocol CashTypeAmountDelegate {
    func cashTypePressed(forCash: MCash, forOrder: MOrder)
}

class CashAmountSelectionTableViewController: UITableViewController {
    
    var cashPosibilities:[MCash] = []
    var delegate: CashTypeAmountDelegate?
    var order: MOrder!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
            self.tableView.reloadData()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return cashPosibilities.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseCashAmountID", forIndexPath: indexPath)
        cell.imageView?.image = cashPosibilities[indexPath.row].image
        var cellText = ""
        if cashPosibilities[indexPath.row].code == 0 {
            cellText = "Monto exacto"
        }
        if cashPosibilities[indexPath.row].code == 1 {
            cellText = cashPosibilities[indexPath.row].amount?.asLocaleCurrency ?? ""
        }
        if cashPosibilities[indexPath.row].code == 2 {
            cellText = "Ingresar monto"
        }
        
        if cashPosibilities[indexPath.row].code == 3 {
            cellText = cashPosibilities[indexPath.row].type!
        }
        
        cell.textLabel?.text = cellText
    
        cell.textLabel?.font = UIFont.boldSystemFontOfSize(13)
        // Configure the cell...

        return cell
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        delegate?.cashTypePressed(self.cashPosibilities[indexPath.row],forOrder: order)
       
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
