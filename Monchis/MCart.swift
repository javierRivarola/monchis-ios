//
//  MCart.swift
//  Monchis
//
//  Created by jrivarola on 10/23/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
import TaskQueue
import Alamofire
import SCLAlertView
import SwiftyJSON

class MCart: NSObject {
    
    static let sharedInstance = MCart()
    let api = MAPI.SharedInstance
    var id: Int?
    var orders:[MOrder] = []
    var totalComputed: Float {
        var total:Float = 0
        for order in orders {
            total = total + order.orderTotalComp
        }
        return total
    }
    var totalComputedCart: Float {
        var total:Float = 0
        for order in orders {
            total = total + order.orderTotalCompCart
        }
        return total
    }
    var total: String?
    
    //        {
    //        var totalPrice:Float = 0
    //        for order in self.orders {
    //            totalPrice = totalPrice + order.total
    //        }
    //        return totalPrice.asLocaleCurrency
    //    }
    
    var delivery_type: String?
    var deliver_date: String?
    
    init (orders: [MOrder]) {
        self.orders = orders
    }
    
    var jsonData: SwiftyJSON.JSON!
    
    override init (){
        
    }
    
    
    
    func loadCart(json: SwiftyJSON.JSON) {
        self.orders = []
        self.jsonData = json["data"]
        self.total = json["total"].string
        self.id = json["id"].int
        var tmpOrders:[MOrder] = []
        if let jsonOrders = json["orders"].array {
            if debugModeEnabled {
                print("CARRITO: \(jsonOrders)")
            }
            for order in jsonOrders {
                let newOrder:MOrder = MOrder(json: order)
                if newOrder.products.count > 0 {
                    tmpOrders.append(newOrder)
                }
            }
            self.orders = tmpOrders
        }
        NSNotificationCenter.defaultCenter().postNotificationName(kCartUpdatedNotification, object: nil)

    }
    
    
    var finalizedProductsQueue: [MProduct] = []
    
    func getOrders(completion: (success:Bool) -> Void){
        //createWaitScreen("Obteniendo datos..", comment: "Obteniendo datos")
        
        
        api.postQueueRequest(.SHOW_CART, parameters: nil) { (json, error) in
            if error == nil {
                if let jsonData = json?["data"] {
                    if debugModeEnabled {
                        print(#function,"orders json: \(json)",error)
                    }
                    self.orders = []
                    self.total = jsonData["total"].string
                    self.id = jsonData["id"].int
                    var tmpOrders:[MOrder] = []
                    if let jsonOrders = jsonData["orders"].array {
                        for order in jsonOrders {
                            let newOrder:MOrder = MOrder(json: order)
                            if newOrder.products.count > 0 {
                                tmpOrders.append(newOrder)
                            }
                        }
                        self.orders = tmpOrders
                        NSNotificationCenter.defaultCenter().postNotificationName(kCartUpdatedNotification, object: nil)
                        completion(success: true)
                    }else{
                        completion(success: false)
                    }
                }
            }else{
                completion(success: false)
                print("error at connecting to server")
            }
        }
        
        //        api.jsonRequest(.SHOW_CART, parameters: nil) { (json, error) -> Void in
        //            if error == nil {
        //                if let jsonData = json?["data"] {
        //                    print(#function,"orders json: \(json)",error)
        //                    self.orders = []
        //                    self.total = jsonData["total"].string
        //                    self.id = jsonData["id"].int
        //                    var tmpOrders:[MOrder] = []
        //                    if let jsonOrders = jsonData["orders"].array {
        //                        for order in jsonOrders {
        //                            let newOrder:MOrder = MOrder(json: order)
        //                            if newOrder.products.count > 0 {
        //                                tmpOrders.append(newOrder)
        //                            }
        //                        }
        //                        self.orders = tmpOrders
        //                        completion(success: true)
        //                    }else{
        //                        completion(success: false)
        //                    }
        //                }
        //            }else{
        //                completion(success: false)
        //                print("error at connecting to server")
        //            }
        //        }
    }
    
    
    func clear(){
        self.orders = []
    }
    var alert: SCLAlertViewResponder!
    var queue: TaskQueue = TaskQueue()
    var requests: [Request] = []
    var lastGetOrders: Request?
    func addProductToCart (parameters: [String:AnyObject], product: MProduct, completion: (success: Bool, parameters: [String:AnyObject], product: MProduct) -> Void) {
        let newProduct = MProduct(fromProduct: product)
        UIView.animateWithDuration(0.3, animations: {
            globalCartViewController?.view.layoutIfNeeded()
            
        })
        newProduct.randomString = randomAlphaNumericString(10)
        self.productsQueue.append(newProduct)
        globalProductsQueueCV?.reloadData()
        globalProductsQueueCVHeightConstraint?.constant = 140
      
        //        let currentTask = {
        //            self.api.jsonRequest(.ADD_TO_CART, parameters: parameters) { (json, error) -> Void in
        //                if debugModeEnabled {
        //                    print(#function,"called with parameters: \(parameters)",json,error)
        //                }
        //                if error == nil {
        //                    if let json = json {
        //                        let success = json["success"].bool ?? false
        //                        completion(success: success,parameters: parameters, product: product)
        //                    }
        //                }else{
        //                    self.queue.retry(1)
        //                    completion(success: false, parameters: parameters, product: product)
        //                }
        //
        //            }
        //
        //        }
        //        let taskpointer = unsafeBitCast(currentTask,AnyObject.self)
        
        
        api.postQueueRequest(.ADD_TO_CART, parameters: parameters) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                
                newProduct.loaded = true
                self.finalizedProductsQueue.append(newProduct)
                if json?["success"].bool == true {
                    if let jsonData = json?["data"] {
                        print(#function,"orders cart: \(jsonData)")
                        self.orders = []
                        self.total = jsonData["total"].string
                        self.id = jsonData["id"].int
                        var tmpOrders:[MOrder] = []
                        if let jsonOrders = jsonData["orders"].array {
                            for order in jsonOrders {
                                let newOrder:MOrder = MOrder(json: order)
                                if newOrder.products.count > 0 {
                                    tmpOrders.append(newOrder)
                                }
                            }
                            self.orders = tmpOrders
                            print("number of orders after added  \(self.orders.count)")
                            NSNotificationCenter.defaultCenter().postNotificationName(kCartUpdatedNotification, object: nil)
                            completion(success: true,parameters: parameters, product: product)
                            
                        }
                    }
                }else{
                    self.alert?.close()
                    self.alert = SCLAlertView().showError("Lo sentimos!", subTitle: json?["data"].string ?? "Ocurrio un error inesperado.")
                    completion(success: false,parameters: parameters, product: product)
                    
                }
                
                
                                globalProductsQueueCV?.performBatchUpdates({
                                    globalProductsQueueCV?.reloadSections(NSIndexSet(index: 0))
                                    }, completion: nil)
                
                                let diff = self.productsQueue.filter({ (prod) -> Bool in
                                    return !prod.loaded
                                })
                                if diff.count == 0 {
                                    globalProductsQueueCVHeightConstraint?.constant = 0
                                    UIView.animateWithDuration(0.3, animations: {
                                        globalCartViewController?.view.layoutIfNeeded()
                                    })
                                }else{
                                    globalProductsQueueCVHeightConstraint?.constant = 140

                                    UIView.animateWithDuration(0.3, animations: {
                                        globalCartViewController?.view.layoutIfNeeded()

                                    })
                                }

//                globalProductsQueueCV?.reloadData()

                
            }else{
                self.alert?.close()
                self.alert = SCLAlertView().showError("Lo sentimos!", subTitle:"Ocurrio un error inesperado.")
                completion(success: false, parameters: parameters, product: product)
            }
            
        }
        
        
    }
    
    
    func randomAlphaNumericString(length: Int) -> String {
        
        let allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let allowedCharsCount = UInt32(allowedChars.characters.count)
        var randomString: String = ""
        for _ in (0..<length) {
            let randomNum = Int(arc4random_uniform(allowedCharsCount))
            let newCharacter = allowedChars[allowedChars.startIndex.advancedBy(randomNum)]
            randomString += String(newCharacter)
        }
        return randomString
    }
    
    func removeTaskFromQueue(taskCmpr: AnyObject) -> Bool{
        let tasks = self.queue.tasks
        var index = 0
        for task in tasks {
            let taskpointer = unsafeBitCast(task,AnyObject.self)
            if taskCmpr === taskpointer {
                print("same task")
                _ = self.queue.tasks.removeAtIndex(index)
                return true
            }else{
                print("not sane task")
            }
            index += 1
        }
        return false
        
    }
    
    
    var productsQueue: [MProduct] = []
    
}