//
//  Delay.swift
//  ImageOpenTransition
//
//  Created by Antonio Alves on 7/23/16.
//  Copyright © 2016 Jive. All rights reserved.
//

import Foundation


func afterDelay(seconds: Double, completion: () -> Void) {
    let delay = seconds * Double(NSEC_PER_SEC)
    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
    dispatch_after(time, dispatch_get_main_queue()) {
        completion()
    }
}
