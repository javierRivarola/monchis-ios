//
//  InfoRestoHeaderTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 10/5/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class InfoRestoHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var restoImageView: UIImageView!
    @IBOutlet weak var restoTituloDescriptionLbl: UILabel!
    @IBOutlet weak var restoDescriptionLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
