//
//  MOrder.swift
//  Monchis
//
//  Created by jrivarola on 7/30/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

import SwiftyJSON

class MOrder: NSObject {
    var order_id: Int?
    var starRatingValue: Int = 5
    var id: Int?
    var branch_id:Int?
    var branchImage: UIImage?
    var address_id: Int? // Address ID of the user where the order is getting delivered
    var cart_id: Int?
    var generate_invoice: Bool?
    var quantity: Int?
    var total: Float?
    var delivery_type: Int?
    var delivery_address: String?
    var invoice_required: Bool = false
    var state: String?
    var delivery_start_at: String?
    var delivered_at: String?
    var branch: MRestaurant!
    var client_cash_amount: String?
    var is_pickup: Bool = false
    var products: [MProduct] = []
    var feedback_stars:Int?
    var feedback_reasons:[String] = []
    var confirmed_at: String?
    var feedback_comments: String?
    var localConfirmedAtTime :String {
        // create dateFormatter with UTC time format
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //        dateFormatter.timeZone = NSTimeZone(name: "UTC")
        let date = dateFormatter.dateFromString(confirmed_at!)
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        let timeStamp = dateFormatter.stringFromDate(date!)
        return timeStamp
    }
    
    var confirmedAtDate : NSDate {
        if let confirmed = self.confirmed_at {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            //        dateFormatter.timeZone = NSTimeZone(name: "UTC")
            if let date = dateFormatter.dateFromString(confirmed) {
                return date
            }else{
                return NSDate()
            }
        }else{
            return NSDate()
        }
    }
    
    var invoice_name: String?
    //    var products: [MProduct]? {
    //        didSet{
    //            total = 0
    //            for product in self.products! {
    //                total = total + product.price!.floatValue
    //            }
    //        }
    //    }
    var status: Status?
    var delivery_price:Float?
    var payMethod: PayMethod?
    var wantsTicket: Bool?
    var invoice_ruc: String?
    var cancel_reason: String?
    var created_at_date: String?
    var orderTotalComp:Float {
        get {
            var subTotal:Float = 0
            if self.products.count > 0 {
                for product in self.products {
                    //subTotal = subTotal + (product.addonsTotalPrice + (product.price ?? 0))*Float(product.quantity ?? 0)
                    subTotal = subTotal + (((product.price ?? 0) + product.userCustomizationsTotalPrice)*Float((product.quantity ?? 0)))
                    
                }
                if self.delivery_type == 1 {
                    subTotal = subTotal + (delivery_price ?? 0)
                }
            }
            return subTotal
        }
    }
    
    var orderTotalCompCart:Float {
        get {
            var subTotal:Float = 0
            if self.products.count > 0 {
                for product in self.products {
                    subTotal = subTotal + (product.addonsTotalPrice + (product.price ?? 0))*Float(product.quantity ?? 0)
                    //                    subTotal = subTotal + (((product.price ?? 0) + product.userCustomizationsTotalPrice)*Float((product.quantity ?? 0)))
                    
                }
                if self.delivery_type == 1 {
                    subTotal = subTotal + (delivery_price ?? 0)
                }
            }
            return subTotal
        }
    }
    
    
    enum PayMethod: Int {
        case CASH = 1
        case CREDIT = 2
    }
    
    enum Status: Int {
        case OPEN = 1
        case CANCELED = 2
        case CONFIRMED = 3
        case IN_PROGRESS = 4
        case DELIVERY = 5
        case DELIVERED = 6
        case FINISHED = 7
        case PICKUP = 9
    }
    
    func addInvoiceData(){
        self.invoice_name = MUser.sharedInstance.invoice_name
        self.invoice_ruc = MUser.sharedInstance.invoice_ruc
    }
    func removeInvoiceData(){
        self.invoice_name = nil
        self.invoice_ruc = nil
    }
    
    override init (){
        
    }
    
    init (json order:  JSON) {
        self.products = []
        self.client_cash_amount = order["client_cash_amount"].string
        let newBranch = MRestaurant(data: order["branch"])
        self.branch = newBranch
        let newFranchise = MFranchise(json: order["branch"]["franchise"])
        self.branch.franchise = newFranchise
        self.order_id = order["order_id"].int
        self.id = order["id"].int
        self.feedback_stars = order["feedback_stars"].int
        self.is_pickup = order["is_pickup"].bool ?? false
        self.feedback_comments = order["feedback_comments"].string
        if let arr = order["feedback_reasons"].array {
            for ar in arr {
                self.feedback_reasons.append(ar.string ?? "")
            }
        }
        self.address_id = order["address_id"].int
        self.confirmed_at = order["confirmed_at"].string
        self.delivery_type = order["delivery_type"].int
        self.created_at_date = order["created_at"].string
        if let tot = order["total"].float {
            self.total = tot
        }
        if let tot = order["total"].string {
            self.total = tot.floatValue
        }
        newBranch.franchise_id = newFranchise.id
        if let delPrice = order["delivery_price"].float {
            self.delivery_price = delPrice
        }
        if let delPrice = order["delivery_price"].string {
            self.delivery_price = delPrice.floatValue
        }
        if let status = order["order_state_id"].int {
            
            if status == MOrder.Status.CANCELED.rawValue {
                self.status = .CANCELED
            }
            if status == MOrder.Status.CONFIRMED.rawValue {
                self.status = .CONFIRMED
            }
            if status == MOrder.Status.DELIVERED.rawValue {
                self.status = .DELIVERED
            }
            if status == MOrder.Status.DELIVERY.rawValue {
                self.status = .DELIVERY
            }
            if status == MOrder.Status.FINISHED.rawValue {
                self.status = .FINISHED
            }
            if status == MOrder.Status.IN_PROGRESS.rawValue {
                self.status = .IN_PROGRESS
            }
            if status == MOrder.Status.PICKUP.rawValue {
                self.status = .PICKUP
            }
            
        }
        
        
        let orderPayMethod = order["payment_type"].string ?? ""
        if orderPayMethod == "CASH" {
            self.payMethod = .CASH
        }
        if orderPayMethod == "POS" {
            self.payMethod = .CREDIT
        }
        self.branch_id = order["branch_id"].int
        self.invoice_ruc = order["invoice_ruc"].string
        self.invoice_name = order["invoice_name"].string
        self.invoice_required = order["invoice_required"].bool ?? false
        self.wantsTicket = self.invoice_required
        
        if let order_products_array = order["order_products"].array {
            for orderProduct in order_products_array {
                let newProduct = MProduct(json: orderProduct["product"])
                newProduct.quantity = orderProduct["quantity"].int ?? 1
                newProduct.comments = orderProduct["comments"].string
                newProduct.current_price = orderProduct["current_price"].string
                newProduct.order_product_id = orderProduct["id"].int
                if let attachmentArray = orderProduct["product"]["attachments"].array {
                    for att in attachmentArray {
                        let newAtt = MAttachment(json: att)
                        newAtt.defaultImageSize = "500x500/"
                        newProduct.attachments = [newAtt]
                    }
                }
                if let productAddons = orderProduct["order_product_addons"].array {
                    for addon in productAddons {
                        let newAddon = MAddon(json: addon["addon"])
                        newProduct.addons.append(newAddon)
                    }
                }else if let productAddons = orderProduct["product"]["addons"].array {
                    for addon in productAddons {
                        let newAddon = MAddon(json: addon["addon"])
                        let customField = MCustomizableField()
                        
                        customField.id = addon["id"].int ?? 0
                        customField.name = addon["addon_category"]["label"].string ?? ""
                        customField.limit = addon["limit"].int ?? 1
                        /* HARD CODED PARA QUE FUNCIONE AHORA*/
                        if customField.limit == 0 {
                            customField.limit = 1
                        }
                        if addon["required"].int == 0 {
                            customField.required = false
                        }else{
                            customField.required = true
                        }
                        
                        if let actionType = addon["addon_category"]["action_type"].int {
                            if actionType == 0 {
                                customField.action = .addition
                            }
                            if actionType == 1 {
                                customField.action = .subtraction
                            }
                            if actionType == 2 {
                                customField.action = .maximum
                            }
                            if actionType == 3 {
                                customField.action = .minimum
                            }
                            if actionType == 4 {
                                customField.action = .average
                            }
                        }
                        /* FIN HARD CODED */
                        if let optionsArray = addon["addon_category"]["addons"].array {
                            for option in optionsArray {
                                let addon = MAddon(json: option)
                                customField.addons.append(addon)
                            }
                        }
                        newProduct.customizableFields.append(customField)

                        newProduct.addons.append(newAddon)
                    }
                }
                self.products.append(newProduct)
            }
            
        }
        if let order_products_array = order["orderProducts"].array {
            for orderProduct in order_products_array {
                let newProduct = MProduct(json: orderProduct["product"])
                newProduct.quantity = orderProduct["quantity"].int ?? 1
                newProduct.comments = orderProduct["comments"].string
                newProduct.order_product_id = orderProduct["id"].int
                if let attachmentArray = orderProduct["product"]["attachments"].array {
                    for att in attachmentArray {
                        let newAtt = MAttachment(json: att)
                        newAtt.defaultImageSize = "500x500/"
                        newProduct.attachments = [newAtt]
                    }
                }
                if let productAddons = orderProduct["order_product_addons"].array {
                    for addon in productAddons {
                        let newAddon = MAddon(json: addon["addon"])
                        newProduct.addons.append(newAddon)
                    }
                }else if let productAddons = orderProduct["product"]["addons"].array {
                    for addon in productAddons {
                        let newAddon = MAddon(json: addon["addon"])
                        newProduct.addons.append(newAddon)
                    }
                }
                self.products.append(newProduct)
            }
            
        }
        
        
    }
    
    class func compareOrders(orders1: [MOrder], orders2: [MOrder]) -> Bool {
        for o in orders1 {
            if !orders2.contains( {ord in
                return ord.order_id == o.order_id
            }) {
                return false
            }
        }
        return true
    }
    
}

func == (lhs: MOrder, rhs: MOrder) -> Bool {
    return lhs.order_id == rhs.order_id
}