//
//  AddressDetailsV3TableViewController.swift
//  Monchis
//
//  Created by Javier Rivarola on 3/22/17.
//  Copyright © 2017 cibersons. All rights reserved.
//

import UIKit
import TextFieldEffects
import GoogleMaps
import PhoneNumberKit
import SCLAlertView

protocol AddressDetailsV3Delegate{
    func didPressEditAddressBtn()
    func didPressSaveAddressBtn()
}

class AddressDetailsV3TableViewController: UITableViewController, UITextFieldDelegate {

  
    @IBOutlet weak var phoneCell: UITableViewCell!
    
    @IBOutlet weak var coolPhoneTxt: PhoneNumberTextField!
    
    var delegate:AddressDetailsV3Delegate?
    var address: MAddress = MAddress()
    @IBOutlet weak var street1Txt: UITextField!
    var editingMode: Bool = false
    var cameFromCart: Bool = false
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var addressNameTxt: UITextField!
    @IBOutlet weak var mapView: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if MUser.sharedInstance.addresses.count >= 1 {
            self.titleLbl.text = "COMPLETA LOS DATOS DE TU DIRECCIÓN"
        }
        if editingMode {
            self.saveAddressBtn.setTitle("GUARDAR DIRECCIÓN", forState: .Normal)
        }
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 200
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.mapView.userInteractionEnabled = false
        
        let marker = GMSMarker(position: address.computedCoordinate)
        marker.map = self.mapView
        
        mapView.camera = GMSCameraPosition(target: marker.position, zoom: 16, bearing: 0, viewingAngle: 0)
        self.addressNameTxt.delegate = self
        referencesTxt.delegate = self
        street2Txt.delegate = self
        street1Txt.delegate = self
        numberTxt.delegate = self
        coolPhoneTxt.delegate = self
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        textField.borderColor = UIColor.lightGrayColor()

        return true
    }
    
    let phoneCellIndex = 5
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == phoneCellIndex && cameFromCart && MUser.sharedInstance.phone != "" {
            return 0
        }else if indexPath.row == phoneCellIndex && cameFromCart && MUser.sharedInstance.phone == ""{
            return UITableViewAutomaticDimension
        }else if indexPath.row == phoneCellIndex && !cameFromCart {
            return 0
        }else{
            return UITableViewAutomaticDimension
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
     
        self.saveAddressBtn.layer.borderColor = UIColor.redColor().CGColor
        self.saveAddressBtn.layer.borderWidth = 1.0
        self.saveAddressBtn.layer.cornerRadius = 5
        self.saveAddressBtn.clipsToBounds = true
        
        if editingMode && !cameFromCart{
            self.street1Txt?.text = address.street1
            self.addressNameTxt.text = address.name
            street2Txt.text = self.address.street2
            numberTxt.text = self.address.number
            referencesTxt.text = self.address.reference
        }
        if cameFromCart && MUser.sharedInstance.addresses.count > 1{
            self.addressNameTxt.text = address.name
            self.street1Txt?.text = address.street1
        }

    }
    @IBAction func saveBtnPressed(sender: UIButton) {
        delegate?.didPressSaveAddressBtn()
        
    }
    
    
    
    
    
    @IBOutlet weak var street2Txt: UITextField!
    
    @IBOutlet weak var numberTxt: UITextField!

    @IBOutlet weak var referencesTxt: UITextField!
    
    
    
    @IBOutlet weak var editAddressBtn: UIButton!
    
    @IBOutlet weak var saveAddressBtn: UIButton!
    
    
    @IBAction func editBtnPressed(sender: UIButton) {
        delegate?.didPressEditAddressBtn()
        
    }
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
