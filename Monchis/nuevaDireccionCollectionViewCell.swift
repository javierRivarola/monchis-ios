//
//  nuevaDireccionCollectionViewCell.swift
//  Monchis
//
//  Created by jrivarola on 8/20/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit



class nuevaDireccionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var addBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addBtn.contentMode = .ScaleAspectFit

    }
  
    @IBAction func addBtnPressed(sender: UIButton) {
        NSNotificationCenter.defaultCenter().postNotificationName("addNewAddressBtnPressed", object: nil)
        
    }
    
   
}
