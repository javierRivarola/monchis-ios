//
//  OrderProductHistoryTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 7/12/15.
//  Copyright © 2015 cibersons. All rights reserved.
//

import UIKit
protocol FavoritesSelectionDelegate  {
   func didSelectProductFavorite(cell: OrderProductHistoryTableViewCell)
}

class OrderProductHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var productLbl: UILabel!
    @IBOutlet weak var favoriteBtn: UIButton!
    var delegate : FavoritesSelectionDelegate?
    var order: MOrder!
    var product: MProduct!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func favoritePressed(sender: UIButton) {
        
        delegate?.didSelectProductFavorite(self)
       
    }
}
