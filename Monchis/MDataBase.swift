//
//  MDataBase.swift
//  Monchis
//
//  Created by jrivarola on 7/3/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
/*
class MDataBase {
    
    
    func createTable() {
        let db = Database()
        let users = db["users"]
        let id = Expression<Int64>("id")
        let name = Expression<String?>("name")
        let email = Expression<String>("email")

    db.create(table: users) { t in
    t.column(id, primaryKey: true)
    t.column(name)
    t.column(email, unique: true)
    }
    // CREATE TABLE "users" (
    //     "id" INTEGER PRIMARY KEY NOT NULL,
    //     "name" TEXT,
    //     "email" TEXT NOT NULL UNIQ UE
    // )
        
        
        var alice: Query?
        if let rowid = users.insert(name <- "Alice", email <- "alice@mac.com").rowid {
            println("inserted id: \(rowid)")
            // inserted id: 1
            alice = users.filter(id == rowid)
        }
        // INSERT INTO "users" ("name", "email") VALUES ('Alice', 'alice@mac.com')
        
        for user in users {
            println("id: \(user[id]), name: \(user[name]), email: \(user[email])")
            // id: 1, name: Optional("Alice"), email: alice@mac.com
        }
        // SELECT * FROM "users"
        
        alice?.update(email <- replace(email, "mac.com", "me.com"))
        // UPDATE "users" SET "email" = replace("email", 'mac.com', 'me.com')
        // WHERE ("id" = 1)
        
        alice?.delete()
        // DELETE FROM "users" WHERE ("id" = 1)
        
        users.count
        // SELECT count(*) FROM "users"
        

        
    }
    
    
    
}

*/
