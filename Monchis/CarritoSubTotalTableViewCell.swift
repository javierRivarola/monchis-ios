//
//  CarritoSubTotalTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 10/12/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class CarritoSubTotalTableViewCell: UITableViewCell {

    @IBOutlet weak var subTotalLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
