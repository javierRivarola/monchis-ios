//
//  LoginContainerViewController.swift
//  Monchis
//
//  Created by Javier Rivarola on 6/8/17.
//  Copyright © 2017 cibersons. All rights reserved.
//

import UIKit

class LoginContainerViewController: UIViewController {

    @IBOutlet weak var loginContainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "loginSegue" {
            loginVC = segue.destinationViewController as! RegistroPopupTableViewController
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        NSNotificationCenter.defaultCenter().postNotificationName("didLoadedControllerNotification", object: nil)

    }
    var loginVC: RegistroPopupTableViewController!
}
