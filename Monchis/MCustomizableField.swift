//
//  MCustomizableField.swift
//  Monchis
//
//  Created by jrivarola on 10/6/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
import SwiftyJSON

class MCustomizableField: NSObject {
    var id: Int?
    var order: Int?
    var required: Bool?
    var name: String = ""
    var addons: [MAddon] = []
    var addonsTotalPrice: Float {
        get {
            var addonsPrice: Float = 0
                for customization in addons {
                    addonsPrice = addonsPrice + customization.price!
                }
                return addonsPrice
            
        }
    }
    var userSelectionsTotalPrice: Float {
        get {
            var total:Float = 0
            if let action = self.action {
                if action == .addition {
                    for index in userSelectedIndexes {
                        total = total + addons[index].price!
                    }
                    return total
                }
                if action == .subtraction {
                    for index in userSelectedIndexes {
                        total = total - addons[index].price!
                    }
                    return total
                }
                if action == .maximum {
                    var selectedAddons: [MAddon] = []
                    for index in userSelectedIndexes {
                        selectedAddons.append(addons[index])
                       
                    }
                    selectedAddons.sortInPlace({$0.price > $1.price})
                    if let maximum = selectedAddons.first {
                        return maximum.price!
                    }
                }
                if action == .minimum {
                    var selectedAddons: [MAddon] = []
                    for index in userSelectedIndexes {
                        selectedAddons.append(addons[index])
                        
                    }
                    selectedAddons.sortInPlace({$0.price < $1.price})
                    if let maximum = selectedAddons.first {
                        return maximum.price!
                    }
                }
                if action == .average {
                    for index in userSelectedIndexes {
                        total = total + addons[index].price!
                    }
                    return total/Float(userSelectedIndexes.count)
                }
            }
           
            return total
        }
    }

    var userSelectedIndexes: [Int] = []
    var multiple: Bool = false
    var limit: Int?
    
    
//    init (json: JSON) {
//        self.name = json["name"].string ?? ""
//        self.id = json["id"].int
//        self.order = json["order"].int
//        
//    }
    
    var selectedText:String {
        var selectedText = ""
        for index in self.userSelectedIndexes {
            if index == self.userSelectedIndexes.last {
                selectedText = selectedText + (self.addons[index].name ?? "")
                
            }else{
                selectedText = selectedText + (self.addons[index].name ?? "") + ", "
            }
        }
        return selectedText
    }
    
    var action: Action?
    
    enum Action: Int {
        case addition = 0
        case subtraction = 1
        case maximum = 2
        case minimum = 3
        case average = 4
    }

}

class MAddon: NSObject {
    var id: Int?
    var name: String?
    var price: Float?
    var etiqueta: String?
    var descripcion: String?
    var cellDescriptionHeight: CGFloat?
    init (json: JSON) {
        self.id = json["id"].int
        self.name = json["addon_name"]["name"].string
        self.price = json["price"].float
        self.etiqueta = json["addon_name"]["label"].string
        self.descripcion = json["addon_name"]["description"].string
    }
    
}

func == (lhs: MCustomizableField, rhs: MCustomizableField) -> Bool {
    return lhs.name == rhs.name
}