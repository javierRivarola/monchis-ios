//
//  ScannerProductTableViewCell.swift
//  Monchis
//
//  Created by Javier Rivarola on 6/22/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit

class ScannerProductTableViewCell: UITableViewCell {

    @IBOutlet weak var deliveryBtn: UIButton!
    @IBOutlet weak var pickupBtn: UIButton!
    @IBOutlet weak var restoBtn: UIButton!
    @IBOutlet weak var restoImgView: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productDescrLbl: UILabel!
    @IBOutlet weak var productImgView: UIImageView!
    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var productImgViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var validityLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBOutlet weak var priceLbl: UILabel!
}
