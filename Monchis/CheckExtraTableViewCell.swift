//
//  CheckExtraTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 4/11/15.
//  Copyright © 2015 cibersons. All rights reserved.
//

import UIKit

class CheckExtraTableViewCell: UITableViewCell {
    var mDelegate: CheckboxDelegate?

    @IBOutlet weak var addonBtn: UIButton!
    @IBOutlet weak var addonPriceLbl: UILabel!
    @IBOutlet weak var addonDescriptionLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        addonBtn?.adjustsImageWhenHighlighted = false
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func didPressAddonBtn(sender: UIButton) {
//        mDelegate?.didSelectCheckbox(self.selected, identifier: sender.tag, title: sender.currentTitle!);
    }

}
