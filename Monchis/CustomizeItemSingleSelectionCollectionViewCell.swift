//
//  CustomizeItemSingleSelectionCollectionViewCell.swift
//  Monchis
//
//  Created by jrivarola on 10/16/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class CustomizeItemSingleSelectionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var imagenFondo: UIImageView!
    @IBOutlet weak var optionLbl: UILabel!
}
