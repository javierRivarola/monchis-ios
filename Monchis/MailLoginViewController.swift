//
//  MailLoginViewController.swift
//  Monchis
//
//  Created by jrivarola on 8/6/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import KeychainSwift

class MailLoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var ingresarBtn: UIButton!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var recuperarPassBtn: UIButton!
    @IBOutlet weak var recuperarEmail: UITextField!
    
    @IBOutlet weak var topConstraintLogo: NSLayoutConstraint!
    let api = MAPI.SharedInstance
    let user = MUser.sharedInstance
    var spinner: MBProgressHUD!
    var selectedTextField: UITextField?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //Register observers
        let center = NSNotificationCenter.defaultCenter()
        
        //Notifications for keyboard
        center.addObserver(self, selector: #selector(MailLoginViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(MailLoginViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    // MARK: Keyboards notifications
    
    func keyboardWillShow(notification: NSNotification){
        _ = notification.userInfo
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            _ = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
            
            if selectedTextField == recuperarEmail {
                self.topConstraintLogo.constant = -keyboardSize.height
                self.view.layoutIfNeeded()
            }
            
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification){
        self.topConstraintLogo.constant = 0
        self.view.layoutIfNeeded()
    }
    
    
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        selectedTextField = textField
        return true
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    @IBAction func ingresarBtnPressed(sender: UIButton) {
        
        if email.text != "" && password.text != "" {
            self.view.endEditing(true)
            spinner = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            spinner.mode = MBProgressHUDMode.Indeterminate
            spinner.label.text = NSLocalizedString("Iniciando Sesión",comment: "Iniciando Sesion")
            spinner.label.font = UIFont(name: "OpenSans-Semibold", size: 14.0)!
            
            let parameters = ["email":email.text!,"password":password.text!, "device_token":MUser.sharedInstance.pushToken,
                "type":"1",
                "model":MUser.sharedInstance.modelName,
                "system":"iOS",
                "version":MUser.sharedInstance.systemVersion]
            api.jsonRequest(.LOGIN_EMAIL, parameters: parameters, completion: { (json, error) -> Void in
                if debugModeEnabled {
                    print(#function,"called with parameters \(parameters)",json,error)
                }
                
                if error == nil {
                    
                    let success = json?["success"].bool ?? false
                    if let data = json?["data"] {
                        if success {
                            //we save the user data
                            self.user.email = self.email?.text ?? ""
                            self.user.password = self.password?.text ?? ""
                            self.user.unparseUserInfoFromApi(data)
                            self.user.logged = true
                            //set the monchis token
                            let monchis_token = data["monchis_token"].string ?? ""
                            self.api.token = monchis_token
                            self.api.mgr.session.configuration.HTTPAdditionalHeaders = ["token": "\(monchis_token)"]
                            print("Monchis token : \(monchis_token)")

                            //redirect user?? show animation? show alert?
                            self.user.authenticationMethod = .Monchis
                            //set automatic login for user
                            NSUserDefaults.standardUserDefaults().setBool(true, forKey: kAutomaticLogin)
                            KeychainSwift().set("monchis", forKey: kMonchisUserAuthenticationMethod)
                            let keychain = KeychainSwift()
                            keychain.set(self.user.email, forKey: kMonchisUserEmail)
                            keychain.set(self.user.password, forKey: kMonchisUserPassword)

                            //we load the user addresses
                            self.loadUserAdresses()
                           // self.registerPushToken()
                        }else{ // show error
                            self.spinner.hideAnimated(true)
                            let message = data.string ?? ""
                            let titleLocalizable = NSLocalizedString("Error",comment: "error")
                            let alert = UIAlertController(title: titleLocalizable, message: message, preferredStyle: .Alert)
                            let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                            alert.addAction(ok)
                            self.presentViewController(alert, animated: true, completion: nil)
                            
                        }
                        
                    }
                    
                }else{ // net error
                    self.spinner.hideAnimated(true)
                    let titleLocalizable = NSLocalizedString("Error",comment: "error")
                    let alert = UIAlertController(title: titleLocalizable, message: error, preferredStyle: .Alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                    alert.addAction(ok)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                
                
            })
        }
        
    }
    
    func loadUserAdresses(){
        
        api.jsonRequest(.USER_ADDRESSES, parameters: nil) { (json, error) -> Void in
            self.spinner.hideAnimated(true)
            if debugModeEnabled {
                print(#function,"called with parameters: nil","json data: \(json)","error: \(error)")
            }
            if error == nil {
                if let json = json {
                    let addresses = json["data"].array ?? []
                    self.user.addresses = []
                    for address in addresses {
                        let newAddr = MAddress()
                        newAddr.unparseAddressFromAPI(address)
                        self.user.addresses.append(newAddr)
                    }
                    
                    if self.user.addresses.count == 0 { //if user has no addresses we show the map
                        self.performSegueWithIdentifier("showMapFromMailLogin", sender: self)
                    }else{
                        self.performSegueWithIdentifier("showMainViewFromMailLogin", sender: self)
                    }
                }
            }else{ // network error
                let titleLocalizable = NSLocalizedString("Error",comment: "error")
                let alert = UIAlertController(title: titleLocalizable, message: error, preferredStyle: .Alert)
                let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                alert.addAction(ok)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    
    @IBAction func backtoLoginPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func recuperarPassPressed(sender: UIButton) {
        if recuperarEmail.text != "" {
            self.view.endEditing(true)
            let parameters = ["email":recuperarEmail.text!]
            api.jsonRequest(.USER_RECOVER_PASSWORD, parameters: parameters, completion: { (json, error) -> Void in
                if error == nil {
                    if let success = json?["success"].bool {
                        if success {
                            let alert = UIAlertController(title: "Monchis", message: "Su contraseña ha sido enviada a su correo electronico.", preferredStyle: .Alert)
                            let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                            alert.addAction(ok)
                            self.presentViewController(alert, animated: true, completion: nil)
                        }else{
                            let mensaje = json?["data"].string ?? "No hay mensaje de error"
                            let alert = UIAlertController(title: "Monchis", message: mensaje, preferredStyle: .Alert)
                            let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                            alert.addAction(ok)
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                    }
                }else{ // network error
                    let titleLocalizable = NSLocalizedString("Error",comment: "error")
                    let alert = UIAlertController(title: titleLocalizable, message: error, preferredStyle: .Alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                    alert.addAction(ok)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                
            })
        }
        
    }
    
    func registerPushToken(){
        let parameters = [  "device_token":MUser.sharedInstance.pushToken,
            "type":"1",
            "model":MUser.sharedInstance.modelName,
            "system":"iOS",
            "version":MUser.sharedInstance.systemVersion
        ]
        MAPI.SharedInstance.jsonRequest(.REGISTER_USER_DEVICE, parameters: parameters) { (json, error) -> Void in
            print(#function,"called with parameters \(parameters)",json,error)
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    
                }else{
                    let message = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Error al registrar dispositivo.", message: message, preferredStyle: .Alert)
                    let cancel = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(cancel)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }else{
                let message = json?["data"].string ?? "Error desconocido"
                let alert = UIAlertController(title: "Error al registrar dispositivo.", message: message, preferredStyle: .Alert)
                let cancel = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(cancel)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
        }
    }

    
}
