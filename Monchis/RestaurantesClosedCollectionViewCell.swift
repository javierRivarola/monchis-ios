//
//  RestaurantesClosedCollectionViewCell.swift
//  Monchis
//
//  Created by jrivarola on 9/9/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class RestaurantesClosedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var gpsFollowImageView: UIImageView!
    @IBOutlet weak var pickupIcon: UIImageView!
    @IBOutlet weak var pickpupLbl: UILabel!
    @IBOutlet weak var gpsFollowLbl: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var stateImageView: UIImageView!
    @IBOutlet weak var stateLabel: UILabel!
}
