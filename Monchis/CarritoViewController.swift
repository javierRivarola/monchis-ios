//
//  CarritoViewController.swift
//  Monchis
//
//  Created by jrivarola on 7/3/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import GMStepper
import DZNEmptyDataSet
import QuartzCore
import ReachabilitySwift
import SCLAlertView
import Mixpanel

import Crashlytics
var globalProductsQueueCV: UICollectionView!
var globalProductsQueueCVHeightConstraint:NSLayoutConstraint!
var globalCartTableView: UITableView!
var globalCartViewController: CarritoViewController!
class CarritoViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, CarritoPayMethodDelegate,CarritoTotalDelegate, UIPopoverPresentationControllerDelegate,CartOrderChangeOrDeleteDelegate, CashTypeAmountDelegate,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource,CarritoDeliveryPickupDelegate,BranchSelectionDelegate, UICollectionViewDataSource, UICollectionViewDelegate,SeeBranchPressed, AddressDetailsV3Delegate,RegisterPopoverDelegate  {
    
    //  MARK: Properties
    var resetPickupSwitch: Bool = true
    var reachability: Reachability?
    var deletingCounter:Int = 0
    
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    var deletingProduct: Bool = false
    var changingProductQuantity: Bool = false
    var headerMonchisImage: UIImageView!
    var orderHistory: [MOrder] = []
    //  Outlets
    @IBOutlet weak var tableView: UITableView!
    var branchSelectionPopOver: BranchSelectionViewController!
    var dimm: UIView!
    //  variables
    var showingProductDetails: Bool = false
    var currentHeaderView: UIView!
    
    lazy var searchBar = UISearchBar(frame: CGRectMake(0, 0, 200, 20))
    var orders: [[MOrder]] = []
    var cart = MCart.sharedInstance
    let api = MAPI.SharedInstance
    let user = MUser.sharedInstance
    var refreshController: UIRefreshControl!
    var changingQuantityTimer: NSTimer!
    var editedSubTotal: Float!
    var editedTotal: Float!
    var refreshControl: UIRefreshControl!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    //  Constants
    struct Constants {
        static let carritoItemReuseID = "carritoItemReuseID"
    }
    
    struct ReuseCell {
        static let carritoItem = "carritoItemReuseID"
        static let carritoSubTotal = "carritoSubTotalReuseID"
        static let carritoDeliveryPrice = "carritoDeliveryPriceReuseID"
        static let carritoPayMethod = "carritoPayMethodReuseID"
        static let carritoTotalPrice = "carritoTotalReuseID"
    }
    
    struct Images {
        static let botonPagoEfectivo = "boton-pago-efectivo"
        static let botonPagoPos = "boton-pago-pos"
        static let botonFactura = "boton-factura"
        static let botonPagoEfectivoActivo = "boton-pago-efectivo-activo"
        static let botonPagoPosActivo = "boton-pago-pos-activo"
        static let botonFacturaActivo = "boton-factura-activo"
        static let deliveryActivo = "icono-delivery-activo"
        static let deliveryInactivo = "icono-delivery-inactivo"
        static let pickupActivo = "icono-pick-up-activo-1"
        static let pickupInactivo = "icono-pick-up-inactivo"
        
    }
    
    struct Segues {
        static let thankyou = "thankyouSegue"
        static let editProduct = "editarProductoSegue"
        static let fillAddressDetails = "fillAddressDetailsSegue"
        static let registerPopover = "registerPopover"
    }
    
    
    @IBOutlet weak var productsQueueCV: UICollectionView!
    // MARK: LifeCycle
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        globalCartViewController = self
        globalProductsQueueCVHeightConstraint = self.collectionViewHeightConstraint
        globalProductsQueueCV = self.productsQueueCV
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 200
        tableView.tableFooterView = UIView(frame: CGRectZero)
        globalCartTableView = tableView
        let img = UIImage(named: "texto-monchis-header")
        headerMonchisImage = UIImageView(frame: CGRectMake(0, 0, 50, 25))
        headerMonchisImage.image = img
        headerMonchisImage.contentMode = .ScaleAspectFit
        self.navigationItem.titleView = headerMonchisImage
        
        searchBar.delegate = self
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CarritoViewController.userAddressChanged), name: kUserDidChangeDefaultAddressNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CarritoViewController.cartUpdated(_:)), name: kCartUpdatedNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CarritoViewController.unwindFromThankYouInicio), name: "unwindFromThankYouInicio", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CarritoViewController.unwindFromThankYouEstados), name: "unwindFromThankYouEstados", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CarritoViewController.refreshWithIndicator), name: "didChangeAllOrdersToNewDeliveryBranchNotification", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CarritoViewController.reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CarritoViewController.getOrders), name: kUserLoggedInNotification, object: nil)
        
        
        if showingProductDetails {
            getDetailsForOrder(orderHistory.first!)
        }
        
        tableView.emptyDataSetDelegate = self
        tableView.emptyDataSetSource = self
        configurePullToRefresh()
        
    }
    
    func refreshWithIndicator(){
        if let keywindow = UIApplication.sharedApplication().delegate?.window {
            let hud = MBProgressHUD.showHUDAddedTo(keywindow!, animated: true)
            hud.label.text = "Actualizando carrito.."
            
        }
        self.orders = []
        self.tableView.reloadData()
        self.tableView.reloadEmptyDataSet()
    }
    
    func unwindFromThankYouEstados(){
        self.tabBarController?.selectedIndex = 4
    }
    
    func unwindFromThankYouInicio(){
        self.tabBarController?.selectedIndex = 0
        
    }
    
    @IBAction func showExplorarTabPressed(sender: UIBarButtonItem) {
        self.tabBarController?.selectedIndex = 0
    }
    
    func reachabilityChanged(notification: NSNotification) {
        
        let reachability = notification.object as! Reachability
        
        if reachability.isReachable() {
            self.tableView.reloadEmptyDataSet()
            if user.logged {
                self.getOrders()
            }
            if reachability.isReachableViaWiFi() {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            print("Network not reachable")
            
        }
    }
    
    func configurePullToRefresh(){
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = UIColor.whiteColor()
        let title = NSMutableAttributedString()
        let tira = NSAttributedString(string: "Tira para actualizar", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        title.appendAttributedString(tira)
        refreshControl?.attributedTitle = title
        refreshControl?.addTarget(self, action: #selector(CarritoViewController.pullToRefresh(_:)), forControlEvents: .ValueChanged)
        refreshControl.layer.zPosition += 1
        self.tableView?.addSubview(refreshControl)
    }
    
    func pullToRefresh(sender: UIRefreshControl){
        getOrders()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    func cartUpdated(notification: NSNotification) {
        reflectBadgeCart()
    }
    
    func reflectBadgeCart(){
        
        if let carritoItem = tabBarController?.tabBar.items?[3] {
            carritoItem.badgeValue = "\(self.cart.orders.count)"
        }
    }
    
    
    func userAddressChanged(){
        //handle address change
        getOrders()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        if user.authenticationMethod != .Unregistered {
            getOrders()
        }
        
        let diff = cart.productsQueue.filter({ (prod) -> Bool in
            return !prod.loaded
        })
        // comentado por ahora
        if diff.count == 0 {
            globalProductsQueueCVHeightConstraint.constant = 0
        }else{
            globalProductsQueueCVHeightConstraint.constant = 140
        }
        //        globalProductsQueueCVHeightConstraint.constant = 0
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
        self.tableView.reloadData()
    }
    
    func getDetailsForOrder(order: MOrder!) {
        createWaitScreen()
        let parameters = ["order_id":order.order_id ?? 0]
        api.jsonRequest(.ORDER_SHOW, parameters: parameters) { (json, error) -> Void in
            if error == nil {
                if let json = json {
                    var tmpOrders:[MOrder] = []
                    let order = json["data"]
                    let newOrder:MOrder = MOrder(json: order)
                    if newOrder.products.count > 0 {
                        tmpOrders.append(newOrder)
                    }
                    self.orderHistory = tmpOrders
                }
                self.dismissWaitScreen()
                self.tableView.reloadData()
            }else{
                self.dismissWaitScreen()
                SCLAlertView().showError("Error", subTitle: error ?? "Ha ocurrido un error inesperado.")
            }
        }
        
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getOrders(){
        //createWaitScreen("Obteniendo datos..", comment: "Obteniendo datos")
        createWaitScreen()
        api.jsonRequest(.SHOW_CART, parameters: nil, cacheRequest: false) { (json, error) -> Void in
            if let keywindow = UIApplication.sharedApplication().delegate?.window {
                MBProgressHUD.hideHUDForView(keywindow!, animated: true)
            }
            if error == nil {
                if let json = json {
                    print("orders json: \(json)")
                    self.cart.loadCart(json["data"])
                }
                self.reflectBadgeCart()
                self.dismissWaitScreen()
                self.tableView.reloadData()
            }else{
                self.dismissWaitScreen()
                
            }
        }
    }
    
    func createWaitScreen(title: String, comment: String){
        
        let spinner = MBProgressHUD.showHUDAddedTo(UIApplication.sharedApplication().keyWindow!, animated: true)
        spinner.mode = MBProgressHUDMode.Indeterminate
        spinner.label.text = NSLocalizedString(title,comment: comment)
        spinner.label.font = UIFont(name: "OpenSans-Semibold", size: 14.0)!
        
    }
    
    //    func dismissWaitScreen(){
    //        MBProgressHUD.hideHUDForView(self.view, animated: true)
    //
    //    }
    
    func createWaitScreen(){
        spinner.startAnimating()
        //        glowMonchis()
    }
    
    func dismissWaitScreen(){
        spinner.stopAnimating()
        headerMonchisImage.layer.shadowColor = UIColor.clearColor().CGColor
        headerMonchisImage.layer.shadowRadius = 0
        headerMonchisImage.layer.shadowOpacity = 0
        headerMonchisImage.layer.removeAllAnimations()
        MBProgressHUD.hideHUDForView(UIApplication.sharedApplication().keyWindow!, animated: true)
        refreshControl?.endRefreshing()
    }
    
    
    
    
    func glowMonchis(){
        colorize(self.headerMonchisImage, color:UIColor(hexString: "#950505")!.CGColor)
    }
    
    func setPaymentInfoForOrder(order: MOrder, paymentMethod: MCash?) {
        createWaitScreen()
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        hud.label.text = ""
        var tmpPayment = 0
        if let paymentMethod =  paymentMethod {
            if paymentMethod.code == 3  {
                tmpPayment = 2
            }else{
                tmpPayment = 1
                if paymentMethod.code == 0 {
                    
                }
            }
        }else{
            if order.payMethod == .CASH {
                tmpPayment = 1
            }else if order.payMethod == .CREDIT{
                tmpPayment = 2
            }
        }
        var parameters: [String:AnyObject] = [:]
        if tmpPayment == 0 {
            parameters = [
                          "invoice_required":order.wantsTicket ?? false ,
                          "invoice_ruc":order.invoice_ruc ?? "",
                          "invoice_name":order.invoice_name ?? "",
                          "order_id": Int(order.order_id ?? 0)]
        }else{
         parameters = ["payment_type":tmpPayment,
                          "client_cash_amount":order.client_cash_amount ?? "",
                          "invoice_required":order.wantsTicket ?? false ,
                          "invoice_ruc":order.invoice_ruc ?? "",
                          "invoice_name":order.invoice_name ?? "",
                          "order_id": Int(order.order_id ?? 0)]
        }
        Answers.logCustomEventWithName("Usuario eligio metodo de pago para Orden",
                                       customAttributes: parameters)
        Mixpanel.mainInstance().track(event: kUserSelectedPaymentMethodForOrder, properties: ["Cantidad de Efectivo":order.client_cash_amount ?? "","Quiere Factura": order.wantsTicket ?? false, "Orden ID": Int(order.order_id ?? 0), "Razon Social":order.invoice_name ?? "","RUC":order.invoice_ruc ?? ""])
        api.jsonRequest(.ORDER_PAYMENT, parameters: parameters) { (json, error) -> Void in
            print(#function,"called with parameters: \(parameters)",json,error)
            hud.hideAnimated(true)
            self.dismissWaitScreen()
            if error == nil {
                if let index = self.cart.orders.indexOf({$0.order_id == order.order_id}) {
                    self.cart.orders[index].invoice_required = order.wantsTicket ?? false
                    if tmpPayment == 1 {
                        
                        self.cart.orders[index].payMethod = .CASH
                    }else if tmpPayment == 2{
                        self.cart.orders[index].payMethod = .CREDIT
                        
                    }
                }
                
                self.tableView.reloadData()
            }else{
                self.tableView.reloadData()
                SCLAlertView().showError("Lo sentimos!", subTitle: error ?? "Ocurrio un error inesperado!")
            }
        }
    }
    
    
    // MARK: UICollectionviewDataSource
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return  1 // change to zero for hidding
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let diff = cart.productsQueue.filter({ (prod) -> Bool in
            return !prod.loaded
        })
        
        return diff.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let diff = cart.productsQueue.filter({ (prod) -> Bool in
            return !prod.loaded
        })
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("reuseCell", forIndexPath: indexPath) as! LoadingCartItemCollectionViewCell
        if let linkString = diff[indexPath.row].attachments.first?.imageComputedURL {
            cell.imgView.hnk_setImageFromURL(linkString, placeholder: NO_ITEM_PLACEHOLDER)
        }
        cell.imgView.layer.cornerRadius = 5
        cell.imgView.clipsToBounds = true
        return cell
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == Segues.editProduct {
            let dvc = segue.destinationViewController as! CustomizarProductoViewController
            let order = sender as? [String:AnyObject]
            dvc.editingProduct = true
            dvc.product = order?["product"] as? MProduct
            dvc.restaurant = order?["branch"] as? MRestaurant
            
        }
        if segue.identifier == Segues.thankyou {
            let dvc = segue.destinationViewController as! OrderPlacedSuccessfulViewController
            dvc.cartID = self.successfulCartId
            NSNotificationCenter.defaultCenter().postNotificationName("refreshOrderStatusNotification", object: nil)
        }
        
        if segue.identifier == Segues.fillAddressDetails {
            addressDetailsPopover = segue.destinationViewController as! AddressDetailsV3TableViewController
            addressDetailsPopover.delegate = self
            addressDetailsPopover.cameFromCart = true
            addressDetailsPopover.address = MUser.sharedInstance.defaultAddress
            addressDetailsPopover.modalPresentationStyle = .Popover
            addressDetailsPopover.popoverPresentationController?.delegate = self
            addressDetailsPopover.popoverPresentationController?.sourceRect = CGRectMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds),0,0)

            addressDetailsPopover.popoverPresentationController?.sourceView = self.view
            addressDetailsPopover.preferredContentSize = CGSizeMake(self.view.bounds.width, self.view.bounds.height)

        }
        if segue.identifier == Segues.registerPopover {
            registerPopover = segue.destinationViewController as! MainRegisterTableViewController
            
            registerPopover.modalPresentationStyle = .Popover
            registerPopover.popoverPresentationController?.delegate = self
            registerPopover.popoverPresentationController?.sourceRect = CGRectMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds),0,0)
            
            registerPopover.popoverPresentationController?.sourceView = self.view
            registerPopover.preferredContentSize = CGSizeMake(self.view.bounds.width, self.view.bounds.height)
            registerPopover.delegate = self
            registerPopover.view.layer.borderColor = UIColor.whiteColor().CGColor
            registerPopover.view.layer.borderWidth = 1.0
        }
        
        
    }
    
    //Register popover delegate
    
    func didSuccededRegistration() {
        registerPopover?.dismissViewControllerAnimated(true, completion: nil)
        finalizarPressed() // user no longer temporal
    }
    
    func didFailedRegistration() {
         // un implemented
    }
    
    var registerPopover: MainRegisterTableViewController!
    
    var addressDetailsPopover: AddressDetailsV3TableViewController!
    var finalizingCart: Bool = false
    func didPressEditAddressBtn(){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton("Cambiar posición del Pin") {
            self.addressDetailsPopover?.dismissViewControllerAnimated(true, completion: nil)
            let mapVC = self.storyboard?.instantiateViewControllerWithIdentifier("AddressMapV3ViewController") as! AddressMapV3ViewController
            mapVC.cameFromCart = true
            mapVC.editingMode = true
            mapVC.address = self.user.defaultAddress.copy() as! MAddress
            self.navigationController?.pushViewController(mapVC, animated: true)
        }
        
        alertView.addButton("Volver", action: {})
        alertView.showNotice("Atención!", subTitle: "Si cambias la posición del Pin, todas las ordenes de tu carrito marcadas como Delivery seran eliminadas.")
        
    
        
    }
    var savingAddressFromCart: Bool = false
    func didPressSaveAddressBtn(){
        if savingAddressFromCart {
            return
        }
        var shouldReturn: Bool = false
        if addressDetailsPopover.addressNameTxt.text == "" {
            addressDetailsPopover.addressNameTxt.addMissingInputAnimation()
            shouldReturn = true
        }
        
        if addressDetailsPopover.street1Txt.text == "" {
            addressDetailsPopover.street1Txt.addMissingInputAnimation()
            shouldReturn = true
            
        }
        
        if addressDetailsPopover.street2Txt.text == "" {
            addressDetailsPopover.street2Txt.addMissingInputAnimation()
            shouldReturn = true
            
        }
        
        if addressDetailsPopover.numberTxt.text == "" {
            addressDetailsPopover.numberTxt.addMissingInputAnimation()
            shouldReturn = true
            
        }
        
        if addressDetailsPopover.referencesTxt.text == "" {
            addressDetailsPopover.referencesTxt.addMissingInputAnimation()
            shouldReturn = true
            
        }
        let strippedPhone = addressDetailsPopover.coolPhoneTxt.text?.removeWhitespace() ?? ""

        if ((MUser.sharedInstance.phone == "" || MUser.sharedInstance.phone == nil ) && (strippedPhone.characters.count <= 9)) {
            addressDetailsPopover.coolPhoneTxt.addMissingInputAnimation()
            SCLAlertView().showNotice("Atención", subTitle: "Debe introducir un número de teléfono valido Ej: 0981 123456")
            shouldReturn = true
            
        }
        
        if shouldReturn {
            return
        }
        savingAddressFromCart = true

            self.user.defaultAddress.name = addressDetailsPopover.addressNameTxt.text!
            self.user.defaultAddress.street1 = addressDetailsPopover.street1Txt.text!
            self.user.defaultAddress.street2 = addressDetailsPopover.street2Txt.text!
            self.user.defaultAddress.number = addressDetailsPopover.numberTxt.text!
            self.user.defaultAddress.reference = addressDetailsPopover.referencesTxt.text!
        

           MBProgressHUD.showHUDAddedTo(UIApplication.sharedApplication().keyWindow!, animated: true)
            self.user.defaultAddress.saveBackend(true, completion: { (success, errorMessage) in
                if success {
                    if (MUser.sharedInstance.phone == "" || MUser.sharedInstance.phone == nil ) {
                        self.user.phone = self.addressDetailsPopover.coolPhoneTxt.text
                        self.user.updateUserData(["phone": self.addressDetailsPopover.coolPhoneTxt.text ?? ""], completion: { (success, erorrMsg) in
                            if success {
                                self.finalizarPressed()
                                self.addressDetailsPopover?.dismissViewControllerAnimated(true, completion: nil)

                            }else{
                                self.savingAddressFromCart = false
                                
                                MBProgressHUD.hideHUDForView(UIApplication.sharedApplication().keyWindow!, animated: true)
                                SCLAlertView().showError("Lo sentimos!", subTitle: errorMessage ?? "Ocurrio un error inesperado!")
                            }
                        })
                    }else{
                        self.addressDetailsPopover?.dismissViewControllerAnimated(true, completion: nil)
                        self.finalizarPressed()
                    }
                    

                }else{
                    self.savingAddressFromCart = false
                    MBProgressHUD.hideHUDForView(UIApplication.sharedApplication().keyWindow!, animated: true)
                    SCLAlertView().showError("Lo sentimos!", subTitle: errorMessage ?? "Ocurrio un error inesperado!")
                }
            })
    }
    
    
    var successfulCartId: Int?
    
    
    // MARK: CashTypeAmountDelegate
    func cashTypePressed(forCash: MCash, forOrder: MOrder) {
        
        paymentMethodPopOver.dismissViewControllerAnimated(true, completion: nil)
        if forCash.type == "Ingrese el monto" {
            let alert = UIAlertController(title: "Ingrese el monto con el que desea pagar.", message: "Por favor ingrese un monto sin puntos ni comas, no debe ser menor al monto total de la orden.", preferredStyle: .Alert)
            alert.addTextFieldWithConfigurationHandler { (textField) -> Void in
                textField.placeholder = "Ej: 125000"
            }
            let aceptar = UIAlertAction(title: "Aceptar", style: .Default) { (action) -> Void in
                let monto = alert.textFields?.first?.text
                if monto?.floatValue < forOrder.orderTotalComp {
                    self.presentViewController(alert, animated: true, completion: nil)
                }else{
                    forOrder.client_cash_amount  = alert.textFields?.first?.text
                    
                    self.setPaymentInfoForOrder(forOrder, paymentMethod: forCash)
                }
                
                
            }
            let cancelar = UIAlertAction(title: "Cancelar", style: .Cancel, handler: nil)
            alert.addAction(cancelar)
            alert.addAction(aceptar)
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            //set amount
            forOrder.client_cash_amount = "\(forCash.amount ?? 0)"
            setPaymentInfoForOrder(forOrder, paymentMethod: forCash)
            
        }
    }
    
    
    // MARK: CarritoPayMethodDelegate
    var paymentMethodPopOver: CashAmountSelectionTableViewController!
    func payCashPressed(cell: CarritoPayMethodTableViewCell) {
        
        cell.payCashButton.setImage(UIImage(named: Images.botonPagoEfectivoActivo), forState: .Normal)
        cell.payCreditButton.setImage(UIImage(named: Images.botonPagoPos), forState: .Normal)
        paymentMethodPopOver = self.storyboard?.instantiateViewControllerWithIdentifier("CashAmountSelectionTableViewController") as! CashAmountSelectionTableViewController
        paymentMethodPopOver.delegate = self
        
        paymentMethodPopOver.order  = cell.order
        var orderTotal: Float = 0
        if cell.order.delivery_type == 2 {
            orderTotal = cell.order.total ?? 0
        }else{
            orderTotal = (cell.order.total ?? 0) + (cell.order.delivery_price ?? 0)
        }
        if orderTotal%100000 != 0 && orderTotal%50000 != 0 && orderTotal%20000 != 0 {
            let montoExacto = MCash()
            montoExacto.type = "Monto exacto \(orderTotal.asLocaleCurrency ?? "")"
            montoExacto.amount = orderTotal
            montoExacto.code = 0
            montoExacto.image = UIImage(named: "moneyClip")
            paymentMethodPopOver.cashPosibilities.append(montoExacto)
        }
        
        var customAmount:Float = 0
        if orderTotal%100000 > 0 {
            let cash100milQuantity = Int(orderTotal/100000 + 1)
            customAmount = Float((cash100milQuantity) * 100000)
            let customMonto = MCash()
            customMonto.amount = customAmount
            customMonto.code = 1
            customMonto.image = UIImage(named: "100mil")
            paymentMethodPopOver.cashPosibilities.append(customMonto)
            
            
        }else{
            let cash100milQuantity = orderTotal/100000
            customAmount = (cash100milQuantity) * 100000
            let customMonto = MCash()
            customMonto.amount = customAmount
            customMonto.image = UIImage(named: "100mil")
            customMonto.code = 1
            
            paymentMethodPopOver.cashPosibilities.append(customMonto)
        }
        let difference = customAmount - orderTotal
        if difference < 10000 {
            let customMontoExtra = MCash()
            customMontoExtra.amount = customAmount + 10000
            customMontoExtra.image = UIImage(named: "100mil-10mil")
            customMontoExtra.code = 1
            
            paymentMethodPopOver.cashPosibilities.append(customMontoExtra)
        }
        
        
        if orderTotal%50000 > 0 {
            let cash50milQuantity = Int(orderTotal/50000 + 1)
            if customAmount != Float((cash50milQuantity) * 50000) {
                customAmount = Float((cash50milQuantity) * 50000)
                let customMonto = MCash()
                customMonto.amount = customAmount
                customMonto.image = UIImage(named: "50mil")
                customMonto.code = 1
                
                paymentMethodPopOver.cashPosibilities.append(customMonto)
                
                
                
                
            }else{
                paymentMethodPopOver.cashPosibilities.last?.image = UIImage(named: "100mil50mil")
                
                
                //20 mil
                let difference =  orderTotal - Float(Int(orderTotal/50000)*50000)
                if difference < 20000 && difference > 10000 {
                    let customMonto = MCash()
                    customMonto.amount = Float(Int(orderTotal/50000)*50000) + 20000
                    customMonto.image = UIImage(named: "70mil")
                    customMonto.code = 1
                    paymentMethodPopOver.cashPosibilities.append(customMonto)
                    
                }
                if difference < 10000 {
                    let customMonto = MCash()
                    customMonto.amount = Float(Int(orderTotal/50000)*50000) + 10000
                    customMonto.image = UIImage(named: "60mil")
                    customMonto.code = 1
                    paymentMethodPopOver.cashPosibilities.append(customMonto)
                }
                if difference < 30000 && difference > 20000  {
                    let customMonto = MCash()
                    customMonto.amount = Float(Int(orderTotal/50000)*50000) + 30000
                    customMonto.image = UIImage(named: "80mil")
                    customMonto.code = 1
                    paymentMethodPopOver.cashPosibilities.append(customMonto)
                }
                if difference < 40000 && difference > 30000  {
                    let customMonto = MCash()
                    customMonto.amount = Float(Int(orderTotal/50000)*50000) + 40000
                    customMonto.image = UIImage(named: "90mil")
                    customMonto.code = 1
                    paymentMethodPopOver.cashPosibilities.append(customMonto)
                }
                
            }
        }else{
            let cash50milQuantity = orderTotal/50000
            customAmount = (cash50milQuantity) * 50000
            let customMonto = MCash()
            customMonto.amount = customAmount
            customMonto.image = UIImage(named: "50mil")
            customMonto.code = 1
            paymentMethodPopOver.cashPosibilities.append(customMonto)
        }
        let customAmountSelection = MCash()
        customAmountSelection.type = "Ingrese el monto"
        customAmountSelection.amount = 0
        customAmountSelection.code = 2
        customAmountSelection.image = UIImage(named: "moneyClip")
        paymentMethodPopOver.cashPosibilities.append(customAmountSelection)
        
        paymentMethodPopOver.modalPresentationStyle = .Popover
        paymentMethodPopOver.preferredContentSize = CGSizeMake(250, 200)
        
        let popoverMenuViewController = paymentMethodPopOver.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = [.Down,.Up]
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = cell.payCashButton
        popoverMenuViewController?.sourceRect = cell.payCashButton.bounds
        popoverMenuViewController?.backgroundColor = UIColor.whiteColor()
        
        presentViewController(
            paymentMethodPopOver,
            animated: true,
            completion: nil)
        
        
    }
    
    func payCreditPressed(cell: CarritoPayMethodTableViewCell) {
        
        
        cell.payCashButton.setImage(UIImage(named: Images.botonPagoEfectivo), forState: .Normal)
        cell.payCreditButton.setImage(UIImage(named: Images.botonPagoPosActivo), forState: .Normal)
        paymentMethodPopOver = self.storyboard?.instantiateViewControllerWithIdentifier("CashAmountSelectionTableViewController") as! CashAmountSelectionTableViewController
        
        //temporal
        paymentMethodPopOver.delegate = self
        let orderIP = self.tableView.indexPathForCell(cell)!
        paymentMethodPopOver.order  = self.cart.orders[orderIP.section]
        let creditoVisa = MCash()
        creditoVisa.amount = 0
        creditoVisa.type = "Credito"
        creditoVisa.code = 3
        let creditoMasterCard = MCash()
        creditoMasterCard.amount = 0
        creditoMasterCard.type = "Debito"
        creditoMasterCard.code = 3
        
        
        paymentMethodPopOver.cashPosibilities.append(creditoVisa)
        paymentMethodPopOver.cashPosibilities.append(creditoMasterCard)
        
        paymentMethodPopOver.modalPresentationStyle = .Popover
        paymentMethodPopOver.preferredContentSize = CGSizeMake(150, 100)
        
        let popoverMenuViewController = paymentMethodPopOver.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = [.Down,.Up]
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = cell.payCreditButton
        popoverMenuViewController?.sourceRect = cell.payCreditButton.bounds
        popoverMenuViewController?.backgroundColor = UIColor.whiteColor()
        
        presentViewController(
            paymentMethodPopOver,
            animated: true,
            completion: nil)
        
        
    }
    
    func wantsTicketPressed(cell: CarritoPayMethodTableViewCell) {
        
        
        
        if let orderIP = self.tableView.indexPathForCell(cell) {
            let order  = self.cart.orders[orderIP.section]
            if cell.wantsTicketBtn.tag == 0 {
                if user.invoice_ruc != nil && user.invoice_name != nil && user.invoice_ruc  != "" && user.invoice_name != "" {
                    cell.wantsTicketBtn.tag = 1
                    cell.wantsTicketBtn.setImage(UIImage(named: Images.botonFacturaActivo), forState: .Normal)
                    order.wantsTicket = true
                    order.addInvoiceData()
                    setPaymentInfoForOrder(order, paymentMethod: nil)
                }else{
                    let alert = UIAlertController(title: NSLocalizedString("Atención!",comment: "Atención"), message: NSLocalizedString("Debes tener un ruc y razon social asociados a tu cuenta si quieres factura a tu nombre. Igual puedes solicitar una factura sin estos datos.", comment: "Debes tener un ruc y razon social asociados a tu cuenta."), preferredStyle: .Alert)
                    let cancelar = UIAlertAction(title: NSLocalizedString("Cancelar", comment: "Cancelar"), style: .Cancel, handler: nil)
                    let asociarRuc = UIAlertAction(title: NSLocalizedString("Asociar RUC", comment: "Asociar RUC"), style: .Default, handler: { (action) -> Void in
                        // handle user ruc asociation
                        /*
                         let rucTxt = alert.textFields![0]
                         let razonSocial = alert.textFields![1]
                         
                         self.user.invoice_name = razonSocial.text
                         self.user.invoice_ruc = rucTxt.text
                         cell.wantsTicketBtn.tag = 1
                         cell.wantsTicketBtn.setImage(UIImage(named: Images.botonFacturaActivo), forState: .Normal)
                         order.wantsTicket = true
                         order.addInvoiceData()
                         self.setPaymentInfoForOrder(order, paymentMethod: nil)
                         */
                        self.showAsociarRucPopover(cell,order: order)
                        
                    })
                    
                    let sinAsociarRuc = UIAlertAction(title: NSLocalizedString("Pedir sin asociar RUC", comment: "Pedir sin asociar RUC"), style: .Default, handler: { (action) -> Void in
                        cell.wantsTicketBtn.tag = 1
                        cell.wantsTicketBtn.setImage(UIImage(named: Images.botonFacturaActivo), forState: .Normal)
                        order.wantsTicket = true
                        order.addInvoiceData()
                        self.setPaymentInfoForOrder(order, paymentMethod: nil)
                        
                    })
                    
                    
                    /*
                     alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
                     textField.placeholder = NSLocalizedString("RUC", comment: "RUC")
                     if self.user.invoice_ruc != nil {
                     textField.text = self.user.invoice_ruc
                     }
                     })
                     
                     alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
                     textField.placeholder = NSLocalizedString("Razón Social", comment: "Razón Social")
                     if self.user.invoice_name != nil {
                     textField.text = self.user.invoice_name
                     }
                     })
                     
                     */
                    alert.addAction(cancelar)
                    alert.addAction(sinAsociarRuc)
                    alert.addAction(asociarRuc)
                    //                    self.presentViewController(alert, animated: true, completion: nil)
                    self.showAsociarRucPopover(cell,order: order)
                    
                }
                
            }else{
                cell.wantsTicketBtn.tag = 0
                order.wantsTicket = false
                order.removeInvoiceData()
                cell.wantsTicketBtn.setImage(UIImage(named: Images.botonFactura), forState: .Normal)
                setPaymentInfoForOrder(order, paymentMethod: nil)
                
            }
            
        }
    }
    
    
    
    
}

extension CarritoViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return cart.orders.count > 0  ? cart.orders.count + 1 : 0 //+ 1 for final steps
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section < cart.orders.count {
            
            var order: MOrder!
            order = cart.orders[indexPath.section]
            
            if indexPath.row < order.products.count { //PRODUCTS
                let cell = tableView.dequeueReusableCellWithIdentifier(ReuseCell.carritoItem, forIndexPath: indexPath) as! CarritoItemTableViewCell
                
                cell.imgView.image = NO_ITEM_PLACEHOLDER
                if indexPath.row%2 == 0 {
                    cell.contentView.backgroundColor = UIColor(hexString: "#f7f2f1")
                }else{
                    cell.contentView.backgroundColor = UIColor.whiteColor()
                }
                let product = order.products[indexPath.row]
                if product.is_voucher == true {
                    cell.cambiarBtn.enabled = false
                    cell.stepper.enabled = false
                    cell.cambiarBtn.backgroundColor = UIColor.darkGrayColor()
                    cell.stepper.buttonsBackgroundColor = UIColor.lightGrayColor()
                    cell.stepper.labelBackgroundColor = UIColor.darkGrayColor()
                }else{
                    cell.cambiarBtn.enabled = true
                    cell.stepper.enabled = true
                    cell.cambiarBtn.backgroundColor = UIColor(hexString: "E0E0E0")
                    cell.stepper.buttonsBackgroundColor = UIColor(hexString: "7ED321")!
                    cell.stepper.labelBackgroundColor = UIColor(hexString: "3FA106")!
                    
                }
                if order.products[indexPath.row].attachments.count > 0 {
                    
                    order.products[indexPath.row].attachments.first?.width = cell.imageView?.bounds.width
                    order.products[indexPath.row].attachments.first?.width = cell.imageView?.bounds.height
                    if let linkString = order.products[indexPath.row].attachments.first?.imageComputedURL {
                        cell.imgView.hnk_setImageFromURL(linkString)
                        if !imageCacheKeys.contains(linkString.URLString) {
                            imageCacheKeys.append(linkString.URLString)
                        }
                        cacheFormats[linkString.URLString] = cell.imgView.hnk_format.name
                        
                    }
                }
                cell.orderTitleLbl.text = order.products[indexPath.row].name
                cell.delegate = self
                cell.orderDescrLbl.text = order.products[indexPath.row].addonsDescription
                let totalPriceWithAddons = order.products[indexPath.row].addonsTotalPrice + (order.products[indexPath.row].price ??  0)
//                let amount = (((order.products[indexPath.row].price ?? 0) + order.products[indexPath.row].userCustomizationsTotalPrice)*Float((order.products[indexPath.row].quantity ?? 0)))

                cell.totalPriceLbl.text = (totalPriceWithAddons*Float((order.products[indexPath.row].quantity ?? 1))).asLocaleCurrency
//                cell.totalPriceLbl.text = (amount).asLocaleCurrency
                cell.totalPriceLbl.text = order.products[indexPath.row].current_price?.floatValue.asLocaleCurrency
                cell.stepper.value = Double(order.products[indexPath.row].quantity ?? 0)
                cell.order = order
                if showingProductDetails {
                    cell.cambiarBtn.hidden = true
                    cell.eliminarBtn.hidden = true
                }
                cell.layoutIfNeeded()
                if showingProductDetails {
                    cell.userInteractionEnabled = false
                }
                cell.preservesSuperviewLayoutMargins = false
                cell.separatorInset = UIEdgeInsetsZero
                cell.layoutMargins = UIEdgeInsetsZero
                return cell
            }
            if indexPath.row == (order.products.count ) { //DELIVERY
                if order.delivery_type == 1 {
                    let cell = tableView.dequeueReusableCellWithIdentifier(ReuseCell.carritoDeliveryPrice, forIndexPath: indexPath) as! CarritoDeliveryPriceTableViewCell
                    cell.layoutIfNeeded()
                    if showingProductDetails {
                        cell.userInteractionEnabled = false
                    }
                    cell.order = order
                    cell.customDelegate = self
                    cell.deliveryPriceLbl.hidden = false
                    cell.deliveryTitleLbl.hidden = false
                    cell.deliveryImage.image = UIImage(named: Images.deliveryActivo)
                    let mut =  NSMutableAttributedString(string: (order.delivery_price?.asLocaleCurrency ?? "" ) + "\nEnviar a: " + MUser.sharedInstance.defaultAddress.name + "\n", attributes: [NSForegroundColorAttributeName: UIColor.darkGrayColor(), NSFontAttributeName: UIFont(name: "OpenSans-Bold", size: 15)!])
                    mut.appendAttributedString(NSAttributedString(string: MUser.sharedInstance.defaultAddress.formattedAddress, attributes: [NSForegroundColorAttributeName: UIColor.darkGrayColor(), NSFontAttributeName: UIFont(name: "OpenSans-Light", size: 13)!]))
                    cell.deliveryPriceLbl.attributedText = mut
                    cell.preservesSuperviewLayoutMargins = false
                    cell.separatorInset = UIEdgeInsetsZero
                    cell.layoutMargins = UIEdgeInsetsZero
                    return cell
                }else{ // PICKUP CELL
                    let cell = tableView.dequeueReusableCellWithIdentifier("pickupCell", forIndexPath: indexPath) as! CarritoPickupTableViewCell
                    let mut = NSMutableAttributedString()
                    
                    mut.appendAttributedString(NSAttributedString(string:order.branch.name + "\n" , attributes: [NSForegroundColorAttributeName: UIColor.darkGrayColor(), NSFontAttributeName: UIFont.boldSystemFontOfSize(15)]))
                    mut.appendAttributedString(NSAttributedString(string:order.branch.formatedAddress, attributes: [NSForegroundColorAttributeName: UIColor.lightGrayColor(), NSFontAttributeName: UIFont.boldSystemFontOfSize(12)]))
                    cell.addressLbl.attributedText = mut
                    cell.delegate = self
                    cell.branch = order.branch
                    cell.preservesSuperviewLayoutMargins = false
                    cell.separatorInset = UIEdgeInsetsZero
                    cell.layoutMargins = UIEdgeInsetsZero
                    return cell
                }
                
                
            }
            if cart.orders.count > 1 {
                if indexPath.row == (order.products.count + 1){ //SUB TOTAL
                    let cell = tableView.dequeueReusableCellWithIdentifier(ReuseCell.carritoSubTotal, forIndexPath: indexPath) as! CarritoSubTotalTableViewCell
                    var subTotal:Float = 0
                    
                    if let editedTotal = editedSubTotal {
                        cell.subTotalLbl.text = editedTotal.asLocaleCurrency
                    }else{
                        for product in order.products {
                            subTotal = subTotal + product.addonsTotalPrice + (product.price ?? 0)
                        }
                        
                        cell.subTotalLbl.text = (order.orderTotalCompCart).asLocaleCurrency
                    }
                    
                    cell.subTotalLbl.text = order.total?.asLocaleCurrency
                    
                    cell.layoutIfNeeded()
                    cell.preservesSuperviewLayoutMargins = false
                    cell.separatorInset = UIEdgeInsetsZero
                    cell.layoutMargins = UIEdgeInsetsZero
                    return cell
                }
                
                
                
                if indexPath.row == (order.products.count + 2) { // PAYMENT METHOD FOR ORDER
                    let cell = tableView.dequeueReusableCellWithIdentifier(ReuseCell.carritoPayMethod, forIndexPath: indexPath) as! CarritoPayMethodTableViewCell
                    cell.order = order
                    
                    
                    
                    if order.payMethod == .CASH {
                        cell.payCashButton.setImage(UIImage(named: Images.botonPagoEfectivoActivo), forState: .Normal)
                        cell.payCreditButton.setImage(UIImage(named: Images.botonPagoPos), forState: .Normal)
                    }
                    if order.payMethod == .CREDIT {
                        cell.payCashButton.setImage(UIImage(named: Images.botonPagoEfectivo), forState: .Normal)
                        cell.payCreditButton.setImage(UIImage(named: Images.botonPagoPosActivo), forState: .Normal)
                    }
                    if  order.payMethod == nil {
                        cell.payCreditButton.setImage(UIImage(named: Images.botonPagoPos), forState: .Normal)
                        cell.payCashButton.setImage(UIImage(named: Images.botonPagoEfectivo), forState: .Normal)
                    }
                    if order.invoice_required {
                        cell.wantsTicketBtn.setImage(UIImage(named: Images.botonFacturaActivo), forState: .Normal)
                        cell.wantsTicketBtn.tag = 1
                    }else{
                        cell.wantsTicketBtn.setImage(UIImage(named: Images.botonFactura), forState: .Normal)
                        cell.wantsTicketBtn.tag = 0
                    }
                    
                    let posEnabled = order.branch?.is_pos_enabled ?? false
                    
                    if posEnabled {
                        cell.payCreditButton.hidden = false
                    }else{
                        cell.payCreditButton.hidden = true
                    }
                    cell.delegate = self
                    cell.layoutIfNeeded()
                    cell.preservesSuperviewLayoutMargins = false
                    cell.separatorInset = UIEdgeInsetsZero
                    cell.layoutMargins = UIEdgeInsetsZero
                    return cell
                }
                
                
                
                
            }else if cart.orders.count == 1 { //no sub total cell
                
                if indexPath.row == (order.products.count + 1) { // PAYMENT METHOD FOR ORDER
                    let cell = tableView.dequeueReusableCellWithIdentifier(ReuseCell.carritoPayMethod, forIndexPath: indexPath) as! CarritoPayMethodTableViewCell
                    cell.order = order
                    if  order.payMethod == nil {
                        cell.payCreditButton.setImage(UIImage(named: Images.botonPagoPos), forState: .Normal)
                        cell.payCashButton.setImage(UIImage(named: Images.botonPagoEfectivo), forState: .Normal)
                    }
                    if order.payMethod == .CASH {
                        cell.payCashButton.setImage(UIImage(named: Images.botonPagoEfectivoActivo), forState: .Normal)
                        cell.payCreditButton.setImage(UIImage(named: Images.botonPagoPos), forState: .Normal)
                    }
                    if order.payMethod == .CREDIT {
                        cell.payCashButton.setImage(UIImage(named: Images.botonPagoEfectivo), forState: .Normal)
                        cell.payCreditButton.setImage(UIImage(named: Images.botonPagoPosActivo), forState: .Normal)
                    }
                    if order.invoice_required {
                        cell.wantsTicketBtn.setImage(UIImage(named: Images.botonFacturaActivo), forState: .Normal)
                    }else{
                        cell.wantsTicketBtn.setImage(UIImage(named: Images.botonFactura), forState: .Normal)
                    }
                    
                    let posEnabled = order.branch?.is_pos_enabled ?? false
                    
                    if posEnabled {
                        cell.payCreditButton.hidden = false
                    }else{
                        cell.payCreditButton.hidden = true
                    }
                    cell.delegate = self
                    cell.layoutIfNeeded()
                    cell.preservesSuperviewLayoutMargins = false
                    cell.separatorInset = UIEdgeInsetsZero
                    cell.layoutMargins = UIEdgeInsetsZero
                    return cell
                }
                
                
                
            }
            
            
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier(ReuseCell.carritoTotalPrice, forIndexPath: indexPath) as! CarritoTotalTableViewCell
            if let editedTotal = editedSubTotal {
                cell.total.text = editedTotal.asLocaleCurrency
            }else{
                cell.total.text = cart.totalComputedCart.asLocaleCurrency
            }
            
            cell.total.text = cart.total?.floatValue.asLocaleCurrency
            
            cell.delegate = self
            cell.layoutIfNeeded()
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsetsZero
            cell.layoutMargins = UIEdgeInsetsZero
            return cell
        }
        return UITableViewCell()
    }
    
    
    
    func seeBranchPressed(branch: MRestaurant) {
        let dvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchesOnMapViewController") as! BranchesOnMapViewController
        dvc.branches = [branch]
        Mixpanel.mainInstance().track(event: kUserPressedSeeOnMapEvent, properties: ["Pantalla":"Carrito", "Local": branch.name])
        self.navigationController?.pushViewController(dvc, animated: true)
    }
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < cart.orders.count {
            if cart.orders.count == 1 {
                return cart.orders[section].products.count + 2 //no sub total cell
            }
            return cart.orders[section].products.count + 3
        }else{
            return 1
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section < cart.orders.count {
            return 55
        }else{
            return CGFloat.min
        }
        
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    
    
    
    func configureHeaderView(section: Int) -> UIView {
        let view = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 55))
        view.backgroundColor = UIColor.whiteColor()
        //let franchiseName = cart.orders[section].first!.franchiseName
        let imgView = UIImageView(frame: CGRectMake(8, 5, 45, 45))
        imgView.layer.cornerRadius = imgView.bounds.width/2
        imgView.contentMode = .ScaleAspectFit
        imgView.clipsToBounds = true
        imgView.image = NO_BRANCH_PLACEHOLDER
        view.addSubview(imgView)
        if headerBlurOn {
            let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light)) as UIVisualEffectView
            visualEffectView.frame = imgView.bounds
            imgView.addSubview(visualEffectView)
        }
        self.cart.orders[section].branch?.franchise?.header_attachment?.width = 45
        self.cart.orders[section].branch?.franchise?.header_attachment?.height = 45
        
        if let imageURL = self.cart.orders[section].branch?.franchise?.header_attachment?.imageComputedURL {
            imgView.hnk_setImageFromURL(imageURL, placeholder: NO_BRANCH_PLACEHOLDER)
            if !imageCacheKeys.contains(imageURL.URLString) {
                imageCacheKeys.append(imageURL.URLString)
            }
            cacheFormats[imageURL.URLString] = imgView.hnk_format.name
            
        }
        
        let lbl = UILabel(frame: CGRectMake(61, 0, tableView.frame.size.width - 200, 55))
        lbl.numberOfLines = 2
        lbl.lineBreakMode = .ByWordWrapping
        if self.cart.orders[section].delivery_type == 1 {
            if let franchiseName = self.cart.orders[section].branch?.franchise?.name {
                lbl.text = franchiseName.uppercaseString
            }
        }else{
            if let branchName = self.cart.orders[section].branch?.name {
                lbl.text = branchName.uppercaseString
            }
        }
        lbl.textColor = UIColor.grayColor()
        view.addSubview(lbl)
        let delBtn = UIButton(frame: CGRectMake(tableView.frame.size.width - 100,15,95,25))
        delBtn.setTitle("ELIMINAR TODOS", forState: .Normal)
        delBtn.titleLabel?.font = UIFont(name: "OpenSans-Bold", size: 9)
        delBtn.setTitleColor(UIColor.redColor(), forState: .Normal)
        view.addSubview(delBtn)
        delBtn.tag = section
        delBtn.addTarget(self, action: #selector(CarritoViewController.deleteOrder(_:)), forControlEvents: .TouchUpInside)
        lbl.numberOfLines = 2
        lbl.font = UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15)
        view.layer.shadowColor = UIColor.blackColor().CGColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSizeZero
        view.layer.shadowRadius = 10
        return view
    }
    
    
    
    func deleteOrder(sender: UIButton) {
        if cart.orders.count > sender.tag {
            let order = cart.orders[sender.tag]
            
            
            do {
                reachability = try Reachability.reachabilityForInternetConnection()
                if !(reachability?.isReachable() ?? false){
                    return
                }
            } catch {
                print("Unable to create Reachability")
            }
            
            
            //animation
            let parameters:[String:AnyObject] = ["order_id":order.order_id!,"comments":"Borrado por usuario"]
            
            
            self.cart.orders.removeAtIndex(sender.tag)
            
            
            CATransaction.begin()
            
            CATransaction.setCompletionBlock({
                
                self.tableView.reloadData()
                
            })
            
            if  self.cart.orders.count == 0 {
                self.tableView.beginUpdates()
                let range = NSMakeRange(0, 2)
                let indexSetToDelete = NSIndexSet(indexesInRange: range)
                self.tableView.deleteSections(indexSetToDelete, withRowAnimation: .Automatic)
                self.tableView.endUpdates()
            }else{
                self.tableView.beginUpdates()
                self.tableView.deleteSections(NSIndexSet(index: sender.tag), withRowAnimation: .Automatic)
                if self.cart.orders.count == 1 {
                    self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: 2, inSection: 0)], withRowAnimation: .None)
                    self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: 2, inSection: 1)], withRowAnimation: .None)
                }
                self.tableView.reloadSections(NSIndexSet(index: self.cart.orders.count + 1), withRowAnimation: .Fade)// total cell
                self.tableView.endUpdates()
                
            }
            
            
            CATransaction.commit()
            
            //        if  self.cart.orders.count == 0 {
            //            self.tableView.beginUpdates()
            //            let range = NSMakeRange(0, 2)
            //            let indexSetToDelete = NSIndexSet(indexesInRange: range)
            //            self.tableView.deleteSections(indexSetToDelete, withRowAnimation: .Automatic)
            //            self.tableView.endUpdates()
            //        }else{
            //            self.tableView.beginUpdates()
            //            self.tableView.deleteSections(NSIndexSet(index: sender.tag), withRowAnimation: .Automatic)
            //
            //
            //            if self.cart.orders.count == 1 {
            //                self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: 2, inSection: 0)], withRowAnimation: .None)
            //                self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: 2, inSection: 1)], withRowAnimation: .None)
            //            }
            ////            self.tableView.reloadSections(NSIndexSet(index: self.cart.orders.count + 1), withRowAnimation: .Fade)// total cell
            //            self.tableView.reloadSections(NSIndexSet(indexesInRange: NSRange(location: 0, length: self.cart.orders.count + 1)), withRowAnimation: .Automatic)
            //            self.tableView.endUpdates()
            //
            //
            //        }
            
            
            let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            api.jsonRequest(.DELETE_ORDER, parameters: parameters) { (json, error) in
                Mixpanel.mainInstance().track(event: kUserDeletedCartOrder, properties: parameters )
                
                hud.hideAnimated(true)
                if debugModeEnabled {
                    print(#function, "called with parameters \(parameters)", json, error)
                }
                if error == nil {
                    self.reflectBadgeCart()
                }else{
                    self.getOrders()
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        if section < cart.orders.count {
            let view = configureHeaderView(section)
            currentHeaderView = view
            return view
        }else{
            return nil
        }
        
    }
    
    
    // MARK: PopOverDelegate
    
    func adaptivePresentationStyleForPresentationController(
        controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    
    
    // MARK: CarritoTotalDelegate
    
    func changeAddrPressed(sender: UIButton){
        //present address change popup
        let alert = UIAlertController(title: NSLocalizedString("Atención!", comment: "Atencion"), message: NSLocalizedString("Si cambias de dirección, tu carrito sera vaciado completamente.", comment: "Si cambias de dirección, todas tus ordenes que estan marcadas como delivery seran enviadas a la nueva dirección que selecciones."), preferredStyle: .Alert)
        
        let cambiar = UIAlertAction(title: NSLocalizedString("Cambiar", comment: "Cambiar"), style: .Default) { (action) -> Void in
            
            let addressPopover = self.storyboard?.instantiateViewControllerWithIdentifier("AddressesPopOverCollectionViewController") as! AddressesPopOverCollectionViewController
            addressPopover.modalPresentationStyle = .Popover
            addressPopover.preferredContentSize = CGSizeMake(200, 80)
            
            let popoverMenuViewController = addressPopover.popoverPresentationController
            popoverMenuViewController?.permittedArrowDirections = .Down
            popoverMenuViewController?.delegate = self
            popoverMenuViewController?.sourceView = sender
            popoverMenuViewController?.sourceRect = sender.bounds
            popoverMenuViewController?.backgroundColor = UIColor(hexString: "#DA1214")
            
            self.presentViewController(
                addressPopover,
                animated: true,
                completion: nil)
            
            
        }
        
        let cancelar = UIAlertAction(title: NSLocalizedString("Cancelar", comment: "Cancelar"), style: .Cancel, handler: nil)
        alert.addAction(cancelar)
        alert.addAction(cambiar)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func logProducts(){
        for order in self.cart.orders {
            for product in order.products {
                var productData: [String:AnyObject] = [:]

                productData["Producto"] = product.name!
                productData["Precio Unitario"] = product.price ?? 0
                productData["Precio Delivery"] = order.delivery_price ?? 0
                productData["Cantidad"] = product.quantity ?? 0
                productData["Modo"] = order.delivery_type == 1 ? "Delivery" : "Pickup"
                productData["Producto ID"] = product.id ?? 0
                productData["Franquicia"] = order.branch.franchise?.name ?? ""
                productData["Franquicia ID"] = order.branch.franchise?.id ?? 0
                productData["Local"] = order.branch?.name ?? ""
                productData["Local ID"] = order.branch?.id ?? 0
                productData["Orden ID"] = order.order_id ?? 0
                Answers.logPurchaseWithPrice(NSDecimalNumber(string: "\(product.price ?? 0)"),
                                             currency: "PYG",
                                             success: true,
                                             itemName: product.name ?? "",
                                             itemType: "Producto",
                                             itemId: "\(product.id ?? 0)",
                                             customAttributes: ["Cantidad":product.quantity ?? 0])
                Mixpanel.mainInstance().track(event: kUserPurchasedProductEvent, properties: productData)

            }
        }
        
        Mixpanel.mainInstance().people.trackCharge(amount: Double(cart.totalComputedCart))
        
        
         
       
        
    }
    
    func finalizarPressed() {
        if user.isTemporal {
            self.performSegueWithIdentifier(Segues.registerPopover, sender: self)
            return
        }
        self.savingAddressFromCart = false
        var ordersHavePaymentMethod: Bool = true
        for order in self.cart.orders {
            if order.client_cash_amount == "0" {
                ordersHavePaymentMethod = false
            }
        }
        if ordersHavePaymentMethod {
            
          
            if user.defaultAddress.needsToFillDetails() || MUser.sharedInstance.phone == "" {
                self.performSegueWithIdentifier(Segues.fillAddressDetails, sender: self)
                return
            }
            
            
            
            
            MBProgressHUD.hideHUDForView(UIApplication.sharedApplication().keyWindow!, animated: true)
            createWaitScreen("Finalizando..", comment: "Finalizando una orden")
            
            api.jsonRequest(MAPI.Command.CONFIRM_CART, parameters: nil) { (json, error) -> Void in
                if debugModeEnabled {
                    print(#function,json,error)
                }
                self.dismissWaitScreen()
                if error == nil {
                    if let success = json?["success"].bool {
                        if success {
                            self.successfulCartId = self.cart.id
                            self.performSegueWithIdentifier("thankyouSegue", sender: self)
                            self.logProducts()
                            self.cart.orders = []
                            NSNotificationCenter.defaultCenter().postNotificationName(kCartUpdatedNotification, object: nil)
//                            self.tableView.beginUpdates()
//                            self.tableView.deleteSections(NSIndexSet(indexesInRange: range), withRowAnimation: .Automatic)
//                            self.tableView.endUpdates()
                            self.tableView.reloadData()
                            NSNotificationCenter.defaultCenter().postNotificationName("updateOrderStateNotification", object: nil)
                            
                        }else{ // error from the BE
                            let errorBE = json?["data"].string ?? ""
                            SCLAlertView().showWarning("Lo sentimos!", subTitle: errorBE)
                            
                        }
                    }
                }else{ //network error
                    if debugModeEnabled {
                        SCLAlertView().showError("Lo sentimos!", subTitle: error ?? "Sin error")
                    }else{
                        SCLAlertView().showError("Lo sentimos!", subTitle: "Ocurrio un error inesperado!.")
                    }
                }
            }
        }else{ // need payment method for orders
            let alert = UIAlertController(title: "Faltan datos para completar tu orden.", message: "Necesitamos que completes los datos de pago para tus ordenes.", preferredStyle: .Alert)
            let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
            alert.addAction(ok)
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
    }
    
    func deleteProduct(order: MOrder, productIndexPath: NSIndexPath) {
        
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
            if !(reachability?.isReachable() ?? false){
                return
            }
        } catch {
            print("Unable to create Reachability")
        }
        
        //animation
        let parameters = ["order_id":order.order_id!,"order_product_id":order.products[productIndexPath.row].order_product_id ?? 0]
        let cell = self.tableView.cellForRowAtIndexPath(productIndexPath) as! CarritoItemTableViewCell
        
        UIView.animateWithDuration(0.3, delay: 0, options: [.CurveEaseOut], animations: {
            cell.layer.transform = CATransform3DMakeRotation(20, 100, 0, 20)
        }) { (fin) in
            UIView.animateWithDuration(0.35, delay: 0, options: [.CurveEaseOut], animations: {
                cell.layer.transform = CATransform3DMakeTranslation(self.tableView.bounds.width, 0,0)
                cell.layer.opacity = 0
            }) { (fin) in
                
                //                print(productIndexPath.row,self.cart.orders[productIndexPath.section].products.count)
                //                self.cart.orders[productIndexPath.section].products.removeAtIndex(productIndexPath.row )
                //
                //                if self.cart.orders[productIndexPath.section].products.count == 0  {
                //                    self.cart.orders.removeAtIndex(productIndexPath.section)
                //                    if  self.cart.orders.count == 0 {
                //                        self.tableView.beginUpdates()
                //                        let range = NSMakeRange(0, 2)
                //                        let indexSetToDelete = NSIndexSet(indexesInRange: range)
                //                        self.tableView.deleteSections(indexSetToDelete, withRowAnimation: .Automatic)
                //                        self.tableView.endUpdates()
                //                    }else{
                //                        self.tableView.beginUpdates()
                //
                //                        print("Will call reload sections for sections ",self.tableView.numberOfSections)
                //
                //
                //                        self.tableView.reloadSections(NSIndexSet(index: self.tableView.numberOfSections), withRowAnimation: .Fade)// total cell
                //                        print("Will call delete sections for sections ",productIndexPath.section)
                //
                //                        self.tableView.deleteSections(NSIndexSet(index: productIndexPath.section), withRowAnimation: .Automatic)
                //                        if self.cart.orders.count == 1 {
                //                            self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: 2, inSection: 0)], withRowAnimation: .None)
                //                            self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: 2, inSection: 1)], withRowAnimation: .None)
                //                        }
                //
                //
                //
                //                        self.tableView.endUpdates()
                ////                        self.tableView.reloadData()
                //
                //                    }
                //                }else {
                //                    self.tableView.beginUpdates()
                //                    self.tableView.deleteRowsAtIndexPaths([productIndexPath], withRowAnimation: .None)
                //                    self.tableView.reloadSections(NSIndexSet(index: productIndexPath.section), withRowAnimation: .Automatic)
                //                    self.tableView.reloadSections(NSIndexSet(index: self.orders.count + 1), withRowAnimation: .Fade)// total cell
                //
                //                    self.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: self.cart.orders[productIndexPath.section].products.count + 2, inSection: productIndexPath.section)] , withRowAnimation: .Fade)
                //                    self.tableView.endUpdates()
                //
                //
                //                }
                //                NSNotificationCenter.defaultCenter().postNotificationName(kCartUpdatedNotification, object: order)
                //
                //
            }
            
            
            
        }
        
        
        //        let hud =  MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        //        hud.labelText = NSLocalizedString("Eliminando..", comment: "Eliminando..")
        
        
        
        Answers.logCustomEventWithName("Usuario elimino un producto del Carrito",
                                       customAttributes: ["Producto": order.products[productIndexPath.row].name!])
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        hud.label.text = "Eliminando.."
        
        api.jsonRequest(.DELETE_PRODUCT_CART, parameters: parameters) { (json, error) -> Void in
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            
            if debugModeEnabled {
                print(#function,"called with parameters \(parameters)",json,error)
            }
            //            hud.hide(true)
            cell.layer.opacity = 1
            cell.layer.transform = CATransform3DIdentity
            if error == nil {
                if let success = json?["success"].bool {
                    if success {
                        Mixpanel.mainInstance().track(event: kUserDeletedProductCartEvent, properties: ["Producto ID": order.products[productIndexPath.row].name!, "Producto": order.products[productIndexPath.row].name!])
                        
                        if let jsonData = json?["data"] {
                            self.cart.loadCart(jsonData)
                            self.tableView.reloadData()
                        }
                        NSNotificationCenter.defaultCenter().postNotificationName(kCartUpdatedNotification, object: order)
                        //DEPRECATED
                        //                        self.cart.orders[productIndexPath.section].products.removeAtIndex(productIndexPath.row)
                        //                        if self.cart.orders[productIndexPath.section].products.count == 0  {
                        //                            self.cart.orders.removeAtIndex(productIndexPath.section)
                        //                            if  self.cart.orders.count == 0 {
                        //                                self.tableView.beginUpdates()
                        //                                let range = NSMakeRange(0, 2)
                        //                                let indexSetToDelete = NSIndexSet(indexesInRange: range)
                        //                                self.tableView.deleteSections(indexSetToDelete, withRowAnimation: .Fade)
                        //                                self.tableView.endUpdates()
                        //                            }else{
                        //                                self.tableView.beginUpdates()
                        //                                self.tableView.deleteSections(NSIndexSet(index: productIndexPath.section), withRowAnimation: .Fade)
                        //
                        //
                        //                                if self.cart.orders.count == 1 {
                        //                                    self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: 2, inSection: 0)], withRowAnimation: .Automatic)
                        //                                    self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: 2, inSection: 1)], withRowAnimation: .Automatic)
                        //                                }
                        //                                self.tableView.endUpdates()
                        //                            }
                        //                        }else {
                        //                            self.tableView.beginUpdates()
                        //                            self.tableView.deleteRowsAtIndexPaths([productIndexPath], withRowAnimation: .Fade)
                        //                            self.tableView.reloadSections(NSIndexSet(index: productIndexPath.section), withRowAnimation: .Fade)
                        //                            self.tableView.endUpdates()
                        //                        }
                        //                        NSNotificationCenter.defaultCenter().postNotificationName(kCartUpdatedNotification, object: order)
                        
                    }else{ // error from the BE
                        //                        let errorBE = json?["data"].string ?? "No error message"
                        //                        let titleLocalizable = NSLocalizedString("Error",comment: "error")
                        //                        let alert = UIAlertController(title: titleLocalizable, message: errorBE, preferredStyle: .Alert)
                        //                        let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                        //                        alert.addAction(ok)
                        //                        self.presentViewController(alert, animated: true, completion: nil)
                        self.tableView.reloadData()
                    }
                }
            }else{ //network error
                self.tableView.reloadData()
                
                //
                //                let titleLocalizable = NSLocalizedString("Error",comment: "error")
                //                let alert = UIAlertController(title: titleLocalizable, message: error, preferredStyle: .Alert)
                //                let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                //                alert.addAction(ok)
                //                self.presentViewController(alert, animated: true, completion: nil)
            }
            self.deletingProduct = false
            
        }
        
        
    }
    
    // MARK: CartOrderChangeDeleteProtocol
    func didPressDeleteOrder(forOrder order: MOrder, cell: CarritoItemTableViewCell) {
        if !deletingProduct {
            deletingProduct = true
            if let indexPath = self.tableView.indexPathForCell(cell) {
                deleteProduct(order, productIndexPath: indexPath)
            }
        }
    }
    
    func didPressChangeOrder(forOrder order: MOrder, cell: CarritoItemTableViewCell) {
        if let indexPath = self.tableView.indexPathForCell(cell) {
            let product = order.products[indexPath.row]
            let branch = order.branch
            product.order_id = order.order_id
            if order.delivery_type == 1 {
                order.branch.mode = 0
            }else{
                order.branch.mode = 1
            }
            let dic:[String:AnyObject] = ["product":product,"branch":branch]
            self.performSegueWithIdentifier(Segues.editProduct, sender: dic)
        }
    }
    
    
    
    func didChangeQuantityForOrder(forOrder order: MOrder, cell: CarritoItemTableViewCell, sender: GMStepper) {
        if let productIndexPath = tableView.indexPathForCell(cell) {
            changingQuantityTimer?.invalidate()
            changingQuantityTimer = NSTimer.scheduledTimerWithTimeInterval(0.4, target: self, selector: #selector(CarritoViewController.refreshQuantityTimer(_:)), userInfo: ["order":order,"cell":cell,"indexPath":productIndexPath], repeats: false)
        }
    }
    
    func refreshQuantityTimer(sender: NSTimer){
        let userInfo = sender.userInfo as! [String:AnyObject]
        let order = userInfo["order"] as! MOrder
        let cell = userInfo["cell"] as! CarritoItemTableViewCell
        let indexPath = userInfo["indexPath"] as! NSIndexPath
        updateQuantityOrder(forOrder: order,cell: cell, indexPath: indexPath)
    }
    
    func updateQuantityOrder(forOrder order: MOrder, cell: CarritoItemTableViewCell, indexPath: NSIndexPath){
        if cell.stepper?.value != Double(order.products[indexPath.row].quantity!) {
            
            if let keywindow = UIApplication.sharedApplication().delegate?.window {
            let hud = MBProgressHUD.showHUDAddedTo(keywindow!, animated: true)
                hud.label.text = NSLocalizedString("Actualizando cantidad..", comment: "Actualizando cantidad..")
            }
            let quantity = cell.stepper.value
            let parameters = ["product_id":order.products[indexPath.row].id!,"order_product_id":order.products[indexPath.row].order_product_id!,"quantity":quantity,"order_id":order.order_id!]
            
            api.jsonRequest(MAPI.Command.EDIT_PRODUCT_QUANTITY_CART, parameters: parameters) { (json, error) -> Void in
                self.editedSubTotal = nil
                self.editedTotal = nil
                if debugModeEnabled {
                    print(#function,"called with parameters \(parameters)",json,error)
                }
                if error == nil {
                    if let success = json?["success"].bool {
                        if success {
                            Answers.logCustomEventWithName("Usuario cambio cantidad desde carrito", customAttributes: [:])
                            Mixpanel.mainInstance().track(event: kUserChangedProductQuantityCartEvent, properties: ["Producto ID": order.products[indexPath.row].id!, "Producto": order.products[indexPath.row].name!,"Cantidad":quantity, "Pantalla": "Carrito"])
                            self.getOrders()
                            //                            order.products![indexPath.row].quantity = Int(quantity ?? 1)
                            //                            self.tableView.reloadData()
                            //                                                        NSNotificationCenter.defaultCenter().postNotificationName(kCartUpdatedNotification, object: order)
                            
                            
                        }else{ // error from the BE
                            if let keywindow = UIApplication.sharedApplication().delegate?.window {
                                MBProgressHUD.hideHUDForView(keywindow!, animated: true)
                            }

                            
                            cell.stepper.value = Double(order.products[indexPath.row].quantity!)
                            let errorBE = json?["data"].string ?? "No error message"
                            let titleLocalizable = NSLocalizedString("Error",comment: "error")
                            let alert = UIAlertController(title: titleLocalizable, message: errorBE, preferredStyle: .Alert)
                            let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                            alert.addAction(ok)
                            self.presentViewController(alert, animated: true, completion: nil)
                            
                        }
                    }
                }else{ //network error
                    if let keywindow = UIApplication.sharedApplication().delegate?.window {
                        MBProgressHUD.hideHUDForView(keywindow!, animated: true)
                    }
                    cell.stepper.value = Double(order.products[indexPath.row].quantity!)
                    
                    let titleLocalizable = NSLocalizedString("Error",comment: "error")
                    let alert = UIAlertController(title: titleLocalizable, message: error, preferredStyle: .Alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                    alert.addAction(ok)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                
                
            }
        }
    }
    
    // MARK: EmptyDataSetSource
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        let image = UIImage(named: "carrito-vacio")
        return image
    }
    
    func imageAnimationForEmptyDataSet(scrollView: UIScrollView!) -> CAAnimation! {
        
        let animation = CABasicAnimation(keyPath: "transform")
        
        animation.fromValue = NSValue(CATransform3D: CATransform3DIdentity)
        animation.toValue = NSValue(CATransform3D:CATransform3DMakeRotation(CGFloat(M_PI_2), 0.0, 0.0, 1.0))
        
        
        
        animation.duration = 0.25
        animation.cumulative = true
        animation.repeatCount = 1
        
        return animation
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let text = NSMutableAttributedString()
        let emptyCart = NSAttributedString(string: NSLocalizedString("Tu carrito está vacio.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
        
        text.appendAttributedString(emptyCart)
        return text
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let subTitle = NSAttributedString(string: NSLocalizedString("Seria lindo empezar a llenarlo!", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(14),NSForegroundColorAttributeName:UIColor.whiteColor()])
        return subTitle
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAnimateImageView(scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    
    // MARK: CarritoDeliveryPickupDelegate
    
    func didSwitchPickup (forOrder: MOrder, wantsPickup: Bool, cell:CarritoDeliveryPriceTableViewCell) {
        
        if wantsPickup {
            
            branchSelectionPopOver = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
            branchSelectionPopOver.delegate = self
            //            branchSelectionPopOver.franchise = forOrder.branch.franchise
            branchSelectionPopOver.order = forOrder
            branchSelectionPopOver.modalPresentationStyle = .Popover
            branchSelectionPopOver.preferredContentSize = CGSizeMake(self.view.bounds.width, self.view.bounds.height*0.7)
            
            //dim the background
            
            dimm = UIView(frame: (UIApplication.sharedApplication().keyWindow!.frame))
            
            dimm.backgroundColor = UIColor(hexString: "#000000")!
            UIApplication.sharedApplication().keyWindow?.addSubview(dimm)
            dimm.alpha = 0
            UIView.animateWithDuration(0.2, animations: { () -> Void in
                self.dimm.alpha = 0.7
            })
            
            let popoverMenuViewController = branchSelectionPopOver.popoverPresentationController
            popoverMenuViewController?.permittedArrowDirections = .Down
            popoverMenuViewController?.delegate = self
            popoverMenuViewController?.backgroundColor = UIColor.whiteColor()
            
            presentViewController(
                branchSelectionPopOver,
                animated: true,completion: nil)
        }else{
            setDeliveryPickup(wantsPickup,order: forOrder, branch: forOrder.branch)
        }
        
    }
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.dimm?.alpha = 0
            
        }) { (fin) -> Void in
            self.dimm?.removeFromSuperview()
            
        }
        self.resetPickupSwitch = true
        self.tableView.reloadData()
        self.resetPickupSwitch = false
    }
    
    // MARK: BranchSelectionDelegate
    
    func didSelectBranch(branch: MRestaurant, order: MOrder?) {
        
        
        
    }
    
    
    func setDeliveryPickup(wantsPickup: Bool,order: MOrder,branch: MRestaurant) {
        var parameters: [String:AnyObject]!
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "Y-m-d H:m:s"
        if !wantsPickup {
            parameters = ["order_id":order.order_id!,"delivery_type":1,"address_id":user.defaultAddress.id]
        }else{
            parameters = ["order_id":order.order_id!,"delivery_type": 2,"branch_id":order.branch!.id!]
        }
        api.jsonRequest(.SET_DELIVERY_INFO, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters \(parameters)",json,error)
            }
            if error == nil{
                if let success = json?["success"].bool {
                    if success {
                        if let orderSection = self.cart.orders.indexOf({$0.order_id == order.order_id}){
                            if !wantsPickup {
                                self.cart.orders[orderSection].delivery_type = 1
                                if let brJson = json?["data"]["branch"] {
                                    let newBranch = MRestaurant(data: brJson)
                                    
                                    self.cart.orders[orderSection].branch = newBranch
                                }else{
                                    self.cart.orders[orderSection].branch = branch
                                    
                                }
                            }else{
                                self.cart.orders[orderSection].delivery_type = 2
                                self.cart.orders[orderSection].branch = branch
                            }
                            
                            //                            self.currentHeaderView = self.configureHeaderView(orderSection)
                            let indexPath = NSIndexPath(forRow: (order.products.count ?? 0), inSection: orderSection)
                            let subTotal = NSIndexPath(forRow: order.products.count + 1, inSection: orderSection)
                            self.tableView.beginUpdates()
                            self.tableView.reloadRowsAtIndexPaths([indexPath,subTotal], withRowAnimation: .Automatic)
                            self.tableView.endUpdates()
                        }
                        
                        
                    }else{
                        if let orderSection = self.cart.orders.indexOf({$0.order_id == order.order_id}){
                            let indexPath = NSIndexPath(forRow: (order.products.count ?? 0), inSection: orderSection)
                            let subTotal = NSIndexPath(forRow: order.products.count + 1, inSection: orderSection)
                            self.tableView.beginUpdates()
                            self.tableView.reloadRowsAtIndexPaths([indexPath,subTotal], withRowAnimation: .Automatic)
                            self.tableView.endUpdates()
                            
                        }
                        let message = json?["data"].string ?? "No hay mensaje de error disponible."
                        let alert = UIAlertController(title: NSLocalizedString("Lo sentimos!", comment: "Lo sentimos!"), message:  NSLocalizedString(message, comment: "Ha ocurrido un error al procesar tu pedido!"), preferredStyle: .Alert)
                        let aceptar = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Cancel, handler: nil)
                        alert.addAction(aceptar)
                        self.presentViewController(alert, animated: true, completion: nil)
                        
                        
                    }
                }
            }else{
                
                if self == self.navigationController?.visibleViewController {
                    
                    let alert = UIAlertController(title: NSLocalizedString("Lo sentimos!", comment: "Lo sentimos!"), message:  NSLocalizedString("Ha ocurrido un error al procesar tu pedido!", comment: "Ha ocurrido un error al procesar tu pedido!"), preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
                if let orderSection = self.cart.orders.indexOf({$0.order_id == order.order_id}){
                    self.tableView.beginUpdates()
                    let indexPath = NSIndexPath(forRow: (order.products.count ?? 0), inSection: orderSection)
                    self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                    self.tableView.endUpdates()
                    
                }
            }
        }
    }
    
    func showAsociarRucPopover(cell: CarritoPayMethodTableViewCell,order: MOrder){
        //dim the background
        
        dimm = UIView(frame: (UIApplication.sharedApplication().keyWindow!.frame))
        
        dimm.backgroundColor = UIColor(hexString: "#000000")!
        UIApplication.sharedApplication().keyWindow?.addSubview(dimm)
        dimm.alpha = 0
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.dimm.alpha = 0.7
        })
        
        let asociarRucPopover = self.storyboard?.instantiateViewControllerWithIdentifier("FillMissingDetailsTableViewController") as! FillMissingDetailsTableViewController
        asociarRucPopover.cell = cell
        asociarRucPopover.parentController = self
        asociarRucPopover.order = order
        asociarRucPopover.modalPresentationStyle = .Popover
        asociarRucPopover.preferredContentSize = CGSizeMake(self.view.bounds.width - 50, self.view.bounds.height/2)
        
        let popoverMenuViewController = asociarRucPopover.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = .Down
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = self.view
        popoverMenuViewController?.sourceRect = CGRectMake(CGRectGetMidX(self.view.bounds), self.view.bounds.height - 50,0,0)
        
        self.presentViewController(
            asociarRucPopover,
            animated: true,
            completion: nil)
        
    }
    
    
    
    @IBAction func unwindFromMissingDetails(segue: UIStoryboardSegue){
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.dimm?.alpha = 0
            
        }) { (fin) -> Void in
            self.dimm?.removeFromSuperview()
            
        }
        
    }
    
    
    
    
}
