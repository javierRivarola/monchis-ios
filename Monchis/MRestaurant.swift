//
//  MRestaurant.swift
//  Monchis
//
//  Created by jrivarola on 9/8/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation


class MRestaurant: NSObject {
    
    var id: Int?
    var name: String = ""
    var menu: [MMenu] = []
    var details: String = ""
    var lat: String = ""
    var lng: String = ""
    var franchise: MFranchise?
    var image: UIImage?
    var category: String =  ""
    var delivery_delay: Int = 0
    var delivery_available: Bool?
    var delivery_gps_tracked: Bool?
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: Double(self.lat.floatValue), longitude: Double(self.lng.floatValue))
    }
    var franchise_id: Int?
    var house_number: String?
    var city_id: Int?
    var geo_location: Bool?
    var is_open:Bool?
    var delivery_price: Float?
    var contact_name:String?
    var attachment: MAttachment?
    var header_attachment: MAttachment?
    var street1: String?
    var street2:String?
    var email: String?
    var timeTables: [MTimeTable] = []
    var is_pos_enabled: Bool?
    var phone: String?
    var distance: String?
    var is_pickup_enabled: Bool?
    var is_delivery_enabled: Bool?
    var list_order: Int?
    var order_minimum:String = ""
    var mode: Int = 0 // Delivery default, 1 for pickup, 2 for pickup location change
    func isClosed () -> Bool {
        if let currentDay = getDayOfWeek() {
            let timeTableDays = timeTables.filter { (table) -> Bool in
                return table.day == currentDay
            }
            for timeTableDay in timeTableDays {
                let formatter = NSDateFormatter()
                formatter.dateFormat = "HH:mm:ss"
                
                var openTime = formatter.dateFromString(timeTableDay.from ?? "")
                var closeTime = formatter.dateFromString(timeTableDay.to ?? "")
                
                let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
                
                
                let componentsNow = calendar.components(([.Day, .Month, .Year,.Hour,.Minute,.Second]), fromDate: getCurrentLocalDate())
                let componentsOpenTime = calendar.components(([.Hour,.Minute,.Second]), fromDate: openTime ?? NSDate())
                let componentsCloseTime = calendar.components(([.Hour,.Minute,.Second]), fromDate: closeTime ?? NSDate())
                componentsCloseTime.timeZone = NSTimeZone(name: "America/Asuncion")
                componentsOpenTime.timeZone = NSTimeZone(name: "America/Asuncion")
                componentsOpenTime.day = componentsNow.day
                componentsOpenTime.month = componentsNow.month
                componentsOpenTime.year = componentsNow.year
                
                componentsCloseTime.day = componentsNow.day
                componentsCloseTime.month = componentsNow.month
                componentsCloseTime.year = componentsNow.year
                openTime = calendar.dateFromComponents(componentsOpenTime)
                closeTime = calendar.dateFromComponents(componentsCloseTime)
                if timeTableDay.from != "00:00:00" && timeTableDay.to != "00:00:00" {
                    if let openTime = openTime, closeTime = closeTime {
                        if getCurrentLocalDate().isBetween(date: openTime,andDate: closeTime) {
                            return false
                        }
                    }
                }
            }
        }
        return true
    }
    func isOpen () -> Bool {
        if let currentDay = getDayOfWeek() {
            let timeTableDays = timeTables.filter { (table) -> Bool in
                return table.day == currentDay
            }
            for timeTableDay in timeTableDays {
                let formatter = NSDateFormatter()
                formatter.dateFormat = "HH:mm:ss"
                
                var openTime = formatter.dateFromString(timeTableDay.from ?? "")
                var closeTime = formatter.dateFromString(timeTableDay.to ?? "")
                
                let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
                
                
                let componentsNow = calendar.components(([.Day, .Month, .Year,.Hour,.Minute,.Second]), fromDate: getCurrentLocalDate())
                let componentsOpenTime = calendar.components(([.Hour,.Minute,.Second]), fromDate: openTime ?? NSDate())
                let componentsCloseTime = calendar.components(([.Hour,.Minute,.Second]), fromDate: closeTime ?? NSDate())
                componentsCloseTime.timeZone = NSTimeZone(name: "America/Asuncion")
                componentsOpenTime.timeZone = NSTimeZone(name: "America/Asuncion")
                componentsOpenTime.day = componentsNow.day
                componentsOpenTime.month = componentsNow.month
                componentsOpenTime.year = componentsNow.year
                
                componentsCloseTime.day = componentsNow.day
                componentsCloseTime.month = componentsNow.month
                componentsCloseTime.year = componentsNow.year
                openTime = calendar.dateFromComponents(componentsOpenTime)
                closeTime = calendar.dateFromComponents(componentsCloseTime)
                if timeTableDay.from != "00:00:00" && timeTableDay.to != "00:00:00" {
                    if let openTime = openTime, closeTime = closeTime {
                        if getCurrentLocalDate().isBetween(date: openTime,andDate: closeTime) {
                            return true
                        }
                    }
                }
            }
        }
        return false
    }
    
    
    func isClosedToday() -> Bool{
        if let currentDay = getDayOfWeek() {
            let timeTableDays = timeTables.filter { (table) -> Bool in
                return table.day == currentDay
            }
            for timeTableDay in timeTableDays {
                let formatter = NSDateFormatter()
                formatter.dateFormat = "HH:mm:ss"
                
                var openTime = formatter.dateFromString(timeTableDay.from ?? "")
                var closeTime = formatter.dateFromString(timeTableDay.to ?? "")
                
                let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
                
                
                let componentsNow = calendar.components(([.Day, .Month, .Year,.Hour,.Minute,.Second]), fromDate: getCurrentLocalDate())
                let componentsOpenTime = calendar.components(([.Hour,.Minute,.Second]), fromDate: openTime ?? NSDate())
                let componentsCloseTime = calendar.components(([.Hour,.Minute,.Second]), fromDate: closeTime ?? NSDate())
                componentsCloseTime.timeZone = NSTimeZone(name: "America/Asuncion")
                componentsOpenTime.timeZone = NSTimeZone(name: "America/Asuncion")
                componentsOpenTime.day = componentsNow.day
                componentsOpenTime.month = componentsNow.month
                componentsOpenTime.year = componentsNow.year
                
                componentsCloseTime.day = componentsNow.day
                componentsCloseTime.month = componentsNow.month
                componentsCloseTime.year = componentsNow.year
                openTime = calendar.dateFromComponents(componentsOpenTime)
                closeTime = calendar.dateFromComponents(componentsCloseTime)
                if timeTableDay.from == "00:00:00" && timeTableDay.to == "00:00:00" {
                   return true
                }
            }
        }
        return false
    }
    
    func hasDelivery() -> Bool{
        return ((delivery_available ?? false) && (is_delivery_enabled ?? false))
    }
    
    func textForScheduleState() -> NSAttributedString {
        var text = NSMutableAttributedString()
        let fontSize:CGFloat = 11
        
        let cerrado = NSAttributedString(string: "Cerrado por hoy", attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Bold", size:fontSize) ?? UIFont.boldSystemFontOfSize(9), NSForegroundColorAttributeName: UIColor.whiteColor()])
        text.appendAttributedString(cerrado)
        if let currentDay = getDayOfWeek() {
            let timeTableDays = self.timeTables.filter { (table) -> Bool in
                return table.day == currentDay
            }
            for timeTableDay in timeTableDays {
                let formatter = NSDateFormatter()
                formatter.dateFormat = "HH:mm:ss"
                
                var openTime = formatter.dateFromString(timeTableDay.from ?? "")
                var closeTime = formatter.dateFromString(timeTableDay.to ?? "")
                
                let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
                
                
                let componentsNow = calendar.components(([.Day, .Month, .Year,.Hour,.Minute,.Second]), fromDate: getCurrentLocalDate())
                let componentsOpenTime = calendar.components(([.Hour,.Minute,.Second]), fromDate: openTime ?? NSDate())
                let componentsCloseTime = calendar.components(([.Hour,.Minute,.Second]), fromDate: closeTime ?? NSDate())
                componentsCloseTime.timeZone = NSTimeZone(name: "America/Asuncion")
                componentsOpenTime.timeZone = NSTimeZone(name: "America/Asuncion")
                componentsOpenTime.day = componentsNow.day
                componentsOpenTime.month = componentsNow.month
                componentsOpenTime.year = componentsNow.year
                
                componentsCloseTime.day = componentsNow.day
                componentsCloseTime.month = componentsNow.month
                componentsCloseTime.year = componentsNow.year
                openTime = calendar.dateFromComponents(componentsOpenTime)
                closeTime = calendar.dateFromComponents(componentsCloseTime)
                //                    let closeTime = formatter.dateFromString(timeTableDay.to ?? "")
                if timeTableDay.from == "00:00:00" && timeTableDay.to == "00:00:00" {
                    
                    
                    
                    text = NSMutableAttributedString()
                    
                    text.appendAttributedString( NSAttributedString(string: "Cerrado por hoy", attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Bold", size:fontSize) ?? UIFont.boldSystemFontOfSize(fontSize), NSForegroundColorAttributeName: UIColor.whiteColor()]))
                    break
                    
                    
                }else{
                    
                    if let openTime = openTime {
                        
                        if getCurrentLocalDate().compare(openTime) == .OrderedAscending {
                            let dateFormatter = NSDateFormatter()
                            
                            //                                let openHour = dateFormatter.dateFromString(timeTableDay.from ?? "0:0")!
                            dateFormatter.dateFormat = "h:mm a"
                            
                            let openAt = NSAttributedString(string: "Cerrado: Abre a las: \(dateFormatter.stringFromDate(openTime))", attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Bold", size:fontSize) ?? UIFont.boldSystemFontOfSize(fontSize), NSForegroundColorAttributeName: UIColor.whiteColor()])
                            text = NSMutableAttributedString()
                            text.appendAttributedString(openAt)
                            break
                            
                        }else{
                            //                                text.appendAttributedString( NSAttributedString(string: "CERRADO POR HOY", attributes: [NSFontAttributeName: UIFont(name: "OpenSans-Bold", size:13) ?? UIFont.boldSystemFontOfSize(10), NSForegroundColorAttributeName: UIColor.whiteColor()]))
                            //                                break
                        }
                    }
                    
                }
            }
        }
        
        
        return text
    }
    
    
    func reloadTime () -> NSDate? {
        var dateToReturn: NSDate?
        if let currentDay = getDayOfWeek() {
            let timeTableDays = timeTables.filter { (table) -> Bool in
                return table.day == currentDay
            }
            for timeTableDay in timeTableDays {
                let formatter = NSDateFormatter()
                formatter.dateFormat = "HH:mm:ss"
                
                var openTime = formatter.dateFromString(timeTableDay.from ?? "")
                var closeTime = formatter.dateFromString(timeTableDay.to ?? "")
                
                let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
                let componentsNow = calendar.components(([.Day, .Month, .Year,.Hour,.Minute,.Second]), fromDate: getCurrentLocalDate())
                let componentsOpenTime = calendar.components(([.Hour,.Minute,.Second]), fromDate: openTime ?? NSDate())
                let componentsCloseTime = calendar.components(([.Hour,.Minute,.Second]), fromDate: closeTime ?? NSDate())
                componentsCloseTime.timeZone = NSTimeZone(name: "America/Asuncion")
                componentsOpenTime.timeZone = NSTimeZone(name: "America/Asuncion")
                componentsOpenTime.day = componentsNow.day
                componentsOpenTime.month = componentsNow.month
                componentsOpenTime.year = componentsNow.year
                
                componentsCloseTime.day = componentsNow.day
                componentsCloseTime.month = componentsNow.month
                componentsCloseTime.year = componentsNow.year
                openTime = calendar.dateFromComponents(componentsOpenTime)
                closeTime = calendar.dateFromComponents(componentsCloseTime)
                
                
                if timeTableDay.from != "00:00:00" && timeTableDay.to != "00:00:00" {
                    if let openTime = openTime, closeTime = closeTime {
                        if getCurrentLocalDate().isBetween(date: openTime,andDate: closeTime) {
                            return closeTime
                        }else if getCurrentLocalDate().compare(openTime) == .OrderedAscending{
                            dateToReturn =  openTime
                        }
                    }
                }
            }
        }
        return dateToReturn
    }
    
    var jsonData: JSON?
    
    var distanceFromMyLocation: String {
        
        let defaultAddressCoordinates = MUser.sharedInstance.defaultAddress.locationAsCoordinate
        let resto1Coordinates = CLLocation(latitude: (self.lat as NSString).doubleValue, longitude: (self.lng as NSString).doubleValue)
        let distance = defaultAddressCoordinates.distanceFromLocation(resto1Coordinates)/1000
        return String(format: "%.2f", distance)
    }
    
    
    var formatedAddress: String {
        if street2 != "" && street1 != "" && house_number != "" {
            return (street1 ?? "") + " "  + (house_number ?? "") + " y " + (street2 ?? "")
        }else {
            return (street1 ?? "") + " "  + (house_number ?? "")
        }
    }
    
    init(data: JSON) {
        self.jsonData = data
        self.id = data["id"].int
        self.name = data["name"].string ?? ""
        self.details = data["details"].string ?? ""
        self.lat = data["lat"].string ?? ""
        self.lng = data["lng"].string ?? ""
        self.phone = data["phone"].string
        self.category = data["category"].string ?? ""
        self.delivery_delay = data["delivery_delay"].int ?? 0
        self.delivery_available = data["delivery_available"].bool
        self.is_pickup_enabled = data["is_pickup_enabled"].bool
        self.order_minimum = data["order_minimum"].string ?? ""
        self.delivery_gps_tracked = data["delivery_gps_tracked"].bool
        self.franchise_id = data["franchise_id"].int
        self.house_number = data["house_number"].string
        self.city_id = data["city_id"].int
        self.geo_location = data["geo_location"].bool
        self.is_open = data["is_open"].bool
        self.contact_name = data["contact_name"].string
        self.street1 = data["street1"].string
        self.street2 = data["street2"].string
        self.delivery_price = data["delivery_price"].float
        if self.delivery_price == nil {
            self.delivery_price = data["delivery_price"].string?.floatValue
        }
        self.is_delivery_enabled = data["is_delivery_enabled"].bool
        self.distance = data["distance"].string
        self.email = data["email"].string
        self.is_pos_enabled = data["is_pos_enabled"].bool
        self.is_open = data["is_open"].bool ?? false
        self.delivery_available = data["delivery_available"].bool ?? false
        self.franchise_id = data["franchise_id"].int
        self.attachment = MAttachment(json: data["franchise"]["icon_attachment"])
        self.header_attachment = MAttachment(json: data["franchise"]["header_attachment"])
        self.franchise = MFranchise(json: data["franchise"])
        if let timeTables = data["time_tables"].array {
            self.timeTables = []
            for timeTable in timeTables {
                self.timeTables.append(MTimeTable(json: timeTable))
            }
        }
        
    }
    override init (){
        super.init()
        
    }
    func getDayOfWeek()->Int? {
        let todayDate = NSDate()
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
        let myComponents = myCalendar?.components(.Weekday, fromDate: todayDate)
        if let weekDay = myComponents?.weekday {
            if weekDay - 1 > 0 {
                return weekDay - 1
            }else{
                return 7
            }
        }else{
            return nil
        }
    }
    
    class func preCacheBranchPictures(branchs: [MRestaurant]) {
        var urls: [NSURL] = []
        for branch in branchs {
            if let imageURL = branch.franchise?.icon_attachment?.imageComputedURL {
                urls.append(imageURL)
            }
        }
        cacheImagesForUrls(urls)
    }
}



