//
//  GlobalConstants.swift
//  Monchis
//
//  Created by jrivarola on 6/19/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
import SCLAlertView
import Crashlytics
import MBProgressHUD

// CONFIGURATION
let debugModeEnabled:Bool = true
var headerBlurOn: Bool = false
let useContainerSizeForPhotos = true
let NO_BRANCH_PLACEHOLDER = UIImage(named: "noBranch")
let NO_ITEM_PLACEHOLDER = UIImage(named: "noItem")
let NO_CATEGORY_PLACEHOLDER = UIImage(named: "noCategory")

//GLOBAL VARIABLES
var categories:[MCategory] = []
var imageCacheKeys:[String] = []
var mainviewImageCacheFormtas: [String:String] = [:]
var cacheFormats: [String:String] = [:]


//GLOBAL CONSTANTS


struct SmallImageSize {
    static let width:CGFloat = 250
    static let height:CGFloat = 250
}

struct MediumImageSize {
    static let width:CGFloat = 500
    static let height:CGFloat = 500
}

struct BigImageSize {
    static let width:CGFloat = 750
    static let height:CGFloat = 750
}

var vCs: [String:UIViewController]! = [String:UIViewController]()
let PLACE_HOLDER_IMAGE = UIImage(named: "monchis_placeholder")
let kUserFavoritesKey = "kUserFavoritesKey"

let googleMapsApiKey = "AIzaSyBQlspb5u1FO00X2Yp0D95dyvoma92ImW4"
let kUserDidChangeDefaultAddressNotification = "userDidChangeDefaultAddressNotification"
let kCartUpdatedNotification = "cartDidUpdateNotification"
let kAutomaticLogin = "kAutomaticLoginKey"
let kMonchisUserEmail = "kMonchisUserEmailKey"
let kMonchisUserPassword = "kMonchisPasswordKey"
let kMonchisUserAuthenticationMethod = "kMonchisUserAuthenticationMethodKey"
let kMonchisUserFacebookID = "kMonchisUserFacebookIDKey"
let kMonchisUserTwitterID = "kMonchisUserTwitterID"

let kShowMapTutorialKey = "kShowMapTutorialKey"
let kShowOrderTutorialKey = "kShowOrderTutorialKey"

let kMonchisUserPushTokenKey = "kMonchisUserPushTokenKey"
let kUserDeliveryType = "kUserDeliveryTypeKey"
let kHistoryDidReload = "kHistoryDidReload"
let kUserWantsToFinalizeOrdersNotification = "kUserWantsToFinalizeOrdersNotification"
// favorites

let favorited = UIImage(named: "icono-favorito-on")
let unFavorited = UIImage(named: "icono-favorito")

func colorize(imageView: UIImageView, color: CGColorRef) {
    
    // Set the image's shadowColor, radius, offset, and
    // set masks to bounds to false
    imageView.layer.shadowColor = color
    imageView.layer.shadowRadius = 20.0
    imageView.layer.shadowOffset = CGSizeZero
    imageView.layer.masksToBounds = false
    
    // Animate the shadow opacity to "fade it in"
    let shadowAnim = CABasicAnimation()
    shadowAnim.keyPath = "shadowOpacity"
    shadowAnim.fromValue = NSNumber(float: 0.0)
    shadowAnim.toValue = NSNumber(float: 0.9)
    shadowAnim.duration = 0.13
    shadowAnim.repeatCount = 100
    shadowAnim.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
    shadowAnim.autoreverses = true
    imageView.layer.addAnimation(shadowAnim, forKey: "shadowOpacity")
    imageView.layer.shadowOpacity = 1
}

func imageWithImage (sourceImage:UIImage, scaledToWidth: CGFloat) -> UIImage {
    let oldWidth = sourceImage.size.width
    let scaleFactor = scaledToWidth / oldWidth
    
    let newHeight = sourceImage.size.height * scaleFactor
    let newWidth = oldWidth * scaleFactor
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
    sourceImage.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return newImage
}


func deliveryOrPickupHistorySearch(branch: MRestaurant, completion: (wantsDelivery: Bool?, newBranch: MRestaurant) -> Void) {
    let alertView = SCLAlertView()
    MBProgressHUD.showHUDAddedTo(UIApplication.sharedApplication().keyWindow!, animated: true)
    
    let latitude = MUser.sharedInstance.defaultAddress.latitude ?? ""
    let longitude = MUser.sharedInstance.defaultAddress.longitude ?? ""
    let parameters:[String:AnyObject] = ["franchise_id":branch.franchise!.id!,"latitude": latitude, "longitude": longitude]
    MAPI.SharedInstance.jsonRequest(.SEARCH_NEAR_BRANCH, parameters: parameters) { (json, error) in
//        if debugModeEnabled {
            print(#function,"called with parameters \(parameters)", json,error)
//        }
        MBProgressHUD.hideHUDForView(UIApplication.sharedApplication().keyWindow!, animated: true)
        var help = 0
        if error == nil {
            if json?["success"].bool == true {
                
                
                let branch = MRestaurant(data: json!["data"])
                
                if branch.hasDelivery() {
                    alertView.addButton("Delivery") {
                        completion(wantsDelivery: true, newBranch: branch)
                    }
                    help = 1
                }
                if branch.franchise?.branches_with_pickup  == true {
                    alertView.addButton("Pasar a buscar") {
                        completion(wantsDelivery: false, newBranch: branch)
                    }
                    help = 1
                }
                if help != 0 {
                    alertView.showSuccess("Atención!", subTitle: "Opciones para " + branch.franchise!.name!, closeButtonTitle: "Atras")
                }else{
                    alertView.addButton("Ver de todas maneras", action: {
                        completion(wantsDelivery: nil, newBranch: branch)
                    })
                    alertView.showNotice("Lo sentimos", subTitle: "Al parecer no hay opciónes disponibles.", closeButtonTitle: "Atras")
                    
                }
                
                
                
                
            }else{
                SCLAlertView().showError("Lo sentimos!", subTitle: json?["data"].string ?? "Ocurrio un error inesperado!")
            }
        }else{
            SCLAlertView().showError("Lo sentimos!", subTitle:"Ocurrio un error inesperado!")
        }
        
        
        
    }
    
    
    
    
}


func deliveryOrPickup(branch: MRestaurant, completion: (wantsDelivery: Bool) -> Void) {
    let alertView = SCLAlertView()
    if branch.hasDelivery()  {
        alertView.addButton("Delivery") {
            Answers.logCustomEventWithName("Usuario cambio a modo Delivery",
                                           customAttributes: ["Local": branch.name ?? ""])
            completion(wantsDelivery: true)
        }
    }
    if branch.franchise?.branches_with_pickup  == true {
        alertView.addButton("Pasar a buscar") {
            Answers.logCustomEventWithName("Usuario cambio a modo Pickup",
                                           customAttributes: ["Franquicia": branch.franchise?.name ?? ""])
            completion(wantsDelivery: false)
        }
    }
    //    alertView.addButton("Atras",backgroundColor: UIColor(hexString: ""), textColor: UIColor.whiteColor()) {
    //        completion(isDelivery: nil)
    //    }
    alertView.showSuccess("Elije una opción", subTitle: "", closeButtonTitle: "Atras")
    
}

func changeBranchMode(branch: MRestaurant, completion: (branch: MRestaurant) -> Void) -> Void{
    let alertView = SCLAlertView()
    if branch.mode == 1 && branch.franchise?.branches_with_pickup == true {
        alertView.addButton("Cambiar de Sucursal", action: {
            branch.mode = 2
            completion(branch: branch)
        })
        if branch.franchise?.branches_with_delivery == true {
            alertView.addButton("Solicitar Delivery") {
                branch.mode = 0
                completion(branch: branch)
            }
            
        }
        //            SCLAlertView().showNotice("Lo sentimos!", subTitle: "El local solo ofrece Pasar a Buscar.")
        //            return
        
    }else{
        if branch.franchise?.branches_with_pickup == true {
            alertView.addButton("Pasar a Buscar") {
                print("Second button tapped")
                branch.mode = 1
                
                completion(branch: branch)
            }
            
        }else{
            SCLAlertView().showNotice("Lo sentimos!", subTitle: "El local solo ofrece Delivery.")
            return
        }
        
    }
    
    //    alertView.addButton("Atras",backgroundColor: UIColor(hexString: ""), textColor: UIColor.whiteColor()) {
    //        completion(isDelivery: nil)
    //    }
    
    
    alertView.showSuccess("Elije una opción", subTitle: "", closeButtonTitle: "Atras")
}

// return MD5 digest of string provided
func md5HashForString(string: String) -> String? {
    
    guard let data = string.dataUsingEncoding(NSUTF8StringEncoding) else { return nil }
    
    var digest = [UInt8](count: Int(CC_MD5_DIGEST_LENGTH), repeatedValue: 0)
    
    CC_MD5(data.bytes, CC_LONG(data.length), &digest)
    
    return (0..<Int(CC_MD5_DIGEST_LENGTH)).reduce("") { $0 + String(format: "%02x", digest[$1]) }
}

extension String {
    
    func fromBase64() -> String? {
        guard let data = NSData(base64EncodedString: self, options: NSDataBase64DecodingOptions(rawValue: 0)) else {
            return nil
        }
        
        return String(data: data, encoding: NSUTF8StringEncoding)
    }
    
    func toBase64() -> String? {
        guard let data = self.dataUsingEncoding(NSUTF8StringEncoding) else {
            return nil
        }
        
        return data.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
    }
}

func getQueryStringParameter(url: String?, param: String) -> String? {
    if let url = url, urlComponents = NSURLComponents(string: url), queryItems = (urlComponents.queryItems) {
        return queryItems.filter({ (item) in item.name == param }).first?.value!
    }
    return nil
}
