//
//  CalificarStarCellTableViewCell.swift
//  Monchis
//
//  Created by Javier Rivarola on 8/29/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit

class CalificarStarCellTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func starPressed(sender: UIButton) {
    }
    var order: MOrder!
    var value = 5
    var parentTV: UITableView!
    @IBAction func didPressFavoriteBtn(sender: UIButton) {
        for i in 1...5 {
            let button = self.viewWithTag(i) as! UIButton
            button.setImage(UIImage(named: "estrella-off"), forState: .Normal)
        }
        
        let tag = sender.tag
        if tag == 1 {
            let desc = NSAttributedString(string: "MUY MALO", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(35),NSForegroundColorAttributeName:UIColor.whiteColor()])
            let text = NSMutableAttributedString(attributedString: NSAttributedString(string: "Seleccionaste\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(13),NSForegroundColorAttributeName:UIColor.whiteColor()]))
            text.appendAttributedString(desc)
        }
        if tag == 2{
            let desc = NSAttributedString(string: "REGULAR", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(35),NSForegroundColorAttributeName:UIColor.whiteColor()])
            let text = NSMutableAttributedString(attributedString: NSAttributedString(string: "Seleccionaste\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(13),NSForegroundColorAttributeName:UIColor.whiteColor()]))
            text.appendAttributedString(desc)
        }
        if tag == 3{
            let desc = NSAttributedString(string: "BUENO", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(35),NSForegroundColorAttributeName:UIColor.whiteColor()])
            let text = NSMutableAttributedString(attributedString: NSAttributedString(string: "Seleccionaste\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(13),NSForegroundColorAttributeName:UIColor.whiteColor()]))
            text.appendAttributedString(desc)
        }
        if tag == 4{
            let desc = NSAttributedString(string: "MUY BUENO", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(35),NSForegroundColorAttributeName:UIColor.whiteColor()])
            let text = NSMutableAttributedString(attributedString: NSAttributedString(string: "Seleccionaste\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(13),NSForegroundColorAttributeName:UIColor.whiteColor()]))
            text.appendAttributedString(desc)
        }
        if tag == 5 {
            let desc = NSAttributedString(string: "EXCELENTE", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(35),NSForegroundColorAttributeName:UIColor.whiteColor()])
            let text = NSMutableAttributedString(attributedString: NSAttributedString(string: "Seleccionaste\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(13),NSForegroundColorAttributeName:UIColor.whiteColor()]))
            text.appendAttributedString(desc)
        }
        
        for i in 1...tag{
            let button = self.viewWithTag(i) as! UIButton
            button.setImage(UIImage(named: "estrella-on"), forState: .Normal)
            value = i
        }
        order?.starRatingValue = value
        parentTV?.beginUpdates()
        parentTV?.endUpdates()
    }

}
