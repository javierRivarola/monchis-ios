//
//  RestosByCategoryViewController.swift
//  Monchis
//
//  Created by jrivarola on 10/13/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class RestosByCategoryViewController: UIViewController {
    
    var buttonTitles : [String] = ["Home", "Places", "Photos", "List", "Tags"]
    
    
     let restaurantCategoriesStringArray = ["Ver Todos","Pizzas","Hamburguesas", "Empanadas", "Oriental", "Sandwiches", "Restaurantes", "Natural Food", "Helados", "Bebidas"]
    @IBOutlet weak var pagesView: SwiftPages!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pagesView.initializeWithVCIDsArrayAndButtonTitlesArray(["RestaurantesCVID"],buttonTitlesArray: restaurantCategoriesStringArray)
    
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
