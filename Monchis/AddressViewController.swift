//
//  AddressViewController.swift
//  Monchis
//
//  Created by Monchis Mac on 3/7/17.
//  Copyright © 2017 cibersons. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import DynamicBlurView
import GooglePlaces
import Crashlytics
import Mixpanel
import CoreLocation
import AMPopTip
import pop
import AudioToolbox

class AddressViewController: UIViewController, CLLocationManagerDelegate,GMSMapViewDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var mapViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var nextBtn: UIButton!
    //Constants
    let locationManager = CLLocationManager()
    let dataProvider = GoogleDataProvider()
    let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
    let baseAutocompleteURL = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
    var gpsLocation:CLLocation!
    var addressToEdit: MAddress!
    //api shared instance
    let api = MAPI.SharedInstance
    let user = MUser.sharedInstance
    var userSelectedLocation: CLLocation!
    var index = 4
    @IBOutlet weak var locationPin: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    
    var editingMode: Bool = false
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI(){
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.myLocationEnabled = true
        mapView.settings.myLocationButton = true
        if editingMode {
            mapView.camera = GMSCameraPosition(target: addressToEdit.computedCoordinate, zoom: 16, bearing: 0, viewingAngle: 0)
        }else{
            mapView.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: -25.33585, longitude: -57.505556), zoom: 13, bearing: 0, viewingAngle: 0)

        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if editingMode {
            showMarkedPin()
        }

            showTip(0)
    }
    

    func showTip(withDelay: Double){
        
        let delay = withDelay * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(time, dispatch_get_main_queue()) {
            if self.tip == nil  {
                self.tip = AMPopTip()
                self.tip.popoverColor = UIColor(hexString: "#353333")!
                var frame = self.locationPin.frame
                frame.origin.y += self.locationPin.frame.height
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = .Center

                let attrString = NSAttributedString(string: "Marca en el mapa donde queda\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor(),NSParagraphStyleAttributeName: paragraphStyle])
                let attrAddr =  NSAttributedString(string: self.addressToEdit.formattedAddress, attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor(),NSParagraphStyleAttributeName: paragraphStyle])
                
                let final = NSMutableAttributedString(attributedString: attrString)
                final.appendAttributedString(attrAddr)
                final.appendAttributedString(NSAttributedString(string: "\nPresionando", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor(),NSParagraphStyleAttributeName: paragraphStyle]))
                final.appendAttributedString(NSAttributedString(string: " aquí", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(15), NSForegroundColorAttributeName: UIColor.whiteColor(),NSParagraphStyleAttributeName: paragraphStyle]))
                self.tip.showAttributedText(final, direction: .Up, maxWidth: self.view.frame.width*0.9, inView: self.view, fromFrame: self.locationPin.frame)
                self.tip.tapHandler = {
                    self.tipHandler()
                }
            }
        }
        
        
    }
    
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func hideTip() {
        tip.hide()
    }
    
    func tipHandler(){
        hideTip()
        showMarkedPin()
    }
    var showingMarkedPin: Bool = false
    func showMarkedPin(){
        if showingMarkedPin {
            return
        }
        showingMarkedPin = true
        dispatch_async(dispatch_get_main_queue()) {
            SystemSoundID.playFileNamed("Pop", withExtenstion: "mp3")
        }
        locationPin.alpha = 0
        locationPin.layer.zPosition = 999
        locationPin.hidden = false
        nextBtn.alpha = 0
        nextBtn.hidden = false
        self.locationPin.pop()
        if self.mapView.camera.zoom < 17.5 {
            CATransaction.begin()
            CATransaction.setValue(NSNumber(double: 1.2), forKey: kCATransactionAnimationDuration)
//            mapView.animateToZoom(17.5)
//                mapView.animateToLocation(myLoc)
                if !editingMode {
                    mapView.animateToCameraPosition(GMSCameraPosition(target: mapView.camera.target, zoom: 17.5, bearing: 0, viewingAngle: 0))
                }else{
                    
                if self.addressToEdit.computedCoordinate.latitude != 0 && self.addressToEdit.computedCoordinate.longitude != 0  {
                    mapView.animateToCameraPosition(GMSCameraPosition(target: self.addressToEdit.computedCoordinate, zoom: 17, bearing: 0, viewingAngle: 0))
                }
            }
        
            CATransaction.commit()
        }
        nextBtn.transform = CGAffineTransformMakeTranslation(0, 200)
        UIView.animateWithDuration(2,delay: 0, usingSpringWithDamping: 0.4,initialSpringVelocity: 0.5 ,options: [.CurveEaseIn], animations: {
            self.mapViewBottomConstraint.constant = 150
            self.nextBtn.transform = CGAffineTransformIdentity

         self.locationPin.alpha = 1
           self.nextBtn.alpha = 1
        }) { (fin) in
            let delay = 0.5 * Double(NSEC_PER_SEC)
            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
            dispatch_after(time, dispatch_get_main_queue()) {
                self.pulseAnimation(self.locationPin, radius: 70)
//                dispatch_after(time, dispatch_get_main_queue()) {
//                self.pulseAnimation(self.locationPin, radius: 60)
//                }
//                dispatch_after(time, dispatch_get_main_queue()) {
//                self.pulseAnimation(self.locationPin, radius: 50)
//                }

            }
            
        }
        
    }
    var tip: AMPopTip!
  
    func pulseAnimation(forImageView: UIImageView, radius: CGFloat ){
        let pulseEffect = JRPulseAnimation(repeatCount: Float.infinity, radius:radius, position:CGPoint(x: forImageView.bounds.width/2,y: forImageView.bounds.height))
        forImageView.layer.addSublayer(pulseEffect)

    }
    
    // MARK: LocationManagerDelegate
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            locationManager.startUpdatingLocation()
          
        }
        if status == .Denied {
            
        }
    }
    
    
    func mapView(mapView: GMSMapView, willMove gesture: Bool) {
        locationManager.stopUpdatingLocation()
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first  {
            gpsLocation = location
            if !editingMode {
                mapView.animateToLocation(location.coordinate)
                
//            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: mapView.camera.zoom + 0.5, bearing: 0, viewingAngle: 0)
                if self.tip != nil {

                }
            }

            self.userSelectedLocation = location
        }
        
    }
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            
            if let address = response?.firstResult() {
                let lines = address.lines!
                let firstLine = lines.joinWithSeparator("\n")
            }
        }
    }
    
    func mapView(mapView: GMSMapView, idleAtCameraPosition position: GMSCameraPosition) {
        self.addressToEdit.coordinate = position.target
        reverseGeocodeCoordinate(position.target)
        
        
    
    }

}
