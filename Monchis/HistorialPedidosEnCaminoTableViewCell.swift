//
//  HistorialPedidosEnCaminoTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 10/2/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class HistorialPedidosEnCaminoTableViewCell: UITableViewCell {

    var delegate: callRestaurantDelegate?
    @IBOutlet weak var recibidoImgView: UIImageView!
    var order: MOrder!
    @IBOutlet weak var preparandoImgView: UIImageView!
    @IBOutlet weak var enCaminoImgView: UIImageView!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var seeOnMapBtn: UIButton!
    @IBOutlet weak var enCaminoView: UIView!
  
    @IBOutlet weak var deliveryOrPickupLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        callBtn.titleLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
        callBtn.titleLabel?.numberOfLines = 0
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func verEnMapaPressed(sender: UIButton) {
    }
    @IBAction func callPressed(sender: UIButton) {
        delegate?.callPressed(sender, order: order)
    }

}
