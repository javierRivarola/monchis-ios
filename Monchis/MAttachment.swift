//
//  MAttachment.swift
//  Monchis
//
//  Created by Monchis Mac on 2/13/17.
//  Copyright © 2017 cibersons. All rights reserved.
//

import Foundation
import SwiftyJSON

class MAttachment: NSObject {
    var id: Int?
    var imagesURLBase: String = "/photo2/"
    var imagesURLBaseResize: String = "/photo2/"
    
    var defaultImageSize: String =  "250x250/" //default
    var filename: String?
    var updated_at: String?
    var width: CGFloat?
    var height: CGFloat?
    var imageSizeFromWidthAndSize: String {
        if width != nil && height != nil {
            return "\(width!)x\(height!)/"
        }else{
            return defaultImageSize
        }
    }
    var mime:String?
    var imageURL: String?
    var imageComputedURL: NSURL? {
        var customSize = ""
        if imageSizeFromWidthAndSize == defaultImageSize {
            customSize = defaultImageSize
        }else{
            customSize = imageSizeFromWidthAndSize
        }
        if filename != nil {
            
            let string = MAPI.SharedInstance.kMAPIClientsURL.URLString + imagesURLBase + customSize + filename! + "/\(self.updated_at ?? "")"
            let encoded = string.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
            
            if let url = NSURL(string: encoded!) {
                return url
            }else{
                return nil
            }
        }else{
            return nil
        }
    }
    
    var resizedImageUrl: NSURL? {
        if filename != nil {
            let string = MAPI.SharedInstance.kMAPIClientsURL.URLString + imagesURLBaseResize + "\(self.width ?? 0)x\(self.height ?? 0)/" + filename! + "/\(self.updated_at ?? "")"
            let encoded = string.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
            if let url = NSURL(string: encoded!) {
                
                return url
            }else{
                return nil
            }
        }
        return nil
    }
    
    var resizedImageUrl2: NSURL? {
        if filename != nil {
            let string = MAPI.SharedInstance.kMAPIClientsURL.URLString + imagesURLBaseResize + "\(self.width ?? 0)/" + filename! + "/\(self.updated_at ?? "")"
            let encoded = string.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
            if let url = NSURL(string: encoded!) {
                
                return url
            }else{
                return nil
            }
        }
        return nil
    }
    init (json: JSON) {
        super.init()
        self.id = json["id"].int;
        self.filename = json["filename"].string
        self.updated_at = json["updated_at"].string
        self.mime = json["mime"].string
    }
}