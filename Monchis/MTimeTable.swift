//
//  MTimeTable.swift
//  Monchis
//
//  Created by jrivarola on 7/12/15.
//  Copyright © 2015 cibersons. All rights reserved.
//

import Foundation
import SwiftyJSON

class MTimeTable: NSObject {
    var id: NSNumber?
    var to: String?
    var from: String?
    var day: NSNumber?
    var branch_id: NSNumber?
    var delivery_zone_id:NSNumber?
    
    init (json: JSON) {
        self.id = json["id"].int
        self.to = json["to"].string
        self.from = json["from"].string
        self.day = json["day"].int
        self.branch_id = json["branch_id"].int
        self.delivery_zone_id = json["delivery_zone_id"].int

    }
}

func == (lhs: MTimeTable, rhs: MTimeTable) -> Bool {
    return lhs.id == rhs.id
}