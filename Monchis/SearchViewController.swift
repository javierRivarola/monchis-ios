//
//  ExploreViewController.swift
//  Monchis
//
//  Created by jrivarola on 8/27/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UISearchResultsUpdating, UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate, UISearchControllerDelegate {
    
    var searchController: UISearchController!
    var searchBar: UISearchBar!
    var filteredData:[[AnyObject]] = []
    
    var restaurants: [MRestaurant] = []
    var products: [MProduct] = []
    @IBOutlet weak var tableView: UITableView!
    struct Constants {
    static let reuseSearchCell = "reuseSearch"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        // Do any additional setup after loading the view.
    }
    
   
    func setupUI(){
        
        self.tableView.scrollsToTop = true

        self.searchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.hidesNavigationBarDuringPresentation = false
            
           // self.tableView.tableHeaderView = controller.searchBar
            self.navigationItem.titleView = controller.searchBar
            return controller
        })()
        
        
        
        
   
        
        searchBar = searchController.searchBar
        searchBar.tintColor = UIColor.redColor()
        
        searchBar.translucent = false
        searchBar.delegate = self
        
        for subView in searchBar.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.attributedPlaceholder =  NSAttributedString(string:NSLocalizedString("Buscar..", comment: "String que indica el placeholder para buscar platos o restaurantes "),
                        attributes:[NSFontAttributeName: UIFont(name: "OpenSans-Semibold", size: textField.font!.pointSize)!])
                }
            }
        }
        //searchBar.scopeButtonTitles = [NSLocalizedString("Restaurantes", comment: "Restaurantes para scope de busqueda por restaurantes "),NSLocalizedString("Platos", comment: "Scope para busqueda por platos ")]
        
        self.definesPresentationContext = true
        tableView.dataSource = self
        tableView.delegate = self
        
        
        self.searchController.active = true
        

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    

   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: UITableViewDataSource
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.reuseSearchCell, forIndexPath: indexPath) 
        cell.textLabel?.text = "asdasdsa"
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return filteredData.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData[section].count
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        filteredData.removeAll(keepCapacity: false)
        filteredData = []
        let searchPredicate = NSPredicate(format: "self.name contains[c] %@", searchController.searchBar.text!)
        
        let array = (restaurants as NSArray).filteredArrayUsingPredicate(searchPredicate)
        filteredData.append(array as! [MRestaurant])
        self.tableView.reloadData()
    }
    
    func didPresentSearchController(searchController: UISearchController) {
        searchBar.becomeFirstResponder()

    }
    

    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        return true
    }
    

}
