//
//  BranchSelectionViewController.swift
//  Monchis
//
//  Created by jrivarola on 22/1/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import SwiftyJSON
import Crashlytics
import Mixpanel


protocol BranchSelectionDelegate {
    func didSelectBranch(branch: MRestaurant,order: MOrder?)
}
class BranchSelectionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetDelegate,DZNEmptyDataSetSource,BranchSelectionMapDelegate {
    
    var franchise: MFranchise!
    @IBOutlet weak var tableView: UITableView!
    
    let api = MAPI.SharedInstance
    var branches: [MRestaurant] = []
    struct Segues {
        static let gotoBranchSegue = "gotoBranchSegue"
        static let unwindFromBranchSelection = "unwindFromBranchSelection"
        static let gotoMapSegue = "gotoMapSegue"
    }
    
    var delegate: BranchSelectionDelegate?
    var order: MOrder!
    var cameFromCustomizeProduct: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Locales de \(franchise.name)"
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        tableView.emptyDataSetDelegate = self
        tableView.emptyDataSetSource = self
        // Do any additional setup after loading the view.
        tableView.reloadEmptyDataSet()
        loadBranchesForFranchise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadBranchesForFranchise(useCache: Bool = true) {
//        let id: Int = order.branch.franchise_id ?? 0
        let id: Int = franchise.id ?? 0
        let parameters = ["franchise_id":id]
        self.tableView.reloadEmptyDataSet()
        api.jsonRequest(.LIST_BRANCHES, parameters: parameters, cacheRequest: true, returnCachedDataIfPossible: useCache) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            
            if error == nil {
                if let json = json {
                    if let branchesArray = json["data"].array {
                        var tempRestos:[MRestaurant] = []
                        for branch in branchesArray {
                            let newBranch = MRestaurant(data: branch)
                            newBranch.mode = 1
//                            newBranch.franchise = self.franchise
//                            newBranch.franchise_id = branch["franchise_id"].int
                            tempRestos.append(newBranch)
                        }
                            self.branches = tempRestos.filter({ (resto) -> Bool in
                                return resto.is_pickup_enabled ?? false
                            })
                        self.branches.sortInPlace({ (rest1, rest2) -> Bool in
                            rest1.distanceFromMyLocation.floatValue < rest2.distanceFromMyLocation.floatValue
                        })
                        self.tableView.reloadData()
                    }
                }
            }else{
                
            }
        }
        api.jsonRequest(.LIST_BRANCHES, parameters: parameters, cacheRequest: true, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            
            if error == nil {
                if let json = json {
                    if let branchesArray = json["data"].array {
                        var tempRestos:[MRestaurant] = []
                        for branch in branchesArray {
                            let newBranch = MRestaurant(data: branch)
                            newBranch.mode = 1
//                            newBranch.franchise = self.franchise
//                            newBranch.franchise_id = branch["franchise_id"].int
                            tempRestos.append(newBranch)
                        }
                        self.branches = tempRestos.filter({ (resto) -> Bool in
                            return resto.is_pickup_enabled ?? false
                        })
                        self.branches.sortInPlace({ (rest1, rest2) -> Bool in
                            rest1.distanceFromMyLocation.floatValue < rest2.distanceFromMyLocation.floatValue
                        })
                        self.tableView.reloadData()
                    }
                }
            }else{
                
            }
        }
        
    }
    
    
    func updateUIForBranches(json: JSON){
        
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Segues.gotoBranchSegue {
            let dvc = segue.destinationViewController as! RestaurantViewController
            selectedBranch.mode = 1
            dvc.restaurant = selectedBranch
        }
        if segue.identifier == Segues.gotoMapSegue {
            let ddvc = segue.destinationViewController as! BranchesOnMapViewController
            ddvc.branches = [selectedBranch]
        }
      
    }

    
    // MARK: UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return branches.count > 0 ? 1 : 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return branches.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("branchReuseCell", forIndexPath: indexPath) as! BranchSelectionTableViewCell
        let branch = branches[indexPath.row]
        cell.branch = branch
        cell.delegate = self
        cell.distanceLbl.text = branch.distanceFromMyLocation + " kms."
        cell.branchNameLbl?.text = branch.name
        cell.branchAddressLbl?.text = branch.formatedAddress
        
//        if indexPath.row % 2 == 0 {
//            cell.contentView.backgroundColor = UIColor(hexString: "#000000",alpha: 0.3)
//        }else{
//            cell.contentView.backgroundColor = UIColor.clearColor()
//        }
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
        return cell
        
        
    }
    
    
    var selectedBranch: MRestaurant!
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedBranch = branches[indexPath.row]
        Answers.logContentViewWithName("Usuario quiere pickup de \(selectedBranch?.name ?? "")",
                                       contentType: "Franquicias",
                                       contentId: "\(selectedBranch!.id!)",
                                       customAttributes: [:])
        Mixpanel.mainInstance().track(event: "Usuario eligio una Franquicia",
                                      properties: ["Franquicia" : selectedBranch?.name ?? "","Pantalla":"Lista de Sucursales", "Modo":"Pickup"])
        if delegate != nil {
            selectedBranch.mode = 1
            self.navigationController?.popViewControllerAnimated(true)
            delegate?.didSelectBranch(selectedBranch,order: self.order)
            NSNotificationCenter.defaultCenter().postNotificationName("refreshBranchForModeChangeNotification", object: ["branch":selectedBranch])
        }else{
            self.performSegueWithIdentifier(Segues.gotoBranchSegue, sender: self)
        }
      
    }
    var doneLoading: Bool = false
    func customViewForEmptyDataSet(scrollView: UIScrollView!) -> UIView! {
        let spinner = UIActivityIndicatorView()
        spinner.color = UIColor.redColor()
        spinner.startAnimating()
        return spinner

    }
    
  
    
//    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
//        return NSAttributedString(string: "", attributes: [NSForegroundColorAttributeName: UIColor.redColor(), NSFontAttributeName: UIFont.boldSystemFontOfSize(17)])
//    }
    
    @IBAction func seeOnMapPressed(sender: UIButton) {
        
    }
    
    func didSelectBranch(branch: MRestaurant) {
        selectedBranch = branch
        selectedBranch.mode = 1
        self.performSegueWithIdentifier(Segues.gotoMapSegue, sender: self)
    }
    
    

    
}
