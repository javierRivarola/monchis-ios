//
//  CarritoTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 7/3/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import GMStepper

protocol CartOrderChangeOrDeleteDelegate {
    func didPressChangeOrder(forOrder order: MOrder, cell: CarritoItemTableViewCell)
    func didPressDeleteOrder(forOrder order: MOrder, cell: CarritoItemTableViewCell)
    func didChangeQuantityForOrder(forOrder order: MOrder, cell: CarritoItemTableViewCell, sender: GMStepper)
}

class CarritoItemTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var orderTitleLbl: UILabel!
    @IBOutlet weak var orderDescrLbl: UILabel!
    @IBOutlet weak var totalPriceLbl: UILabel!
    @IBOutlet weak var stepper: GMStepper!
    @IBOutlet weak var eliminarBtn: UIButton!
    @IBOutlet weak var cambiarBtn: UIButton!
    var order:MOrder!
    var delegate: CartOrderChangeOrDeleteDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func didChangeQuantityForOrder(sender: GMStepper) {
        if order != nil {
        delegate?.didChangeQuantityForOrder(forOrder: order, cell: self, sender: sender)
        }
    }
    @IBAction func changeOrderPressed(sender: UIButton) {
      
        delegate?.didPressChangeOrder(forOrder: order, cell: self )
    }

    @IBAction func deleteOrderPressed(sender: UIButton) {
        delegate?.didPressDeleteOrder(forOrder: order,cell: self)
    }
}
