//
//  MFavorite.swift
//  Monchis
//
//  Created by jrivarola on 10/14/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation

class MFavorite  {
    var product: MProduct!
    var order: MOrder!
    var franchise: MFranchise!
    var branch: MRestaurant!
}

func == (lhs: MFavorite, rhs: MFavorite) -> Bool {
    return (lhs.product.id == rhs.product.id) || (lhs.order.order_id == lhs.order.order_id)
}