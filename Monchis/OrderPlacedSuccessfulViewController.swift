//
//  OrderPlacedSuccessfulViewController.swift
//  Monchis
//
//  Created by jrivarola on 16/11/15.
//  Copyright © 2015 cibersons. All rights reserved.
//

import UIKit
import FBSDKShareKit

class OrderPlacedSuccessfulViewController: UIViewController {

    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var shareFBImgView: UIImageView!
    @IBOutlet weak var shareBtn: FBSDKShareButton!
    var cartID: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        shareBtn.setTitle("Comparti y participá de sorteos!", forState: .Normal)
        
        if let url = NSURL(string: MAPI.SharedInstance.kMAPIClientsURL.URLString + "/cart/\(cartID!)/share/facebook") {
          
          
                        let link = FBSDKShareLinkContent()
                        link.imageURL = url
            
                        link.contentURL = NSURL(string: "http://monchis.com.py")
                        self.shareBtn.shareContent = link
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func verEstadoPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
        NSNotificationCenter.defaultCenter().postNotificationName("unwindFromThankYouEstados", object: nil)
    }
   
    @IBAction func volverInicioPressed(sender: UIButton) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        NSNotificationCenter.defaultCenter().postNotificationName("unwindFromThankYouInicio", object: nil)
    }

}
