//
//  SideMenuViewController.swift
//  Monchis
//
//  Created by jrivarola on 7/7/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import DynamicBlurView
import MBProgressHUD
import FacebookLogin
import MessageUI
import SCLAlertView
import Crashlytics
import SCLAlertView
import Mixpanel

class SideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate, LoginViewControllerDelegate {
    
    
    @IBOutlet weak var sideConstraint: NSLayoutConstraint!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet var longPressGesture: UILongPressGestureRecognizer!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var userFBImageView: UIImageView!
    var unwindedFromMapController: Bool = false
    
    @IBOutlet weak var conectadoViaLbl: UILabel!
    var direccionesCVC: UICollectionView!
    var selectedRowIndex: NSIndexPath!
    var direccionesSelected: Bool = true
    var noAddrsLblPOINTER: UILabel!
    var sizeOfPF:CGSize!
    var waitBlurView: DynamicBlurView!
    
    // user shared instance
    let user = MUser.sharedInstance
    // api
    let api = MAPI.SharedInstance
    struct Segues {
        static let showMapView = "showMapSegue"
        static let showProfile = "gotoPerfilSegue"
        static let showHelp = "gotoHelpSegue"
    }
    
    //     let controllersDic = ["Inicio":"InicioNC","Acerca de Monchis":"aboutNC","Ayuda":"AyudaNC","Perfil":"PerfilNC"]
    let controllersDic:[String:String] = ["Volver":"InicioTC","Perfil":"PerfilNC"]
    // MARK: LifeCycle
    
    @IBAction func debugSwitched(sender: UISwitch) {
        user.debugMode = sender.on
    }
    @IBOutlet weak var debugSwitch: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.autoresizingMask = [.FlexibleTopMargin, .FlexibleBottomMargin, .FlexibleWidth]
        if user.isTemporal {
            userLabel?.text = "Invitado"
        }else{
            userLabel?.text = "\(user.first_name) \(user.last_name)"
        }
        setupDebugSwitch()

        
        switch user.authenticationMethod {
        case .Facebook :
            conectadoViaLbl.text = "Via Facebook"
        case .Twitter:
            conectadoViaLbl.text = "Via Twitter"
        case .Monchis:
            conectadoViaLbl.text = "Via Monchis"
        case .Unregistered:
            conectadoViaLbl.text = "Iniciando sesión..."
        }
        
        /* MAKE ROUND PROFILE IMAGE */
        userFBImageView.layer.cornerRadius = userFBImageView.bounds.width/2
        userFBImageView.clipsToBounds = true
     
        registerNotifications()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if user.isTemporal {
            userLabel?.text = "Invitado"
        }else{
            userLabel?.text = "\(user.first_name) \(user.last_name)"
        }
        
        
        switch user.authenticationMethod {
        case .Facebook :
            conectadoViaLbl.text = "Via Facebook"
            let facebookProfileUrl = "http://graph.facebook.com/\(self.user.facebook_id)/picture?type=large"
            userFBImageView.hnk_setImageFromURL(NSURL(string: facebookProfileUrl)!)
            
            
        case .Twitter:
            conectadoViaLbl.text = "Via Twitter"
        case .Monchis:
            conectadoViaLbl.text = "Via Monchis"
        case .Unregistered:
            conectadoViaLbl.text = "Iniciando sesión..."
        }
        setupDebugSwitch()
    }
    
    func setupDebugSwitch(){
        if user.email == "javier@4qstudios.com.py" || user.email == "mbcpublicidades@gmail.com" || user.email == "jfrancopy1@gmail.com" {
            debugSwitch.hidden = false
        }else{
            debugSwitch.hidden = true
        }
        debugSwitch.setOn(user.debugMode, animated: true)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if unwindedFromMapController {
            direccionesCVC?.reloadData()
            unwindedFromMapController = false
        }
    }
    
    
    // Register notifications
    func registerNotifications(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SideMenuViewController.willShowMenuViewController), name: "willShowMenuViewController", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SideMenuViewController.didRecognizePanGesture(_:)), name: "didRecognizePanGesture", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SideMenuViewController.willHideMenuViewController), name: "willHideMenuViewController", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SideMenuViewController.addNewAddress), name: "addNewAddressBtnPressed", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SideMenuViewController.newUserRegisteredNotification), name: "newUserRegisteredNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SideMenuViewController.updateUI), name: "userLoggedInNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SideMenuViewController.updateAddressUI), name: "updateAddressUINotification", object: nil)

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SideMenuViewController.openAddressBox), name: "openAddressBoxNotification", object: nil)
        
    }
    
    func updateUI(){
        if user.isTemporal {
            userLabel?.text = "Invitado"
        }else{
            userLabel?.text = "\(user.first_name) \(user.last_name)"
        }
        tableView.reloadData()
        switch user.authenticationMethod {
        case .Facebook :
            conectadoViaLbl.text = "Via Facebook"
            let facebookProfileUrl = "http://graph.facebook.com/\(self.user.facebook_id)/picture?type=large"
            userFBImageView.hnk_setImageFromURL(NSURL(string: facebookProfileUrl)!)
        case .Twitter:
            conectadoViaLbl.text = "Via Twitter"
        case .Monchis:
            conectadoViaLbl.text = "Via Monchis"
        case .Unregistered:
            conectadoViaLbl.text = "Iniciando sesión..."
        }
        setupDebugSwitch()

    }
    
    func updateAddressUI(){
        tableView.beginUpdates()
        tableView.reloadData()
        tableView.endUpdates()
    }

    func openAddressBox(){
        if !direccionesSelected{
            
            //getUserAddresses()
            let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0)) as! SideMenuTableViewCell
            UIView.animateWithDuration(0.5, animations: {
                cell.arrowImgView.transform = CGAffineTransformMakeRotation(1.5708)
            })
            direccionesSelected = true
            tableView.beginUpdates()
            
            tableView.reloadRowsAtIndexPaths([ NSIndexPath(forRow: 2, inSection: 0)], withRowAnimation: .Automatic)
            tableView.endUpdates()
            
        }
    }
    
    func newUserRegisteredNotification(){
      updateUI()
    }
    
    
    //Facebook profile update
    func onProfileUpdated(sender: NSNotification){
        
    }
    
    
    // MARK:  Selectors RESideMenu
    func willShowMenuViewController(){
        
    }
    
    
    func didRecognizePanGesture(sender: NSNotification){
        
    }
    
    
    
    func willHideMenuViewController(){
        
    }
    
    
    
    
    func createWaitScreen(){
        
        let spinner = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinner.mode = MBProgressHUDMode.Indeterminate
        spinner.label.text = NSLocalizedString("Buscando Restaurantes",comment: "Buscando restaurantes para nueva direccion de delivery")
        spinner.label.font = UIFont(name: "OpenSans-Semibold", size: 14.0)!
        
    }
    
    func dismissWaitScreen(){
        dispatch_async(dispatch_get_main_queue()) {
            MBProgressHUD.hideHUDForView(self.view, animated: true)
        }
    }
    
    
    
    @IBAction func didLongPressAtAddress(sender: UILongPressGestureRecognizer) {
        if sender.state == .Ended {
            return
        }
        let location = sender.locationInView(direccionesCVC)
        let ip = direccionesCVC.indexPathForItemAtPoint(location)
        if ip != nil {
            
            if ip?.row < user.addresses.count {
                let alert = UIAlertController(title: NSLocalizedString("Opciones para dirección",  comment: "Opciones para la direccion seleccionada"), message: "", preferredStyle: .ActionSheet)
                let delete = UIAlertAction(title: NSLocalizedString("Borrar", comment: "Borrar"), style: .Destructive, handler: { (action) -> Void in
                    
                    /*      ####    BORRAR ADDRESS ####   */
                    
                    customWaitScreen("Borrando dirección...", comment: "Borrando direccion seleccionada", view: self.view, blurView: nil)
                    
                    let parameters = self.user.addresses[ip!.row].parseAddressForAPI()
                    
                    
                    
                    
                    self.api.jsonRequest(MAPI.Command.USER_ADDRESSES_DELETE, parameters: parameters) { (json, error) -> Void in
                        dismissCustomWaitScreen(self.view, blurView: nil)
                        
                        print(json,error)
                        
                        if error == nil {
                            //self.dismissWaitScreen()
                            if let json = json {
                                let success = json["success"].bool ?? false
                                if success {
                                    Mixpanel.mainInstance().track(event: "Usuario Borro Direccion",properties: parameters)

                                    
                                    self.direccionesCVC.performBatchUpdates({ () -> Void in
                                        self.user.addresses.removeAtIndex(ip!.row)
                                        self.direccionesCVC.deleteItemsAtIndexPaths([ip!])
                                        
                                        }, completion: {fin in
                                            if self.user.addresses.count == 0 {
                                                self.noAddrsLblPOINTER.hidden = false
                                            }else{
                                                self.noAddrsLblPOINTER.hidden = true
                                                
                                            }
                                    })
                                }else{ // error at deleting
                                    SCLAlertView().showError("Lo sentimos!",subTitle: json["data"].string ?? "Ocurrio un error inesperado!")
                                }
                            }
                        }else{ // posibly network error
                            SCLAlertView().showError("Lo sentimos!",subTitle: "Ocurrio un error inesperado!")
                        }
                    }
                    
                })
                let edit = UIAlertAction(title: NSLocalizedString("Editar", comment: "Editar"), style: .Default, handler: { (action) -> Void in
                    let filter = MCart.sharedInstance.orders.filter({ (ordr) -> Bool in
                        return ordr.delivery_type == 1
                    })
                    if self.user.addresses[ip!.row].is_default && filter.count > 0 {
                        
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        
                        alertView.addButton("Cambiar posición del Pin") {
                            self.editMapMode = true
                            self.performSegueWithIdentifier(Segues.showMapView, sender: self.user.addresses[ip!.row])
                        }
                        
                        alertView.addButton("Volver", action: {})
                        alertView.showNotice("Atención!", subTitle: "Si cambias la posición del Pin, todas las ordenes de tu carrito marcadas como Delivery seran eliminadas.")

                        
                        
                    }else{
                        self.editMapMode = true
                        self.performSegueWithIdentifier(Segues.showMapView, sender: self.user.addresses[ip!.row])
                    }
                    
                })
                let cancel = UIAlertAction(title: NSLocalizedString("Cancelar", comment: "Cancelar"), style: .Cancel, handler: nil)
                alert.addAction(edit)
                if !self.user.addresses[ip!.row].is_default {
                    alert.addAction(delete)
                }
                alert.addAction(cancel)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.presentViewController(alert, animated: true, completion: nil)
                })
                
            }
        }
        
    }
    
    
    func logout() {
        Mixpanel.mainInstance().track(event: "Usuario Cerro Sesion")
        self.user.logged = false
        self.tabBarController?.selectedIndex = 0
        self.sideMenuViewController.hideMenuViewController()
        loginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
        loginVC.delegate = self
        self.presentViewController(loginVC, animated: true, completion: nil)
    }
    
    
    // MARK: UITableViewDataSource
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if user.authenticationMethod == .Unregistered {
            if indexPath.row == 0 {
                return UITableViewCell()
            }
        }
        
        if indexPath.row == 2 && direccionesSelected {
            let  cell = tableView.dequeueReusableCellWithIdentifier("direccionesCell", forIndexPath: indexPath) as! SideMenuTVC
            direccionesCVC = cell.collectionView
            if !(direccionesCVC.gestureRecognizers!).contains(longPressGesture) {
                direccionesCVC.addGestureRecognizer(longPressGesture)
            }
            noAddrsLblPOINTER = cell.noAddressesLbl
            if self.user.addresses.count == 0 {
                cell.noAddressesLbl.hidden = false
            }else{
                cell.noAddressesLbl.hidden = true
            }
            
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView?.reloadData()
            return cell
        }
        
        if indexPath.row == 2 && !direccionesSelected {
            return UITableViewCell()
        }
        
        let  cell = tableView.dequeueReusableCellWithIdentifier("profileReuseID", forIndexPath: indexPath) as! SideMenuTableViewCell
        
        cell.menuIconImageView?.contentMode = UIViewContentMode.ScaleAspectFill
        
        
        if indexPath.row  == 0 {
            if user.isTemporal {
                cell.menuTitle?.text = NSLocalizedString("Registrarme / Iniciar sesión", comment: "titulo del menu para ingresar a los datos del usuario")
            }else{
                cell.menuTitle?.text = NSLocalizedString("Perfil", comment: "titulo del menu para ingresar a los datos del usuario")
            }
            
            cell.menuIconImageView?.image = UIImage(named: "icono-perfil")
        }
        
        if indexPath.row  == 1{
            
            cell.menuTitle?.text = NSLocalizedString("Tus Direcciónes", comment: "titulo del menu para ingresar a las direcciones del usuario")
            cell.menuIconImageView?.image = UIImage(named: "icono-direcciones")
            
        }
        
        if indexPath.row  == 3{
            cell.menuTitle?.text = NSLocalizedString("Ayuda", comment: "titulo del menu para ingresar a la ayuda de monchis")
            cell.menuIconImageView?.image = UIImage(named: "icono-ayuda-side-menu")
            
        }
        
        if indexPath.row == 4 {
            cell.menuTitle?.text = NSLocalizedString("Tengo un Restaurante", comment: "titulo del menu para ingresar a la ayuda de monchis")
            cell.menuIconImageView?.image = UIImage(named: "icono-tengo-un-restaurante")
        }
        
        if indexPath.row == 5 {
            cell.menuTitle?.text = NSLocalizedString("Reporta un error", comment: "titulo del menu para ingresar a la ayuda de monchis")
            cell.menuIconImageView?.image = UIImage(named: "icono-reportar-error")
        }
        if indexPath.row  == 6 {
            cell.menuTitle?.text = NSLocalizedString("Acerca de Monchis", comment: "titulo del menu para ingresar acerca de monchis")
            cell.menuIconImageView?.image = UIImage(named: "ic_about")
            
        }
        if indexPath.row  == 7 {
            if user.authenticationMethod == .Unregistered {
                cell.menuTitle?.text = NSLocalizedString("Salir", comment: "titulo del menu para cerrar sesión")
            }else{
                cell.menuTitle?.text = NSLocalizedString("Cerrar sesión", comment: "titulo del menu para cerrar sesión")
            }
            
            cell.menuTitle?.font = UIFont(name: "OpenSans-Bold", size: 18.0)
            cell.menuIconImageView?.image = UIImage(named: "icono-cerrar")
            
        }
        
        return cell
        
        
        //        else{
        //
        //            let cell = tableView.dequeueReusableCellWithIdentifier("profileReuseID", forIndexPath: indexPath) as! SideMenuTableViewCell
        //
        //            cell.menuTitle?.font = UIFont(name: "OpenSans-Semibold", size: 18.0)
        //
        //
        //
        //
        //            if indexPath.row  == 0 {
        //                cell.menuTitle?.text = NSLocalizedString("Perfil", comment: "titulo del menu para ingresar a los datos del usuario")
        //                cell.menuIconImageView?.image = UIImage(named: "icono-perfil")
        //            }
        //
        //
        //            if indexPath.row  == 1 {
        //                cell.menuTitle?.text = NSLocalizedString("Tus Direcciónes", comment: "titulo del menu para ingresar a las direcciones del usuario")
        //                cell.menuIconImageView?.image = UIImage(named: "icono-direcciones")
        //
        //            }
        //
        //            if indexPath.row  == 2 {
        //                cell.menuTitle?.text =  NSLocalizedString("Ayuda", comment: "titulo del menu para ingresar a la ayuda de monchis")
        //                cell.menuIconImageView?.image = UIImage(named: "icono-ayuda-side-menu")
        //            }
        //
        //            if indexPath.row  == 3 {
        //                cell.menuTitle?.text = NSLocalizedString("Acerca de Monchis", comment: "titulo del menu para ingresar acerca de monchis")
        //                cell.menuIconImageView?.image = UIImage(named: "ic_about")
        //            }
        //
        //            if indexPath.row  == 4 {
        //                if user.authenticationMethod == .Unregistered {
        //                    cell.menuTitle?.text = NSLocalizedString("Salir", comment: "titulo del menu para cerrar sesión")
        //                }else{
        //                    cell.menuTitle?.text = NSLocalizedString("Cerrar sesión", comment: "titulo del menu para cerrar sesión")
        //                }
        //                cell.menuTitle?.font = UIFont(name: "OpenSans-Bold", size: 18.0)
        //                cell.menuIconImageView?.image = UIImage(named: "icono-cerrar")
        //            }
        //
        //            if sizeOfPF != nil {
        //                cell.menuIconImageView?.bounds.size = sizeOfPF
        //            }
        //
        //            return cell
        //        }
        
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRectZero)
    }
    
    
    // MARK: TableViewDelegate/Datasource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if user.isTemporal {
            return 7
        }
        return 8
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.whiteColor() //make the text white
        header.alpha = 0.5 //make the header transparent
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        if indexPath.row == 6 {
            UIApplication.sharedApplication().openURL(NSURL(string: "http://monchis.com.py")!)
        }
        
        if indexPath.row == 3 {
            self.performSegueWithIdentifier(Segues.showHelp, sender: self)
        }
        if indexPath.row == 4 {
            let mailComposeViewController = configuredMailComposeViewControllerResto()
            if MFMailComposeViewController.canSendMail() {
                self.presentViewController(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }
        }
        if indexPath.row == 5 {
            let mailComposeViewController = configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                self.presentViewController(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }
        }
        if indexPath.row == 7 {
            logout()
        }
        if indexPath.row == 1 && !direccionesSelected{
            
            //getUserAddresses()
            if user.logged {
            let cell = tableView.cellForRowAtIndexPath(indexPath) as! SideMenuTableViewCell
            UIView.animateWithDuration(0.5, animations: {
                cell.arrowImgView.transform = CGAffineTransformMakeRotation(1.5708)
            })
            direccionesSelected = true
            tableView.beginUpdates()
           
            tableView.reloadRowsAtIndexPaths([ NSIndexPath(forRow: 2, inSection: 0)], withRowAnimation: .Automatic)
            tableView.endUpdates()
            }
            
        }else if indexPath.row == 1 && direccionesSelected {
            if user.logged {

            let cell = tableView.cellForRowAtIndexPath(indexPath) as! SideMenuTableViewCell

            UIView.animateWithDuration(0.5, animations: {
                cell.arrowImgView.transform = CGAffineTransformIdentity
            })
            direccionesSelected = false
            tableView.beginUpdates()
            tableView.reloadRowsAtIndexPaths([ NSIndexPath(forRow: 2, inSection: 0)], withRowAnimation: .Automatic)
            tableView.endUpdates()
            }
        }
        if indexPath.row == 0 {
            if user.isTemporal {
                loginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
                loginVC.delegate = self
                self.presentViewController(loginVC, animated: true, completion: nil)
                
            }else{
                if user.logged {
                    self.performSegueWithIdentifier(Segues.showProfile, sender: self)
                }
            }
        }
        
    }
    
    var loginVC: LoginViewController!
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["info@monchis.com.py"])
        mailComposerVC.setSubject("Reportar un error o evento inesperado")
        return mailComposerVC
    }
    
    
    func configuredMailComposeViewControllerResto() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["info@monchis.com.py"])
        mailComposerVC.setSubject("Tengo un Restaurante")
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        SCLAlertView().showError("Lo sentimos", subTitle: "Al parecer no tienes un correo configurado.")
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if user.authenticationMethod == .Unregistered {
            if indexPath.row == 0 {
                return 0
            }
        }
        
        if indexPath.row == 2 && direccionesSelected{
            return 200
        }
        if indexPath.row == 2 && !direccionesSelected{
            return 0
        }
        return 65
    }
    
    
    // END TableViewDataSource/Delegate
    
    
    
    
    func getUserAddresses(){
        
        api.jsonRequest(.USER_ADDRESSES, parameters: nil) { (json, error) -> Void in
            
            if error == nil {
                if let json = json {
                    let addresses = json["data"].array ?? []
                    self.user.addresses = []
                    for address in addresses {
                        let newAddr = MAddress(json: address)
                        self.user.addresses.append(newAddr)
                    }
                    self.direccionesCVC?.reloadData()
                    
                }
            }else{ // network error
                
            }
        }
        
    }
    
    
    
    
    
    // MARK: - Navigation
    var editMapMode: Bool = false
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Segues.showMapView {
            let dvc = segue.destinationViewController as! UINavigationController
            let map = dvc.viewControllers.first as! AddressMapV3ViewController
            if self.editMapMode {
                let addressToEdit = sender as! MAddress
                map.address = addressToEdit.copy() as! MAddress
            }
            map.editingMode = editMapMode

        }
    }
    
    @IBAction func unwindFromMapViewForNewAddress(segue: UIStoryboardSegue){
        if segue.identifier == "unwindToSideMenu" {
            _ = segue.sourceViewController as! MapViewController
            unwindedFromMapController = true
            if self.user.addresses.count == 0 {
                self.noAddrsLblPOINTER.hidden = false
            }else{
                NSNotificationCenter.defaultCenter().postNotificationName(kUserDidChangeDefaultAddressNotification, object: nil)
                self.noAddrsLblPOINTER.hidden = true
            }
        }
    }
    
}

// MARK: Direcciones Extension
extension SideMenuViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        //return user.addresses.count + 1 //addresses plus add btn
        
        return user.addresses.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("reuseID", forIndexPath: indexPath) as! DireccionesSideMenuCollectionViewCell
        cell.image.image = UIImage(named: "pin-direccion-gris")
        cell.titulo.text = user.addresses[indexPath.row].name
        
        // Configure the cell
        if user.addresses[indexPath.row].is_default {
            
            cell.image.image = UIImage(named: "pin-direccion-verde")
            
            UIView.animateWithDuration(0.4, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: [.Repeat, .AllowUserInteraction, .CurveEaseInOut, .Autoreverse], animations: {
                cell.image.transform = CGAffineTransformMakeTranslation(0, -5)
                
                }, completion: nil)
            
        }else {
            UIView.animateWithDuration(0.1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options:  .CurveEaseInOut , animations: {
                cell.image.transform = CGAffineTransformIdentity
                
                }, completion: nil)
        }
        
        return cell
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if indexPath.row < user.addresses.count {
            return CGSize(width: collectionView.bounds.width, height: 40)
            
        }else{
            return CGSize(width: 60, height: collectionView.bounds.height)
        }
    }
    
    
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "reuseHeaderID", forIndexPath: indexPath) as! DireccionesSideMenuCollectionReusableView
        
        if indexPath.section == 0 {
            header.image.image = UIImage(named: "home")
            header.titulo.text = "Home"
        }
        if indexPath.section == 1 {
            header.image.image = UIImage(named: "schoolAddress")
            header.titulo.text = "School"
            
        }
        if indexPath.section == 2 {
            header.titulo.text = "Work"
            header.image.image = UIImage(named: "work")
        }
        header.transform = CGAffineTransformMakeTranslation(0, 100)
        
        UIView.animateWithDuration(0.4, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.6, options: .CurveEaseInOut, animations: {
            header.transform = CGAffineTransformMakeRotation(CGFloat(M_2_PI))
            header.transform = CGAffineTransformMakeTranslation(0, 0)
            
            }, completion: nil)
        //        UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseIn, animations: {
        //            header.transform = CGAffineTransformMakeTranslation(0, 0)
        //            }, completion: {fin in
        //
        //        })
        return header
    }
    
    func addNewAddress(){
        self.editMapMode = false
        self.performSegueWithIdentifier(Segues.showMapView, sender: self)
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row < user.addresses.count {
            
            // find old default and animate it back
            if self.user.addresses.count > 0 {
                if self.user.defaultAddress.id == user.addresses[indexPath.row].id {
                    return
                }
                if user.defaultAddress.id != 0 {
                    let index = user.addresses.indexOf(user.defaultAddress)
                    let oldCell = collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: index!, inSection: 0)) as? DireccionesSideMenuCollectionViewCell
                    UIView.animateWithDuration(0.5, delay: 0, options: .BeginFromCurrentState , animations: {
                        oldCell?.image.transform = CGAffineTransformMakeTranslation(0, 0)
                        },completion: nil)
                    
                    
                    
                    //set the new default address
                    if MCart.sharedInstance.orders.filter({ (order) -> Bool in
                        return order.delivery_type == 1
                    }).count > 0  {
                        let alertS = SCLAlertView()
                        alertS.addButton("Cambiar", action: {
                            self.updateAddress(self.user.defaultAddress,newAddr: self.user.addresses[indexPath.row])
                        })
                        alertS.showNotice("Atención!", subTitle: NSLocalizedString("Si cambias de dirección, las ordenes en tu carrito marcadas como Delivery seran eliminadas. Estas seguro que deseas continuar?", comment: "Si cambias de dirección, todas tus ordenes que estan marcadas como delivery seran enviadas a la nueva dirección que selecciones."), closeButtonTitle: "Cancelar")
                    }else{
                        updateAddress(user.defaultAddress,newAddr: user.addresses[indexPath.row])
                    }
                    
                }
                
            }
        }
        
    }
    

    
    
    func updateAddress(oldAddr: MAddress, newAddr: MAddress){
        oldAddr.is_default = false
        newAddr.is_default = true
//        let parameters:[String:AnyObject] =  newAddr.parseAddressForAPI()!
        let parameters:[String:AnyObject] =  ["id":newAddr.id,"is_default":newAddr.is_default]

        //we create the wait screen
        let spinner = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinner.mode = MBProgressHUDMode.Indeterminate
        spinner.label.text = NSLocalizedString("Cambiando dirección..",comment: "Cambiando la dirección default para el envio")
        spinner.label.font = UIFont(name: "OpenSans-Semibold", size: 14.0)!
        api.jsonRequest(MAPI.Command.USER_ADDRESSES_UPDATE, parameters: parameters) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters \(parameters),",json,error)
            }
            if error  == nil {
                if let success = json?["success"].bool {
                    if success {
                        NSNotificationCenter.defaultCenter().postNotificationName(kUserDidChangeDefaultAddressNotification, object: nil)
                        Answers.logCustomEventWithName("Usuario cambio dirección de Delivery",
                                                       customAttributes: ["User ID":self.user.id])
                        Mixpanel.mainInstance().track(event: "Usuario Cambio Direccion",properties: parameters)
                        self.user.addresses.sortInPlace({ (addr1, addr2) -> Bool in
                            return addr1.is_default
                        })
                        Mixpanel.mainInstance().people.set(property: "Direccion Default", to: ["Latitud": self.user.defaultAddress.latitude ?? "",
                            "Longitud": self.user.defaultAddress.longitude ?? "",
                            "Nombre": self.user.defaultAddress.name ?? ""])

                    }else{
                        oldAddr.is_default = true
                        newAddr.is_default = false
                        SCLAlertView().showError("Lo sentimos!",subTitle: json?["data"].string ?? "Ocurrio un error inesperado!")
                    }
                }
                
            }else{ //network error
                SCLAlertView().showError("Lo sentimos!",subTitle: "Ocurrio un error inesperado!")

            }
            self.direccionesCVC.reloadData()
            self.direccionesCVC.scrollToItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), atScrollPosition: .Top, animated: true)
            self.dismissWaitScreen()
        }
    }
    
    //login delegates
    
    func didSuccededFacebookLogin(){
        loginVC.dismissViewControllerAnimated(true, completion: nil)
        if MUser.sharedInstance.addresses.count == 0 {
            let addressVC = self.storyboard?.instantiateViewControllerWithIdentifier("AddressMapV3ViewController") as! AddressMapV3ViewController
            self.presentViewController(addressVC, animated: true, completion: nil)
        }
        self.updateUI()
    }
    func didSuccededMonchisLogin(){
        loginVC.dismissViewControllerAnimated(true, completion: nil)
        if MUser.sharedInstance.addresses.count == 0 {
            let addressVC = self.storyboard?.instantiateViewControllerWithIdentifier("AddressMapV3ViewController") as! AddressMapV3ViewController
            self.presentViewController(addressVC, animated: true, completion: nil)
        }
        self.updateUI()
    }
    func didSuccededRegistration(){
        loginVC.dismissViewControllerAnimated(true, completion: nil)
        self.updateUI()
    }
    
    
}


