//
//  AddressesPopOverCollectionViewController.swift
//  Monchis
//
//  Created by jrivarola on 18/11/15.
//  Copyright © 2015 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD

class AddressesPopOverCollectionViewController: UICollectionViewController {
    let cart = MCart.sharedInstance
    struct ReuseCell {
        static let address = "direccionesPopOverReuseID"
    }
    let user = MUser.sharedInstance
    let api = MAPI.SharedInstance
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return user.addresses.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ReuseCell.address, forIndexPath: indexPath) as! DireccionesSideMenuCollectionViewCell
        
        cell.image.image = UIImage(named: "pin-direccion-gris")
        cell.titulo.text = user.addresses[indexPath.row].name
        
        // Configure the cell
        if user.addresses[indexPath.row].is_default {
            
            cell.image.image = UIImage(named: "pin-direccion-verde")
            
            UIView.animateWithDuration(0.4, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: [.Repeat, .AllowUserInteraction, .CurveEaseInOut, .Autoreverse], animations: {
                cell.image.transform = CGAffineTransformMakeTranslation(0, -5)
                
                }, completion: nil)
            
        }else {
            UIView.animateWithDuration(0.1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options:  .CurveEaseInOut , animations: {
                cell.image.transform = CGAffineTransformIdentity
                
                }, completion: nil)

        }

        
        // Configure the cell
    
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSize(width: 80, height: collectionView.bounds.height)
        
    }
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        /*
        if lastCell != nil {
        
        UIView.animateWithDuration(0.5, delay: 0, options: .BeginFromCurrentState , animations: {
        self.lastCell?.image.transform = CGAffineTransformMakeTranslation(0, 0)
        self.lastCell?.contentView.backgroundColor = UIColor.clearColor()
        }, completion: {fin in
        //self.lastCell.image.layer.removeAllAnimations()
        self.lastCell?.chosen = false
        
        })
        }
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! DireccionesSideMenuCollectionViewCell
        cell.chosen = true
        if lastCell != cell {
        cell.contentView.backgroundColor = UIColor(hexString: "#B41717", alpha: 0.5)
        lastCell = cell
        UIView.animateWithDuration(0.4, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .Repeat | .AllowUserInteraction | .CurveEaseInOut | .Autoreverse, animations: {
        cell.image.transform = CGAffineTransformMakeTranslation(0, -5)
        
        }, completion: {fin in
        
        })
        }else{
        UIView.animateWithDuration(0.5, delay: 0, options: .BeginFromCurrentState , animations: {
        self.lastCell.image.transform = CGAffineTransformMakeTranslation(0, 0)
        }, completion: {fin in
        //self.lastCell.image.layer.removeAllAnimations()
        self.lastCell?.contentView.backgroundColor = UIColor.clearColor()
        self.lastCell?.chosen = false
        
        self.lastCell = nil
        })
        
        }
        */
        
        
        // createWaitScreen()
        
        
        if indexPath.row < user.addresses.count {
            
            // find old default and animate it back
            if self.user.addresses.count > 0 {
                
                if user.defaultAddress.id != 0 {
                    let index = user.addresses.indexOf(user.defaultAddress)
                    let oldCell = collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: index!, inSection: 0)) as? DireccionesSideMenuCollectionViewCell
                    UIView.animateWithDuration(0.5, delay: 0, options: .BeginFromCurrentState , animations: {
                        oldCell?.image.transform = CGAffineTransformMakeTranslation(0, 0)
                        },completion: nil)
                    
                    
                    //we create the wait screen
                    MBProgressHUD.showHUDAddedTo(self.collectionView!, animated: true)
                    //set the new default address
                    //set the new default address
                  
                    self.updateAddress(user.defaultAddress,addr: user.addresses[indexPath.row])
                    
                }
                
            }
        }
        
    }
    
    func updateAddress(oldAddr: MAddress,addr: MAddress){
        oldAddr.is_default = false
        addr.is_default = true
        let parameters:[String:AnyObject] =  ["is_default": true, "id": addr.id]
        
        api.jsonRequest(MAPI.Command.USER_ADDRESSES_UPDATE, parameters: parameters) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters \(parameters)", json,error)
            }
            MBProgressHUD.hideHUDForView(self.collectionView!, animated: true)

            if error  == nil {
                if let success = json?["success"].bool {
                    if success {
                        NSNotificationCenter.defaultCenter().postNotificationName(kUserDidChangeDefaultAddressNotification, object: nil)

                        NSNotificationCenter.defaultCenter().postNotificationName("didChangeAllOrdersToNewDeliveryBranchNotification", object: self)
                        
                        self.user.defaultAddress.is_default = false
                        addr.is_default = true
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }else{
                        oldAddr.is_default = true
                        addr.is_default = false
                        self.collectionView?.reloadData()
                        let alert = UIAlertController(title: "Lo sentimos", message: json?["data"].string ?? "Ocurrio un error inesperado.", preferredStyle: .Alert)
                        let ok = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                        alert.addAction(ok)
                        self.presentViewController(alert, animated: true, completion: nil)
                        
                    }
                }
                
                
                
            }else{ //network error
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let cancel = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(cancel)
                self.presentViewControllerFromVisibleViewController(alert, animated: true, completion: nil)
            }
            self.collectionView?.reloadData()
        }
    }
    
    //DEPRECATED
//    func refreshAllOrdersForNewAddress(address: MAddress){
//        counter = self.cart.orders.count
//        for order in self.cart.orders {
//            setDelivery(order,address: address)
//        }
//    }
//    
//    var counter: Int!
//    
//    func setDelivery(order: MOrder, address: MAddress) {
//        var parameters: [String:AnyObject]!
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "Y-m-d H:m:s"
//        let now = dateFormatter.stringFromDate(NSDate())
//            parameters = ["order_id":order.order_id!,"delivery_type":1,"address_id":address.id ?? 0]
//       
//        api.jsonRequest(.SET_DELIVERY_INFO, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
//            if debugModeEnabled {
//                print(#function,"called with parameters \(parameters)",json,error)
//            }
//            if error == nil{
//                if let success = json?["success"].bool {
//                    if success {
//                        self.counter = self.counter - 1
//                        if let orderSection = self.cart.orders.indexOf({$0.order_id == order.order_id}){
//                            if let br = json?["data"]["branch"] {
//                                let newBranch = MRestaurant(data: br)
//                                self.cart.orders[orderSection].branch = newBranch
//
//                            }
//                                self.cart.orders[orderSection].delivery_type = 1
//                            
//                        }
//                        if self.counter == 0 {
//                            NSNotificationCenter.defaultCenter().postNotificationName("didChangeAllOrdersToNewDeliveryBranchNotification", object: self)
//                            
//                            self.user.defaultAddress.is_default = false
//                            address.is_default = true
//                            self.updateAddress(address)
//
//                        }
//                        
//                    }else{
//                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
//
//                        let message = json?["data"].string ?? "No hay mensaje de error disponible."
//                        let alert = UIAlertController(title: NSLocalizedString("Lo sentimos!", comment: "Lo sentimos!"), message:  NSLocalizedString(message, comment: "Ha ocurrido un error al procesar tu pedido!"), preferredStyle: .Alert)
//                        let aceptar = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Cancel, handler: nil)
//                        alert.addAction(aceptar)
//                        self.presentViewController(alert, animated: true, completion: nil)
//                        
//                        
//                    }
//                }
//            }else{
//                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
//
//                if self == self.navigationController?.visibleViewController {
//                    
//                    let alert = UIAlertController(title: NSLocalizedString("Lo sentimos!", comment: "Lo sentimos!"), message:  NSLocalizedString("Ha ocurrido un error al procesar tu pedido!", comment: "Ha ocurrido un error al procesar tu pedido!"), preferredStyle: .Alert)
//                    let aceptar = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Cancel, handler: nil)
//                    alert.addAction(aceptar)
//                    self.presentViewController(alert, animated: true, completion: nil)
//                    
//                }
//                
//                    
//                }
//            }
//        }
//
//

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */
    
    

}
