//
//  CartSideSubTotalTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 9/12/15.
//  Copyright © 2015 cibersons. All rights reserved.
//

import UIKit

class CartSideSubTotalTableViewCell: UITableViewCell {

    @IBOutlet weak var subTotal: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
