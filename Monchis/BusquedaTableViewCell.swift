//
//  BusquedaTableViewCell.swift
//  Monchis
//
//  Created by Javier Rivarola on 12/29/15.
//  Copyright © 2015 cibersons. All rights reserved.
//

import UIKit

class BusquedaTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
