//
//  VideoTutoMapaViewController.swift
//  Monchis
//
//  Created by Javier Rivarola on 7/15/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class VideoTutoMapaViewController: UIViewController {

    var playerController: AVPlayerViewController!
    var player: AVPlayer!
    @IBOutlet weak var videoView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("view video load called")
        playerController = AVPlayerViewController()
        playerController.showsPlaybackControls = false
        if mapTuto {
            let showTuto = NSUserDefaults.standardUserDefaults().boolForKey(kShowMapTutorialKey)
            if !showTuto {
                // Do any additional setup after loading the view.
                
                for v in self.videoView.subviews {
                    v.removeFromSuperview()
                }
                if let path = NSBundle.mainBundle().pathForResource("crear-direccion", ofType:"mp4") {
                    let url = NSURL.fileURLWithPath(path)
                      player = AVPlayer(URL: url)
                    playerController.player = player
                    self.videoView.addSubview(playerController.view)
                    self.addChildViewController(playerController)
                    self.playerController.didMoveToParentViewController(self)
                    self.player.play()

                }else{
                    print("Could not load video")
                }
            }
        }
        if orderTuto {
            let showTuto = NSUserDefaults.standardUserDefaults().boolForKey(kShowOrderTutorialKey)
            if !showTuto {
                
                
                
               
                
                // Do any additional setup after loading the view.
                
                for v in self.videoView.subviews {
                    v.removeFromSuperview()
                }
                if let path = NSBundle.mainBundle().pathForResource("realizar-pedido", ofType:"mp4") {
                    let url = NSURL.fileURLWithPath(path)
                      player = AVPlayer(URL: url)
                    playerController.player = player
                    self.videoView.addSubview(playerController.view)
                    self.addChildViewController(playerController)
                    self.playerController.didMoveToParentViewController(self)
                    self.player.play()
                }else{
                    print("Could not load video")
                }
            }
        }

          }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    

    var mapTuto: Bool = false
    var orderTuto: Bool = false
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        playerController.view.frame = self.videoView.bounds

           }
    
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var dontShowAgainBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBAction func entendidoPressed(sender: UIButton) {
        player?.pause()
        player = nil
        self.dismissViewControllerAnimated(true, completion: {fin in
            
            NSNotificationCenter.defaultCenter().postNotificationName("checkUniversalLinks", object: nil)
        
        
    })

    }

    @IBAction func dontShowAgainPressed(sender: UIButton) {
        if mapTuto {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: kShowMapTutorialKey)
        }else{
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: kShowOrderTutorialKey)
        }
        player?.pause()
        self.dismissViewControllerAnimated(true, completion: {fin in
        
            NSNotificationCenter.defaultCenter().postNotificationName("checkUniversalLinks", object: nil)

        
        })

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
