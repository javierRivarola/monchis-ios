//
//  MultipleOptionsTableViewCell.swift
//  Monchis
//
//  Created by Javier Rivarola on 8/3/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit

class MultipleOptionsTableViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate ,UIScrollViewDelegate {

    var customField: MCustomizableField!
    let user = MUser.sharedInstance
    var itemIndexPath: NSIndexPath!
    var parentController: CustomizarProductoViewController!
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 50
        tableView.delaysContentTouches = true
        tableView.tableFooterView = UIView(frame: CGRectZero)
        // Initialization code
        tableView.panGestureRecognizer.delaysTouchesBegan = tableView.delaysContentTouches
        tableView.layoutIfNeeded()
        tableView.sizeToFit()
//        self.addonsTVHeightConstraint.constant = self.tableView.contentSize.height

    }
    
    
    
    @IBOutlet weak var addonsTVHeightConstraint: NSLayoutConstraint!
    
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // MARK: - Properties
    
    /// Flag which tells if the cell is expanded.
    internal private(set) var expanded = false
    
    // MARK: - Actions
    
    /**
     Public setter of the `expanded` property (this should be overriden by a subclass for custom UI update)
     
     :param: expanded `true` if the cell should be expanded, `false` if it should be collapsed.
     :param: animated If `true` action should be animated.
     */
    internal func setExpanded(expanded: Bool, animated: Bool) {
        self.expanded = expanded
        
    }
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customField?.addons.count ?? 0
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return customField?.addons[indexPath.row].cellDescriptionHeight ?? 50
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("checkExtraReuseID", forIndexPath: indexPath) as! CheckExtraTableViewCell
        cell.addonBtn.tag = indexPath.row
        cell.addonBtn.setTitle(customField.addons[indexPath.row].etiqueta, forState: .Normal)
        cell.addonBtn?.adjustsImageWhenHighlighted = false
        if customField.userSelectedIndexes.contains(indexPath.row) { //addon is selected
            cell.addonDescriptionLbl.text = customField.addons[indexPath.row].descripcion
            cell.contentView.backgroundColor = UIColor(hexString: "#EFFBE4")
            cell.addonBtn.setImage(UIImage(named: "checkbox-marcado"), forState: UIControlState.Normal)
           
        }else{ // addon not selected
            cell.addonDescriptionLbl.text = ""
            cell.contentView.backgroundColor = UIColor.whiteColor()
            cell.addonBtn.setImage(UIImage(named: "checkbox-vacio"), forState: UIControlState.Normal)
        }
        let price = customField.addons[indexPath.row].price
        if price == 0 {
            cell.addonPriceLbl.text =  NSLocalizedString("Sin costo.", comment: "Addon de item sin costo")
        }else{
            cell.addonPriceLbl.text = customField.addons[indexPath.row].price?.asLocaleCurrency
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        customField?.addons[indexPath.row].cellDescriptionHeight = cell.bounds.height + 1
//        self.addonsTVHeightConstraint.constant = self.tableView.contentSize.height
        
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if !customField.userSelectedIndexes.contains(indexPath.row) { // selection not in model
            if customField.userSelectedIndexes.count == customField.limit! {
                //show alert
                print("error, solo se pueden elegir \(customField.limit) opciones")
            }else{
                customField.userSelectedIndexes.append(indexPath.row)
                //                //add checkmark
                
                
                //                cell.addonBtn.setImage(UIImage(named: "checkbox-marcado"), forState: UIControlState.Normal)
                //                cell.contentView.backgroundColor = UIColor(hexString: "#EFFBE4")
                //                let price = customField.addons[indexPath.row].price
                //                if price == 0 {
                //                    cell.addonPriceLbl.text = NSLocalizedString("Sin costo.", comment: "Addon de item sin costo")
                //                }else{
                //                    cell.addonPriceLbl.text = customField.addons[indexPath.row].price?.asLocaleCurrency
                //                }
                //                cell.addonDescriptionLbl.text = customField.addons[indexPath.row].descripcion
            }
        }else{ //already in model, deselect
            let index =  customField.userSelectedIndexes.indexOf(indexPath.row)
            customField.userSelectedIndexes.removeAtIndex(index!)
            //            cell.contentView.backgroundColor = UIColor.whiteColor()
            //            cell.addonBtn.setImage(UIImage(named: "checkbox-vacio"), forState: UIControlState.Normal)
            //            cell.addonPriceLbl.text = ""
            //            cell.addonDescriptionLbl.text = ""
        }
        parentController?.updatePriceLabel()
        let customizationIndexPath = NSIndexPath(forRow: itemIndexPath.row + 1, inSection: itemIndexPath.section)
        //for animating
//        let itemPrice = customField.addons[itemIndexPath.row].price
//        if itemPrice > 0 {
//        let cell = tableView.cellForRowAtIndexPath(indexPath) as! CheckExtraTableViewCell
//        
//        let lbl = UILabel(frame: cell.addonPriceLbl.frame)
//        lbl.textColor = UIColor(hexString: "#47C266")
//        lbl.font = UIFont.boldSystemFontOfSize(12)
//        lbl.text = "+ \(itemPrice?.asLocaleCurrency)"
//        self.tableView.addSubview(lbl)
//            UIView.animateWithDuration(5, animations: {
//                lbl.alpha = 0
//                lbl.transform = CGAffineTransformMakeTranslation(0, 25)
//                }, completion: { (fin) in
//                    lbl.removeFromSuperview()
//            })
//            
//        }
        
        //        self.tableView.scrollEnabled = false
        //        self.parentController?.tableView.scrollEnabled = false
        self.tableView.beginUpdates()
        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        self.tableView.endUpdates()
        self.tableView.sizeToFit()
//        self.addonsTVHeightConstraint.constant = self.tableView.contentSize.height
        
        
        //        let offset = parentController?.tableView.contentOffset
        //        UIView.setAnimationsEnabled(false)
        //        parentController?.tableView.beginUpdates()
        //        parentController?.tableView.reloadRowsAtIndexPaths([itemIndexPath], withRowAnimation: .None)
        //
        //        parentController?.tableView.endUpdates()
        //        UIView.setAnimationsEnabled(true)
        //        parentController?.tableView.layoutIfNeeded()
        //        parentController?.tableView.contentOffset = offset!
        
        
        //                parentController?.tableView.reloadRowsAtIndexPaths([itemIndexPath], withRowAnimation: .Automatic)
        parentController?.tableView.beginUpdates()
        parentController?.tableView.reloadRowsAtIndexPaths([customizationIndexPath,itemIndexPath], withRowAnimation: .Automatic)
        parentController?.tableView.endUpdates()
        //        self.tableView.scrollEnabled = true
        //        self.parentController?.tableView.scrollEnabled = true
        
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        //return 50
    }
    
    
    
   }
