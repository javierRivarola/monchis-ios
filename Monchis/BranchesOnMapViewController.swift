//
//  BranchesOnMapViewController.swift
//  Monchis
//
//  Created by jrivarola on 2/2/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import GoogleMaps

class BranchesOnMapViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    var branches: [MRestaurant] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        locationManager.delegate = self
        mapView.settings.myLocationButton = true
        mapView.camera = GMSCameraPosition(target: branches.first!.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        mapView.myLocationEnabled = true
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        drawBranchesOnMap()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func volverPressed(sender: UIBarButtonItem) {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    var markers: [GMSMarker] = []
    
    func drawBranchesOnMap(){
        mapView.clear()
        markers = []
        for branch in branches {
            let newMarker = GMSMarker()
            let lat = Double(branch.lat.floatValue ?? 0)
            let lng = Double(branch.lng.floatValue ?? 0)
            let branchCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            newMarker.position = branchCoordinate
            newMarker.title = branch.name
            newMarker.snippet = "Presiona para Navegar"
            
            
            
            
            
            newMarker.map = mapView
            mapView.selectedMarker = newMarker
            markers.append(newMarker)
        }
        zoomMapToFitMarkers(markers)
    }
    
    func zoomMapToFitMarkers(markers: [GMSMarker]) {
        var bounds:GMSCoordinateBounds = GMSCoordinateBounds()
        let amountOfMarkersToFit = 5
        var counter = 0
        for marker in markers {
            if counter < amountOfMarkersToFit {
                bounds = bounds.includingCoordinate(marker.position)
            }
            counter += 1
        }
        var location = CLLocationCoordinate2D(latitude: markers.first!.position.latitude + 0.01, longitude: markers.first!.position.longitude + 0.01)

        if mapView.myLocation != nil {
            location = CLLocationCoordinate2D(latitude: mapView.myLocation!.coordinate.latitude, longitude: mapView.myLocation!.coordinate.longitude )
            bounds = bounds.includingCoordinate(location)
        }else{
            bounds = bounds.includingCoordinate(MUser.sharedInstance.defaultAddress.computedCoordinate)
        }
        CATransaction.begin()
        CATransaction.setValue(1.0, forKey: kCATransactionAnimationDuration)
        mapView.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(bounds, withPadding: 80))
        CATransaction.commit()
    }
    
    
    func mapView(mapView: GMSMapView, didTapInfoWindowOfMarker marker: GMSMarker) {
        let alert = UIAlertController(title: NSLocalizedString("Abrir en Google Maps", comment: "Deseas abrir Google Maps para navegar?"), message: NSLocalizedString("Deseas abrir Google Maps para navegar?", comment: "Deseas abrir Google Maps para navegar?"), preferredStyle: .Alert)
        let cancel = UIAlertAction(title: NSLocalizedString("Cancelar", comment: "Cancelar"), style: .Cancel, handler: nil)
        let aceptar = UIAlertAction(title: NSLocalizedString("Ir a GMaps.", comment: "Ir a GMaps."), style: .Default) { (action) -> Void in
            if (UIApplication.sharedApplication().canOpenURL(NSURL(string:"comgooglemaps://")!)) {
                UIApplication.sharedApplication().openURL(NSURL(string:
                    "comgooglemaps://?saddr=&daddr=\(marker.position.latitude),\(marker.position.longitude)&directionsmode=driving")!)
                
            } else {
                let alert = UIAlertController(title: NSLocalizedString("Lo sentimos!", comment: "Lo sentimos!"), message: NSLocalizedString("No tienes Google Maps disponible.", comment: "No tienes Google Maps disponible."), preferredStyle: .Alert)
                let cancel = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Cancel, handler: nil)
                alert.addAction(cancel)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
        
        
        
        alert.addAction(cancel)
        alert.addAction(aceptar)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    func mapView(mapView: GMSMapView, didTapMarker marker: GMSMarker) -> Bool {
        let alert = UIAlertController(title: NSLocalizedString("Abrir en Google Maps", comment: "Deseas abrir Google Maps para navegar?"), message: NSLocalizedString("Deseas abrir Google Maps para navegar?", comment: "Deseas abrir Google Maps para navegar?"), preferredStyle: .Alert)
        let cancel = UIAlertAction(title: NSLocalizedString("Cancelar", comment: "Cancelar"), style: .Cancel, handler: nil)
        let aceptar = UIAlertAction(title: NSLocalizedString("Ir a GMaps.", comment: "Ir a GMaps."), style: .Default) { (action) -> Void in
            if (UIApplication.sharedApplication().canOpenURL(NSURL(string:"comgooglemaps://")!)) {
                UIApplication.sharedApplication().openURL(NSURL(string:
                    "comgooglemaps://?saddr=&daddr=\(marker.position.latitude),\(marker.position.longitude)&directionsmode=driving")!)
                
            } else {
                let alert = UIAlertController(title: NSLocalizedString("Lo sentimos!", comment: "Lo sentimos!"), message: NSLocalizedString("No tienes Google Maps disponible.", comment: "No tienes Google Maps disponible."), preferredStyle: .Alert)
                let cancel = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Cancel, handler: nil)
                alert.addAction(cancel)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
        
        
        
        alert.addAction(cancel)
        alert.addAction(aceptar)
        self.presentViewController(alert, animated: true, completion: nil)
        return true
    }
    
    // MARK: LocationManagerDelegate
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            drawBranchesOnMap()
            locationManager.stopUpdatingLocation()
        }
    }


}
