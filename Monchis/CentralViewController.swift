//
//  CentralViewController.swift
//  Monchis
//
//  Created by jrivarola on 8/25/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import SwiftyJSON
import ReachabilitySwift
import SCLAlertView
import KeychainSwift
import DZNEmptyDataSet
import PagingMenuController
import FBSDKShareKit
import Crashlytics
import KeychainSwift
import Mixpanel
import AVFoundation
import SwiftyJSON


var WaitScreen: LoadingViewController!
var universalLinkType: String = ""
var monchisTabController: UITabBarController!
var reachability: Reachability!
var globalCentralViewController: CentralViewController!
var globalNotificationOrder: MOrder!


class CentralViewController: UIViewController, UIGestureRecognizerDelegate,iCarouselDataSource, iCarouselDelegate, SearchResultSelectedDelegate, DZNEmptyDataSetSource, UITabBarControllerDelegate, BranchSelectionDelegate, PromotionsFlowLayoutDelegate, UIScrollViewDelegate {
    let initialColor = UIColor.blackColor().colorWithAlphaComponent(0.13)
    
    var timer: NSTimer!
    var searchData = [[]]
    var searchController: UISearchController!
    var searchTableViewController:SearchTableViewController!
    var searchBar: UISearchBar!
    let api = MAPI.SharedInstance
    let user = MUser.sharedInstance
    let promotions = MPromotions.SharedInstance
    var recomendations: [MProduct] = []
    var dismissedLoadedScreen: Bool = false
    var lastTimeForPromotions: NSDate!
    var loadedCategories: Bool = false
    var loadedPromotions: Bool = false
    var promotionsHeightCache: [String:CGFloat] = [:]
    var promotionsWidthCache: [String:CGFloat] = [:]
    var loadedHistory: Bool = false
    let categorias = ["Ver Todos","Pizzas","Hamburguesas", "Empanadas", "Oriental", "Sandwiches", "Restaurantes", "Natural Food", "Helados", "Bebidas"]
    
    var selectedBranch: MRestaurant!
    var promocionesCarousel:iCarousel! {
        didSet {
            promocionesCarousel.currentItemIndex = Int(round(CGFloat(promotions.products.count/2)))
        }
    }
    var cachedPromotions: SwiftyJSON.JSON!

    var userHistoryCV: UICollectionView!
    var notificationOrder: MOrder! {
        didSet{
            self.tabBarController?.selectedIndex = 4
            
            //                            let order = MOrder(json: info["order"] as! SwiftyJSON.JSON)
            let order = MOrder()
            order.branch = MRestaurant()
            order.branch.lat = "-25.323642842396712"
            order.branch.lng = "-57.624165415763855"
            let delay = 0.5 * Double(NSEC_PER_SEC)
            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
            dispatch_after(time, dispatch_get_main_queue()) {
                globalOrderStatusController.notificationOrder = order
            }
            
        }
    }
    var promotionsCollectionView: UICollectionView!
    struct Constants {
        static let reuseIdentifier = "CategoriasCellReuseID"
        static let promocionesReuseIdentifier = "PromoCell"
        static let reusePromoCellID = "promoReuseID"
        static let reuseCategoriasCellID = "categoriasReuseID"
        static let reuseCollectionViewRecomendaciones = "recomendadosCellID"
        static let reuseRecomendaciones = "reuseRecomendaciones"
        static let ReuseSearchCellID = "ReuseSearchCellID"
        static let reuseHistorial = "historialContentCollectionCell"
    }
    struct Segues {
        static let CategoriaSeleccionada = "categoriaSeleccionada"
        static let ScannerSegue = "valesSegue"
        static let TutoSegue = "videoTutoSegue"
    }
    
    
    let CATEGORIAS_CELL_HEIGHT: CGFloat = 160
    let PROMOCIONES_SECTION_HEIGHT: CGFloat = 250
    let RECOMENDACIONES_SECTION_HEIGHT: CGFloat = 265
    let RECOMMENDED_BANNER_WIDHT: CGFloat = 200
    
    var data = ["1","2","3","4"]
    
    var historyCollectionView: UICollectionView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    let images = ["Ver Todos":"icono-ver-todas","Pizzas":"icono-pizzas","Hamburguesas":"icono-hamburguesas", "Empanadas":"icono-empanadas", "Oriental":"icono-oriental", "Sandwiches":"icono-sandwiches", "Restaurantes":"icono-restaurantes", "Natural Food":"icono-natural-food", "Helados":"icono-helados", "Bebidas":"icono-bebidas","Burger King":"promo1","Don Vito":"promo2","Pizza Hut":"promo3"]
    var controllerArray : [UIViewController] = []
    
    @IBOutlet weak var searchBtn: UIButton!
    // MARK: LifeCycle
    var loadingVC: LoadingViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        monchisTabController = self.tabBarController
        checkUniversalLinks()
        registerForNotifications()
        configureSearch()
        //        if user.logged {
        //            loadHistory()
        //        }
        //        getPromotions()
        getFranchisesCategories()
    }
    
    
    
    
    // 1. Returns the photo height
    func collectionView(collectionView:UICollectionView, heightForPhotoAtIndexPath indexPath:NSIndexPath , withWidth width:CGFloat) -> CGFloat {
        if let photo = promotions.products[indexPath.item].photo {
            let boundingRect =  CGRect(x: 0, y: 0, width: width, height: CGFloat(MAXFLOAT))
            let rect  = AVMakeRectWithAspectRatioInsideRect(photo.size, boundingRect)
            
            return rect.size.height
        }else{
            return 200
        }
    }
    
    // 2. Returns the annotation size based on the text
    func collectionView(collectionView: UICollectionView, heightForAnnotationAtIndexPath indexPath: NSIndexPath, withWidth width: CGFloat) -> CGFloat {
        //            let annotationPadding = CGFloat(4)
        //            let annotationHeaderHeight = CGFloat(17)
        //
        //            let photo = promotions.products[indexPath.item].photo
        //            let font = UIFont(name: "AvenirNext-Regular", size: 10)!
        //            let commentHeight = photo.heightForComment(font, width: width)
        //            let height = annotationPadding + annotationHeaderHeight + commentHeight + annotationPadding
        //            return height
        
        return 0
    }
    
    
    
    
    func checkUniversalLinks(){
        let showTuto = NSUserDefaults.standardUserDefaults().boolForKey(kShowOrderTutorialKey)
        
        if universalLinkType != "" && user.logged && showTuto{
            self.tabBarController?.selectedIndex = 0
            let delay = 0.8 * Double(NSEC_PER_SEC)
            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
            dispatch_after(time, dispatch_get_main_queue()) {
                let decoded = getQueryStringParameter(universalLinkType, param: "q")?.fromBase64() ?? ""
                let newURL = "https://admin.monchis.com.py?\(decoded)"
                
                let type = getQueryStringParameter(newURL, param: "type")
                
                let franchiseid = getQueryStringParameter(newURL, param: "franchiseid")
                let productid = getQueryStringParameter(newURL, param: "productid")
                
                let tmpBranch = MRestaurant()
                tmpBranch.franchise = MFranchise()
                tmpBranch.franchise?.id = Int(franchiseid ?? "0")
                if type == "product" {
                    Mixpanel.mainInstance().track(event: kUserOpenedUniversalLinkEvent, properties: ["Producto ID": productid ?? "", "Franquicia ID": franchiseid ?? "", "Tipo": type ?? ""])
                    
                    deliveryOrPickupHistorySearch(tmpBranch, completion: { (isDelivery, newBranch) in
                        universalLinkType = ""
                        
                        let productoVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
                        if isDelivery == nil || isDelivery == true {
                            productoVC.product = MProduct()
                            productoVC.product.id = Int(productid ?? "0")
                            let branch = newBranch
                            productoVC.restaurant = branch
                            branch.mode = 0
                            self.searchController.active = false
                            self.navigationController?.pushViewController(productoVC, animated: true)
                            
                        }else{//pickup
                            self.searchController.active = false
                            let product = MProduct()
                            product.id = Int(productid ?? "0")
                            self.selectedProduct = product
                            let bvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                            bvc.franchise = newBranch.franchise
                            bvc.delegate = self
                            self.navigationController?.pushViewController(bvc, animated: true)
                            
                        }
                    })
                    
                }
                if type == "branch" {
                    self.selectedProduct = nil
                    Mixpanel.mainInstance().track(event: kUserOpenedUniversalLinkEvent, properties: ["Local ID": franchiseid ?? "","Tipo": type ?? ""])
                    
                    deliveryOrPickupHistorySearch(tmpBranch, completion: { (isDelivery, newBranch) in
                        universalLinkType = ""
                        if isDelivery == nil || isDelivery == true {
                            
                            let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
                            newBranch.mode = 0
                            restaurantVC.restaurant = newBranch
                            
                            //            controllers?.append(categoriasVC!)
                            self.searchController.active = false
                            self.navigationController?.pushViewController(restaurantVC, animated: true)
                            
                            
                            
                        }else{//pickup
                            self.searchController.active = false
                            self.selectedBranch = newBranch
                            let bvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                            bvc.franchise = newBranch.franchise
                            bvc.delegate = self
                            self.navigationController?.pushViewController(bvc, animated: true)
                            
                        }
                    })
                    
                }
                //            if type == "voucher"{
                //                let voucher = getQueryStringParameter(newURL, param: "v")
                //            }
                
                
            }
        }
    }
    
    
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        if tabBarController.selectedIndex == 0 {
            Answers.logCustomEventWithName("Usuario cambio a tab Explorar",
                                           customAttributes:[:])
            Mixpanel.mainInstance().track(event: kUserChangedToTabEvent, properties: ["Tab":"Inicio"])
        }
        if tabBarController.selectedIndex == 1 {
            Answers.logCustomEventWithName("Usuario cambio a tab Historial",
                                           customAttributes:[:])
            Mixpanel.mainInstance().track(event: kUserChangedToTabEvent, properties: ["Tab":"Categorias"])
        }
        if tabBarController.selectedIndex == 2 {
            Answers.logCustomEventWithName("Usuario cambio a tab Favoritos",
                                           customAttributes:[:])
            Mixpanel.mainInstance().track(event: kUserChangedToTabEvent, properties: ["Tab":"Historial"])
        }
        if tabBarController.selectedIndex == 3 {
            Answers.logCustomEventWithName("Usuario cambio a tab Carrito",
                                           customAttributes:[:])
            Mixpanel.mainInstance().track(event: kUserChangedToTabEvent, properties: ["Tab":"Carrito"])
        }
        if tabBarController.selectedIndex == 4 {
            Answers.logCustomEventWithName("Usuario cambio a tab Pedidos",
                                           customAttributes:[:])
            Mixpanel.mainInstance().track(event: kUserChangedToTabEvent, properties: ["Tab":"Pedidos"])
        }
        
    }
    
    
    //MARK: ScrollViewDelegate
    
    func scrollViewDidScrollToTop(scrollView: UIScrollView) {
        if scrollView == promotionsCollectionView {
            
        }
    }
    
    
    func reachabilityChanged(notification: NSNotification) {
        
        let reachability = notification.object as! Reachability
        
        if reachability.isReachable() {
            
            
            if !user.logged {
                user.automaticUserLogin({ (success) in
                    if success {
                        self.loadHistory()
                        
                    }
                })
            }else{
                loadHistory()
            }
            
            getPromotions()
            getFranchisesCategories()
            print("Reachable via WiFi")
            if reachability.isReachableViaWiFi() {
            } else {
                print("Reachable via Cellular")
            }
        } else {
            print("Network not reachable")
            if MUser.sharedInstance.logged {
                SCLAlertView().showNotice("Revisa tu conexión!", subTitle: "Al parecer no tienes una conexión a internet.")
                MAPI.SharedInstance.needsToShowAlertForCacheBrowsing = true
            }
        }
    }
    
    func updateUIForHistory(json: [SwiftyJSON.JSON]){
        var tmpOrders:[MOrder] = []
        loadedHistory = true
        for order in json {
            let newOrder:MOrder = MOrder(json: order)
            tmpOrders.append(newOrder)
        }
        //        if !MOrder.compareOrders(self.user.orderHistory, orders2: tmpOrders) {
        self.user.orderHistoryForCentralController = tmpOrders
        
        //        }
        self.historyCollectionView.reloadData()
        dismissLoadingScreen()
    }
    
    
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        if user.logged {
            if scrollView.tag == 2 {
                if user.orderHistoryByProducts.count == 0 {
                    return NSAttributedString(string: "Aún no tienes pedidos realizados...", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
                }
            }
            if scrollView.tag == 1 {
                
            }
        }else{
            if scrollView.tag == 2 {
                
                return NSAttributedString(string: "Obteniendo historial...", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
            }
            
            
        }
        return nil
    }
    
    func customViewForEmptyDataSet(scrollView: UIScrollView) -> UIView? {
        if scrollView.tag == 1 {
            let sp = UIActivityIndicatorView()
            sp.tintColor = .whiteColor()
            sp.startAnimating()
            return sp
        }
        return nil
    }
    
    func loadHistory(){
        
        let parameters:[String : AnyObject] =   [
            "limit":15,
            "state": ["finished","delivered","pickup"],
            "offset":0
        ]
        
        api.returnCachedData(.ORDER_HISTORY, parameters: parameters) { (json) in
            if debugModeEnabled {
                print(#function,"called from central view controller", parameters,json)
            }
            if debugModeEnabled {
                print("returnCachedData for loadhistory","called with parameters: nil",json)
            }
            if let json = json?["data"]["orders"].array {
                self.updateUIForHistory(json)
            }
        }
        
        api.postQueueRequest(.ORDER_HISTORY, parameters: parameters) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,json,error)
            }
            if error == nil {
                if let json = json?["data"]["orders"].array {
                    self.updateUIForHistory(json)
                    
                }
            }
        }
    }
    
    
    func reflectBadgeCart(){
        if let carritoItem = tabBarController?.tabBar.items?[3] {
            carritoItem.badgeValue = "\(MCart.sharedInstance.orders.count)"
        }
    }
    
    func registerForNotifications(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CentralViewController.setHistoryTabSelected), name: "showHistoryOrderTabNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CentralViewController.setCartTabSelected), name: kUserWantsToFinalizeOrdersNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CentralViewController.setHistoryTabSelected), name: "scrollToOrderNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CentralViewController.updateUIAfterUserLogin), name: "userLoggedInNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CentralViewController.showLoginScreenAfterFailedLogin), name: "showLoginScreenAfterFailedLogin", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CentralViewController.checkUniversalLinks), name: "checkUniversalLinks", object: nil)
        
        registerReachNotifications()
    }
    
    func showLoginScreenAfterFailedLogin(){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
        
        self.presentViewController(vc, animated: false, completion: {fin in
            if self.user.authenticationMethod == .Monchis {
                vc.emailTxt?.text = KeychainSwift().get(kMonchisUserEmail)
                vc.passTxt?.text = KeychainSwift().get(kMonchisUserPassword)
            }
        })
    }
    
    func registerReachNotifications(){
        //REACHIBILITY NOTIFICATOR
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
            
            reachability.whenReachable = { reachability in
                // this is called on a background thread, but UI updates must
                // be on the main thread, like this:
                dispatch_async(dispatch_get_main_queue()) {
                    if reachability.isReachableViaWiFi() {
                        print("Reachable via WiFi")
                    } else {
                        print("Reachable via Cellular")
                    }
                }
            }
            
            
            reachability.whenUnreachable = { reachability in
                // this is called on a background thread, but UI updates must
                // be on the main thread, like this:
                dispatch_async(dispatch_get_main_queue()) {
                    print("Not reachable")
                }
            }
            
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CentralViewController.reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
            do {
                try reachability.startNotifier()
                
                
            } catch {
                print("Unable to start notifier")
            }
        } catch {
            print("Unable to create Reachability")
        }
        
        
    }
    
    
    
    func setHistoryTabSelected(){
        self.tabBarController?.selectedIndex = 1
    }
    
    func setCartTabSelected(){
        self.tabBarController?.selectedIndex = 3
        let cartNavigationController = self.tabBarController?.selectedViewController as? UINavigationController
        cartNavigationController?.popToRootViewControllerAnimated(false)
        //        let section =  MCart.sharedInstance.orders.count > 0 ?  MCart.sharedInstance.orders.count + 1 : 0
        //        cartViewController.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection:section), atScrollPosition: .Bottom, animated: true)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.historyCollectionView = embededMainPageVC.historyCV
        self.promotionsCollectionView = embededMainPageVC.promosCV
        self.historyCollectionView.delegate = self
        self.historyCollectionView.dataSource = self
        self.promotionsCollectionView.dataSource = self
        self.promotionsCollectionView.delegate = self
        
        
        self.promotionsCollectionView.scrollsToTop = false
        self.historyCollectionView.scrollsToTop = false

        if let layout = promotionsCollectionView?.collectionViewLayout as? PromotionsFlowLayout {
            layout.delegate = self
        }
        getPromotions()
        if user.logged {
            loadHistory()
            getFavorites()
            MCart.sharedInstance.getOrders({ (success) -> Void in
                self.reflectBadgeCart()
            })
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func updateUIAfterUserLogin(){
        //                loadedHistory = false
        //                loadedCategories = false
        //                loadedPromotions = false
        //                dismissedLoadedScreen = false
        //                self.tableView.hidden = true
        //                self.spinner.startAnimating()
        //        //api calls
        getFranchisesCategories()
        getPromotions()
        reflectBadgeCart()
        loadHistory()
        getFavorites()
        checkUniversalLinks()
        MCart.sharedInstance.getOrders({ (success) -> Void in
            self.reflectBadgeCart()
        })
    }
    
    func doneVideoTutoPressed(sender: UIButton) {
        hideTutorial()
    }
    
    func dontShowAgainTutoPressed(sender: UIButton) {
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: kShowOrderTutorialKey)
        hideTutorial()
    }
    
    func hideTutorial(){
        videoTutoController.player?.pause()
        videoTutoController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if self.navigationController!.respondsToSelector(Selector("interactivePopGestureRecognizer")) {
            self.navigationController!.interactivePopGestureRecognizer!.enabled = false
        }
    }
    
    var selectedProduct: MProduct!
    func didSelectBranch(branch: MRestaurant,order: MOrder?)
    {
        if selectedProduct != nil {
            let customizeProductVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
            customizeProductVC.restaurant = branch
            customizeProductVC.product = selectedProduct
            self.navigationController?.pushViewController(customizeProductVC, animated: true)
        }else{
            let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
            branch.mode = 1
            restaurantVC.restaurant = branch
            self.navigationController?.pushViewController(restaurantVC, animated: true)
        }
        
    }
    
    var preloadedControllers: Bool = false
    
    func updateUIForCategories(json: [SwiftyJSON.JSON]){
        loadedCategories = true
        var tmpCat: [MCategory] = []
        for category in json {
            tmpCat.append(MCategory(json: category))
        }
        categories = tmpCat
        preloadedControllers = false
        
        
        
        
        //        if !preloadedControllers {
        //            self.preloadControllers()
        //            preloadedControllers = true
        //        }
        self.dismissLoadingScreen()
        
    }
    
    func getFranchisesCategories(){
        
        api.returnCachedData(.LIST_CATEGORIES, parameters: nil) { (json) in
            if debugModeEnabled {
                print("returnCachedData for CATEGORIES","called with parameters: nil",json)
            }
            if let jsonArray = json?["data"].array {
                self.updateUIForCategories(jsonArray)
            }
            
        }
        
        
        
        
        api.postQueueRequest(.LIST_CATEGORIES, parameters: nil) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,json,error)
            }
            if error == nil {
                if let jsonCategories = json?["data"].array {
                    self.updateUIForCategories(jsonCategories)
                    
                }
                
            }else{ // network error
                
                
            }
        }
    }
    
    
    
    
    
    
    
    func preloadControllers(){
        menus = []
        for categoria in categories {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantesCVID") as! RestaurantesCollectionViewController
            vc.category = categoria
            vc.title = categoria.name
            controllersForLoading.append(vc)
        }
        
    }
    
    func configureSearch(){
        //SearchBar Controller
        searchTableViewController = storyboard?.instantiateViewControllerWithIdentifier("SearchTableViewController") as! SearchTableViewController
        searchTableViewController.searchDelegate = self
        searchController = UISearchController(searchResultsController: searchTableViewController)
        searchController.hidesNavigationBarDuringPresentation = false
        //searchController.searchBar.searchBarStyle = UISearchBarStyle.Minimal
        searchController.searchBar.sizeToFit()
        searchBar = searchController.searchBar
        searchBar.tintColor = UIColor(hexString: "#FF8000")
        searchBar.translucent = false
        searchBar.delegate = self
        for subView in searchBar.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.attributedPlaceholder =  NSAttributedString(string:"Buscar.. (min 3 caracteres)",
                                                                          attributes:[NSFontAttributeName: UIFont(name: "OpenSans-Semibold", size: 13)!])
                }
            }
        }
        searchBar.scopeButtonTitles = ["Productos","Restaurantes"]
        // searchTableViewController.tableView.registerClass(BusquedaTableViewCell.self, forCellReuseIdentifier: "productCellReuseID")
        
        searchController.definesPresentationContext = true
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = true // important
        //self.navigationItem.titleView = searchBar
        searchController.delegate = self
    }
    
    
    
    
    func setupUI(){
        
        //preload all views
        
        for viewController in self.tabBarController!.viewControllers! as! [UINavigationController] {
            var aView = viewController.topViewController?.view.description
        }
        
        
        self.tabBarController?.delegate = self
        if globalNotificationOrder != nil {
            self.tabBarController?.selectedIndex = 4
            
            let delay = 0.5 * Double(NSEC_PER_SEC)
            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
            dispatch_after(time, dispatch_get_main_queue()) {
                globalOrderStatusController.notificationOrder = globalNotificationOrder
            }
        }
        globalCentralViewController = self
        searchBtn?.imageView?.contentMode = .ScaleAspectFit
        if let carritoItem = tabBarController?.tabBar.items?[3] {
            carritoItem.badgeValue = "0"
        }
        let showTuto = NSUserDefaults.standardUserDefaults().boolForKey(kShowOrderTutorialKey)
        if user.logged {
            
            if !showTuto {
                let delay = 0.5 * Double(NSEC_PER_SEC)
                let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                dispatch_after(time, dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier(Segues.TutoSegue, sender: self)
                }
            }
        }
        
        //configure timer
        //        timer = NSTimer.scheduledTimerWithTimeInterval(3.5, target: self, selector: #selector(CentralViewController.animateSlider), userInfo: nil, repeats: true)
        
        //reschedule carousel timer every 6 seconds
        //        NSTimer.scheduledTimerWithTimeInterval(6, target: self, selector: #selector(CentralViewController.configureSlider), userInfo: nil, repeats: true)
        
        
        // MORE TAB
        self.tabBarController?.moreNavigationController.navigationBar.tintColor = UIColor.grayColor()
        self.tabBarController?.moreNavigationController.navigationBar.translucent = false
        self.tabBarController?.moreNavigationController.navigationBar.barTintColor = UIColor.whiteColor()
        let img = UIImage(named: "texto-monchis-header")
        let headerMonchisImage = UIImageView(frame: CGRectMake(0, 0, 50, 25))
        headerMonchisImage.image = img
        headerMonchisImage.contentMode = .ScaleAspectFit
        self.navigationItem.titleView = headerMonchisImage
        
        self.tabBarController?.moreNavigationController.navigationItem.titleView = headerMonchisImage
        
       
        
    }
    
    
    
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    // MARK: Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Segues.CategoriaSeleccionada {
            let dvc = segue.destinationViewController as! MainViewPagingMenuViewController
            dvc.selectedPageIndex = (sender as! NSIndexPath).row
            dvc.categorySelected = categories[(sender as! NSIndexPath).row]
            // dvc.controllersForLoading = controllerArray
        }
        if segue.identifier == Segues.TutoSegue {
            videoTutoController = segue.destinationViewController as! VideoTutoMapaViewController
            videoTutoController.orderTuto = true
        }
        if segue.identifier == "MainPageSegue" {
            embededMainPageVC = segue.destinationViewController as! MainPageTableViewController

            
            
        }
    }
    
    var embededMainPageVC: MainPageTableViewController!
    var videoTutoController: VideoTutoMapaViewController!
    
    
    
    // MARK: Carousel DataSource/Delegate
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView
    {
        
        
        var productImageView: UIImageView
        //create new view if no view is available for recycling
        if (view == nil)
        {
            if index > promotions.products.count {
                return UIView()
            }
            let parentView = UIView(frame: CGRect(x:0, y:0, width: 170, height: 200))
            parentView.backgroundColor = UIColor.whiteColor()
            let view = UIView(frame: CGRect(x:10, y:0, width: 140, height: 150))
            view.backgroundColor = UIColor.clearColor()
            
            productImageView = UIImageView(frame:CGRect(x:0, y:0, width: 150, height: 150))
            promotions.products[index].attachments.first?.width = 150
            promotions.products[index].attachments.first?.height = 150
            if let imgUrl = promotions.products[index].attachments.first?.imageComputedURL {
                productImageView.hnk_setImageFromURL(imgUrl, placeholder: NO_ITEM_PLACEHOLDER)
            }
            
            productImageView.contentMode = UIViewContentMode.ScaleAspectFit
            productImageView.tag = 2
            
            view.addSubview(productImageView)
            
            let description = UIView(frame: CGRect(x:0, y:75, width: 150, height: 75))
            description.backgroundColor = UIColor(hexString: "#000000",alpha: 0.7)
            
            let lbl = UILabel()
            lbl.textColor = UIColor.whiteColor()
            lbl.numberOfLines = 0
            lbl.text = promotions.products[index].product_description
            lbl.lineBreakMode = .ByWordWrapping
            lbl.translatesAutoresizingMaskIntoConstraints = false
            lbl.font = UIFont.systemFontOfSize(11)
            
            
            description.addSubview(lbl)
            let topConstraint = NSLayoutConstraint(item: lbl, attribute: .Top, relatedBy: .Equal, toItem: description, attribute: .Top, multiplier: 1, constant: 0)
            
            let botConstraint = NSLayoutConstraint(item: lbl, attribute: .Bottom, relatedBy: .Equal, toItem: description, attribute: .Bottom, multiplier: 1, constant: 0)
            
            let leftConstraint = NSLayoutConstraint(item: lbl, attribute: .Left, relatedBy: .Equal, toItem: description, attribute: .Left, multiplier: 1, constant: 4)
            
            let rightConstraint = NSLayoutConstraint(item: lbl, attribute: .Right, relatedBy: .Equal, toItem: description, attribute: .Right, multiplier: 1, constant: 4)
            NSLayoutConstraint.activateConstraints([topConstraint,botConstraint,leftConstraint,rightConstraint])
            
            lbl.tag = 10
            //view.addSubview(description)
            
            let branchImageView = UIImageView(frame: CGRect(x:52.5, y:155, width: 41, height: 41))
            branchImageView.tag = 20
            branchImageView.layer.cornerRadius = 40/2
            branchImageView.layer.zPosition = 1000
            branchImageView.clipsToBounds = true
            branchImageView.contentMode = .ScaleAspectFit
            branchImageView.image = NO_BRANCH_PLACEHOLDER
            if promotions.products[index].branch_products.count > 0 {
                promotions.products[index].branch_products.first?.attachment?.width = 40
                promotions.products[index].branch_products.first?.attachment?.height = 40
                if let url = promotions.products[index].branch_products.first?.attachment?.imageComputedURL {
                    branchImageView.hnk_setImageFromURL(url,placeholder: NO_BRANCH_PLACEHOLDER)
                }
            }
            
            view.addSubview(branchImageView)
            parentView.addSubview(view)
            parentView.layer.cornerRadius = 5
            parentView.clipsToBounds = true
            parentView.layer.shouldRasterize = true
            parentView.layer.rasterizationScale = self.view.window?.screen.scale ??  1
            return parentView
            //customView.addSubview(itemView)
        }else{
            if index > promotions.products.count {
                return UIView()
            }
            let productImageView = view?.viewWithTag(2) as? UIImageView
            let branchImageView = view?.viewWithTag(20) as? UIImageView
            promotions.products[index].attachments.first?.width = productImageView?.bounds.width
            promotions.products[index].attachments.first?.height = productImageView?.bounds.height
            if let imgUrl = promotions.products[index].attachments.first?.imageComputedURL {
                productImageView?.hnk_setImageFromURL(imgUrl, placeholder: NO_ITEM_PLACEHOLDER)
                
            }
            productImageView?.contentMode = UIViewContentMode.ScaleAspectFit
            let lbl = view?.subviews.first?.viewWithTag(10) as? UILabel
            lbl?.text = promotions.products[index].product_description
            branchImageView?.image = NO_BRANCH_PLACEHOLDER
            if promotions.products[index].branch_products.count > 0 {
                promotions.products[index].branch_products.first?.attachment?.width = branchImageView?.bounds.width
                promotions.products[index].branch_products.first?.attachment?.height = branchImageView?.bounds.height
                
                if let url = promotions.products[index].branch_products.first?.attachment?.imageComputedURL {
                    branchImageView?.hnk_setImageFromURL(url,placeholder: NO_BRANCH_PLACEHOLDER)
                }
            }
            return view ?? UIView()
            // customView = view!
        }
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
    {
        if (option == .Spacing)
        {
            return value * 1.1
        }
        return value
    }
    
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int
    {
        return promotions.products.count > 0 ? promotions.products.count : 0
    }
    
    func carousel(carousel: iCarousel, didSelectItemAtIndex index: Int) {
        if timer.valid {
            timer.invalidate()
        }
        Answers.logCustomEventWithName("Usuario eligio una promocion",
                                       customAttributes: ["Producto": self.promotions.products[index].name!])
        Mixpanel.mainInstance().track(event: kUserSelectedProductEvent,
                                      properties: ["Producto":self.promotions.products[index].name!, "Producto ID": self.promotions.products[index].id!, "Tipo":"Promocion","Pantalla":"Explorar"])
        deliveryOrPickupHistorySearch(self.promotions.products[index].branch_products.first!, completion: { (isDelivery, newBranch) in
            if isDelivery == nil || isDelivery == true  {
                
                let productoVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
                productoVC.product = self.promotions.products[index]
                let branch = newBranch
                branch.mode = 0
                productoVC.restaurant = branch
                productoVC.refreshBranch()
                self.navigationController?.pushViewController(productoVC, animated: true)
            }else{
                let branchSelectionVC = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                branchSelectionVC.franchise = newBranch.franchise
                
                self.selectedProduct = self.promotions.products[index]
                branchSelectionVC.delegate = self
                self.navigationController?.pushViewController(branchSelectionVC, animated: true)
            }
            
        })
    }
    
    
    func configureSlider(){
        if !timer.valid {
            timer = NSTimer.scheduledTimerWithTimeInterval(3.5, target: self, selector: #selector(CentralViewController.animateSlider), userInfo: nil, repeats: true)
        }
    }
    
    func carouselWillBeginDragging(carousel: iCarousel) {
        if timer.valid {
            timer.invalidate()
        }
    }
    
    func animateSlider(){
        var nextIndex = promocionesCarousel.currentItemIndex+1
        if nextIndex >= promocionesCarousel.numberOfItems {
            nextIndex = 0
        }
        
        promocionesCarousel.scrollToItemAtIndex(nextIndex, animated: true)
    }
    
}


extension CentralViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if collectionView == promotionsCollectionView {
            
            if let url = promotions.products[indexPath.row].attachments.first?.imageComputedURL {
//                return CGSizeMake(collectionView.bounds.width/2, (promotionsHeightCache[url.URLString]) ?? 200)
                return CGSizeMake(collectionView.bounds.width/2, 200)
            }else{
                return CGSizeMake(collectionView.bounds.width/2, 200)
                
            }
            
            
        }else{
            return CGSizeMake(RECOMMENDED_BANNER_WIDHT, collectionView.bounds.height)
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        if collectionView == promotionsCollectionView{
            return 0
        }else{
            return 10
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        if collectionView == promotionsCollectionView{
            return 0
        }else{
            return 10
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        if collectionView == promotionsCollectionView{
            // return categorias.count
            return promotions.products.count
        }else{
            return user.orderHistoryByProducts.count
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if collectionView == historyCollectionView {
            
            /// new version
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Constants.reuseHistorial, forIndexPath: indexPath) as! HistorialContentCollectionViewCell
            let product = user.orderHistoryByProducts[indexPath.row]
            cell.imageView.image = PLACE_HOLDER_IMAGE
            product.attachments.first?.width = cell.imageView.bounds.width
            product.attachments.first?.width = cell.imageView.bounds.height
            
            if let imgUrl = product.attachments.first?.imageComputedURL {
                cell.imageView.hnk_setImageFromURL(imgUrl, placeholder: NO_ITEM_PLACEHOLDER)
            }
            
            cell.product = product
            cell.productNameLbl.text = product.name
            cell.productPriceLbl.text = product.price?.asLocaleCurrency
            cell.quantityLbl.text = "\(product.quantity ?? 1)"
            cell.quantityView.layer.cornerRadius = 25/2
            cell.quantityView.clipsToBounds = true
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Constants.promocionesReuseIdentifier, forIndexPath: indexPath) as! PromotionsCollectionViewCell
            if let url = promotions.products[indexPath.row].attachments.first?.imageComputedURL {
                cell.productImgView.hnk_setImageFromURL(url, placeholder: NO_ITEM_PLACEHOLDER)
                
                cell.productImgView.hnk_setImageFromURL(url, placeholder: NO_ITEM_PLACEHOLDER, success: { (img) in
                    let height = img.size.height * img.scale
                    self.promotionsHeightCache[url.URLString] = height
                    
                    cell.productImgView.image = img
                })
            }
            return cell
            
        }
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if collectionView.tag == 1 {
            if indexPath.row == collectionView.numberOfItemsInSection(0) - 1 {
                Answers.logCustomEventWithName("Usuario eligio Vales",
                                               customAttributes: [:])
                Mixpanel.mainInstance().track(event: kUserSelectedFranchiseCategoryEvent,
                                              properties: ["Categoria": "Vales"])
                self.performSegueWithIdentifier(Segues.ScannerSegue, sender: self)
            }else{
                Answers.logCustomEventWithName("Usuario eligio una categoria de franquicias",
                                               customAttributes: ["Categoria": categories[indexPath.row].name ?? ""])
                Mixpanel.mainInstance().track(event: kUserSelectedFranchiseCategoryEvent,
                                              properties: ["Categoria" : categories[indexPath.row].name ?? ""])
                if MainPage == nil {
                    self.performSegueWithIdentifier(Segues.CategoriaSeleccionada, sender: indexPath)
                }else {
                    MainPage.pagingMenuController.moveToMenuPage(indexPath.row,animated:false)
                    self.navigationController?.pushViewController(MainPage, animated: true)
                    
                }
            }
        }
        if collectionView.tag == 2 {
            //historial
            Answers.logCustomEventWithName("Usuario eligio un producto del Historial",
                                           customAttributes: ["Producto": self.user.orderHistoryByProducts[indexPath.row].name!])
            Mixpanel.mainInstance().track(event: kUserSelectedProductEvent,
                                          properties: ["Producto":self.user.orderHistoryByProducts[indexPath.row].name!, "Producto ID":self.user.orderHistoryByProducts[indexPath.row].id ?? 0, "Tipo":"Ultimos Pedidos", "Pantalla": "Explorar"])
            selectedProduct = user.orderHistoryByProducts[indexPath.row]
            
            deliveryOrPickupHistorySearch(user.orderHistoryByProducts[indexPath.row].branch_products.first!, completion: { (isDelivery, newBranch) in
                if isDelivery == nil || isDelivery == true  {
                    let productoVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
                    let product = self.user.orderHistoryByProducts[indexPath.row]
                    let branch = newBranch
                    productoVC.product = product
                    branch.mode = 0
                    productoVC.restaurant = branch
                    productoVC.refreshBranch()
                    self.navigationController?.pushViewController(productoVC, animated: true)
                }else{
                    let branchSelectionVC = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                    branchSelectionVC.delegate = self
                    branchSelectionVC.franchise = newBranch.franchise
                    self.navigationController?.pushViewController(branchSelectionVC, animated: true)
                }
                
            })
            
        }
    }
    
    
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if collectionView.tag == 0 {
            cell.alpha = 0
            cell.transform = CGAffineTransformMakeTranslation(0, -100)
            UIView.animateWithDuration(0.5, delay: 0, options: .CurveEaseInOut, animations: {
                cell.alpha = 1
                cell.transform = CGAffineTransformIdentity
                }, completion: nil)
        }
    }
    
    
}



extension CentralViewController:  UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
    
    // MARK: UISearchBarDelegate
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            if searchBar.selectedScopeButtonIndex == 0 {
                searchTableViewController.lastProductSearched = ""
                searchTableViewController.loadingBeforeEmptyDataSet = false
            }else{
                searchTableViewController.loadingBeforeEmptyDataSet = false
                searchTableViewController.lastFranchiseSearched = ""
            }
        }
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        print(#function)
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        searchTableViewController.scopeIndex = selectedScope
        if selectedScope == 0 {
            searchBar.text = searchTableViewController.lastProductSearched
        }else{
            searchBar.text = searchTableViewController.lastFranchiseSearched
        }
    }
    
    // MARK: UISearchResultsUpdating
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if let text = searchController.searchBar.text {
            if text.characters.count >= 3 {
                self.searchTableViewController.treshold = true
                
                //                let delay = 0.5 * Double(NSEC_PER_SEC)
                //                let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                //                dispatch_after(time, dispatch_get_main_queue()) {
                if searchController.searchBar.selectedScopeButtonIndex == 0 {
                    self.searchTableViewController.searchProductWithText(text)
                }else{
                    self.searchTableViewController.searchFranchisesWithText(text)
                }
            }
        }else{
            self.searchTableViewController.loadingBeforeEmptyDataSet = false
            self.searchTableViewController.treshold = false
            self.searchTableViewController.products = []
            self.searchTableViewController.restaurants = []
            self.searchTableViewController.tableView.reloadData()
        }
        //        }
        print(#function)
    }
    
    // MARK: UISearchControllerDelegate
    
    func willPresentSearchController(searchController: UISearchController) {
        print(#function)
        
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
    }
    
    
    
    func willDismissSearchController(searchController: UISearchController) {
        
        self.tabBarController!.setTabBarVisible(true, animated: true)
    }
    
    
    @IBAction func searchPressed(sender: UIButton) {
        if searchController.active {
            self.tabBarController!.setTabBarVisible(true, animated: true)
            self.searchController.dismissViewControllerAnimated(true, completion: nil)
        }else{
            self.tabBarController!.setTabBarVisible(false, animated: true)
            self.presentViewController(self.searchController, animated: true){
                
            }
            
        }
        
        
    }
    
    func dismissLoadingScreen(){
        print("Dissmiss loading screen \(loadedHistory) \(loadedPromotions) \(loadedCategories)")
        if user.logged {
            if loadedHistory && loadedPromotions && loadedCategories {
                if !dismissedLoadedScreen {
                    dismissedLoadedScreen = true
                    self.spinner.stopAnimating()
                    UIView.animateWithDuration(0.3, animations: {
                        
                    })
                }
            }
        }else{
            if loadedPromotions && loadedCategories {
                if !dismissedLoadedScreen {
                    dismissedLoadedScreen = true
                    self.spinner.stopAnimating()
                    UIView.animateWithDuration(0.3, animations: {
                        
                    })
                }
            }
        }
        
    }
    
    
    func updateUIForPromotions(json: [SwiftyJSON.JSON]) {
        loadedPromotions = true
        var tmp: [MProduct] = []
        for data in json {
            let newProduct = MProduct(json: data)
            if let attachmentsArray = data["attachments"].array {
                for att in attachmentsArray {
                    let newAtt = MAttachment(json: att)
                    newProduct.attachments = [newAtt]
                }
                
            }
            
            if let branchProducts = data["branch_products"].array {
                var tmpBranches:[MRestaurant] = []
                for bProduct in branchProducts {
                    let newBranch = MRestaurant(data: bProduct["branch"])
                    
                    tmpBranches.append(newBranch)
                }
                newProduct.branch_products = tmpBranches
                
            }
            
            tmp.append(newProduct)
            if let url = newProduct.attachments.first?.imageComputedURL {
                let imgView = UIImageView(frame: CGRectMake(0,0,1000,1000))
                imgView.hnk_setImageFromURL(url, placeholder: NO_ITEM_PLACEHOLDER) { img in
                    newProduct.photo = img
                }
            }
            
            
        }
        
        //        if !MProduct.compareProducts(tmp, prod2: promotions.products) {
        self.promotions.products = tmp
        dispatch_async(dispatch_get_main_queue()) {
            self.promotionsCollectionView.reloadData()
        }
        
        
        //        }
        dismissLoadingScreen()
        //        self.promocionesCarousel?.reloadData()
        
    }
    
    
    
    func getPromotions(){
        
        let now = NSDate()
        let elapsedTime = now.timeIntervalSinceDate(lastTimeForPromotions ?? now)
        if Int(elapsedTime) > 60*60 || elapsedTime == 0  {
            lastTimeForPromotions = now
            api.returnCachedData(.GET_PROMOTIONS, parameters: nil) { (json) in
                if debugModeEnabled {
                    print("returnCachedData for promotions","called with parameters: nil",json)
                }
                if let jsonArray = json?["data"].array {
                    self.updateUIForPromotions(jsonArray)
                }
                self.cachedPromotions = json
                
            }
            api.postQueueRequest(.GET_PROMOTIONS, parameters: nil) { (json, error) in
                
                if debugModeEnabled {
                    print(#function,"called with parameters: nil",json,error)
                }
                if error == nil {
                    if let jsonArray = json?["data"].array {
                        if self.cachedPromotions == json {
                            return
                        }
                        self.updateUIForPromotions(jsonArray)
                    }
                }else{
                    
                }
                
            }
        }
        
        
        //
        //        api.jsonRequest(.GET_PROMOTIONS, parameters: nil, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
        //            if debugModeEnabled {
        //                print(#function,"called with parameters: nil",json,error)
        //            }
        //            if error == nil {
        //                if let jsonArray = json?["data"].array {
        //                    self.promotions = []
        //                    for data in jsonArray {
        //                        let newProduct = MProduct(json: data)
        //                        if let attachmentsArray = data["attachments"].array {
        //                            for att in attachmentsArray {
        //                                let newAtt = MAttachment(json: att)
        //                                newProduct.attachments = [newAtt]
        //                            }
        //
        //                        }
        //
        //                        if let branchProducts = data["branch_products"].array {
        //                            var tmpBranches:[MRestaurant] = []
        //                            for bProduct in branchProducts {
        //                                let newBranch = MRestaurant(data: bProduct["branch"])
        //
        //                                tmpBranches.append(newBranch)
        //                            }
        //                            newProduct.branch_products = tmpBranches
        //
        //                        }
        //
        //                        self.promotions.append(newProduct)
        //                    }
        //
        //                    self.tableView.beginUpdates()
        //                    let sect = NSIndexSet(index: 0)
        //                    self.tableView.reloadSections(sect, withRowAnimation: .Automatic)
        //                    self.tableView.endUpdates()
        //                    self.promocionesCarousel.reloadData()
        //                }
        //
        //            }else{
        //
        //            }
        //        }
    }
    
    
    func getUserRecomendations(){
        api.jsonRequest(.GET_RECOMENDATIONS, parameters: nil, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: nil",json,error)
            }
            if error == nil {
                if let success = json?["success"].bool {
                    if success {
                        if let jsonArray = json?["data"].array {
                            self.recomendations = []
                            for data in jsonArray {
                                let newProduct = MProduct(json: data)
                                if let attachmentsArray = data["attachments"].array {
                                    for att in attachmentsArray {
                                        let newAtt = MAttachment(json: att)
                                        newProduct.attachments = [newAtt]
                                    }
                                    
                                }
                                
                                if let branchProducts = data["branches"].array {
                                    var tmpBranches:[MRestaurant] = []
                                    for bProduct in branchProducts {
                                        let newBranch = MRestaurant(data: bProduct)
                                        tmpBranches.append(newBranch)
                                    }
                                    newProduct.branch_products = tmpBranches
                                    
                                }
                                
                                self.recomendations.append(newProduct)
                            }
                            self.userHistoryCV?.reloadData()
                            
                            
                        }
                    }else{ //some kind of error
                        print(json?["data"])
                    }
                }
                
                
            }else{
                print("network error on recomendation")
            }
        }
        
    }
    
    //    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    //        if tableView == searchTableViewController.tableView {
    //            if searchController.searchBar.selectedScopeButtonIndex == 0 { //product
    //                let selectedProduct = searchTableViewController.products[indexPath.section][indexPath.row]
    //                var controllers = self.navigationController?.viewControllers
    //                let categoriasVC = self.storyboard?.instantiateViewControllerWithIdentifier("MainViewPagingMenuViewController")
    //                let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController")
    //                let categoriasMenuVC = self.storyboard?.instantiateViewControllerWithIdentifier("MenuCategoriaViewController")
    //                let productoVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as? CustomizarProductoViewController
    //                productoVC?.product = selectedProduct
    //                let restaurant = MRestaurant()
    //                restaurant.franchise = selectedProduct.franchise
    //                restaurant.id = selectedProduct.branch_products.first?.branch_id
    //                productoVC?.restaurant = restaurant
    //
    //                controllers?.append(categoriasVC!)
    //                controllers?.append(restaurantVC!)
    //                controllers?.append(categoriasMenuVC!)
    //                controllers?.append(productoVC!)
    //                self.searchController.active = false
    //                self.navigationController?.setViewControllers(controllers!, animated: true)
    //
    //            }else{ //restaurant
    //                var controllers = self.navigationController?.viewControllers
    //                let categoriasVC = self.storyboard?.instantiateViewControllerWithIdentifier("MainViewPagingMenuViewController")
    //                let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
    //                let selectedRestaurant = searchTableViewController.restaurants[indexPath.row]
    //                restaurantVC.restaurant = selectedRestaurant
    //                controllers?.append(categoriasVC!)
    //                controllers?.append(restaurantVC)
    //                self.searchController.active = false
    //                self.navigationController?.setViewControllers(controllers!, animated: true)
    //
    //            }
    //        }
    //    }
    
    // MARK: SearchResultSelectedDelegate
    func didSelectSearchResult(product: MProduct?, branch: MRestaurant?) {
        selectedProduct = nil
        selectedBranch = nil
        if product != nil {
            
            deliveryOrPickupHistorySearch(product!.branch_products.first!, completion: { (isDelivery, newBranch) in
                let productoVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
                if isDelivery == nil || isDelivery == true {
                    productoVC.product = product
                    let branch = newBranch
                    productoVC.restaurant = branch
                    branch.mode = 0
                    productoVC.restaurant?.franchise = product?.franchise
                    self.searchController.active = false
                    self.navigationController?.pushViewController(productoVC, animated: true)
                    
                }else{//pickup
                    self.searchController.active = false
                    self.selectedProduct = product
                    let bvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                    bvc.franchise = newBranch.franchise
                    bvc.delegate = self
                    self.navigationController?.pushViewController(bvc, animated: true)
                    
                }
            })
            
            
        }else{
            deliveryOrPickupHistorySearch(branch!, completion: { (isDelivery, newBranch) in
                
                if isDelivery == nil || isDelivery == true {
                    
                    let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
                    newBranch.mode = 0
                    restaurantVC.restaurant = newBranch
                    
                    //            controllers?.append(categoriasVC!)
                    self.searchController.active = false
                    self.navigationController?.pushViewController(restaurantVC, animated: true)
                    
                    
                    
                }else{//pickup
                    self.searchController.active = false
                    self.selectedBranch = newBranch
                    let bvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                    bvc.franchise = newBranch.franchise
                    bvc.delegate = self
                    self.navigationController?.pushViewController(bvc, animated: true)
                    
                }
            })
            
            
        }
        
    }
    
    
    func getFavorites(){
        api.jsonRequest(.PRODUCT_FAVORITE_INFO, parameters: nil, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: nil)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    if let branchesFavorites = json?["data"]["branches"].array {
                        MUser.sharedInstance.branchesFavorites = []
                        
                        for branch in branchesFavorites {
                            let newBranch = MRestaurant(data: branch["branch"])
                            let newFranchise = MFranchise(json: branch["branch"]["franchise"])
                            newBranch.franchise = newFranchise
                            MUser.sharedInstance.branchesFavorites.append(newBranch)
                        }
                    }
                    if let productFavorites = json?["data"]["products"].array {
                        MUser.sharedInstance.productFavorites = []
                        for product in productFavorites {
                            let newProduct = MProduct(json: product["product"])
                            
                            if let productAttachmentsArray = product["product"]["attachments"].array {
                                for att in productAttachmentsArray {
                                    let newAtt = MAttachment(json: att)
                                    newProduct.attachments = [newAtt]
                                }
                            }
                            let newBranch = MRestaurant(data: product["branch"])
                            let newFranchise = MFranchise(json: product["branch"]["franchise"])
                            newBranch.franchise = newFranchise
                            newProduct.branch_products = [newBranch]
                            MUser.sharedInstance.productFavorites.append(newProduct)
                        }
                    }
                    
                }else{
                    //something went wrong
                }
            }else{
                print(error)
            }
        }
    }
    
}





