//
//  MShopCar.swift
//  Monchis
//
//  Created by jrivarola on 10/14/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation

class MShopCar {
    
    var orders: [MOrder]?
    
    static let sharedInstance = MShopCar()
    
    init (orders: [MOrder]) {
        self.orders = orders
    }
    
    init (){
        
    }
    func clear(){
        self.orders = []
    }
}