//
//  NoHUIButton.swift
//  Monchis
//
//  Created by Javier Rivarola on 6/6/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit

class NoHUIButton: UIButton {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    override var highlighted: Bool {
        didSet { if highlighted { highlighted = false } }
    }

}
