//
//  RegisterTableViewController.swift
//  Monchis
//
//  Created by jrivarola on 18/2/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import KeychainSwift

class RegisterTableViewController: UITableViewController, SSRadioButtonControllerDelegate {
    let api = MAPI.SharedInstance
    let user = MUser.sharedInstance
    
    
    @IBOutlet weak var femaleBtn: SSRadioButton!
    @IBOutlet weak var maleBtn: SSRadioButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 40
        tableView.rowHeight = UITableViewAutomaticDimension
        userAddressessCounter = self.user.addresses.count
        datePicker = UIDatePicker()
        datePicker.date = NSDate()
        datePicker.datePickerMode = .Date
        datePicker.addTarget(self, action: #selector(RegisterTableViewController.datePickerValueChanged(_:)), forControlEvents: .ValueChanged)

        fechaNacimientoTxt.inputView = datePicker
        radioButtonController.addButton(maleBtn)
        radioButtonController.addButton(femaleBtn)
        radioButtonController.delegate = self
        
    }
     var radioButtonController: SSRadioButtonsController = SSRadioButtonsController()
    var datePicker: UIDatePicker!
    
    func didSelectButton(aButton: UIButton?) {
        if let button = aButton {
            if button == maleBtn {
                user.gender = "M"
            }else{
                user.gender = "F"
            }
        }else{ //deselected
            user.gender = ""
        }
    }
    
    func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        fechaNacimientoTxt.text = dateFormatter.stringFromDate(sender.date)
        
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
   
    
    @IBOutlet weak var fechaNacimientoTxt: UITextField!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passTxt: UITextField!
    @IBOutlet weak var confirmPassTxt: UITextField!
    @IBOutlet weak var documentTxt: UITextField!
    @IBOutlet weak var rucTxt: UITextField!
    @IBOutlet weak var razonTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    // MARK: - Table view data source
    
    
    
    
    
    func registerUser(){
        if passTxt.text == confirmPassTxt.text  {
        if  passTxt.text != "" && nameTxt.text != "" && lastName.text != "" && emailTxt.text != "" { //add animation
            self.view.endEditing(true)
            
            
           
            
            let spinner = MBProgressHUD.showHUDAddedTo(UIApplication.sharedApplication().keyWindow!, animated: true)
            //            let dateFormatter = NSDateFormatter()
            //            dateFormatter.dateFormat = "yyyy-MM-dd"
            //
            //
            //            let birthDate = dateFormatter.stringFromDate(datePicker.date)
            spinner.mode = MBProgressHUDMode.Indeterminate
            spinner.label.text = NSLocalizedString("Registrando",comment: "Registrando nuevo usuario")
            spinner.label.font = UIFont(name: "OpenSans-Semibold", size: 14.0)!
            
            let parameters =    [  "first_name":nameTxt.text ?? "",
                "last_name":lastName.text ?? "",
                "email": emailTxt.text ?? "",
                "password":passTxt.text ?? "",
                "document_number": documentTxt.text ?? "",
                "phone": phoneTxt.text ?? "",
                "device_token":MUser.sharedInstance.pushToken,
                "gender":user.gender ?? "",
                "type":"1",
                "model":MUser.sharedInstance.modelName,
                "system":"iOS",
                "version":MUser.sharedInstance.systemVersion
            ]
            
            api.jsonRequest(.REGISTER_MAIL, parameters: parameters) { json, error in
                if debugModeEnabled {
                    print(#function,"parameters: \(parameters)",json,error)
                }
                
                if error == nil {
                    
                    
                    let success = json?["success"].bool ?? false
                    if let data = json?["data"] {
                        if success {
                            self.user.unparseUserInfoFromApi(data)
                            let monchis_token = data["monchis_token"].string ?? ""
                            self.api.token = monchis_token
                            self.api.mgr.session.configuration.HTTPAdditionalHeaders = ["token": "\(monchis_token)"]
                            print("Monchis token : \(monchis_token)")
                            //set automatic login for user
                            NSUserDefaults.standardUserDefaults().setBool(true, forKey: kAutomaticLogin)
                          
                            
                            //login user after registration succesfull
                            self.user.authenticationMethod = .Monchis
                            self.user.password = self.passTxt.text ?? ""
                            self.user.email = self.emailTxt.text ?? ""
                            self.user.logged = true
                            let keychain = KeychainSwift()
                            keychain.set(self.user.email, forKey: kMonchisUserEmail)
                            keychain.set(self.user.password, forKey: kMonchisUserPassword)
                            KeychainSwift().set("monchis", forKey: kMonchisUserAuthenticationMethod)
                            self.saveUserCurrentAddresses()
                        }else{ // show error
                            MBProgressHUD.hideHUDForView(UIApplication.sharedApplication().keyWindow!,animated: true)
                            
                            let message = data.string ?? ""
                            let titleLocalizable = NSLocalizedString("Error",comment: "error")
                            let alert = UIAlertController(title: titleLocalizable, message: message, preferredStyle: .Alert)
                            let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                            alert.addAction(ok)
                            self.presentViewController(alert, animated: true, completion: nil)
                            
                        }
                    }else{
                        print("bad response")
                        
                    }
                }else{ //network error
                    MBProgressHUD.hideHUDForView(UIApplication.sharedApplication().keyWindow!, animated: true)
                    
                    print("network error")
                    
                    let titleLocalizable = NSLocalizedString("Error",comment: "error")
                    let alert = UIAlertController(title: titleLocalizable, message: error, preferredStyle: .Alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                    alert.addAction(ok)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
            
        }else{
            let titleLocalizable = NSLocalizedString("Lo sentimos!",comment: "Lo sentimos!")
            let messageLocalizable = NSLocalizedString("Debes completar todos los datos.",comment: "Debes completar todos los datos.")
            let alert = UIAlertController(title: titleLocalizable, message: messageLocalizable, preferredStyle: .Alert)
            let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
            alert.addAction(ok)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        }else{
            let titleLocalizable = NSLocalizedString("Lo sentimos!",comment: "Lo sentimos!")
            let messageLocalizable = NSLocalizedString("Las contraseñas no coinciden.",comment: "Las contraseñas no coinciden.")
            let alert = UIAlertController(title: titleLocalizable, message: messageLocalizable, preferredStyle: .Alert)
            let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
            alert.addAction(ok)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func didPressRegisterButton(sender: UIButton) {
        registerUser()
    }
    
    
    func handleSuccessfullAddressCreation(){
        userAddressessCounter -= 1
        if userAddressessCounter == 0 {
            //we are done
            print("all addressess has been saved")
             MBProgressHUD.hideHUDForView(UIApplication.sharedApplication().keyWindow!,animated: true)
            NSNotificationCenter.defaultCenter().postNotificationName("newUserRegisteredNotification", object: nil)
            self.performSegueWithIdentifier("unwindFromRegistrarse", sender: self)
            
        }
    }
    
    var userAddressessCounter: Int = 0
    func saveUserCurrentAddresses(){
        for address in self.user.addresses {
            let addrPrmt =  address.parseAddressForAPI()
            self.api.jsonRequest(MAPI.Command.USER_ADDRESSES_CREATE, parameters: addrPrmt) { (json, error) -> Void in
                if debugModeEnabled {
                    print(#function,"called with parameters: \(addrPrmt)",json, error)
                    
                }
                dismissCustomWaitScreen(self.view, blurView: nil)
                if error == nil {
                    if let json = json {
                        let success = json["success"].bool ?? false
                        if success {
                            self.handleSuccessfullAddressCreation()
                        }else{
                            
                        }
                    }
                }else{ // net error
                    
                }
            }
        }
    }
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)
    
    // Configure the cell...
    
    return cell
    }
    */
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
