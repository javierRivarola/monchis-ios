//
//  HistorialContentCollectionViewCell.swift
//  Monchis
//
//  Created by jrivarola on 1/2/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit

protocol HistorialContentCellDelegate {
    func didPressOrderAgain(product: MProduct)
}

class HistorialContentCollectionViewCell: UICollectionViewCell {
    
    
    var customDelegate: HistorialContentCellDelegate?
    var product: MProduct!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var quantityView: UIView!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBAction func orderAgainPressed(sender: UIButton) {
        customDelegate?.didPressOrderAgain(product)
       
    }
    
 
}
