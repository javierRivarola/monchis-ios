//
//  MenuCategoriaTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 9/9/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class MenuCategoriaTableViewCell: UITableViewCell {

    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDescription: UILabel!
    @IBOutlet weak var itemFromLabel: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
