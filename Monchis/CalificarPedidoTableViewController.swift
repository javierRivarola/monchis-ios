//
//  CalificarPedidoTableViewController.swift
//  Monchis
//
//  Created by Javier Rivarola on 8/29/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD
import SCLAlertView
import Crashlytics
import Mixpanel

class CalificarPedidoTableViewController: UITableViewController {
    var order: MOrder!
    let api = MAPI.SharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
        let bg = UIImageView(image: UIImage(named: "background-completoChico"))
        bg.contentMode = .ScaleAspectFill
        bg.clipsToBounds = true
        self.tableView.backgroundView = bg
        bg.layer.zPosition = -1
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 50
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        spinner = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
        spinner.startAnimating()
        spinner.hidesWhenStopped = true
        self.view.addSubview(spinner)
        spinner.center = view.center
        getReasons()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    var doneLoadingReasons: Bool = false
    func getReasons(){
        api.networkPostRequest(.GET_REASONS, parameters: nil) { (json, error) in
            if error == nil {
                if let success = json?["success"].bool {
                    if success {
                        if let reasonsArray = json?["data"].array {
                            for r in reasonsArray {
                                self.reasons.append(Reason(json: r))
                            }
                            self.updateUI()
                        }
                    }
                }
            }
        }
    }
    var reasons: [Reason] = []
    
    func updateUI(){
        self.doneLoadingReasons = true
        self.tableView.reloadData()
        self.spinner.stopAnimating()
    }
    var spinner : UIActivityIndicatorView!

    struct Reason {
        var id: Int = 0
        var reason_text: String = ""
        
        init(json: SwiftyJSON.JSON) {
            self.id = json["id"].int ?? 0
            self.reason_text = json["reason_text"].string ?? ""
        }
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return doneLoadingReasons ? 3 : 0
    }

    var numberOfRows: Int = 0
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 1 ? reasons.count + 1 : 1
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 1 && indexPath.row > 0 {
            let reason = reasons[indexPath.row - 1]
            if selectedIds.contains(reason.id) {
                selectedIds.removeAtIndex(selectedIds.indexOf(reason.id)!)
            }else{
                selectedIds.append(reason.id)
            }
            tableView.beginUpdates()
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            tableView.endUpdates()
        }
        self.view.endEditing(true)
    }

    var selectedIds:[Int] = []
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("starCell", forIndexPath: indexPath) as! CalificarStarCellTableViewCell
            cell.order = self.order
            cell.parentTV = self.tableView
            return cell
        }
        if indexPath.section == 1{
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCellWithIdentifier("tituloCell", forIndexPath: indexPath)
                return cell
            }
            let cell = tableView.dequeueReusableCellWithIdentifier("optionCell", forIndexPath: indexPath) as! RateOptionTableViewCell
            let reason = reasons[indexPath.row - 1]
            cell.rateLbl.text = reason.reason_text
            cell.rateLbl.layer.cornerRadius = 10
            cell.rateLbl.clipsToBounds = true
            if selectedIds.contains(reason.id) {
                cell.rateLbl.backgroundColor = UIColor(hexString: "#F96332")
            }else{
                cell.rateLbl.backgroundColor = UIColor(hexString:"#F2EFC5" )
            }
            return cell
        }
        if indexPath.section == 2{
            let cell = tableView.dequeueReusableCellWithIdentifier("calificaCell", forIndexPath: indexPath)
            if self.textView == nil {
                self.textView = cell.viewWithTag(1) as! UITextView
            }else{
                (cell.viewWithTag(1) as! UITextView).text = textView.text
            }
            self.textView?.layer.cornerRadius = 10
            self.textView?.clipsToBounds = true
            let calificarBtn = cell.viewWithTag(2) as! UIButton
            calificarBtn.addTarget(self, action: #selector(CalificarPedidoTableViewController.calificarBtnPressed), forControlEvents: .TouchUpInside)
            return cell
        }

        let cell = tableView.dequeueReusableCellWithIdentifier("starCell", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    
    var textView: UITextView!
    
    
    func calificarBtnPressed() {
        saveFeedback()
    }
    
    func saveFeedback(){
        self.view.endEditing(true)
        MBProgressHUD.showHUDAddedTo((self.navigationController!.view)!, animated: true)
        let parameters:[String:AnyObject] = ["order_id":self.order.id ?? self.order.order_id ??  0,"feedback_reasons":selectedIds,"feedback_stars":self.order.starRatingValue,"feedback_comments": textView.text ?? ""]
        api.networkPostRequest(MAPI.Command.SAVE_REASONS, parameters: parameters) { (json, error) in
            MBProgressHUD.hideHUDForView((self.navigationController!.view)!, animated: true)
            print("save feedback",json,error)
            if error == nil {
                if let success = json?["success"].bool {
                    if success {
                        
                        var stringContent: [String] = []
                        for reason in self.reasons {
                            if self.selectedIds.contains(reason.id) {
                                stringContent.append(reason.reason_text)
                            }
                        }
                        
                        Answers.logRating(self.order.starRatingValue,
                                          contentName: "Orden de usuario",
                                          contentType: "Orden de usuario",
                                          contentId: "\(self.order.id ?? self.order.order_id ??  0)",
                                          customAttributes: ["feedback_comments":self.textView.text ?? "","reasons":stringContent])
                        Mixpanel.mainInstance().track(event: "Rating de Orden", properties: ["Estrellas": self.order.starRatingValue,"Comentarios":self.textView.text ?? "", "Razones":stringContent, "Orden ID": self.order.order_id ?? 0])
                        let appearance = SCLAlertView.SCLAppearance(
                            showCircularIcon: true,
                            showCloseButton: false
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.addButton("Listo!", action: {
                            self.navigationController?.popViewControllerAnimated(true)
                        })
                        alertView.showSuccess("Gracias por tu aporte!.", subTitle: "Tendremos en cuenta todas tus sugerencias para ir mejorando el servicio.")
                        

                    }
                }
            }
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
