//
//  MixpanelEvents.swift
//  Monchis
//
//  Created by Javier Rivarola on 2/23/17.
//  Copyright © 2017 cibersons. All rights reserved.
//

import Foundation


let kUserPurchasedProductEvent = "Usuario Finalizo Carrito"
let kUserDeletedProductCartEvent = "Usuario Elimino un Producto del Carrito"
let kUserChangedProductQuantityCartEvent = "Usuario Cambio Cantidad de Producto"
let kUserSelectedPaymentMethodForOrder = "Usuario Eligio Metodo de Pago para Orden"
let kUserPressedSeeOnMapEvent = "Usuario Vio Mapa de Local"
let kUserDeletedCartOrder = "Usuario Elimino Orden"
let kUserCalledBranchEvent = "Usuario LLamo a Local"
let kUserPressedOrderDetailsEvent = "Usuario Presiono Ver Detalles de Orden"
let kUserOpenedUniversalLinkEvent = "Usuario Abrio Universal Link"
let kUserChangedToTabEvent = "Usuario Cambio a Tab"
let kUserSelectedProductEvent = "Usuario Eligio un Producto"
let kUserSelectedFranchiseCategoryEvent = "Usuario Eligio una Categoria de Franquicias"