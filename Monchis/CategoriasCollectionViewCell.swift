//
//  CategoriasCollectionViewCell.swift
//  Monchis
//
//  Created by jrivarola on 6/22/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class CategoriasCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titulo: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
//        self.imageView.layer.cornerRadius = 5
//        self.imageView.clipsToBounds = true
//        self.layer.cornerRadius = 10
//        self.clipsToBounds = true
        //self.imageView.layer.cornerRadius = self.imageView.bounds.width/2

    }
    
    
    override var layoutMargins: UIEdgeInsets {
        get { return UIEdgeInsetsZero }
        set(newVal) {}
    }
}
