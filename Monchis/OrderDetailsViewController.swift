//
//  OrderDetailsViewController.swift
//  Monchis
//
//  Created by jrivarola on 15/1/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import Haneke

class OrderDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var order: MOrder!
    let user = MUser.sharedInstance
    let reuseIdentifier = "orderDetailsReuse"
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var detallesLbl: UILabel!
    var cameFromSideMenu: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        tableView.tableFooterView = UIView(frame: CGRectZero)
//        deliveryPrice.text = order.delivery_price?.asLocaleCurrency
//        totalPriceLbl.text = order.total?.asLocaleCurrency
//        if order.payMethod == .CASH {
//            payMethodLbl.text = NSLocalizedString("Efectivo", comment: "User paid with cash")
//        }else{
//            payMethodLbl.text = NSLocalizedString("Credito", comment: "User paid with credit or debit card")
//        }
//        if order.delivery_type ==  1 {
//        if let index = user.addresses.indexOf({$0.id == order.address_id}){
//            sentAddressLbl.text = user.addresses[index].name
//        }else{
//            sentAddressLbl.text = NSLocalizedString("La dirección de envio ya no existe.", comment: "Mensaje diciendo que la direccion de envio del pedido no existe")
//        }
//        }else{
//            sentToLbl.hidden = true
//        }
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = user.locale
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = dateFormatter.dateFromString(order.confirmed_at ?? "") {
            dateFormatter.dateFormat = "d MMM yyyy HH:mm"
            fechaLbl.text = dateFormatter.stringFromDate(date).uppercaseString

        }
        
        // Do any additional setup after loading the view.
      
    }
    func goBack(){
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var rightMenuBtn: UIBarButtonItem!
    @IBOutlet weak var fechaLbl: UILabel!

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     
    }
    
    
    override func viewWillAppear(animated: Bool) {
      
    }

     
    // MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let products = order!.products
        return order.delivery_type == 1 ? products.count + 3 : products.count + 2

    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row < order.products.count {
            let product = order.products[indexPath.row]
            
            let cell = tableView.dequeueReusableCellWithIdentifier("productoCell", forIndexPath: indexPath) as! DetallesOrdenProductoTableViewCell
            cell.productName.text = product.name
            cell.productPrice.text = product.price?.asLocaleCurrency
            cell.productAddons.text = product.addonsDescription
            cell.productQuantityLbl.text = "x\(product.quantity ?? 1)"
            if let comment = order.feedback_comments {
                if comment  != "" {
                    let obs = NSAttributedString(string: "Observaciónes: ", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(13),NSForegroundColorAttributeName: UIColor.blackColor()])
                    let txt = NSMutableAttributedString(attributedString: obs)
                    txt.appendAttributedString(NSAttributedString(string: comment, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(13), NSForegroundColorAttributeName: UIColor.darkGrayColor()]))
                    cell.productComments.attributedText = txt
                }
            }
            if let imgUrl = product.attachments.first?.imageComputedURL {
                cell.productImg.hnk_setImageFromURL(imgUrl, placeholder: NO_ITEM_PLACEHOLDER)
            }else{
                cell.productImg?.image = NO_ITEM_PLACEHOLDER
            }
            return cell

        }else{
            if indexPath.row == order.products.count { //Delivery o pickup
                if order.delivery_type ==  1{
                    let cell = tableView.dequeueReusableCellWithIdentifier("deliveryCell", forIndexPath: indexPath) as! DeliveryDetalleOrdenTableViewCell
                    cell.deliveryPriceLbl.text = order.branch?.delivery_price?.asLocaleCurrency
                    cell.totalLbl.text = ((order.total ?? 0) + (order.branch?.delivery_price ?? 0)).asLocaleCurrency
                    return cell
                }
                
                let cell = tableView.dequeueReusableCellWithIdentifier("pickupCell", forIndexPath: indexPath) as! PickupDetalleOrdenTableViewCell
                
                cell.totalLbl.text = order.total?.asLocaleCurrency
                
                return cell
              
            }
            if indexPath.row == order.products.count + 1 { //Metodo de pago
                let cell = tableView.dequeueReusableCellWithIdentifier("metodoPagoCell", forIndexPath: indexPath) as! MetodoPagoDetalleOrdenTableViewCell
                if order.payMethod == .CASH {
                    cell.metodoPagoLbl.text = "EFECTIVO"
                }else{
                    cell.metodoPagoLbl.text = "POS"
                }
                return cell
            }
            if indexPath.row == order.products.count + 2 { //Enviado a si corresponde
                let cell = tableView.dequeueReusableCellWithIdentifier("enviadoCell", forIndexPath: indexPath) as! EnviadoDetalleOrdenTableViewCell
                cell.addressNameLbl.text = self.user.addresses.filter {$0.id == order.address_id}.first?.name ?? "Dirección ya no existe."
                cell.addressDescrLbl.text = self.user.addresses.filter {$0.id == order.address_id}.first?.formattedAddress ?? ""
                return cell
            }

        }
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       return 55
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
        let view = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 55))
        view.backgroundColor = UIColor.whiteColor()
        //let franchiseName = cart.orders[section].first!.franchiseName
        let imgView = UIImageView(frame: CGRectMake(8, 5, 45, 45))
        imgView.layer.cornerRadius = imgView.bounds.width/2
        imgView.contentMode = .ScaleAspectFit
        imgView.clipsToBounds = true
        imgView.image = UIImage(named: "placeholder-640x480")
        view.addSubview(imgView)
        order?.branch?.franchise?.header_attachment?.width = 45
        order?.branch?.franchise?.header_attachment?.height = 45
        if let imageURL = order?.branch?.franchise?.header_attachment?.imageComputedURL {
            imgView.hnk_setImageFromURL(imageURL, placeholder: NO_BRANCH_PLACEHOLDER)
        }
        
        let lbl = UILabel(frame: CGRectMake(61, 5, tableView.frame.size.width - 60, 50))
        if order.delivery_type == 1 {
            if let franchiseName = order?.branch?.franchise?.name {
                lbl.text = franchiseName.uppercaseString
            }
        }else{
            if let branchName = order?.branch?.name {
                lbl.text = branchName.uppercaseString
            }
        }
        lbl.textColor = UIColor.grayColor()
        view.addSubview(lbl)
        lbl.numberOfLines = 2
        lbl.lineBreakMode = .ByWordWrapping
        lbl.font = UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15)
        view.layer.shadowColor = UIColor.blackColor().CGColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSizeZero
        
        return view
        }else{
            return nil
        }
  
    }
    
}
