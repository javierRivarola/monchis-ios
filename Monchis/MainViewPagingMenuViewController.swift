//
//  MainViewPagingMenuViewController.swift
//  Monchis
//
//  Created by jrivarola on 8/7/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import PagingMenuController
import SCLAlertView
import Mixpanel
import SwiftyJSON

var currentRestaurantVC: RestaurantesCollectionViewController!
var controllersForLoading: [UIViewController] = []
var menus:[MenuItemViewCustomizable] = []
var MainPage: MainViewPagingMenuViewController!
class MainViewPagingMenuViewController: UIViewController, UIGestureRecognizerDelegate, PagingMenuControllerDelegate,SearchResultSelectedDelegate, BranchSelectionDelegate{
    
    // MARK: Properties
    var selectedProduct: MProduct!
    var selectedBranch: MRestaurant!
    var wantsDelivery: Bool?
    var selectedFranchise: MFranchise!
    var headerMonchisImage: UIImageView!
    let user = MUser.sharedInstance
    var searchController: UISearchController!
    var searchTableViewController:SearchTableViewController!
    var searchBar: UISearchBar!
    var selectedPageIndex: Int!
    var selectedRestaurant: MRestaurant!
    var restaurants: [MRestaurant] = []
    var filteredData: [[AnyObject]] = []
    var categorySelected: MCategory!
    var selectedScope: Int = 0
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    let api = MAPI.SharedInstance
    @IBOutlet var rightSwipeGesture: UISwipeGestureRecognizer!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet var leftSwipeGesture: UISwipeGestureRecognizer!
    struct Segues {
        static let selectedRestaurantSegue = "selectedRestaurantSegue"
        static let deliveryTypeSelectionSegue = "DeliveryTypeSelectionSegue"
        static let pickupSegue = "pickupSegue"
    }
    @IBOutlet weak var searchBtn: UIButton!
    
    struct Constants {
        static let RestaurantesClosedCellReuseID = "RestaurantesClosedCellReuseID"
        static let reuseIdentifier = "RestaurantesCellReuseID"
        static let ReuseSearchCellID = "ReuseSearchCellID"
    }
    
    // MARK: LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        selectedPageIndex = 0
        MainPage = self
       //        generateViewControllers()
//        configurePagingMenu()
        registerForNotifications()
        getFranchisesCategories()
        
    }
    
    var categories:[MCategory] = []
    var loadedCategories: Bool = false
    func getFranchisesCategories(){
        
        api.returnCachedData(.LIST_CATEGORIES, parameters: nil) { (json) in
            if debugModeEnabled {
                print("returnCachedData for CATEGORIES","called with parameters: nil",json)
            }
            if let jsonArray = json?["data"].array {
                self.updateUIForCategories(jsonArray, isCache: true)
            }
            
        }
        
        
        
        
        api.postQueueRequest(.LIST_CATEGORIES, parameters: nil) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,json,error)
            }
            if error == nil {
                if let jsonCategories = json?["data"].array {
                    self.updateUIForCategories(jsonCategories, isCache: false)
                    
                }
                
            }else{ // network error
                
                
            }
        }
    }
    
    var cachedCategories: [SwiftyJSON.JSON]!
    var pagingMenuReady: Bool = false
    func updateUIForCategories(json: [SwiftyJSON.JSON], isCache: Bool){
        loadedCategories = true
        var tmpCat: [MCategory] = []
        for category in json {
            tmpCat.append(MCategory(json: category))
        }
        if isCache {
            cachedCategories = json
        }else{
            if json == cachedCategories {
                return
            }
        }
        categories = tmpCat
        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            print("This is run on the background queue")
            if !self.vcGenerated {
                self.generateViewControllers()
            }
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print("This is run on the main queue, after the previous code in outer block")
    
                                    if !self.pagingMenuReady {
                                        self.configurePagingMenu()
                                    }
                
            })
        })

        
        
        
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        print("touch canceled")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if let navContr = self.navigationController {
            if navContr.respondsToSelector(Selector("interactivePopGestureRecognizer")) {
                navContr.interactivePopGestureRecognizer?.enabled = false
            }
            
        }
    }
    
    func registerForNotifications(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainViewPagingMenuViewController.userDefaultAddressChanged), name: kUserDidChangeDefaultAddressNotification, object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
  
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    func userDefaultAddressChanged(){
        //handle new default address
        
    }
    
    
    
    func setupUI(){
        searchBtn?.imageView?.contentMode = .ScaleAspectFit

        self.title = "Categorias"
        shadowView.layer.shadowColor = UIColor.blackColor().CGColor
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shadowOffset = CGSizeZero
        shadowView.layer.shadowRadius = 10
        //SearchBar Controller
        searchTableViewController = storyboard?.instantiateViewControllerWithIdentifier("SearchTableViewController") as! SearchTableViewController
        searchController = UISearchController(searchResultsController: searchTableViewController)
        
        searchController.hidesNavigationBarDuringPresentation = false
        //searchController.searchBar.searchBarStyle = UISearchBarStyle.Minimal
        
        searchController.searchBar.sizeToFit()
        searchBar = searchController.searchBar
        searchBar.tintColor = UIColor(hexString: "#FF8000")
        
        searchBar.translucent = false
        
        searchBar.delegate = self
        for subView in searchBar.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.attributedPlaceholder =  NSAttributedString(string:"Buscar.. (min 3 caracteres)",
                                                                          attributes:[NSFontAttributeName: UIFont(name: "OpenSans-Semibold", size: 13)!])
                }
            }
        }
        searchBar.scopeButtonTitles = ["Productos","Restaurantes"]
        
        searchTableViewController.definesPresentationContext = true
        searchTableViewController.searchDelegate = self
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = true // important
        //self.navigationItem.titleView = searchBar
        searchController.delegate = self
       
        
        // custom gestures
        
        self.view.addGestureRecognizer(rightSwipeGesture)
        rightSwipeGesture.addTarget(self, action: #selector(MainViewPagingMenuViewController.rightSwipeGestureAction(_:)))
        
        self.view.addGestureRecognizer(leftSwipeGesture)
        leftSwipeGesture.addTarget(self, action: #selector(MainViewPagingMenuViewController.leftSwipeGestureAction(_:)))
        
    }
    
    func rightSwipeGestureAction(sender: UISwipeGestureRecognizer){
        if pagingMenuController.currentPage - 1 >= 0 {
            pagingMenuController.moveToMenuPage(pagingMenuController.currentPage - 1,animated:true)
            Mixpanel.mainInstance().track(event: "Usuario eligio una categoria de franquicias",
                                          properties: ["Categoria" : categories[pagingMenuController.currentPage].name ?? "","Pantalla":"Franquicias por Categoria"])
        }else{
             pagingMenuController.moveToMenuPage(controllersForLoading.count - 1,animated:true)
            Mixpanel.mainInstance().track(event: "Usuario eligio una categoria de franquicias",
                                          properties: ["Categoria" : categories[controllersForLoading.count - 1].name ?? "","Pantalla":"Franquicias por Categoria"])
        }
    }
    
    func leftSwipeGestureAction(sender: UISwipeGestureRecognizer){
        if pagingMenuController.currentPage + 1 < controllersForLoading.count {
            pagingMenuController.moveToMenuPage(pagingMenuController.currentPage + 1,animated:true)
            Mixpanel.mainInstance().track(event: "Usuario eligio una categoria de franquicias",
                                          properties: ["Categoria" : categories[pagingMenuController.currentPage].name ?? "","Pantalla":"Franquicias por Categoria"])
        }else{
            pagingMenuController.moveToMenuPage(0,animated:true)
            Mixpanel.mainInstance().track(event: "Usuario eligio una categoria de franquicias",
                                          properties: ["Categoria" : categories.first?.name ?? "","Pantalla":"Franquicias por Categoria"])
        }
        

    }
    
    var pagingMenuController: PagingMenuController!
    
    
    
    
    
    
    
    func configurePagingMenu(){
        
        
    
        struct MenuOptions: MenuViewCustomizable {
            var selectedBackgroundColor: UIColor {
                return .clearColor()
            }
            var backgroundColor: UIColor {
                return .clearColor()
            }
            
            
            var displayMode: MenuDisplayMode  {
                return .Infinite(widthMode: .Flexible, scrollingMode: .ScrollEnabledAndBouces)
            }
            var itemsOptions: [MenuItemViewCustomizable] {
                return menus
            }
            var focusMode: MenuFocusMode {
                return .Underline(height: 3, color: UIColor.redColor(), horizontalPadding: 5, verticalPadding: 10)
            }
            
            var height: CGFloat {
                return 55
            }
        }
        struct PagingMenuOptions: PagingMenuControllerCustomizable {
            var componentType: ComponentType {
                return .All(menuOptions: MenuOptions(), pagingControllers: controllersForLoading)
            }
            var backgroundColor: UIColor {
                return .clearColor()
            }
            
            var defaultPage: Int {
                return 0
            }
            var scrollEnabled: Bool {
                return false
            }
        }
        
        
        
        let options = PagingMenuOptions()
    

        currentRestaurantVC = controllersForLoading[selectedPageIndex] as! RestaurantesCollectionViewController
        
//        options.menuDisplayMode = .Standard(widthMode: .Flexible, centerItem: false, scrollingMode: .ScrollEnabledAndBouces)
        
       pagingMenuController = PagingMenuController(options: options)
        pagingMenuController.delegate = self
        pagingMenuController.view.translatesAutoresizingMaskIntoConstraints = false
        
        
        
        self.addChildViewController(pagingMenuController)
        self.view.addSubview(pagingMenuController.view)
        pagingMenuController.didMoveToParentViewController(self)
        
        let topConstraint = NSLayoutConstraint(item: pagingMenuController.view, attribute: .Top, relatedBy: .Equal, toItem: self.view, attribute: .Top, multiplier: 1, constant: 63)
        
        let botConstraint = NSLayoutConstraint(item: pagingMenuController.view, attribute: .Bottom, relatedBy: .Equal, toItem: self.view, attribute: .Bottom, multiplier: 1, constant: 0)
        
        let leftConstraint = NSLayoutConstraint(item: pagingMenuController.view, attribute: .Left, relatedBy: .Equal, toItem: self.view, attribute: .Left, multiplier: 1, constant: 0)
        
        let rightConstraint = NSLayoutConstraint(item: pagingMenuController.view, attribute: .Right, relatedBy: .Equal, toItem: self.view, attribute: .Right, multiplier: 1, constant: 0)
        NSLayoutConstraint.activateConstraints([topConstraint,botConstraint,leftConstraint,rightConstraint])
        pagingMenuController.moveToMenuPage(selectedPageIndex, animated: false)
        self.pagingMenuReady = true

    }
    struct MenuItemGeneric: MenuItemViewCustomizable {
        private let title: String
        
        var displayMode: MenuItemDisplayMode {
            let title = MenuItemText(text: self.title,color: .darkGrayColor(), selectedColor: UIColor.redColor(), font: UIFont.systemFontOfSize(14), selectedFont: UIFont.boldSystemFontOfSize(16))
            return .Text(title: title)
        }
        
        init(title: String) {
            self.title = title
        }
        
        
    }
    
    func generateViewControllers(){
        
        controllersForLoading = []
        menus = []
        for categoria in categories
        {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantesCVID") as! RestaurantesCollectionViewController
//            vc.collectionView?.delegate = self
            vc.spinner = spinner
            
            let op = MenuItemGeneric(title: categoria.name!)
            menus.append(op)
            vc.category = categoria
            vc.title = categoria.name
            vc.category = categoria
            controllersForLoading.append(vc)
        }
        vcGenerated = true
        
    }
    
    var vcGenerated: Bool = false
    func createWaitScreen(){

            let spinner = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            spinner.mode = MBProgressHUDMode.Indeterminate
            spinner.label.text = NSLocalizedString("Buscando Restaurantes",comment: "Buscando restaurantes para categoria nueva")
            spinner.label.font = UIFont(name: "OpenSans-Semibold", size: 14.0)!
        
        
    }
    
    func dismissWaitScreen(){
        dispatch_async(dispatch_get_main_queue()) {
            MBProgressHUD.hideHUDForView(self.view, animated: true)
        }
    }
    
    @IBAction func unwindFromPickupDeliverySelection(segue: UIStoryboardSegue) {
        if let wantsDelivery = wantsDelivery {
            if wantsDelivery {
                let rVC = self.storyboard?.instantiateViewControllerWithIdentifier("DeliveryBranchSelectionViewController") as! DeliveryBranchSelectionViewController
                rVC.selectedPageIndex = self.selectedPageIndex
                rVC.categorySelected = categories[selectedPageIndex]
                rVC.franchise = selectedFranchise
                self.navigationController?.viewControllers.append(rVC)
                self.navigationController?.showViewController(rVC, sender: self)
                
                self.wantsDelivery = nil
                
            }else{
                let rVC = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                rVC.franchise = selectedFranchise
                
                self.navigationController?.viewControllers.append(rVC)
                self.navigationController?.showViewController(rVC, sender: self)
                self.wantsDelivery = nil
            }
        }

       
    }
    
    
    
    
    // MARK: PageMenu Delegate
    
    
    
   
    func didMoveToPageMenuController(menuController: UIViewController, previousMenuController: UIViewController) {
            currentRestaurantVC = menuController as! RestaurantesCollectionViewController
        
        //  loadBranchesForCategory(categories[page].id!)
        
        
    }
    
    func willMoveToPageMenuController(menuController: UIViewController, previousMenuController: UIViewController) {
        
        currentRestaurantVC = menuController as! RestaurantesCollectionViewController
        
        if filterType == "Delivery" {
            currentRestaurantVC.filterSegmentedControl?.selectedSegmentIndex = 0
            currentRestaurantVC?.collectionView?.reloadEmptyDataSet()
        }else{
            currentRestaurantVC.filterSegmentedControl?.selectedSegmentIndex = 1
            currentRestaurantVC?.collectionView?.reloadEmptyDataSet()

        }
    
        currentRestaurantVC.filterSegmentedControl?.sendActionsForControlEvents(.ValueChanged)
     
//        let numberOfRestos = currentRestaurantVC.restaurantes.count
//        if numberOfRestos > 0 && numberOfRestos < 4 {
//            currentRestaurantVC.collectionView?.scrollToItemAtIndexPath(NSIndexPath(forItem: 0,inSection: 0), atScrollPosition: .Top, animated: false)
//        }
      
//        currentRestaurantVC.filterSegmentedControl?.selectedSegmentIndex = (previousMenuController as! RestaurantesCollectionViewController).filterSegmentedControl?.selectedSegmentIndex ?? 0
//        (menuController as! RestaurantesCollectionViewController).filterSegmentedControl?.sendActionsForControlEvents(.ValueChanged)
        
    }
    
    // MARK: UIGestureRecognizerDelegate
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    

    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Segues.selectedRestaurantSegue {
            let dvc = segue.destinationViewController as! RestaurantViewController
            dvc.restaurant = selectedRestaurant
        }
        
        if segue.identifier == Segues.deliveryTypeSelectionSegue {
            let dvc = segue.destinationViewController as! PickupDeliveryViewController
            dvc.franchise = selectedFranchise
        }
        
    }
    
    
}

//extension MainViewPagingMenuViewController: UICollectionViewDelegate {
//    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
//        //  selectedRestaurant = categories[selectedPageIndex].restaurants[indexPath.row]        
//        selectedRestaurant = currentRestaurantVC.restaurantes[indexPath.row]
//        if selectedRestaurant.isOpen() {
//            SCLAlertView().showNotice("Atención", subTitle: "Debes elegir si deseas la opción Delivery o Pasar a buscar")
//        }else{
//            self.performSegueWithIdentifier(Segues.selectedRestaurantSegue, sender: self)
//        }
//    }
//    
//    
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//        return CGSizeMake(self.view.bounds.width/2, self.view.bounds.width/2 + 70)
//    }
//    
//    
//}

extension MainViewPagingMenuViewController:  UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
    
    // MARK: UISearchBarDelegate
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            if searchBar.selectedScopeButtonIndex == 0 {
                searchTableViewController.lastProductSearched = ""
                searchTableViewController.loadingBeforeEmptyDataSet = false
            }else{
                searchTableViewController.loadingBeforeEmptyDataSet = false
                searchTableViewController.lastFranchiseSearched = ""
            }
        }
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        print(#function)
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        searchTableViewController.scopeIndex = selectedScope
        if selectedScope == 0 {
            searchBar.text = searchTableViewController.lastProductSearched
        }else{
            searchBar.text = searchTableViewController.lastFranchiseSearched
        }
    }
    
    // MARK: UISearchResultsUpdating
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if let text = searchController.searchBar.text {
            if text.characters.count >= 3 {
                self.searchTableViewController.treshold = true
                
                //                let delay = 0.5 * Double(NSEC_PER_SEC)
                //                let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                //                dispatch_after(time, dispatch_get_main_queue()) {
                if searchController.searchBar.selectedScopeButtonIndex == 0 {
                    self.searchTableViewController.searchProductWithText(text)
                }else{
                    self.searchTableViewController.searchFranchisesWithText(text)
                }
                //                }
            }else{
                self.searchTableViewController.loadingBeforeEmptyDataSet = false
                self.searchTableViewController.treshold = false
                self.searchTableViewController.products = []
                self.searchTableViewController.restaurants = []
                self.searchTableViewController.tableView.reloadData()
            }
        }
    }
    
    // MARK: UISearchControllerDelegate
    
    func willPresentSearchController(searchController: UISearchController) {
        print(#function)
        
        
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
    }
    
    
    
    func willDismissSearchController(searchController: UISearchController) {
      
        self.tabBarController!.setTabBarVisible(true, animated: true)
    }
    
    
    @IBAction func searchPressed(sender: UIButton) {
        
        if searchController.active {
            self.tabBarController!.setTabBarVisible(true, animated: true)
            self.searchController.dismissViewControllerAnimated(true, completion: nil)
        }else{
            self.tabBarController!.setTabBarVisible(false, animated: true)
            self.presentViewController(self.searchController, animated: true){
                
            }
            
        }
    }
    
   
    
    
    // MARK: SearchResultSelectedDelegate
    func didSelectSearchResult(product: MProduct?, branch: MRestaurant?) {
        selectedProduct = nil
        selectedBranch = nil
        if product != nil {
            
            deliveryOrPickupHistorySearch(product!.branch_products.first!, completion: { (isDelivery, newBranch) in
                let productoVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
                if isDelivery == nil || isDelivery == true {
                    productoVC.product = product
                    let branch = newBranch
                    productoVC.restaurant = branch
                    branch.mode = 0
                    productoVC.restaurant?.franchise = product?.franchise
                    self.searchController.active = false
                    self.navigationController?.pushViewController(productoVC, animated: true)
                    
                }else{//pickup
                    self.searchController.active = false
                    self.selectedProduct = product
                    let bvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                    bvc.franchise = newBranch.franchise
                    bvc.delegate = self
                    self.navigationController?.pushViewController(bvc, animated: true)
                    
                }
            })
            
            
        }else{
            deliveryOrPickupHistorySearch(branch!, completion: { (isDelivery, newBranch) in
                
                if isDelivery == nil || isDelivery == true {
                    
                    let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
                    newBranch.mode = 0
                    restaurantVC.restaurant = newBranch
                    
                    //            controllers?.append(categoriasVC!)
                    self.searchController.active = false
                    self.navigationController?.pushViewController(restaurantVC, animated: true)
                    
                    
                    
                }else{//pickup
                    self.searchController.active = false
                    self.selectedBranch = newBranch
                    let bvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                    bvc.franchise = newBranch.franchise
                    bvc.delegate = self
                    self.navigationController?.pushViewController(bvc, animated: true)
                    
                }
            })
            
            
        }
        
    }


    func didSelectBranch(branch: MRestaurant,order: MOrder?)
    {
        if selectedProduct != nil {
            let customizeProductVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
            customizeProductVC.restaurant = branch
            customizeProductVC.product = selectedProduct
            self.navigationController?.pushViewController(customizeProductVC, animated: true)
        }else{
            let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
            branch.mode = 1
            restaurantVC.restaurant = branch
            self.navigationController?.pushViewController(restaurantVC, animated: true)
        }
        
    }
    
    
    
}

extension MainViewPagingMenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return filteredData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.ReuseSearchCellID, forIndexPath: indexPath)
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData[section].count
    }
    
}

