//
//  RestaurantViewController.swift
//  Monchis
//
//  Created by jrivarola on 9/8/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import Haneke
import DZNEmptyDataSet
import RESideMenu
import SwiftyJSON
import ReachabilitySwift
import QuartzCore
import pop
import Crashlytics
import Mixpanel

class RestaurantViewController: UIViewController,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource,SearchResultSelectedDelegate, UITabBarControllerDelegate, RESideMenuDelegate, BranchSelectionDelegate{
    
    @IBOutlet weak var pickupDeliveryHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var restoInfoBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var restoHeaderImageView: UIImageView!
    var searchController: UISearchController!
    var searchTableViewController:SearchTableViewController!
    var searchBar: UISearchBar!
    var menus:[MMenu] = []
    var filteredMenu: [MMenu] = []
    var selectedProduct: MProduct!
    var selectedBranch: MRestaurant!
    var selectedMenu: MMenu!
    var restaurant: MRestaurant!
    let api = MAPI.SharedInstance
    var refreshControl: UIRefreshControl!
    var loadingBeforeEmptyDataSet: Bool = true
    var localSearchActive: Bool = false
    let notification = CWStatusBarNotification()
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var closedRestoIconImgView: UIImageView!
    @IBOutlet weak var noCoverRestoIconImgView: UIImageView!
    @IBOutlet weak var leadingHeaderImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    
    @IBOutlet weak var bgImgView: UIImageView!
    @IBOutlet weak var deliveryPickupLbl: UILabel!
    @IBOutlet weak var deliveryPickupImageView: UIImageView!
    struct Constants
    {
        static let menuRestaurantReuseID = "menuRestaurantReuseID"
        static let ReuseSearchCellID = "ReuseSearchCellID"
    }
    
    struct Segues {
        static let selectedCategoryMenu = "selectedCategoryMenuSegue"
        static let infoResto = "infoRestoSegue"
    }
    
    @IBOutlet weak var statesSpinner: UIActivityIndicatorView!
    
    // MARK: LifeCycle
    var branchMode: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.delegate = nil
        setupUI()
        if let franchiseID = restaurant?.franchise?.id {
            loadBranchProductsCategories(Int(franchiseID))
            Answers.logContentViewWithName("Usuario vio categorias de menu de Local.",
                                           contentType: "Franquicias",
                                           contentId: "\(franchiseID)",
                                           customAttributes: [:])
       
        }
        registerForNotifications()
//        setupLocalNotifications()
        animationsForStates()
        configurePullToRefresh()
       
    }
    
    func configurePullToRefresh(){
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.whiteColor()
        let title = NSMutableAttributedString()
        let tira = NSAttributedString(string: "Tira para actualizar", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        title.appendAttributedString(tira)
        refreshControl.attributedTitle = title
        refreshControl.addTarget(self, action: #selector(RestaurantViewController.pullToRefresh(_:)), forControlEvents: .ValueChanged)
        self.tableView?.addSubview(refreshControl)
    }
    
    @IBAction func shareBranchPressed(sender: UIButton) {
        
        var array:[AnyObject] = []
        let text = "Mira este Local en Monchis! - " + self.restaurant.name
        let urlString = "https://admin.monchis.com.py/?q="
        let query = "franchiseid=\(self.restaurant.franchise!.id!)&type=branch"
        let hash = query.toBase64() ?? ""
        
        let link = NSURL(string: urlString + hash)!
        array.append(text)
        array.append(link)
        if let img = self.restoHeaderImageView.image {
            array.insert(img, atIndex: 0)
        }
        let activityViewController = UIActivityViewController(activityItems: array, applicationActivities: nil)
        activityViewController.setValue("Mira este Local en Monchis!", forKey: "subject")
        
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        activityViewController.completionWithItemsHandler = { activityType, success, items, error in
            if !success {
                print("cancelled")
                return
            }
            
            Answers.logShareWithMethod(activityType ?? "",
                                       contentName: self.restaurant.name,
                                       contentType: "Restaurante",
                                       contentId: "\(self.restaurant.id!)",
                                       customAttributes: nil)
            
            Mixpanel.mainInstance().track(event: "Usuario Compartio una Franquicia",
                                          properties: ["Franquicia" : self.restaurant.franchise?.name ?? "","Pantalla":"Categorias de Menu de Franquicia", "Metodo": activityType ?? ""])
            
        }

        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    
    let TipInCellAnimatorStartTransform:CATransform3D = {
        let rotationDegrees: CGFloat = -15.0
        let rotationRadians: CGFloat = rotationDegrees * (CGFloat(M_PI)/180.0)
        let offset = CGPointMake(-20, -20)
        var startTransform = CATransform3DIdentity
        startTransform = CATransform3DRotate(CATransform3DIdentity,
                                             rotationRadians, 0.0, 0.0, 1.0)
        startTransform = CATransform3DTranslate(startTransform, offset.x, offset.y, 0.0)
        
        return startTransform
    }()
    
    
    @IBAction func editModePressed(sender: UIButton) {
        self.selectedBranch = nil
        self.selectedProduct = nil
        changeBranchMode(self.restaurant) { (branch) in
            self.branchMode = branch.mode
            if branch.mode == 1 || branch.mode == 2{
                let bsvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                bsvc.delegate = self
                bsvc.franchise = self.restaurant.franchise
                self.navigationController?.pushViewController(bsvc, animated: true)
            }else {
                Mixpanel.mainInstance().track(event: "Usuario Cambio Modo de Local",
                                              properties: ["Pantalla":"Categorias de Menu de Franquicia", "Franquicia": self.restaurant.franchise?.name ?? "","Antes":self.restaurant.mode,"Despues":branch.mode, ])
                self.refreshBranchUI()
                self.refreshBranch()
                NSNotificationCenter.defaultCenter().postNotificationName("refreshBranchForModeChangeNotification", object: nil)
            }
        }
    }
    
    func didSelectBranch(branch: MRestaurant, order: MOrder?) {
        if selectedProduct != nil && selectedBranch == nil {
            let customizeProductVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
            customizeProductVC.restaurant = branch
            customizeProductVC.product = selectedProduct
            
            self.navigationController?.pushViewController(customizeProductVC, animated: true)
        }else if selectedBranch != nil && selectedProduct == nil{
            let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
            branch.mode = 1
            restaurantVC.restaurant = branch
            
            self.navigationController?.pushViewController(restaurantVC, animated: true)
        }else{
            Mixpanel.mainInstance().track(event: "Usuario Cambio Modo de Local",
                                          properties: ["Pantalla":"Categorias de Menu de Franquicia", "Franquicia": self.restaurant.franchise?.name ?? "","Antes":self.restaurant.mode,"Despues":branch.mode, ])
            self.restaurant = branch
            self.refreshBranchUI()
        }
    }
    
    func animateCells(){
        
        var delay:Double = 0
        for cell in self.tableView.visibleCells as! [RestaurantMenuTableViewCell] {
            UIView.animateWithDuration(0.22, delay: delay*0.22, options: [.CurveEaseIn, .AllowUserInteraction,.Autoreverse], animations: {
                cell.titleLabel?.transform = CGAffineTransformMakeScale(1.08, 1.08)
                }, completion: {fin in
                    cell.titleLabel?.transform = CGAffineTransformMakeScale(1, 1)
            })
            
            delay+=1
        }
    }
    
    var animationTimer: NSTimer!
    
    func pullToRefresh(sender: UIRefreshControl){
        if let franchiseID = restaurant?.franchise?.id {
            loadBranchProductsCategories(Int(franchiseID))
            
        }
    }
    
    func refreshBranchUI(){
        //header
        refreshBranchUIForModeChange()
        
        
        animationsForStates()
    }
    
    func registerForNotifications(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RestaurantViewController.userDefaultAddressChanged), name: kUserDidChangeDefaultAddressNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RestaurantViewController.reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RestaurantViewController.refreshBranchUIForModeChangeNotificationHandler(_:)), name: "refreshBranchForModeChangeNotification", object: nil)
        
    }
    
    
    
    func reachabilityChanged(notification: NSNotification) {
        
        if let reachability = notification.object as? Reachability {
        
        if reachability.isReachable() {
            self.tableView?.reloadEmptyDataSet()
            if let franchiseID = restaurant?.franchise?.id {
                loadBranchProductsCategories(Int(franchiseID))
            }
            animationsForStates()
            if branchMode == 0 {
                refreshBranch()
            }else{
                self.refreshBranchUI()
            }
            
            if reachability.isReachableViaWiFi() {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            print("Network not reachable")
            
        }
        }
    }
    
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        animationTimer?.invalidate()
    }
    
    
    
    func userDefaultAddressChanged(){
        //handle new default address
        refreshBranch()
    }
    
    override func viewWillLayoutSubviews() {
        if let navContr = self.navigationController {
            if navContr.respondsToSelector(Selector("interactivePopGestureRecognizer")) {
                navContr.interactivePopGestureRecognizer?.enabled = false
            }
            
        }
    }
    
    
    
    var headerMonchisImage: UIImageView!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //favorites
        if let franchiseID = restaurant?.franchise?.id {
            loadBranchProductsCategories(Int(franchiseID))
        }
//        if let _ = MUser.sharedInstance.branchesFavorites.indexOf({$0.id == restaurant.id}) {
//            self.favoriteBtn.setImage(favorited, forState: .Normal)
//        }else{
//            self.favoriteBtn.setImage(unFavorited, forState: .Normal)
//            
//        }
        animationsForStates()
        //        refreshBranch()
        //        animationTimer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: Selector("animateCells"), userInfo: nil, repeats: true)
    }
    
    func setupLocalNotifications(){
        notification.notificationStyle = .NavigationBarNotification
        
        if restaurant?.delivery_available != nil && restaurant?.is_open != nil {
            if (restaurant.delivery_available!) && (restaurant.is_open!) { // open an delivery available
                
            }
            
            if (restaurant.delivery_available!) && (restaurant.isClosed()) {
                notification.notificationLabelBackgroundColor = UIColor.redColor()
                
                
                let stillBuyMessage =  NSAttributedString(string: "El local se encuentra cerrado!", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(15)])
                self.notification.displayNotificationWithAttributedString(stillBuyMessage, forDuration: 1)
                
                
            }
            if !(restaurant.delivery_available!) && (!restaurant.isClosed() && restaurant.mode == 0) {
                notification.notificationLabelBackgroundColor = UIColor(hexString: "#05c0ff")!
                let stillBuyMessage =  NSMutableAttributedString(string: "El local se encuentra fuera del area de cobertura para tu dirección: ", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(15)])
                
                let userDefaultAddress = NSAttributedString(string: MUser.sharedInstance.defaultAddress.name ?? "", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(15)])
                stillBuyMessage.appendAttributedString(userDefaultAddress)
                
                self.notification.displayNotificationWithAttributedString(stillBuyMessage, forDuration: 0.2)
            }
            
            if !(restaurant.delivery_available!) && (restaurant.isClosed()) {
                
            }
            
        }
        
        
        
    }
    
    
    func animatonForClosedState() {
        
        closedRestoIconImgView?.hidden = false
        UIView.animateWithDuration(0.5, delay: 0, options: [.Autoreverse,.Repeat], animations: { () -> Void in
            self.closedRestoIconImgView?.alpha = 0.1
            }, completion: { (fin) -> Void in
        })
        
        
    }
    
    func animationForOutOfCoverState(){
        if restaurant?.mode == 0 {
        noCoverRestoIconImgView?.hidden = false
        }else{
            noCoverRestoIconImgView?.hidden = true
        }
        UIView.animateWithDuration(0.5, delay: 0, options: [.Autoreverse,.Repeat], animations: { () -> Void in
            self.noCoverRestoIconImgView?.alpha = 0.1
            }, completion: { (fin) -> Void in
                
        })
    }
    @IBOutlet weak var pickupDeliveryView: UIView!
    
    func animationsForStates(){
        closedRestoIconImgView?.alpha = 1
        noCoverRestoIconImgView?.alpha = 1
        noCoverRestoIconImgView?.hidden = true
        closedRestoIconImgView?.hidden = true
        
        leadingHeaderImageConstraint?.constant = 8
        
//        if (restaurant.delivery_available!) && !(restaurant.isClosed()) { // open an delivery available
//            leadingHeaderImageConstraint?.constant = 8
//            
//        }
//        
//        if (restaurant.delivery_available!) && (restaurant.isClosed()) {
//            leadingHeaderImageConstraint?.constant = 31
//            
//            animatonForClosedState()
//        }
//        if !(restaurant.delivery_available!) && (!restaurant.isClosed()) {
//            if restaurant.mode == 0 {
//                leadingHeaderImageConstraint?.constant = 31
//            }else{
//                leadingHeaderImageConstraint?.constant = 8
//            }
//            animationForOutOfCoverState()
//        }
//        
//        if !(restaurant.delivery_available!) && (restaurant.isClosed()) {
//            leadingHeaderImageConstraint?.constant = 31
//            
//            animatonForClosedState()
//            animationForOutOfCoverState()
//            
//        }
//        
        if restaurant != nil {
            if restaurant.mode == 0 {
                if restaurant.hasDelivery() && (restaurant.isOpen()) { // open an delivery available
                    leadingHeaderImageConstraint?.constant = 8
                }
                if restaurant.hasDelivery() &&  (!restaurant.isOpen()) {
                    leadingHeaderImageConstraint?.constant = 31
                    animatonForClosedState()
                }
                if !restaurant.hasDelivery() && (restaurant.isOpen()) {
                    leadingHeaderImageConstraint?.constant = 31
                    animationForOutOfCoverState()
                }
                if !restaurant.hasDelivery() && (!restaurant.isOpen()) {
                    leadingHeaderImageConstraint?.constant = 31
                    animationForOutOfCoverState()
                    animatonForClosedState()
                }
            }else{
                if restaurant.isOpen() {
                    leadingHeaderImageConstraint?.constant = 8
                }else{
                    leadingHeaderImageConstraint?.constant = 31
                    animatonForClosedState()
                }
            }
        }
        UIView.animateWithDuration(0.2) {
            self.view.setNeedsLayout()
        }
        
    }
    
    
    func sideMenu(sideMenu: RESideMenu!, willShowMenuViewController menuViewController: UIViewController!) {
        notification.dismissNotification()
    }
    
    func refreshBranchUIForModeChangeNotificationHandler(){
        
    }
    func refreshBranchUIForModeChange(){
        if restaurant.mode == 0 {
            noCoverRestoIconImgView.hidden = false

            deliveryPickupImageView?.image = UIImage(named: "icono-delivery")
            deliveryPickupLbl?.text = "Delivery a: \(MUser.sharedInstance.defaultAddress.name)"
            restoInfoBtn?.setTitle(restaurant?.franchise?.name?.uppercaseString, forState: .Normal)
            UIView.animateWithDuration(1.0, animations: {
                self.pickupDeliveryView?.backgroundColor = UIColor(hexString: "#363A40")
                self.bgImgView?.alpha = 1
            })
        }else{
            restoInfoBtn?.setTitle(restaurant?.name.uppercaseString, forState: .Normal)
            deliveryPickupImageView?.image = UIImage(named: "icono-pickup-activo-sobre-activo")
            deliveryPickupLbl?.text = "Para buscar de sucursal"
            noCoverRestoIconImgView.hidden = true
            UIView.animateWithDuration(1.0, animations: {
                self.pickupDeliveryView?.backgroundColor = UIColor(hexString: "#CF1B1B")
                self.bgImgView?.alpha = 0
                
            })
        }
        self.tableView?.reloadEmptyDataSet()
        self.tableView?.reloadData()
    }
    func refreshBranchUIForModeChangeNotificationHandler(notification: NSNotification){
        if let branch = notification.object?["branch"] as? MRestaurant {
            if self.restaurant.franchise?.name == branch.franchise?.name {
                self.restaurant = branch
                refreshBranchUIForModeChange()
            }
        }else{
            self.refreshBranch()
        }
    }
    
    func setupUI(){
        searchBtn?.imageView?.contentMode = .ScaleAspectFit
        self.tableView.scrollsToTop = true

        self.tabBarController?.delegate = self
        self.branchMode = restaurant.mode
        self.tableView.estimatedRowHeight = 50
        self.tableView.rowHeight = UITableViewAutomaticDimension
        //header
        self.refreshBranchUIForModeChange()
        restoHeaderImageView.layer.cornerRadius = restoHeaderImageView.bounds.width/2
        restoHeaderImageView.clipsToBounds = true
        self.restaurant?.franchise?.header_attachment?.width = 45
        self.restaurant?.franchise?.header_attachment?.height = 45
        if let imageURL = self.restaurant?.franchise?.header_attachment?.imageComputedURL {
            restoHeaderImageView.hnk_setImageFromURL(imageURL, placeholder: NO_BRANCH_PLACEHOLDER)
            if !imageCacheKeys.contains(imageURL.URLString) {
                imageCacheKeys.append(imageURL.URLString)
            }
            cacheFormats[imageURL.URLString] = restoHeaderImageView.hnk_format.name
            
        }
        shadowView.layer.shadowColor = UIColor.blackColor().CGColor
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shadowOffset = CGSizeZero
        shadowView.layer.shadowRadius = 10
        
        
        if headerBlurOn {//blur the image
            let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light)) as UIVisualEffectView
            visualEffectView.frame = restoHeaderImageView.bounds
            restoHeaderImageView.addSubview(visualEffectView)
        }
        
        //SearchBar Controller
        searchTableViewController = storyboard?.instantiateViewControllerWithIdentifier("SearchTableViewController") as! SearchTableViewController
        searchController = UISearchController(searchResultsController: searchTableViewController)
        
        searchController.hidesNavigationBarDuringPresentation = false
        //searchController.searchBar.searchBarStyle = UISearchBarStyle.Minimal
        
        searchController.searchBar.sizeToFit()
        searchBar = searchController.searchBar
        searchBar.tintColor = UIColor(hexString: "#FF8000")
        
        searchBar.translucent = false
        
        searchBar.delegate = self
        for subView in searchBar.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.attributedPlaceholder =  NSAttributedString(string:"Buscar.. (min 3 caracteres)",
                                                                          attributes:[NSFontAttributeName: UIFont(name: "OpenSans-Semibold", size: 13)!])
                }
            }
        }
        searchBar.scopeButtonTitles = ["Productos","Restaurantes"]
        searchTableViewController.definesPresentationContext = true
        searchTableViewController.searchDelegate = self
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = true // important
        //self.navigationItem.titleView = searchBar
        searchController.delegate = self
        

        restoInfoBtn.sizeToFit()
        
        self.title = "Menú"
        
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        //favorites
        
//        if let _ = MUser.sharedInstance.branchesFavorites.indexOf({$0.id == restaurant.id}) {
//            self.favoriteBtn.setImage(favorited, forState: .Normal)
//        }else{
//            self.favoriteBtn.setImage(unFavorited, forState: .Normal)
//            
//        }
        self.tableView.reloadEmptyDataSet()

    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // MARK: TabBarControllerDelegate
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        if notification.notificationIsShowing {
            notification.dismissNotification()
        }
    }
    
    func loadBranchProductsCategories(franchise_id: Int){
        let parameters = ["franchise_id": franchise_id]
        
        api.returnCachedData(.LIST_PRODUCTS, parameters: parameters, completion: { (json) in
            if debugModeEnabled {
                print("returnCachedData for list products","called with parameters: nil",json)
            }
            if let jsonArray = json?["data"].array {
                self.updateUIForMenu(jsonArray)
            }
            
        })
        
        
        //self.collectionView?.reloadEmptyDataSet()
        
        api.postQueueRequest(.LIST_PRODUCTS, parameters: parameters) { (json, error) in
            if debugModeEnabled {
                print(#function,"called with parameters: nil",json,error)
            }
            if error == nil {
                if let jsonArray = json?["data"].array {
                    self.updateUIForMenu(jsonArray)
                }
            }else{
                
            }
            
        }
        
        
    }
    
    func updateUIForMenu(json: [SwiftyJSON.JSON]) {
        var tmp: [MMenu] = []
        for data in json {
            let menuItem = MMenu(json: data)
            tmp.append(menuItem)
        }
        print("tmp count \(tmp.count)")
        self.loadingBeforeEmptyDataSet = false
        
        self.dismissWaitScreen()
        self.restaurant.menu = tmp
        self.tableView.reloadEmptyDataSet()
        self.tableView.reloadData()

    }
    

    
    func createWaitScreen(){
        spinner.startAnimating()
        //glowMonchis()
    }
    func unGlowMonchis(){
        headerMonchisImage.layer.shadowColor = UIColor.clearColor().CGColor
        headerMonchisImage.layer.shadowRadius = 0
        headerMonchisImage.layer.shadowOpacity = 0
        headerMonchisImage.layer.removeAllAnimations()
    }
    func dismissWaitScreen(){
        spinner?.stopAnimating()
        refreshControl?.endRefreshing()
        //unGlowMonchis()
    }
    
    
    
    
    func glowMonchis(){
        colorize(self.headerMonchisImage, color:UIColor(hexString: "#950505")!.CGColor)
    }
    
    
    
    // MARK: - Navigation
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == Segues.selectedCategoryMenu {
            let dvc = segue.destinationViewController as! MenuCategoriaViewController
            dvc.restaurant = self.restaurant
            dvc.selectedMenu = selectedMenu
        }
        
        if segue.identifier == Segues.infoResto {
            let dvc = segue.destinationViewController as! InfoRestoViewController
            dvc.restaurant = self.restaurant
        }
    }
}

extension RestaurantViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var menu: MMenu!
        if localSearchActive {
            menu = filteredMenu[indexPath.row]
        }else{
            menu = restaurant?.menu[indexPath.row]
        }
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.menuRestaurantReuseID, forIndexPath: indexPath) as! RestaurantMenuTableViewCell
        cell.titleLabel.text = menu.name
        if indexPath.row%2 == 0 {
            if restaurant.mode == 0 {
                cell.contentView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.15)
            }else{
                cell.contentView.backgroundColor = .whiteColor()
            }
        }else{
            if restaurant.mode == 0 {
                cell.contentView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.2)
            }else{
                cell.contentView.backgroundColor = UIColor(hexString: "#EAEAEA")
            }
            
        }
        if restaurant.mode == 0 {
            cell.titleLabel.textColor = UIColor.whiteColor()
            cell.arrowImgView.image = UIImage(named: "flecha derecha")
        }else{
            cell.titleLabel.textColor = UIColor.darkGrayColor()
            cell.arrowImgView.image = UIImage(named: "flecha derecha gris")
            
        }
        
        cell.layoutIfNeeded()
        return cell
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return restaurant?.menu.count > 0 ? 1 : 0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        selectedMenu = restaurant?.menu[indexPath.row]
        Mixpanel.mainInstance().track(event: "Usuario Eligio una Categoria de Menu",
                                      properties: ["Categoria de Menu" : selectedMenu?.name ?? "","Pantalla":"Categorias de Menu de Franquicia", "Franquicia": self.restaurant.franchise?.name ?? ""])
        self.performSegueWithIdentifier(Segues.selectedCategoryMenu, sender: self)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurant?.menu.count ?? 0
    }
    
}


extension RestaurantViewController:  UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
    
    // MARK: UISearchBarDelegate
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            if searchBar.selectedScopeButtonIndex == 0 {
                searchTableViewController.lastProductSearched = ""
                searchTableViewController.loadingBeforeEmptyDataSet = false
            }else{
                searchTableViewController.loadingBeforeEmptyDataSet = false
                searchTableViewController.lastFranchiseSearched = ""
            }
        }
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        print(#function)
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        searchTableViewController.scopeIndex = selectedScope
        if selectedScope == 0 {
            searchBar.text = searchTableViewController.lastProductSearched
        }else{
            searchBar.text = searchTableViewController.lastFranchiseSearched
        }
    }
    
    // MARK: UISearchResultsUpdating
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if let text = searchController.searchBar.text {
            if text.characters.count >= 3 {
                self.searchTableViewController.treshold = true
                
                //                let delay = 0.5 * Double(NSEC_PER_SEC)
                //                let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                //                dispatch_after(time, dispatch_get_main_queue()) {
                if searchController.searchBar.selectedScopeButtonIndex == 0 {
                    self.searchTableViewController.searchProductWithText(text)
                }else{
                    self.searchTableViewController.searchFranchisesWithText(text)
                }
                //                }
            }else{
                self.searchTableViewController.loadingBeforeEmptyDataSet = false
                self.searchTableViewController.treshold = false
                self.searchTableViewController.products = []
                self.searchTableViewController.restaurants = []
                self.searchTableViewController.tableView.reloadData()
            }
        }
        print(#function)
    }
    
    func searchTextInMenu(text: String) {
        if searchController.searchBar.text?.characters.count > 0 {
            filteredMenu = self.restaurant.menu.filter({ (menu) -> Bool in
                return menu.name.rangeOfString(text) != nil
            })
        }else{
            filteredMenu = self.restaurant.menu
        }
        self.tableView.reloadData()
    }
    
    // MARK: UISearchControllerDelegate
    
    func willPresentSearchController(searchController: UISearchController) {
        print(#function)
        
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
    }
    
    
    
    func willDismissSearchController(searchController: UISearchController) {
        
        self.tabBarController!.setTabBarVisible(true, animated: true)
        self.tableView.reloadData()
     
        
    }
    
    @IBAction func searchPressed(sender: UIButton) {
        
        if searchController.active {
            self.tabBarController!.setTabBarVisible(true, animated: true)
            self.searchController.dismissViewControllerAnimated(true, completion: nil)
        }else{
            self.tabBarController!.setTabBarVisible(false, animated: true)
            self.presentViewController(self.searchController, animated: true){
                
            }
            
        }
    }
    
    
    
    
    
    // MARK: EmptyDataSetSource
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        if connectedToNetwork() {
            if restaurant.mode == 0 {
                let image = UIImage(named: "carrito-vacio")
                return image
            }else{
                let image = UIImage(named: "carrito-vacio-rojo")
                return image
            }
        }else{
            if restaurant.mode == 0 {
                
                return UIImage(named: "icono-error-red")
            }else{
                return UIImage(named: "icono-error-red-rojo")
            }
        }
    }
    
    func imageAnimationForEmptyDataSet(scrollView: UIScrollView!) -> CAAnimation! {
        
        let animation = CABasicAnimation(keyPath: "transform")
        
        animation.fromValue = NSValue(CATransform3D: CATransform3DIdentity)
        animation.toValue = NSValue(CATransform3D:CATransform3DMakeRotation(CGFloat(M_PI_2), 0.0, 0.0, 1.0))
        
        animation.duration = 0.25
        animation.cumulative = true
        animation.repeatCount = MAXFLOAT
        return animation
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        var color : UIColor!
        if restaurant.mode == 0 {
            color = UIColor.whiteColor()
        }else{
            color = UIColor(hexString: "#cf1b1b")
        }
        if connectedToNetwork() {
            if loadingBeforeEmptyDataSet {
                
                let text = NSAttributedString(string: NSLocalizedString("Obteniendo Menú...", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(18),NSForegroundColorAttributeName:color])
                
                return text
            }else{
                let text = NSAttributedString(string: NSLocalizedString("Menú en proceso de carga.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(18),NSForegroundColorAttributeName:color])
                
                return text
            }
        }else{
            let text = NSAttributedString(string: NSLocalizedString("No tienes conexión a internet.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(18),NSForegroundColorAttributeName:color])
            
            return text
        }
    }
    
    
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) ->
        NSAttributedString! {
            var color : UIColor!
            if restaurant.mode == 0 {
                color = UIColor.whiteColor()
            }else{
                color = UIColor(hexString: "#cf1b1b")
            }
            if connectedToNetwork() {
                if loadingBeforeEmptyDataSet {
                    return nil
                }else{
                    let subTitle = NSAttributedString(string: NSLocalizedString("< Volver atras", comment: ""),attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(18),NSForegroundColorAttributeName:color])
                    
                    return subTitle
                }
            }else{
                return nil
            }
    }
    
    func emptyDataSetShouldAnimateImageView(scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    // MARK: SearchResultSelectedDelegate
    func didSelectSearchResult(product: MProduct?, branch: MRestaurant?) {
        selectedProduct = nil
        selectedBranch = nil
        if product != nil {
            
            deliveryOrPickupHistorySearch(product!.branch_products.first!, completion: { (isDelivery, newBranch) in
                let productoVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
                if isDelivery == nil || isDelivery == true {
                    productoVC.product = product
                    let branch = newBranch
                    productoVC.restaurant = branch
                    branch.mode = 0
                    productoVC.restaurant?.franchise = product?.franchise
                    self.searchController.active = false
                    self.navigationController?.pushViewController(productoVC, animated: true)
                    
                }else{//pickup
                    self.searchController.active = false
                    self.selectedProduct = product
                    let bvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                    bvc.franchise = newBranch.franchise
                    bvc.delegate = self
                    self.navigationController?.pushViewController(bvc, animated: true)
                    
                }
            })
            
            
        }else{
            deliveryOrPickupHistorySearch(branch!, completion: { (isDelivery, newBranch) in
                
                if isDelivery == nil || isDelivery == true {
                    
                    let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
                    newBranch.mode = 0
                    restaurantVC.restaurant = newBranch
                    
                    //            controllers?.append(categoriasVC!)
                    self.searchController.active = false
                    var controllers = self.navigationController!.viewControllers
                    controllers.removeLast()
                    controllers.append(restaurantVC)
                    self.navigationController?.setViewControllers(controllers, animated: true)

                    
                    
                    
                }else{//pickup
                    self.searchController.active = false
                    self.selectedBranch = newBranch
                    let bvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                    bvc.franchise = newBranch.franchise
                    bvc.delegate = self
                    self.navigationController?.pushViewController(bvc, animated: true)
                    
                }
            })
            
            
        }
        
    }
    
    

    

    
    @IBAction func favoritePressed(sender: UIButton) {
            if MUser.sharedInstance.branchesFavorites.contains(restaurant){
                self.favoriteBtn.setImage(unFavorited, forState: .Normal)
                deleteFavorite()
                self.favoriteBtn.pop()
            }else{
                self.favoriteBtn.setImage(favorited, forState: .Normal)
                self.favoriteBtn.pop()
                addFavorite()
            }
    }
    
    
    func addFavorite(){
        
        let parameters = ["branch_id":restaurant.id!]
        api.jsonRequest(.FAVORITE_PRODUCT, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    MUser.sharedInstance.branchesFavorites.append(self.restaurant)
                }else{
                    self.favoriteBtn.setImage(unFavorited, forState: .Normal)
                    let error = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Lo sentimos!", message: error, preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }else{
                self.favoriteBtn.setImage(unFavorited, forState: .Normal)
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func deleteFavorite(){
        let parameters = ["branch_id":restaurant.id!]
        api.jsonRequest(.DELETE_PRODUCT_FAVORITE, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    /* handle favorite removal */
                    
                    if let index = MUser.sharedInstance.branchesFavorites.indexOf({$0.id == self.restaurant.id}) {
                        MUser.sharedInstance.branchesFavorites.removeAtIndex(index)
                        self.favoriteBtn.setImage(unFavorited, forState: .Normal)
                        
                    }
                    
                    
                }else{
                    let error = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Lo sentimos!", message: error, preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }else{
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    func refreshBranch(){
        self.refreshBranchUI()
        statesSpinner.startAnimating()
        self.leadingHeaderImageConstraint.constant = 31
        UIView.animateWithDuration(0.2) {
            self.view.setNeedsLayout()
        }
        noCoverRestoIconImgView.hidden = true
        closedRestoIconImgView.hidden = true
        let currentDay = getDayOfWeek()!
        let latitude = MUser.sharedInstance.defaultAddress.latitude ?? ""
        let longitude = MUser.sharedInstance.defaultAddress.longitude ?? ""
        let parameters: [String:AnyObject] = ["franchise_id":self.restaurant.franchise_id!,"day":currentDay,"latitude":latitude,"longitude":longitude,"group_by_franchise":true]
        api.jsonRequest(.SEARCH_BRANCHES, parameters: parameters, returnCachedDataIfPossible: false ) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            self.statesSpinner.stopAnimating()
            
            if error == nil {
                if let restaurantsArray = json?["data"].array {
                    for data in restaurantsArray {
                        let newBranch = MRestaurant(data: data["branch"])
                        newBranch.menu = self.restaurant.menu
                        self.restaurant = newBranch
//                        self.restaurant.mode = self.branchMode
                    }
                    self.refreshBranchUI()
                    self.dismissWaitScreen()
                    
                }else{// network error
                    
                    
                }
                
            }
        }
        
    }
    
    
    
    
    
}

