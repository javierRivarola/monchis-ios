//
//  LoginViewController.swift
//  Monchis
//
//  Created by jrivarola on 6/22/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import DynamicBlurView
import KeychainSwift
import FacebookLogin
import SCLAlertView
import Crashlytics
import Mixpanel
import JRMFloatingAnimation

protocol LoginViewControllerDelegate {
    func didSuccededFacebookLogin()
    func didSuccededMonchisLogin()
    func didSuccededRegistration()
}
class LoginViewController: UIViewController, UITextFieldDelegate, RegisterPopoverDelegate, LoginVCDelegate {
    
    
    // MARK: Properties
    
    var user = MUser.sharedInstance
    var delegate: LoginViewControllerDelegate?
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var logoIV: UIImageView!
    @IBOutlet weak var facebookLogin: UIButton!
    @IBOutlet weak var ingresarSinRegistrarteBtn: UIButton!
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var initSesBtn: UIButton!
    @IBOutlet weak var registerHeightConstraint: NSLayoutConstraint!
    var fbLoginManager: LoginManager!
    var blurView: DynamicBlurView!
    var api  = MAPI.SharedInstance
    var dismissTapGesture: UITapGestureRecognizer!
    struct Segues {
        static let ShowMap = "showMapSegue"
        static let MainViewFromLogin = "MainViewFromLogin"
    }
    
  
    var floatingView: JRMFloatingAnimationView!
    @IBOutlet weak var centerYConstraint: NSLayoutConstraint!
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        facebookLogin.imageView?.contentMode = .ScaleAspectFit
        initSesBtn.imageView?.contentMode = .ScaleAspectFit
        registerBtn.imageView?.contentMode = .ScaleAspectFit
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.addDelegate), name: "didLoadedControllerNotification", object: nil)
         fbLoginManager = user.facebookLoginManager
         floatingView = JRMFloatingAnimationView(startingPoint: logoIV.center)
        floatingView.pop = true
        floatingView.floatingShape = .CurveRight
        floatingView.varyAlpha = true
        floatingView.maxFloatObjectSize = 40
        floatingView.animationDuration = 2.5
//        animationTimer = NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: #selector(LoginViewController.animateFood), userInfo: nil, repeats: true)

        
        self.view.addSubview(floatingView)
    }
    
    func addDelegate(){
        loginVC.loginVC?.delegate = self
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        animationTimer?.invalidate()
    }
    var animationTimer: NSTimer!
    

    @IBAction func backBtnPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func registerBtnPressed(sender: UIButton) {
        self.performSegueWithIdentifier("registerSegue", sender: self)
    }
    
    var animatedLogo: Bool = false
    
    func animateFood(){
        floatingView.addImage(UIImage(named: "pizza1")!)
        floatingView.animate()
        floatingView.floatingShape = .CurveLeft

        floatingView.addImage(UIImage(named: "pizza2")!)
        floatingView.animate()
        floatingView.floatingShape = .CurveRight
        floatingView.addImage(UIImage(named: "pizza3")!)
        floatingView.animate()
        floatingView.floatingShape = .CurveLeft
        floatingView.addImage(UIImage(named: "pizza4")!)
        floatingView.animate()
        floatingView.floatingShape = .CurveRight
        floatingView.addImage(UIImage(named: "pizza5")!)
        floatingView.animate()
        floatingView.floatingShape = .CurveRight

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
      

        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if !animatedLogo {
        logoIV.transform = CGAffineTransformMakeTranslation(0,-logoIV.bounds.height)
        self.logoIV.transform = CGAffineTransformMakeScale(-1, 1)
        self.logoIV.alpha = 0
        UIView.animateWithDuration(1.0, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.3, options: [.CurveEaseIn,.CurveEaseOut], animations: {
            self.logoIV.alpha = 1
            self.logoIV.transform = CGAffineTransformIdentity
            self.logoIV.transform = CGAffineTransformMakeTranslation(0, 0)
            
            
        }) { (fin) in
            self.glowMonchis()
            self.animatedLogo = true
        }
        }
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
//        if let email = NSUserDefaults.standardUserDefaults().objectForKey(kMonchisUserEmail) as? String {
//            emailTxt.text = email
//        }
//        if let password = NSUserDefaults.standardUserDefaults().objectForKey(kMonchisUserPassword) as? String {
//            passTxt.text = password
//        }
        
        if MUser.sharedInstance.isTemporal {
            backBtn.hidden = false
        }
       


    }
    @IBOutlet weak var ingresaFbLbl: UILabel!
    
    
    func loginMonchis(){
        createWaitScreen("Iniciando Sesión..", comment: "Iniciando sesion con facebook")
        let keychain = KeychainSwift()
        let mail = keychain.get(kMonchisUserEmail)
        let password = keychain.get(kMonchisUserPassword)
        
        let parameters = ["email":mail ?? "","password":password ?? "", "device_token":MUser.sharedInstance.pushToken,
            "type":"1",
            "model":MUser.sharedInstance.modelName,
            "system":"iOS",
            "version":MUser.sharedInstance.systemVersion]
        api.jsonRequest(.LOGIN_EMAIL, parameters: parameters, completion: { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters \(parameters)",json,error)
            }
            
            if error == nil {
                
                let success = json?["success"].bool ?? false
                if let data = json?["data"] {
                    if success {
                        //we save the user data
                        self.user.email = mail ?? ""
                        self.user.password = password ?? ""
                        self.user.unparseUserInfoFromApi(data)
                        self.user.logged = true
                        //set the monchis token
                        if let monchis_token = data["monchis_token"].string{
                            self.api.token = monchis_token
                            print("Monchis token : \(monchis_token)")

                        }
                        
                        //redirect user?? show animation? show alert?
                        self.user.authenticationMethod = .Monchis
                        
                        //we load the user addresses
                        self.loginHandler()
                         self.registerPushToken()
                    }else{ // show error
                        self.dismissWaitScreen()
                        let message = data.string ?? "Ha ocurrido un error desconocido."
                        
                        SCLAlertView().showError("Lo sentimos", subTitle: message)

                        
                    }
                    
                }
                
            }else{ // net error
                self.dismissWaitScreen()
                SCLAlertView().showError("Lo sentimos", subTitle: error  ?? "Ha ocurrido un error desconocido.")
            }
        })
    }
    
    func dismissRequest(){
        dispatch_async(dispatch_get_main_queue()) {
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            self.blurView?.removeFromSuperview()
            self.blurView?.removeGestureRecognizer(self.dismissTapGesture)
            self.api.currentRequest?.cancel()
        }
    }
    
    
    
    func colorize(imageView: UIImageView, color: CGColorRef) {
        
        // Set the image's shadowColor, radius, offset, and
        // set masks to bounds to false
        imageView.layer.shadowColor = color
        imageView.layer.shadowRadius = 20.0
        imageView.layer.shadowOffset = CGSizeZero
        imageView.layer.masksToBounds = false
        
        // Animate the shadow opacity to "fade it in"
        let shadowAnim = CABasicAnimation()
        shadowAnim.keyPath = "shadowOpacity"
        shadowAnim.fromValue = NSNumber(float: 0.0)
        shadowAnim.toValue = NSNumber(float: 0.9)
        shadowAnim.duration = 0.13
        shadowAnim.repeatCount = 4
        shadowAnim.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        shadowAnim.autoreverses = true
        imageView.layer.addAnimation(shadowAnim, forKey: "shadowOpacity")
        imageView.layer.shadowOpacity = 1
    }
    
    func glowMonchis(){
        colorize(self.logoIV, color:UIColor(hexString: "#950505")!.CGColor)
    }
    

    
    @IBAction func loginFacebookPressed(sender: UIButton) {
        self.view.endEditing(true)
        print("login facebook pressed")
        createWaitScreen("Iniciando Sesión..", comment: "Iniciando sesion con facebook")
        
        fbLoginManager.logIn([.PublicProfile,.Email],viewController: self, completion: { loginResult in
            
            switch loginResult {
            case .Failed(let error):
                print(error)
                // Process error
                self.dismissWaitScreen()
                self.fbLoginManager.logOut()
            case .Cancelled:
                print("User cancelled login.")
                self.dismissWaitScreen()
                self.fbLoginManager.logOut()
            case .Success( _, _, let accessToken):
                self.user.facebook_token = accessToken.authenticationToken
                self.getUserFacebookData()
                
            }
            
            
        })
        
        
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Segues.ShowMap {
            //customization
            let destinationNavigationController = segue.destinationViewController as? UINavigationController
            let targetController = destinationNavigationController?.topViewController as? MapViewController
            targetController?.saveBtnTag = 0
            
        }
        if segue.identifier == Segues.MainViewFromLogin {
            //customize view for user
            
        }
        
        if segue.identifier == "registerSegue" {
            registerVC = segue.destinationViewController as! MainRegisterTableViewController
            registerVC.delegate = self
        }
        if segue.identifier == "loginSegue" {
            loginVC = segue.destinationViewController as! LoginContainerViewController
        }
    }
    var registerVC: MainRegisterTableViewController!
    var loginVC: LoginContainerViewController!
    
    //delegate register VC
    func didSuccededRegistration() {
        registerVC?.dismissViewControllerAnimated(true, completion: nil)
        delegate?.didSuccededRegistration()
    }
    
    func didFailedRegistration() {
        
    }
    
    func didSuccededLogin() {
        loginVC.dismissViewControllerAnimated(true, completion: nil)
        delegate?.didSuccededMonchisLogin()
    }
    
    
    var userLocationGranted: Bool = false
    // Facebook Delegate Methods
    

    
    func createWaitScreen(title: String, comment: String) {
        let spinner = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        spinner.mode = MBProgressHUDMode.Indeterminate
        spinner.label.text = NSLocalizedString(title,comment: comment)
        spinner.label.font = UIFont.init(name: "OpenSans-Semibold", size: 14.0)!
        
    }
    
    func dismissWaitScreen(animated: Bool = true){
        MBProgressHUD.hideHUDForView(self.view, animated: animated)
    }
    
    func getUserFacebookData()
    {
        
        MUser.sharedInstance.getUserFacebookData { (success, registerParameters) in
            if success {
//                if MUser.sharedInstance.isTemporal {
//                    MUser.sharedInstance.updateUserData(registerParameters!, completion: { (success, erorrMsg) in
//                        if success {
//                            Mixpanel.mainInstance().track(event: "Registro", properties: ["Metodo":"Facebook"])
//                            //                        self.loginHandler()
//                            self.delegate?.didSuccededFacebookLogin()
//                        }else{
//                            self.dismissWaitScreen()
//                            SCLAlertView().showError("Error al iniciar sesión con Facebook", subTitle: erorrMsg ?? "Ocurrio un error inesperado, intenta de nuevo")
//                            
//                        }
//                    })
//                }else{
               
                MUser.sharedInstance.registerFacebookUser(registerParameters, completion: { (success) in
                    self.dismissWaitScreen()

                    if success {
                        Mixpanel.mainInstance().track(event: "Registro", properties: ["Metodo":"Facebook"])
                        //                        self.loginHandler()
                        self.delegate?.didSuccededFacebookLogin()
                    }else{
                        SCLAlertView().showError("Error al iniciar sesión con Facebook", subTitle: "Ocurrio un error inesperado, intenta de nuevo")
                        
                    }
                })
                }
                
//            }
        }
        
    }
    
    
    func loginHandler(){
    
        NSUserDefaults.standardUserDefaults().setObject(user.email, forKey: kMonchisUserEmail)
        NSUserDefaults.standardUserDefaults().setObject(user.password, forKey: kMonchisUserPassword)
        NSUserDefaults.standardUserDefaults().synchronize()
        self.dismissWaitScreen()
        self.delegate?.didSuccededMonchisLogin()
        
        }

    
    
    
    
    
    
    
    @IBAction func skipPressed(sender: UIButton) {
        self.user.authenticationMethod = .Unregistered
        self.performSegueWithIdentifier(Segues.ShowMap, sender: self)
    }
    
    
    func welcomeAnimation(){
        
     
        
        //login button animations
        facebookLogin.transform = CGAffineTransformMakeTranslation(-self.view.bounds.width, 0)
        
        UIView.animateWithDuration(0.7, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.6, options: .CurveEaseInOut, animations: {
            self.facebookLogin.transform = CGAffineTransformMakeTranslation(0, 0)
            
            }, completion: {fin in
                
        })
        
    }
    
    // MARK: undiwnd from any view controller to login screen
    @IBAction func unwindToLogin(segue: UIStoryboardSegue){
        
    }
    
    func registerPushToken(){
        var app_version:String = ""
        if let version = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String {
            app_version = version
        }
        let parameters = [  "device_token":MUser.sharedInstance.pushToken,
            "type":"1",
            "model":MUser.sharedInstance.modelName,
            "system":"iOS",
            "version":MUser.sharedInstance.systemVersion,
            "app_version":app_version
        ]
        MAPI.SharedInstance.jsonRequest(.REGISTER_USER_DEVICE, parameters: parameters) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    print("success at registering device data")
                }else{
                    let message = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Error al registrar dispositivo.", message: message, preferredStyle: .Alert)
                    let cancel = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(cancel)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }else{
                let message = json?["data"].string ?? "Error desconocido"
                let alert = UIAlertController(title: "Error al registrar dispositivo.", message: message, preferredStyle: .Alert)
                let cancel = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(cancel)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
        }
    }
    
    
    // USER DATA LOAD
    
    func getFavorites(){
        api.jsonRequest(.PRODUCT_FAVORITE_INFO, parameters: nil, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: nil)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    if let branchesFavorites = json?["data"]["branches"].array {
                        MUser.sharedInstance.branchesFavorites = []
                        
                        for branch in branchesFavorites {
                            let newBranch = MRestaurant(data: branch["branch"])
                            let newFranchise = MFranchise(json: branch["branch"]["franchise"])
                            newBranch.franchise = newFranchise
                            MUser.sharedInstance.branchesFavorites.append(newBranch)
                        }
                    }
                    if let productFavorites = json?["data"]["products"].array {
                        MUser.sharedInstance.productFavorites = []
                        for product in productFavorites {
                            let newProduct = MProduct(json: product["product"])
                            for att in product["product"]["attachments"].array! {
                                let newAtt = MAttachment(json: att)
                                newProduct.attachments = [newAtt]
                            }
                            let newBranch = MRestaurant(data: product["branch"])
                            let newFranchise = MFranchise(json: product["branch"]["franchise"])
                            newBranch.franchise = newFranchise
                            newProduct.branch_products = [newBranch]
                            MUser.sharedInstance.productFavorites.append(newProduct)
                        }
                    }
                }else{
                    //something went wrong
                }
            }else{
                print(error)
            }
        }
    }
    
    func loadHistory(){
        
        let parameters:[String : AnyObject] =   [
            "limit":10,
            "state": ["finished","delivered"]
        ]
        
        api.jsonRequest(.ORDER_HISTORY, parameters: parameters, cacheRequest: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                if let json = json {
                    if let success = json["success"].bool {
                        if success {
                            if let jsonOrders = json["data"]["orders"].array {
                                self.user.orderHistory = []
                                var tmpOrders:[MOrder] = []
                                
                                for order in jsonOrders {
                                    let newOrder:MOrder = MOrder(json: order)
                                    tmpOrders.append(newOrder)
                                }
                                self.user.orderHistory = tmpOrders
                            }
                            
                        }else{
                            
                        }
                    }
                    
                }
                
            }else{
                
            }
        }
        
        
    }
    
    
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passTxt: UITextField!
    
    
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    

    
    
    @IBAction func ingresarPressed(sender: UIButton) {
        if emailTxt.text != "" && passTxt.text != "" {
            self.view.endEditing(true)
            let spinner = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            spinner.mode = MBProgressHUDMode.Indeterminate
            
            
            MUser.sharedInstance.loginMonchis(emailTxt.text, password: passTxt.text, completion: { (success, error) in
                if success {
                    self.loginHandler()
                    self.registerPushToken()
                }else{
                    spinner.hideAnimated(true)
                    let message = error ?? "Error desconocido."
                    SCLAlertView().showError("Error al iniciar sesión.", subTitle: message)
                }
            })
            
            
            spinner.label.text = NSLocalizedString("Iniciando Sesión",comment: "Iniciando Sesion")
            spinner.label.font = UIFont.init(name: "OpenSans-Semibold", size: 14.0)!
//            var app_version:String = ""
//            if let version = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String {
//                app_version = version
//            }
//            let parameters = ["email":emailTxt.text!,"password":passTxt.text!, "device_token":MUser.sharedInstance.pushToken,
//                "type":"1",
//                "model":MUser.sharedInstance.modelName,
//                "system":"iOS",
//                "version":MUser.sharedInstance.systemVersion,
//                "app_version":app_version]
//            api.jsonRequest(.LOGIN_EMAIL, parameters: parameters, completion: { (json, error) -> Void in
//                if debugModeEnabled {
//                    print(#function,"called with parameters \(parameters)",json,error)
//                }
//                
//                if error == nil {
//                    
//                    let success = json?["success"].bool ?? false
//                    if let data = json?["data"] {
//                        if success {
//                            //we save the user data
//                            self.user.email = self.emailTxt.text ?? ""
//                            self.user.password = self.passTxt?.text ?? ""
//                            self.emailTxt.text = ""
//                            self.passTxt.text = ""
//                            self.user.unparseUserInfoFromApi(data)
//                            Answers.logLoginWithMethod("Monchis",
//                                success: true,
//                                customAttributes: ["Usuario":self.user.email])
//                            
//                            Mixpanel.mainInstance().track(event: "Login",
//                                properties:["Metodo": "Monchis"])
//                            
//                            Mixpanel.mainInstance().identify(distinctId: self.user.email)
//
//                            //set the monchis token
//                            let monchis_token = data["monchis_token"].string ?? ""
//                            self.api.token = monchis_token
//                            self.api.mgr.session.configuration.HTTPAdditionalHeaders = ["token": "\(monchis_token)"]
//                            print("Monchis token : \(monchis_token)")
//                            
//                            //redirect user?? show animation? show alert?
//                            self.user.authenticationMethod = .Monchis
//                            //set automatic login for user
//                            NSUserDefaults.standardUserDefaults().setBool(true, forKey: kAutomaticLogin)
//                            KeychainSwift().set("monchis", forKey: kMonchisUserAuthenticationMethod)
//                            let keychain = KeychainSwift()
//                            keychain.set(self.user.email, forKey: kMonchisUserEmail)
//                            keychain.set(self.user.password, forKey: kMonchisUserPassword)
//                            self.user.logged = true
//                            print("user email saved: ",keychain.get(kMonchisUserEmail))
//                            print("user password saved: ",keychain.get(kMonchisUserPassword))
//                            //we load the user addresses
//                            self.loginHandler()
//                             self.registerPushToken()
//                        }else{ // show error
//                            spinner.hideAnimated(true)
//                            let message = data.string ?? ""
//                            SCLAlertView().showError("Error al iniciar sesión.", subTitle: message)
//
//                        }
//                        
//                    }
//                    
//                }else{ // net error
//                    spinner.hideAnimated(true)
//                    SCLAlertView().showError("Error al iniciar sesión.", subTitle: error ?? "Ocurrio un error inesperado.")
//
//                }
//                
//                
//            })
        }

    }
    
}
