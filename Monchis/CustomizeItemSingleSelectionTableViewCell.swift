//
//  CustomizeItemSingleSelectionTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 10/16/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class CustomizeItemSingleSelectionTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var addonTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
