//
//  DireccionesSideMenuCollectionReusableView.swift
//  Monchis
//
//  Created by jrivarola on 7/7/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class DireccionesSideMenuCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var image: UIImageView!
}
