//
//  MainRegisterTableViewController.swift
//  Monchis
//
//  Created by Javier Rivarola on 5/10/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import KeychainSwift
import Crashlytics
import Mixpanel


protocol RegisterPopoverDelegate {
    func didSuccededRegistration()
    func didFailedRegistration()
}

class MainRegisterTableViewController: UITableViewController {

    @IBOutlet weak var monchisLogo: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        let bg = UIImageView(image: UIImage(named: "fondoComida"))
        bg.contentMode = .ScaleAspectFill
        self.tableView.backgroundView = bg
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView(frame: CGRectZero)

    }

    @IBOutlet weak var nombres: UITextField!
    @IBOutlet weak var apellidos: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var pass1: UITextField!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    let api = MAPI.SharedInstance
    let user = MUser.sharedInstance
    
    struct Segues {
        static let unwindToLogin = "unwindFromRegistrarseToLoginSegue"
        static let showMap = "showMapSegue"
        static let showMainView = "showMainViewFromRegister"
    }
    @IBAction func togglePasswordVisibility(sender: UIButton) {
        self.pass1.font = nil
        self.pass1.font = UIFont.systemFontOfSize(14)
        pass1.secureTextEntry = !pass1.secureTextEntry
    }
    @IBOutlet weak var registerBtn: UIButton!
    var datePicker = UIDatePicker()
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
     
    }

    
    func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.monchisLogo.transform = CGAffineTransformMakeScale(-1, 1)
        self.monchisLogo.alpha = 0
        UIView.animateWithDuration(1.0, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.3, options: [.CurveEaseIn,.CurveEaseOut], animations: {
            self.monchisLogo.alpha = 1
            self.monchisLogo.transform = CGAffineTransformIdentity
            
        }) { (fin) in
            
        }
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.view.endEditing(true)
    }
    
    var selectedTextField: UITextField?
    var dismissTapGesture: UITapGestureRecognizer!
    
    func dismissWaitScreen(animated: Bool = true){
        if let wind = UIApplication.sharedApplication().delegate?.window {
            MBProgressHUD.hideHUDForView(wind!, animated: animated)
        }
    }
    
    @IBAction func backToLoginPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion:nil)
    }
    
    @IBAction func registrarsePressed(sender: UIButton) {
        self.view.endEditing(true)
        
        if  pass1.text != "" && nombres.text != "" && apellidos.text != "" && email.text != ""{ //add animation
            self.view.endEditing(true)
            
            if let wind = UIApplication.sharedApplication().delegate?.window {

            let spinner = MBProgressHUD.showHUDAddedTo(wind!, animated: true)
            spinner.mode = MBProgressHUDMode.Indeterminate
            spinner.label.text = NSLocalizedString("Registrando",comment: "Registrando nuevo usuario")
            spinner.label.font = UIFont(name: "OpenSans-Semibold", size: 14.0)!
            }
            let parameters =    [  "first_name":nombres.text ?? "",
                                   "last_name":apellidos.text ?? "",
                                   "email": email.text ?? "",
                                   "password":pass1.text ?? "",
                                   "device_token":MUser.sharedInstance.pushToken,
                                   "type":"1",
                                   "model":MUser.sharedInstance.modelName,
                                   "system":"iOS",
                                   "version":MUser.sharedInstance.systemVersion
            ]
            
            api.jsonRequest(.UPDATE_USER, parameters: parameters) { json, error in
                if debugModeEnabled {
                    print(#function,"parameters: \(parameters)",json,error)
                }
                
                if error == nil {
                    
                    
                    let success = json?["success"].bool ?? false
                    if let data = json?["data"] {
                        if success {
                            self.user.unparseUserInfoFromApi(data)
                            if let monchis_token = data["monchis_token"].string {
                                if monchis_token != "" {
                                    self.api.token = monchis_token
                                    print("Monchis token : \(monchis_token)")
                                }
                            }
                            KeychainSwift().set(self.nombres.text ?? "", forKey: "userFirstName")
                            KeychainSwift().set(self.apellidos.text ?? "", forKey: "userLastName")
                            //login user after registration succesfull
                            self.user.authenticationMethod = .Monchis
                            self.user.password = self.pass1.text!
                            Answers.logSignUpWithMethod("Monchis",
                                                        success: true,
                                                        customAttributes: ["Email":self.user.email])

                            Mixpanel.mainInstance().identify(distinctId: self.user.email)
                            Mixpanel.mainInstance().track(event: "Registro",
                                                          properties:["Metodo": "Monchis"])
                            self.loadUserAdresses()
                        }else{ // show error
                            self.dismissWaitScreen()
                            
                            let message = data.string ?? ""
                            let titleLocalizable = NSLocalizedString("Error",comment: "error")
                            let alert = UIAlertController(title: titleLocalizable, message: message, preferredStyle: .Alert)
                            let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                            alert.addAction(ok)
                            self.presentViewController(alert, animated: true, completion: nil)
                            
                        }
                    }else{
                        print("bad response")
                        
                    }
                }else{ //network error
                    self.dismissWaitScreen()
                    
                    print("network error")
                    
                    let titleLocalizable = NSLocalizedString("Error",comment: "error")
                    let alert = UIAlertController(title: titleLocalizable, message: error, preferredStyle: .Alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                    alert.addAction(ok)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
            
        }else{ // passwords dont match
            let titleLocalizable = NSLocalizedString("Lo sentimos!",comment: "Lo sentimos!")
            let messageLocalizable = NSLocalizedString("Debes completar todos los datos.",comment: "Debes completar todos los datos.")
            let alert = UIAlertController(title: titleLocalizable, message: messageLocalizable, preferredStyle: .Alert)
            let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
            alert.addAction(ok)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    
    }
    
    
    func dismissRequest(){
        api.currentRequest?.cancel()
        dispatch_async(dispatch_get_main_queue()) {

            MBProgressHUD.hideHUDForView(self.view, animated: true)
        }
    }
    
    // MARK: UITextField Delegate
    
    func textFieldDidEndEditing(textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        selectedTextField = textField
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    var delegate: RegisterPopoverDelegate?
    
    func loadUserAdresses(){
        
                        //set automatic login for user
                        NSUserDefaults.standardUserDefaults().setBool(true, forKey: kAutomaticLogin)
                        self.user.logged = true
                        let keychain = KeychainSwift()
                        keychain.set(self.user.email, forKey: kMonchisUserEmail)
                        keychain.set(self.user.password, forKey: kMonchisUserPassword)
                        KeychainSwift().set("monchis", forKey: kMonchisUserAuthenticationMethod)
                        
                        self.user.authenticationMethod = .Monchis
                        self.registerPushToken()
                        self.dismissWaitScreen()
                        delegate?.didSuccededRegistration()

    }
    
    func registerPushToken(){
        
        let parameters = [  "device_token":MUser.sharedInstance.pushToken,
                            "type":"1",
                            "model":MUser.sharedInstance.modelName,
                            "system":"iOS",
                            "version":MUser.sharedInstance.systemVersion
        ]
        MAPI.SharedInstance.jsonRequest(.REGISTER_USER_DEVICE, parameters: parameters) { (json, error) -> Void in
            print(#function,"called with parameters \(parameters)",json,error)
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    
                }else{
                    let message = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Error al registrar dispositivo.", message: message, preferredStyle: .Alert)
                    let cancel = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(cancel)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }else{
                let message = json?["data"].string ?? "Error desconocido"
                let alert = UIAlertController(title: "Error al registrar dispositivo.", message: message, preferredStyle: .Alert)
                let cancel = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(cancel)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
        }
    }
    
    
    

}
