//
//  MBranch.swift
//  Monchis
//
//  Created by Javier Rivarola on 12/3/15.
//  Copyright © 2015 cibersons. All rights reserved.
//

import Foundation
class MBranch: NSObject {
    var street2: String?
    var city_id: NSNumber?
    var admin_email: String?
    var is_pos_enabled: Bool?
    var delivery_gps_tracked:Bool?
    var is_pickup_enabled: Bool?
    var lng: String?
    var geo_location: Bool?
    var delivery_delay: String?
    var name: String?
    var house_number:String?
    var id: NSNumber?
    var delivery_price: String?
    var enabled: Bool?
    var franchise_id:NSNumber?
    var lat:String?
    var order_minimum:String?
    var franchise:MFranchise?
    var promotion_available:NSNumber?
    var street1:String?
    var is_delivery_enabled:Bool?
}
func == (lhs: MBranch, rhs: MBranch) -> Bool {
    return lhs.name == rhs.name
}