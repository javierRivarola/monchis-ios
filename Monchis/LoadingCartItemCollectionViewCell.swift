//
//  LoadingCartItemCollectionViewCell.swift
//  Monchis
//
//  Created by Javier Rivarola on 8/1/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit

class LoadingCartItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var imgView: UIImageView!
}
