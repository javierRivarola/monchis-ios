//
//  NoCoverCollectionViewCell.swift
//  Monchis
//
//  Created by jrivarola on 6/1/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit

class NoCoverCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var gpsFollowImageView: UIImageView!
    @IBOutlet weak var gpsFollowLbl: UILabel!
    @IBOutlet weak var coolView: UIVisualEffectView!
    @IBOutlet weak var pickupLbl: UILabel!
    @IBOutlet weak var pickupIcon: UIImageView!
}
