//
//  DireccionesSideMenuCollectionViewCell.swift
//  Monchis
//
//  Created by jrivarola on 7/7/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class DireccionesSideMenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var image: UIImageView!
    var chosen: Bool = false
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titulo.text = ""
    }
}
