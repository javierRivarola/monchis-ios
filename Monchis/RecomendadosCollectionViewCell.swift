//
//  RecomendadosCollectionViewCell.swift
//  Monchis
//
//  Created by jrivarola on 9/3/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class RecomendadosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var branchNameLbl: UILabel!
    @IBOutlet weak var newImageView: UIImageView!
    @IBOutlet weak var newLbl: UILabel!
}
