//
//  SearchTableViewController.swift
//  Monchis
//
//  Created by jrivarola on 22/12/15.
//  Copyright © 2015 cibersons. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import Alamofire
import Haneke
import Crashlytics
import Mixpanel

protocol SearchResultSelectedDelegate {
    func didSelectSearchResult(product: MProduct?, branch: MRestaurant?)
}

class SearchTableViewController: UITableViewController,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource {
    
    var restaurants: [MRestaurant]! = []
    var products: [[MProduct]]! = []
    let api = MAPI.SharedInstance
    var treshold: Bool = false
    var loadingBeforeEmptyDataSet: Bool = true
    var spinner: UIActivityIndicatorView!
    var searchDelegate: SearchResultSelectedDelegate?
    var scopeIndex: Int = 0 {
        didSet {
            self.tableView.tableFooterView = UIView(frame: CGRectZero)
            self.tableView.reloadData()
        }
    }
    struct Cell {
        static let productCell = "productCellReuseID"
    }
    
    let limit = 40
    var productOffset: Int  {
        var tmp = 0
        for allPr in self.products {
            tmp += allPr.count
        }
        return tmp
    }
    var currentPageRows: Int = 0
    var totalProducts:Int = 0
    var totalBranches: Int = 0
    var searchingFromApi: Bool = false
    var lastProductSearched:String = ""
    var lastFranchiseSearched: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.emptyDataSetDelegate = self
        self.tableView.emptyDataSetSource = self
//        self.tableView.rowHeight = UITableViewAutomaticDimension
//        self.tableView.estimatedRowHeight = 60
    
        spinner = UIActivityIndicatorView()
        spinner.hidesWhenStopped = true
        spinner.tintColor = UIColor.redColor()
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if scopeIndex == 0 {
            return products.count 
        }else{
            return restaurants.count > 0 ? 1 : 0
        }
    }
    
   

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if scopeIndex == 0 {
            return products[section].count
        }else{
            return restaurants.count
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if scopeIndex == 0 {
            let franchiseName = products[section].first?.franchise?.name ?? ""
            return franchiseName
        }else{
            return nil
        }
    }
    
    func searchProductWithText(text: String) {
        
        if text != lastProductSearched {
            totalProducts = 0
            products = []
        }
       
        
        let parameters = ["name": text,"limit":limit,"offset":productOffset]
        lastProductSearched = text
        self.tableView.reloadData()
        spinner.startAnimating()
        loadingBeforeEmptyDataSet = true
        self.tableView.reloadEmptyDataSet()
        api.searchRequest?.cancel()
        Answers.logSearchWithQuery(text,
                                   customAttributes: ["Categoria":"Productos"])
        Mixpanel.mainInstance().track(event: "Usuario Busco", properties: ["Categoria":"Productos","Query": text])
        api.jsonSearchRequest(.SEARCH_PRODUCT, parameters: parameters as? [String : AnyObject]) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters \(parameters)",json,error)
            }
            if error == nil {
                if let success = json?["success"].bool {
                    if success {
                        self.totalProducts = json?["data"]["count"].int ?? 0
                        if let productsArray = json?["data"]["results"].array {
                            var tmpProducts: [MProduct] = []
                            for product in productsArray {
                                let newProduct = MProduct(json: product)

                                let newFranchise = MFranchise(json: product["franchise"])
                                newProduct.franchise = newFranchise
                                if let attachmentArray = product["attachments"].array {
                                    for att in attachmentArray {
                                        let newAtt = MAttachment(json: att)
                                        newProduct.attachments = [newAtt]
                                    }
                                }
                                
                                if let branchProducts = product["branch_products"].array {
                                    var tmpBranchProductsArray:[MRestaurant] = []
                                    for brProduct in branchProducts {
                                        let newBranchProduct = MRestaurant(data: brProduct["branch"])
                                        newBranchProduct.attachment = newFranchise.icon_attachment
                                        newBranchProduct.header_attachment = newFranchise.header_attachment
                                        tmpBranchProductsArray.append(newBranchProduct)
                                    }
                                    newProduct.branch_products = tmpBranchProductsArray
                                }
                                
                                tmpProducts.append(newProduct)
                            }
                            
                            var tmpFranchises: [MFranchise] = []
                            var franchiseAlreadyInside: Bool = false
                            //filter results
                            for product in tmpProducts {
                                let franchise = product.franchise
                                
                                for fr in tmpFranchises {
                                    if franchise.id == fr.id {
                                        franchiseAlreadyInside = true
                                    }
                                }
                                
                                if !franchiseAlreadyInside {
                                    tmpFranchises.append(franchise)
                                }
                                franchiseAlreadyInside = false

                            }
                            
                            for franchise in tmpFranchises {
                                let filter1 = tmpProducts.filter({ (prod) -> Bool in
                                    return prod.franchise.id == franchise.id
                                })
                                if filter1.count > 0 {
                                    if self.products.last?.first?.franchise.id == franchise.id {
                                        self.products[self.products.count - 1] = self.products[self.products.count - 1] + filter1
                                    }else{
                                    self.products.append(filter1)
                                    }
                                }
                            }
                            
                            self.spinner.stopAnimating()
                            self.loadingBeforeEmptyDataSet = false
                            self.tableView.tableFooterView = UIView(frame: CGRectZero)
                            self.tableView.reloadData()
                        }
                    }
                }
            }else{
                print(error)
            }
        }
    }
    
    
    func searchFranchisesWithText(text: String) {
        if text != lastFranchiseSearched {
            self.restaurants = []
            self.totalBranches = 0
        }
        let parameters:[String:AnyObject] = ["text": text,"limit":limit,"offset":self.restaurants.count,"group_by_franchise":true]
        lastFranchiseSearched = text
        self.tableView.reloadData()
        spinner.startAnimating()
        loadingBeforeEmptyDataSet = true
        self.tableView.reloadEmptyDataSet()
        api.searchRequest?.cancel()
        Answers.logSearchWithQuery(text,
                                   customAttributes: ["Categoria":"Locales"])
        Mixpanel.mainInstance().track(event: "Usuario Busco", properties: ["Categoria":"Locales","Query": text])

        api.jsonSearchRequest(.SEARCH_BRANCHES, parameters: parameters) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters \(parameters)",json,error)
            }
            if error == nil {
                if let success = json?["success"].bool {
                    if success {
                        if let branchesArray = json?["data"].array {
                            
                            self.restaurants = []
                            for branch in branchesArray {
                                let newBranch = MRestaurant(data: branch["branch"])
                                self.restaurants.append(newBranch)
                            }
                            self.spinner.stopAnimating()
                            self.loadingBeforeEmptyDataSet = false
                            self.tableView.tableFooterView = UIView(frame: CGRectZero)

                            self.tableView.reloadData()
                        }
                    }
                }
            }else{
                self.spinner.stopAnimating()
                self.loadingBeforeEmptyDataSet = false
                self.tableView.tableFooterView = UIView(frame: CGRectZero)
                
                self.tableView.reloadData()
                print(error)
                
            }
        }
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if scopeIndex == 0 {
            let product = products[indexPath.section][indexPath.row]
            searchDelegate?.didSelectSearchResult(product, branch: nil)

        }else{
            let branch = restaurants[indexPath.row]
            searchDelegate?.didSelectSearchResult(nil, branch: branch)

        }
    }
   
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if scopeIndex == 0 {
        let cell = self.tableView.dequeueReusableCellWithIdentifier(Cell.productCell, forIndexPath: indexPath) as! BusquedaTableViewCell
//            cell.imageView?.frame = CGRectMake(0, 0, 100, 100)

        if let imgURL = products[indexPath.section][indexPath.row].attachments.first?.imageComputedURL {
            cell.imgView?.hnk_setImageFromURL(imgURL, placeholder: PLACE_HOLDER_IMAGE)
        }
        cell.titleLbl?.text = products[indexPath.section][indexPath.row].name
//        cell.detailTextLabel?.text = "Desde \(products[indexPath.row].price?.floatValue.asLocaleCurrency ?? "Sin datos")"
            
            
            
            return cell


        }else{
            let cell = self.tableView.dequeueReusableCellWithIdentifier(Cell.productCell, forIndexPath: indexPath) as! BusquedaTableViewCell
            if let imgURL = restaurants[indexPath.row].franchise?.icon_attachment?.imageComputedURL {
              
                cell.imgView?.hnk_setImageFromURL(imgURL, placeholder: PLACE_HOLDER_IMAGE)
            }
            cell.titleLbl?.text = restaurants[indexPath.row].franchise?.name
           // cell.detailTextLabel?.text = "Dirección \(restaurants[indexPath.row].street1 ?? " ") \(restaurants[indexPath.row].street2 ?? " ") "
            return cell

        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 65.0
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: EmptyDataSetSource
    
    
    func customViewForEmptyDataSet(scrollView: UIScrollView!) -> UIView! {
        if !treshold {
            let noResults = UILabel(frame: CGRectMake(0,0,self.tableView.bounds.width,40))
            noResults.textAlignment = .Center
            noResults.text = "Minimo 3 caracteres..."
            noResults.textColor = .darkGrayColor()
            return noResults
        }
        
        if loadingBeforeEmptyDataSet {
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
            spinner.hidesWhenStopped = true
            spinner.tintColor = UIColor.darkGrayColor()
            spinner.startAnimating()
            return spinner

        }
       
        let noResults = UILabel(frame: CGRectMake(0,0,self.tableView.bounds.width,40))
        noResults.textAlignment = .Center
        noResults.text = "No hay resultados."
        noResults.textColor = .darkGrayColor()
        return noResults

    }
    
   
    
          
  
 
 
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return true
    }
    //
    //    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
    //
    //    }
    //
    //    func backgroundColorForEmptyDataSet(scrollView: UIScrollView!) -> UIColor! {
    //
    //    }
    
    
    // MARK: EmptyDataSetDelegate
    
    var footerView: UIView = UIView(frame: CGRectZero)
    
    func createFooterViewForLoading(){
        footerView.frame = CGRectMake(0, 0, self.view.bounds.width, 40)
        let activity = UIActivityIndicatorView()
        activity.hidesWhenStopped = true
        activity.color = UIColor.redColor()
        activity.center = footerView.center
        activity.tag = 10
        footerView.addSubview(activity)
    }
    
    
    override func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if scopeIndex == 0 {
        let endOfTable: Bool = (scrollView.contentOffset.y >= (CGFloat(productOffset * 65) - scrollView.frame.size.height))
        if productOffset < totalProducts && totalProducts > 0 {
            if endOfTable && !scrollView.dragging && !scrollView.decelerating {
                createFooterViewForLoading()
                self.searchProductWithText(lastProductSearched)
                self.tableView.tableFooterView = footerView
                
                let activity = footerView.viewWithTag(10) as! UIActivityIndicatorView
                activity.startAnimating()
            }
        }else{
            tableView.tableFooterView = UIView(frame: CGRectZero)
        }
        
        }else{
            let endOfTable: Bool = (scrollView.contentOffset.y >= (CGFloat(totalBranches * 65) - scrollView.frame.size.height))

            if self.restaurants.count < totalBranches && totalBranches > 0 {
                if endOfTable && !scrollView.dragging && !scrollView.decelerating {
                    createFooterViewForLoading()
                    self.searchFranchisesWithText(lastFranchiseSearched)
                    self.tableView.tableFooterView = footerView
                    
                    let activity = footerView.viewWithTag(10) as! UIActivityIndicatorView
                    activity.startAnimating()
                }
            }else{
                tableView.tableFooterView = UIView(frame: CGRectZero)
            }
        }
    }

    
    func verticalOffsetForEmptyDataSet(scrollView: UIScrollView!) -> CGFloat {
        return -100
    }

}
