//
//  RightMenuTotalTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 10/13/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class RightMenuTotalTableViewCell: UITableViewCell {

    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var subTotal: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

   
}
