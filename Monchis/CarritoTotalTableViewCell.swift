//
//  CarritoTotalTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 10/14/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

protocol CarritoTotalDelegate  {
    func changeAddrPressed(sender: UIButton)
    func finalizarPressed()
}

class CarritoTotalTableViewCell: UITableViewCell {

    @IBOutlet weak var endBtn: UIButton!
    @IBOutlet weak var total: UILabel!
    var delegate: CarritoTotalDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func finalizarPressed(sender: UIButton) {
        delegate?.finalizarPressed()
        
    }
    @IBAction func changeAddrPressed(sender: UIButton) {
        delegate?.changeAddrPressed(sender)
    }
}
