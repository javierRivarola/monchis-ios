//
//  InfoRestoViewController.swift
//  Monchis
//
//  Created by jrivarola on 10/5/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import Crashlytics

class InfoRestoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,RateFranchiseDelegate {

    // MARK: Properties
    var restaurant: MRestaurant!
    @IBOutlet weak var restoImageView: UIImageView!
    @IBOutlet weak var restoNameLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    let api = MAPI.SharedInstance
    @IBOutlet weak var branchFavoriteBtn: UIButton!
    @IBOutlet weak var shadowView: UIView!
    //constants
    
    struct ReuseID {
        static let InforHeaderCell = "infoRestoHeaderReuseID"
        static let InfoContentCell = "infoRestoContentReuseID"
        static let infoRestoRating = "infoRestoRating"
    }
    
    // MARK: LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shadowView.layer.shadowColor = UIColor.blackColor().CGColor
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shadowOffset = CGSizeZero
        shadowView.layer.shadowRadius = 10
        restoImageView.layer.cornerRadius = restoImageView.bounds.width/2
        restoImageView.clipsToBounds = true
        
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 200
        tableView.tableFooterView = UIView(frame: CGRectZero)
        
        restoNameLbl.text = restaurant?.name.uppercaseString
        // Do any additional setup after loading the view.
        let img = UIImage(named: "texto-monchis-header")
        let imgView = UIImageView(frame: CGRectMake(0, 0, 50, 25))
        imgView.image = img
        imgView.contentMode = .ScaleAspectFit
        self.navigationItem.titleView = imgView
        
        //blur the image
        if headerBlurOn {
            let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light)) as UIVisualEffectView
            visualEffectView.frame = restoImageView.bounds
            restoImageView.addSubview(visualEffectView)

        }
        
        //favorites
        
//        if let _ = MUser.sharedInstance.branchesFavorites.indexOf({$0.id == restaurant?.id }) {
//            self.branchFavoriteBtn.setImage(favorited, forState: .Normal)
//        }else{
//            self.branchFavoriteBtn.setImage(unFavorited, forState: .Normal)
//            
//        }
    }
    
    override func viewWillLayoutSubviews() {
        if self.navigationController != nil {
            if self.navigationController!.respondsToSelector(Selector("interactivePopGestureRecognizer")) {
                self.navigationController!.interactivePopGestureRecognizer!.enabled = false
            }
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if useContainerSizeForPhotos {

        self.restaurant?.franchise?.header_attachment?.width = restoImageView.frame.size.width
        self.restaurant?.franchise?.header_attachment?.height = restoImageView.frame.size.height
        }
        if let headerImgURL = self.restaurant?.franchise?.header_attachment?.imageComputedURL {
            restoImageView.hnk_setImageFromURL(headerImgURL)
        }
    }
   
    
    override func viewWillAppear(animated: Bool) {
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: UITableViewDataSource
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
        let cell = tableView.dequeueReusableCellWithIdentifier(ReuseID.InforHeaderCell, forIndexPath: indexPath) as! InfoRestoHeaderTableViewCell
            cell.restoDescriptionLbl.text = restaurant?.details
            if useContainerSizeForPhotos {
                self.restaurant?.franchise?.header_attachment?.width = 1920
                self.restaurant?.franchise?.header_attachment?.height = 1080
            }
            if let restoImgURL = self.restaurant?.franchise?.header_attachment?.imageComputedURL {
                cell.restoImageView?.hnk_setImageFromURL(restoImgURL)
            }
            cell.restoTituloDescriptionLbl.text = ""
            cell.restoDescriptionLbl.text = restaurant?.franchise?.franchise_info ?? "No hay descripción."
            cell.restoImageView.contentMode = .ScaleAspectFit
            return cell
        }
        
        if indexPath.section == 1{
            let cell = tableView.dequeueReusableCellWithIdentifier(ReuseID.InfoContentCell, forIndexPath: indexPath) as! InfoRestoContentTableViewCell
        
            cell.telLbl.text = "Telefono: \(restaurant?.franchise?.phone ?? "Sin datos")"
            cell.emailLbl.text = "Email: \(restaurant?.franchise?.email ?? "Sin datos")"
            cell.addrLbl.text = "Dirección: \(restaurant?.street1 ?? " ") \(restaurant?.street2 ?? " ") "
            cell.layoutIfNeeded()
            return cell
        }
        
        if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCellWithIdentifier(ReuseID.infoRestoRating, forIndexPath: indexPath) as! InfoRestoRatingTableViewCell
            cell.thinkAboutLbl.text = "¿Qué pensas de \(restaurant.name)?"
            cell.delegate = self
            return cell
        }
        
        return UITableViewCell()
        
    }
    @IBAction func favoriteBranchPressed(sender: UIButton) {
        if MUser.sharedInstance.authenticationMethod != .Unregistered {
        if MUser.sharedInstance.branchesFavorites.contains(restaurant!){
            branchFavoriteBtn.setImage(unFavorited, forState: .Normal)
            deleteFavorite()
        }else{
            branchFavoriteBtn.setImage(favorited, forState: .Normal)
            addFavorite(
            )
        }
        }
        
    }
    
    @IBAction func shareBranchPressed(sender: UIButton) {
        
            var array:[AnyObject] = []
            let text = "Mira este Local en Monchis! - " + self.restaurant.name
            let urlString = "https://admin.monchis.com.py/?q="
            let query = "franchiseid=\(self.restaurant.franchise!.id!)&type=branch"
            let hash = query.toBase64() ?? ""
            
            let link = NSURL(string: urlString + hash)!
            array.append(text)
            array.append(link)
            if let img = self.restoImageView.image {
                array.insert(img, atIndex: 0)
            }
            let activityViewController = UIActivityViewController(activityItems: array, applicationActivities: nil)
            activityViewController.setValue("Mira este Local en Monchis!", forKey: "subject")
            
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        activityViewController.completionWithItemsHandler = { activityType, success, items, error in
            if !success {
                print("cancelled")
                return
            }
            
            Answers.logShareWithMethod(activityType ?? "",
                                       contentName: self.restaurant.name,
                                       contentType: "Restaurante",
                                       contentId: "\(self.restaurant.id!)",
                                       customAttributes: nil)
            
        }
            self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    
    func addFavorite(){
        
        let parameters = ["branch_id":restaurant.id!]
        api.jsonRequest(.FAVORITE_PRODUCT, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    MUser.sharedInstance.branchesFavorites.append(self.restaurant!)
                }else{
                    self.branchFavoriteBtn.setImage(unFavorited, forState: .Normal)
                    let error = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Lo sentimos!", message: error, preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }else{
                self.branchFavoriteBtn.setImage(unFavorited, forState: .Normal)
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func deleteFavorite(){
        let parameters = ["branch_id":restaurant.id!]
        api.jsonRequest(.DELETE_PRODUCT_FAVORITE, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    /* handle favorite removal */
                    
                    if let index = MUser.sharedInstance.branchesFavorites.indexOf(self.restaurant!) {
                        MUser.sharedInstance.branchesFavorites.removeAtIndex(index)
                        self.branchFavoriteBtn.setImage(unFavorited, forState: .Normal)
                        
                    }
                    
                    
                }else{
                    let error = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Lo sentimos!", message: error, preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }else{
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func didPressRateFranchise(withValue value: Int)
    {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        let parameters: [String:AnyObject] = ["franchise_id":restaurant.franchise_id ?? 0,"value":value]
        api.jsonRequest(.RATE_FRANCHISE, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters \(parameters)",json,error)
            }
            
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            if error == nil {
                let alert = UIAlertController(title: NSLocalizedString("Listo!", comment: "Listo"), message: NSLocalizedString("Tu puntaje ha sido agregado!", comment: "Puntaje de franquicia agregado"), preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }else{
                print("error happened")
            }
        }

    }
    
    
    // MARK: UITableViewDelegate

}
