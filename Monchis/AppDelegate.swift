//
//  AppDelegate.swift
//  Monchis
//
//  Created by jrivarola on 6/19/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import Fabric
import Crashlytics
import Haneke
import Siren
import ReachabilitySwift
import SCLAlertView
import FacebookCore
import GooglePlaces
import Mixpanel
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var user = MUser.sharedInstance
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        GMSServices.provideAPIKey(googleMapsApiKey)
        GMSPlacesClient.provideAPIKey(googleMapsApiKey)

//        let dataStack = DATAStack(modelName: "Monchis")

        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
        self.window?.tintColor = UIColor.redColor()
        //Global customization
        let customFont = UIFont(name: "OpenSans-Semibold", size: 13.0)
        UINavigationBar.appearance().titleTextAttributes = [ NSFontAttributeName: customFont!]
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: customFont!], forState: UIControlState.Normal)

        NSTimeZone.setDefaultTimeZone(NSTimeZone(name: "America/Asuncion")!)
        
        // Siren is a singleton
        let siren = Siren.sharedInstance
        Siren.sharedInstance.forceLanguageLocalization = SirenLanguageType.Spanish
//        Siren.sharedInstance.countryCode = "py"
        // Optional: Defaults to .Option
        siren.alertType = .Force
        
        /*
         Replace .Immediately with .Daily or .Weekly to specify a maximum daily or weekly frequency for version
         checks.
         */
        siren.checkVersion(.Immediately)

        if let notiInfo = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? NSDictionary {
            print("noti info", notiInfo)
            
            if let info = notiInfo["aps"] as? Dictionary<String, AnyObject>
            {
                if let action = info["action"] as? String {
                    if action == "navigateToBranch" {
                        
                        //                            let order = MOrder(json: info["order"] as! SwiftyJSON.JSON)
                        let order = MOrder()
                        order.branch = MRestaurant()
                        order.branch.lat = "-25.323642842396712"
                        order.branch.lng = "-57.624165415763855"
                        globalNotificationOrder = order
                    }
                }
            }
            
           

        }
        Fabric.with([Crashlytics.self])
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        Mixpanel.initialize(token: "0879ca5e73ef77a4843f9dc7ca156e25")
        Mixpanel.mainInstance().loggingEnabled = true
        Mixpanel.mainInstance().track(event: "App Open")
        return true
    }
    
    
    
           
    func application(application: UIApplication,
        openURL url: NSURL,
        sourceApplication: String?,
        annotation: AnyObject) -> Bool {
            

                return ApplicationDelegate.shared.application(
                application,
                openURL: url,
                sourceApplication: sourceApplication,
                annotation: annotation)
    
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        Mixpanel.mainInstance().track(event: "App Close")


    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        
//        MUser.sharedInstance.refreshToken()
        Siren.sharedInstance.checkVersion(.Immediately)
        Mixpanel.mainInstance().track(event: "App Open")


    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        application.applicationIconBadgeNumber = 0
        Siren.sharedInstance.checkVersion(.Daily)

        
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        Mixpanel.mainInstance().track(event: "App Close")

        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "cibersons.Monchis" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] 
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Monchis", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        return nil
        //OLD
//        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
//        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Monchis.sqlite")
//        var error: NSError? = nil
//        var failureReason = "There was an error creating or loading the application's saved data."
//        do {
//            try coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
//        } catch var error1 as NSError {
//            error = error1
//            coordinator = nil
//            // Report any error we got.
//            var dict = [String: AnyObject]()
//            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
//            dict[NSLocalizedFailureReasonErrorKey] = failureReason
//            dict[NSUnderlyingErrorKey] = error
//            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
//            // Replace this with code to handle the error appropriately.
//            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//            NSLog("Unresolved error \(error), \(error!.userInfo)")
//            abort()
//        } catch {
//            fatalError()
//        }
//        
//        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    
    func application(application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        //send this device token to server
        let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
        var tokenString = ""
        
        for i in 0 ..< deviceToken.length {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        print("Monchis Push Token: \(tokenString)")
        Mixpanel.mainInstance().people.addPushDeviceToken(deviceToken)
        MUser.sharedInstance.pushToken = tokenString
        NSUserDefaults.standardUserDefaults().setValue(tokenString, forKey: kMonchisUserPushTokenKey)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    //Called if unable to register for APNS.
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        
        print(error)
        
    }
    
  
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        print("Recived: \(userInfo)")

        if let info = userInfo["aps"] as? Dictionary<String, AnyObject>
        {
            if let action = info["action"] as? String {
                if action == "update" {
                    if let key = info["data"] as? String {
                        MAPI.SharedInstance.cache.remove(key: key)
                        print("Cache key removed: \(key)")
                    }
                }
                if action == "updateImage" {
                    MAPI.SharedInstance.imagesCache.removeAll()
                    if let key = info["data"] as? String {
                        for url in imageCacheKeys {
                            if url.containsString(key) {
                                MAPI.SharedInstance.imagesCache.remove(key: url, formatName: cacheFormats[url] ?? "")
                                MAPI.SharedInstance.imagesCache.remove(key: url, formatName: mainviewImageCacheFormtas[url] ?? "")
                                print("cache key removed", key, "format ", cacheFormats[url] ?? "", mainviewImageCacheFormtas[url] ?? "")
                            }
                        }
                    }
                }
                if action == "orderStatusChanged" {
                    NSNotificationCenter.defaultCenter().postNotificationName("updateOrderStateNotification", object: nil)
                }
                if action == "navigateToBranch" {
                    monchisTabController?.selectedIndex = 4
                    let order = MOrder()
                    order.branch = MRestaurant()
                    order.branch.lat = "-25.323642842396712"
                    order.branch.lng = "-57.624165415763855"
                    
                    let delay = 0.5 * Double(NSEC_PER_SEC)
                    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                    dispatch_after(time, dispatch_get_main_queue()) {
                        globalOrderStatusController?.notificationOrder = order
                    }
                }

            }
        }
        completionHandler(.NewData)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        print("Recived: \(userInfo)")
//        Parsing userinfo:
                if let info = userInfo["aps"] as? Dictionary<String, AnyObject>
                {
                    if let action = info["action"] as? String {
                        if action == "update" {
                            if let key = info["data"] as? String {
                                MAPI.SharedInstance.cache.remove(key: key)
                                print("Cache key removed: \(key)")
                            }
                        }
                        if action == "orderStatusChanged" {
                                NSNotificationCenter.defaultCenter().postNotificationName("updateOrderStateNotification", object: nil)
                        }
                        if action == "navigateToBranch" {
                            monchisTabController?.selectedIndex = 4
                            let order = MOrder()
                            order.branch = MRestaurant()
                            order.branch.lat = "-25.323642842396712"
                            order.branch.lng = "-57.624165415763855"
                            let delay = 0.5 * Double(NSEC_PER_SEC)
                            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                            dispatch_after(time, dispatch_get_main_queue()) {
                                globalOrderStatusController?.notificationOrder = order
                            }
                        }
                    }
                }
    }
    
    
    
    func openAppFromViewController(viewController: UIViewController, withID id: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewControllerWithIdentifier(id)
        self.window!.rootViewController = initialViewController
    }

    
//    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
//        print("Recived: \(userInfo)")
//        //        Parsing userinfo:
//        if let info = userInfo["aps"] as? Dictionary<String, AnyObject>
//        {
//            if let action = info["action"] as? String {
//                if action == "update" {
//                    if let key = info["data"] as? String {
//                        MAPI.SharedInstance.cache.remove(key: key)
//                        print("Cache key removed: \(key)")
//                    }
//                }
//                if action == "updateImage" {
//                    MAPI.SharedInstance.imagesCache.removeAll()
//                    if let key = info["data"] as? String {
//                        for url in imageCacheKeys {
//                            if url.containsString(key) {
//                                MAPI.SharedInstance.imagesCache.remove(key: url, formatName: cacheFormats[url] ?? "")
//                                MAPI.SharedInstance.imagesCache.remove(key: url, formatName: mainviewImageCacheFormtas[url] ?? "")
//                                print("cache key removed", key, "format ", cacheFormats[url] ?? "", mainviewImageCacheFormtas[url] ?? "")
//                            }
//                        }
//                    }
//                }
//                if action == "orderStatusChanged" {
//                    NSNotificationCenter.defaultCenter().postNotificationName("updateOrderStateNotification", object: nil)
//                }
//            }
//        }
//        completionHandler(UIBackgroundFetchResult.NewData)
//    }
    
    func open(url: NSURL,
                options: [String : Any] = [:],
                completionHandler completion: ((Bool) -> Void)? = nil) {
        
    }
    
    func application(application: UIApplication, continueUserActivity userActivity: NSUserActivity, restorationHandler: ([AnyObject]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            let url = userActivity.webpageURL!
            universalLinkType = url.URLString
            NSNotificationCenter.defaultCenter().postNotificationName("checkUniversalLinks", object: nil)
        }
        return true
    }
    
    


    

}

