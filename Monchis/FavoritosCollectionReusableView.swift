//
//  FavoritosCollectionReusableView.swift
//  Monchis
//
//  Created by jrivarola on 8/13/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class FavoritosCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var imageView: UIImageView!
}
