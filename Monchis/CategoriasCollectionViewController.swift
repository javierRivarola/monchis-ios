//
//  CategoriasCollectionViewController.swift
//  Monchis
//
//  Created by jrivarola on 6/22/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit


class CategoriasCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    
    struct Constants {
    static let reuseIdentifier = "CategoriasCellReuseID"
    }
    
    
    let categorias = ["Pizzas","Hamburguesas", "Empanadas", "Oriental", "Sandwiches", "Restaurantes", "Natural Food", "Helados", "Bebidas"]
    let images = ["Pizzas":"pizzas","Hamburguesas":"hamburguesas", "Empanadas":"empanadas", "Oriental":"oriental", "Sandwiches":"sandwiches", "Restaurantes":"restaurantes", "Natural Food":"natural", "Helados":"helados", "Bebidas":"bebidas"]
    var firstLaunch: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
     
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "reusableHeaderID", forIndexPath: indexPath) as! CategoriasHeaderCollectionReusableView
        
        header.titulo.text = "Selecciona tu categoria favorita!"
        return header
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        /*     if segmentedControl.selectedSegmentIndex == 0 {
        
        
        return CGSizeMake(50*self.scaleCategorias, 50*self.scaleCategorias)
        }else{
        
        return CGSizeMake(50*self.scaleRestaurantes, 50*self.scaleRestaurantes)
        }
        
        */
        
        return CGSizeMake(collectionView.frame.size.width/2, 200)
        
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        return categorias.count
    }
    
   
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Constants.reuseIdentifier, forIndexPath: indexPath) as! CategoriasCollectionViewCell
        cell.titulo.text = categorias[indexPath.row]
        
        cell.imageView.image = UIImage(named: images[categorias[indexPath.row]]!)
//        if indexPath.row%2 == 0 {
//            cell.backgroundColor = UIColor(hexString: "#080808", alpha: 0.2)
//        }else{
//            cell.backgroundColor = UIColor(hexString: "#080808", alpha: 0.1)
//        }
        //animations
//        cell.alpha = 0
//
//        UIView.animateWithDuration(0.5, delay: 0,  options: .AllowUserInteraction | .CurveEaseIn, animations: {
//            cell.alpha = 1
//            }, completion: nil)
//        if firstLaunch {
//            
//            cell.transform = CGAffineTransformMakeScale(0.5, 0.5)
//            
//            
//            if (indexPath.row%2 == 0) {
//            cell.transform = CGAffineTransformMakeTranslation(200, 0)
//            }else{
//                cell.transform = CGAffineTransformMakeTranslation(-200, 0)
//
//            }
//            
//                     UIView.animateWithDuration(1.3, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.3 ,options: .CurveEaseInOut | .AllowUserInteraction, animations: {
//                cell.transform = CGAffineTransformMakeTranslation(0, 0)
//                cell.transform = CGAffineTransformMakeScale(1, 1)
//
//            
//                }, completion: nil)
//            
//            if indexPath.row  == categorias.count - 1 {
//                firstLaunch = false
//            }
//        }
    
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
