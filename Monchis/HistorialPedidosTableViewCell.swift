//
//  HistorialPedidosTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 7/3/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
protocol callRestaurantDelegate {
    func callPressed(sender: UIButton, order: MOrder)
    func seeOrderDetailsPressed(order: MOrder)
    func calificarPressedForOrder(order:MOrder)
    func seeMapPressed(order: MOrder)
}
import Crashlytics

class HistorialPedidosTableViewCell: UITableViewCell {
    
    @IBOutlet weak var branchImgView: UIImageView!
    @IBOutlet weak var branchNameLbl: UILabel!
    @IBOutlet weak var enCaminoView: UIView!
    @IBOutlet weak var recibidoView: UIView!
    @IBOutlet weak var preparandoView: UIView!
    @IBOutlet weak var countDownLbl: UILabel!
    @IBOutlet weak var callBtn: UIButton!
    var delegate:callRestaurantDelegate?
    @IBOutlet weak var enCaminoImgView: UIImageView!
    @IBOutlet weak var preparandoImgView: UIImageView!
    @IBOutlet weak var recibidoImgView: UIImageView!
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var headGreenView: UIView!
    var order: MOrder!
    
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var calificaPedidoBtn: UIButton!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var deliveryOrPickupLbl: UILabel!
    var timer: NSTimer!
    
    @IBOutlet weak var mapBtnHeightConstraint: NSLayoutConstraint!
    override func prepareForReuse() {
        super.prepareForReuse()
//        timer.invalidate()
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        calificaPedidoBtn.imageView?.contentMode = .ScaleAspectFit
        callBtn.titleLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
        callBtn.titleLabel?.numberOfLines = 0
        branchImgView.layer.cornerRadius = 45/2
        mapBtn?.imageView?.contentMode = .ScaleAspectFit
        mapBtn?.titleLabel?.lineBreakMode = .ByWordWrapping
        mapBtn?.titleLabel?.numberOfLines = 2
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(HistorialPedidosTableViewCell.update), userInfo: nil, repeats: true)
    

    }
    @IBOutlet weak var mapBtn: UIButton!
    @IBAction func mapPressed(sender: UIButton) {
        delegate?.seeMapPressed(order)
    }
    
    @IBAction func calificarPressed(sender: UIButton) {
        delegate?.calificarPressedForOrder(order)
    }
    func update() {
        if order.status == MOrder.Status.CANCELED {
            countDownLbl.text = "00:00"
            return
        }
        let confirmedAtDate = order.confirmedAtDate
        
        
        
        
        
        // here we set the current date
        let calendar = NSCalendar.currentCalendar()
        
        
        let now = NSDate()
        let components = calendar.components([.Hour , .Minute , .Month, .Year ,.Day], fromDate: confirmedAtDate)
        let hour = components.hour
        let minutes = components.minute
        let month = components.month
        let year = components.year
        let day = components.day
        
        
        // here we set the due date. When the timer is supposed to finish
        
        let userCalendar = NSCalendar.currentCalendar()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"

        
        let competitionDate = NSDateComponents()
        competitionDate.year = year
        competitionDate.month = month
        competitionDate.day = day


        competitionDate.hour = hour
        competitionDate.minute = minutes + Int(order.branch.delivery_delay)
        let competitionDay = userCalendar.dateFromComponents(competitionDate)!
        
        // Here we compare the two dates
        competitionDay.timeIntervalSinceDate(now)
        
        let dayCalendarUnit: NSCalendarUnit = [.Day, .Hour, .Minute,.Second]
        
        //here we change the seconds to hours,minutes and days
        let CompetitionDayDifference = userCalendar.components(
            dayCalendarUnit, fromDate: now, toDate: competitionDay,
            options: NSCalendarOptions.init(rawValue: 0))
        //finally, here we set the variable to our remaining time
        let hoursLeft = CompetitionDayDifference.hour
        let minutesLeft = CompetitionDayDifference.minute
        let secondsLeft = CompetitionDayDifference.second
        let finalCountDown = userCalendar.dateFromComponents(CompetitionDayDifference)!
                
        if hoursLeft >= 0 && minutesLeft >= 0 && secondsLeft > 0 {
            countDownLbl.textColor = UIColor.grayColor()
            countDownLbl.text = dateFormatter.stringFromDate(finalCountDown)
        }
        if hoursLeft == 0 && minutesLeft == 0 && secondsLeft == 0 {
            countDownLbl.textColor = UIColor.grayColor()
            countDownLbl.text = dateFormatter.stringFromDate(finalCountDown)
        }
        if hoursLeft <= 0 && minutesLeft <= 0 && secondsLeft < 0 {
            countDownLbl.textColor = UIColor.grayColor()
            
//            let absHLeft = abs(hoursLeft)*(abs(daysLeft) + 1)
//            let absMLeft = abs(minutesLeft)
//            let absSLeft = abs(secondsLeft)
//            var hourString: String!
//            var minuteString: String!
//            var secondString: String!
//            if absHLeft/10 < 1 {
//                hourString = "0\(absHLeft)"
//            }else{
//                hourString = "\(absHLeft)"
//            }
//        
//            if absMLeft/10 < 1 {
//                minuteString = "0\(absMLeft)"
//            }else{
//                minuteString = "\(absMLeft)"
//
//            }
//            if absSLeft/10 < 1 {
//                secondString = "0\(absSLeft)"
//            }else{
//                secondString = "\(absSLeft)"
//
//            }
            countDownLbl.text = "00:00"
        }

        
    }
    

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func callPressed(sender: UIButton) {
        delegate?.callPressed(sender, order: order)
    }
    @IBAction func seeDetailsPressed(sender: UIButton) {
        delegate?.seeOrderDetailsPressed(order)
    }
    
}
