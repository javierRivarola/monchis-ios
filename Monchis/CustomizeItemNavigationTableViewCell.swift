//
//  CustomizeItemNavigationTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 9/14/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

protocol AddToCartDelegate {
    func addToCart()
}

class CustomizeItemNavigationTableViewCell: UITableViewCell {

    @IBOutlet weak var addToCartBtn: UIButton!
    var delegate: AddToCartDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func addToCartPressed(sender: UIButton) {
        delegate?.addToCart()
    }
}
