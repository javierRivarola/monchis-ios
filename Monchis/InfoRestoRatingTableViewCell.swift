//
//  InfoRestoRatingTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 3/2/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol RateFranchiseDelegate {
    func didPressRateFranchise(withValue value: Int)
}
class InfoRestoRatingTableViewCell: UITableViewCell {
    let api = MAPI.SharedInstance
    var delegate : RateFranchiseDelegate?
    var restaurant: MRestaurant!
    @IBOutlet weak var thinkAboutLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var value = 1
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func didPressFavoriteBtn(sender: UIButton) {
        for  i in 1...5 {
            let button = self.viewWithTag(i) as! UIButton
            button.setImage(UIImage(named: "icono-favorito"), forState: .Normal)
        }

        let tag = sender.tag
        for i in 1...tag{
            let button = self.viewWithTag(i) as! UIButton
            button.setImage(UIImage(named: "icono-favorito-on"), forState: .Normal)
            value = i
        }
    }

    @IBAction func calificarPressed(sender: UIButton) {
      delegate?.didPressRateFranchise(withValue: value)
    }
    
    
}
