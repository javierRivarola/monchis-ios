//
//  DeliveryBranchSelectionViewController.swift
//  Monchis
//
//  Created by jrivarola on 25/1/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import PagingMenuController
class DeliveryBranchSelectionViewController: UIViewController, UIGestureRecognizerDelegate, PagingMenuControllerDelegate,SearchResultSelectedDelegate, RestaurantesCollectionViewControllerDelegate{
        
        // MARK: Properties
        var wantsDelivery: Bool?
        var selectedFranchise: MFranchise!
        var headerMonchisImage: UIImageView!
        let user = MUser.sharedInstance
        var searchController: UISearchController!
        var searchTableViewController:SearchTableViewController!
        var searchBar: UISearchBar!
        var controllersForLoading: [UIViewController] = []
        var selectedPageIndex: Int!
        var selectedRestaurant: MRestaurant!
        var restaurants: [MRestaurant] = []
        var filteredData: [[AnyObject]] = []
        var categorySelected: MCategory!
    var franchise: MFranchise!
        var selectedScope: Int = 0
        @IBOutlet weak var spinner: UIActivityIndicatorView!
        let api = MAPI.SharedInstance
        
    @IBOutlet weak var franchiseHeaderImageView: UIImageView!
    @IBOutlet weak var franchiseHeaderNameBtn: UIButton!
        @IBOutlet weak var shadowView: UIView!
        struct Segues {
            static let selectedRestaurantSegue = "selectedRestaurantSegue"
            static let embededBranchesSegue = "embededBranchesSegue"
            static let showRestoInfoSegue = "ShowRestoInfoSegue"
        }
        
        struct Constants {
            static let RestaurantesClosedCellReuseID = "RestaurantesClosedCellReuseID"
            static let reuseIdentifier = "RestaurantesCellReuseID"
            static let ReuseSearchCellID = "ReuseSearchCellID"
        }
        
        // MARK: LifeCycle
        
        override func viewDidLoad() {
            super.viewDidLoad()
            setupUI()
            registerForNotifications()
            
        }
        
        override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
            print("touch canceled")
        }
        
        override func viewWillLayoutSubviews() {
            super.viewWillLayoutSubviews()
            if let navContr = self.navigationController {
                if navContr.respondsToSelector(Selector("interactivePopGestureRecognizer")) {
                    navContr.interactivePopGestureRecognizer?.enabled = false
                }
                
            }
        }
        
        func registerForNotifications(){
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DeliveryBranchSelectionViewController.userDefaultAddressChanged), name: kUserDidChangeDefaultAddressNotification, object: nil)
        }
        
        deinit {
            NSNotificationCenter.defaultCenter().removeObserver(self)
        }
        
        
        override func viewDidAppear(animated: Bool) {
            super.viewDidAppear(animated)
            
        }
        
        
        func userDefaultAddressChanged(){
            //handle new default address
            
        }
        
        
        
        func setupUI(){
            
            shadowView.layer.shadowColor = UIColor.blackColor().CGColor
            shadowView.layer.shadowOpacity = 1
            shadowView.layer.shadowOffset = CGSizeZero
            shadowView.layer.shadowRadius = 10
            
            franchiseHeaderImageView.layer.cornerRadius = 45/2
            
            //SearchBar Controller
            searchTableViewController = storyboard?.instantiateViewControllerWithIdentifier("SearchTableViewController") as! SearchTableViewController
            searchController = UISearchController(searchResultsController: searchTableViewController)
            
            searchController.hidesNavigationBarDuringPresentation = false
            //searchController.searchBar.searchBarStyle = UISearchBarStyle.Minimal
            
            searchController.searchBar.sizeToFit()
            searchBar = searchController.searchBar
            searchBar.tintColor = UIColor.redColor()
            
            searchBar.translucent = false
            
            searchBar.delegate = self
            for subView in searchBar.subviews  {
                for subsubView in subView.subviews  {
                    if let textField = subsubView as? UITextField {
                        textField.attributedPlaceholder =  NSAttributedString(string:NSLocalizedString("Buscar..", comment: "String que indica el placeholder para buscar platos o restaurantes "),
                            attributes:[NSFontAttributeName: UIFont(name: "OpenSans-Semibold", size: textField.font!.pointSize)!])
                    }
                }
            }
            
            searchTableViewController.definesPresentationContext = true
            searchTableViewController.searchDelegate = self
            searchController.searchResultsUpdater = self
            searchController.dimsBackgroundDuringPresentation = true // important
            //self.navigationItem.titleView = searchBar
            searchController.delegate = self
            searchBar.scopeButtonTitles = [NSLocalizedString("Productos", comment: "titulo para scope de busqueda por Productos "),NSLocalizedString("Restaurantes", comment: "Scope para busqueda por Restaurantes ")]
            
            if let franchiseImgUrl = franchise.header_attachment?.imageComputedURL {
                franchiseHeaderImageView.hnk_setImageFromURL(franchiseImgUrl, placeholder: PLACE_HOLDER_IMAGE)
            }
            
            franchiseHeaderNameBtn.setTitle(franchise.name?.uppercaseString, forState: .Normal)
            
           
            
        }
        
    
        
    
        
        
        func createWaitScreen(){
                let spinner = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                spinner.mode = MBProgressHUDMode.Indeterminate
                spinner.label.text = NSLocalizedString("Buscando Restaurantes",comment: "Buscando restaurantes para categoria nueva")
                spinner.label.font = UIFont(name: "OpenSans-Semibold", size: 14.0)!
            
            
        }
        
        func dismissWaitScreen(){
            dispatch_async(dispatch_get_main_queue()) {
                MBProgressHUD.hideHUDForView(self.view, animated: true)
            }
        }
        
       
        
        // MARK: RestaurantesCollectionViewDelegate
    
    func didSelectRestaurant(restaurant: MRestaurant) {
        self.selectedRestaurant = restaurant
        self.performSegueWithIdentifier(Segues.selectedRestaurantSegue, sender: self)
    }

    
    
        
        
    var restaurantsCVC: RestaurantesCollectionViewController!
    
        // MARK: - Navigation
        
        // In a storyboard-based application, you will often want to do a little preparation before navigation
        
        override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
            if segue.identifier == Segues.selectedRestaurantSegue {
                let dvc = segue.destinationViewController as! RestaurantViewController
                selectedRestaurant.mode = 1
                dvc.restaurant = selectedRestaurant
            }
            
            if segue.identifier == Segues.embededBranchesSegue {
                restaurantsCVC = segue.destinationViewController as! RestaurantesCollectionViewController
                restaurantsCVC.franchiseSelected = franchise
                restaurantsCVC.category = categorySelected
                restaurantsCVC.collectionView?.delegate = self
                
            }
            if segue.identifier == Segues.showRestoInfoSegue {
                let dvc = segue.destinationViewController as! InfoRestoViewController
                dvc.restaurant = selectedRestaurant
            }
            
        }
        
        
    }
    
    extension DeliveryBranchSelectionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
        func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
            
              selectedRestaurant = restaurantsCVC.restaurants[indexPath.row]

            self.performSegueWithIdentifier(Segues.selectedRestaurantSegue, sender: self)
                    
        }
        
        func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
            //#warning Incomplete method implementation -- Return the number of sections
            return 1
        }
        
        func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            
            return CGSizeMake(collectionView.frame.size.width/2, 170)
            
        }
        
        
        
        
        func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            //#warning Incomplete method implementation -- Return the number of items in the section
            
            return categories[selectedPageIndex].restaurants.count
        }
        
        func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
            let currentRestaurant = categories[selectedPageIndex].restaurants[indexPath.row]
            
            if currentRestaurant.isClosed() {
                
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Constants.RestaurantesClosedCellReuseID, forIndexPath: indexPath) as! RestaurantesClosedCollectionViewCell
                cell.gpsFollowImageView.hidden = !currentRestaurant.delivery_gps_tracked!
                
                if let imgURL = currentRestaurant.attachment?.imageComputedURL {
                    cell.imageView.hnk_setImageFromURL(imgURL)
                }else{
                    cell.imageView?.hnk_setImage(UIImage(named: "placeholder-640x480")!, animated: true, success: nil)
                }
                return cell
            }
            
            // NOT CLOSED
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Constants.reuseIdentifier, forIndexPath: indexPath) as! RestaurantesCollectionViewCell
            
            
            cell.timeLabel.text = "\(currentRestaurant.delivery_delay)"
            if let imgURL = currentRestaurant.attachment?.imageComputedURL {
                cell.imageView.hnk_setImageFromURL(imgURL)
            }else{
                cell.imageView?.hnk_setImage(UIImage(named: "placeholder-640x480")!, animated: true, success: nil)
            }
            return cell
            
        }
        
        
    }
    
    extension DeliveryBranchSelectionViewController:  UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
        
        // MARK: UISearchBarDelegate
        
        func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
            
        }
        
        func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
            print(#function)
        }
        
        func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
            searchTableViewController.scopeIndex = selectedScope
        }
        
        // MARK: UISearchResultsUpdating
        func updateSearchResultsForSearchController(searchController: UISearchController) {
            if let text = searchController.searchBar.text {
                if text.characters.count >= 3 {
                    let delay = 0.5 * Double(NSEC_PER_SEC)
                    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                    dispatch_after(time, dispatch_get_main_queue()) {
                        if searchController.searchBar.selectedScopeButtonIndex == 0 {
                            self.searchTableViewController.searchProductWithText(text)
                        }else{
                            self.searchTableViewController.searchFranchisesWithText(text)
                        }
                    }
                }else{
                    self.searchTableViewController.loadingBeforeEmptyDataSet = true
                    self.searchTableViewController.products = []
                    self.searchTableViewController.restaurants = []
                    self.searchTableViewController.tableView.reloadData()
                }
            }
            print(#function)
        }
        
        // MARK: UISearchControllerDelegate
        
        func willPresentSearchController(searchController: UISearchController) {
            print(#function)
            
            
        }
        
        func searchBarCancelButtonClicked(searchBar: UISearchBar) {
            
        }
        
        
        
        func willDismissSearchController(searchController: UISearchController) {
            self.tabBarController!.setTabBarVisible(true, animated: true)
        }
        
        
        @IBAction func searchPressed(sender: UIButton) {
            
            if searchController.active {
                self.tabBarController!.setTabBarVisible(true, animated: true)
                self.searchController.dismissViewControllerAnimated(true, completion: nil)
            }else{
                self.tabBarController!.setTabBarVisible(false, animated: true)
                self.presentViewController(self.searchController, animated: true){
                    
                }
                
            }
        }
        
        
        // MARK: SearchResultSelectedDelegate
        func didSelectSearchResult(product: MProduct?, branch: MRestaurant?) {
            if product != nil {
                var controllers = self.navigationController?.viewControllers
                
                let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
                let categoriasMenuVC = self.storyboard?.instantiateViewControllerWithIdentifier("MenuCategoriaViewController") as! MenuCategoriaViewController
                let productoVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
                
                productoVC.product = product
                productoVC.restaurant = product?.branch_products.first
                productoVC.restaurant?.franchise = product?.franchise
                restaurantVC.restaurant = product?.branch_products.first
                categoriasMenuVC.restaurant = product?.branch_products.first
                
                //            controllers?.append(restaurantVC)
                //            controllers?.append(categoriasMenuVC)
                controllers?.append(productoVC)
                self.searchController.active = false
                self.navigationController?.setViewControllers(controllers!, animated: true)
            }else{
                var controllers = self.navigationController?.viewControllers
                let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
                
                restaurantVC.restaurant = branch
                controllers?.append(restaurantVC)
                self.searchController.active = false
                self.navigationController?.setViewControllers(controllers!, animated: true)
                
            }
            
        }
        
        
        
        
    }
    
    extension DeliveryBranchSelectionViewController: UITableViewDataSource, UITableViewDelegate {
        
        func numberOfSectionsInTableView(tableView: UITableView) -> Int {
            return filteredData.count
        }
        
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCellWithIdentifier(Constants.ReuseSearchCellID, forIndexPath: indexPath)
            
            return cell
        }
        
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return filteredData[section].count
        }
        
}

