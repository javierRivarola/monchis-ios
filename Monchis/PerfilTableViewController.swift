//
//  PerfilTableViewController.swift
//  Monchis
//
//  Created by jrivarola on 18/12/15.
//  Copyright © 2015 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import Mixpanel
import SCLAlertView
import PhoneNumberKit


class PerfilTableViewController: UITableViewController , UITextFieldDelegate, SSRadioButtonControllerDelegate{

    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var rucTxt: UITextField!
    @IBOutlet weak var connectedAsTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!
    @IBOutlet weak var birthDateTxt: UITextField!
    @IBOutlet weak var mailTxt: UITextField!
    @IBOutlet weak var razonSocialTxt: UITextField!
    let datePicker: UIDatePicker = UIDatePicker()
    
    @IBOutlet weak var coolPhoneTxt: PhoneNumberTextField!
    @IBOutlet weak var femaleBtn: SSRadioButton!
    @IBOutlet weak var maleBtn: SSRadioButton!
    @IBOutlet weak var documentNumber: UITextField!
    let user = MUser.sharedInstance
    let api = MAPI.SharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    var radioButtonController: SSRadioButtonsController = SSRadioButtonsController()
    
    func setupUI(){
        let bg = UIImageView(image: UIImage(named: "background-completoChico"))
        bg.contentMode = .ScaleAspectFill
        bg.clipsToBounds = true
        self.tableView.backgroundView = bg
        saveBtn.layer.cornerRadius = 3
        saveBtn.clipsToBounds = true
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        datePicker.datePickerMode = .Date
        datePicker.locale = user.locale
        datePicker.addTarget(self, action: #selector(PerfilTableViewController.datePickerValueChanged(_:)), forControlEvents: .ValueChanged)
        birthDateTxt.inputView = datePicker
        passwordTxt.delegate = self
        radioButtonController.addButton(maleBtn)
        radioButtonController.addButton(femaleBtn)
        radioButtonController.delegate = self
        populateFieldsWithUserData()

        
    }
    
    func didSelectButton(aButton: UIButton?) {
        if let button = aButton {
            if button == maleBtn {
                user.gender = "M"
            }else{
                user.gender = "F"
            }
        }else{ //deselected
            user.gender = ""
        }
    }
    
    func populateFieldsWithUserData(){
        connectedAsTxt.text = user.first_name
        lastNameTxt.text =  user.last_name
        passwordTxt.text = user.password
        mailTxt.text = user.email
        rucTxt.text = user.invoice_ruc
        documentNumber.text = user.document_number
        coolPhoneTxt.text = user.phone
        razonSocialTxt.text = user.invoice_name
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        birthDateTxt.text = user.birth_date
        if user.gender == "M" {
            maleBtn.selected = true
        }
        if user.gender == "F" {
            femaleBtn.selected = true
        }

    }
    
    func createWaitScreen() {
        if let app = UIApplication.sharedApplication().delegate as? AppDelegate, let window = app.window {
            let spinner = MBProgressHUD.showHUDAddedTo(window, animated: true)
            spinner.mode = MBProgressHUDMode.Indeterminate
            spinner.label.text = NSLocalizedString("Actualizando Datos..",comment: "Actualizando datos del usuario")
            spinner.label.font = UIFont(name: "OpenSans-Semibold", size: 14.0)!
        }
    }
    
    func dismissWaitScreen(animated: Bool = true){
        if let app = UIApplication.sharedApplication().delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.hideHUDForView(window, animated: animated)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        birthDateTxt.text = dateFormatter.stringFromDate(sender.date)

    }

    @IBAction func saveChangesPressed(sender: UIButton) {
        let strippedPhone = coolPhoneTxt.text?.removeWhitespace() ?? ""
        if strippedPhone.characters.count <= 9 {
            SCLAlertView().showNotice("Atención", subTitle: "Debe introducir un número de teléfono valido Ej: 0981 123456")
            return
        }
        if connectedAsTxt.text != "" && lastNameTxt.text != "" && mailTxt.text != "" {
            updateUserData()
        }else{

            SCLAlertView().showError("Lo sentimos!", subTitle: "No puedes dejar campos en blanco.")
        }
            
    }
    
    func updateUserData(){
       
        self.view.endEditing(true)
        createWaitScreen()
        let parameters = ["first_name":connectedAsTxt.text ?? "","last_name":lastNameTxt.text ?? "","email":mailTxt.text ?? "","invoice_ruc":rucTxt.text ?? "","invoice_name":razonSocialTxt.text ?? "", "birth_date":birthDateTxt.text ?? "", "phone": coolPhoneTxt.text ?? "","document_number": documentNumber.text ?? "", "gender":user.gender ?? ""]
            api.jsonRequest(.UPDATE_USER, parameters: parameters) { (json, error) -> Void in
                if debugModeEnabled {
                    print(#function,"called with parameters\(parameters)",json,error)
                }
                self.dismissWaitScreen()
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    
                    self.user.first_name = self.connectedAsTxt.text ?? ""
                    self.user.last_name = self.lastNameTxt.text ?? ""
                    self.user.email = self.mailTxt.text ?? ""
                    self.user.birth_date = self.birthDateTxt.text ?? ""
                    self.user.invoice_name = self.razonSocialTxt.text ?? ""
                    self.user.invoice_ruc = self.rucTxt.text ?? ""
                    self.user.phone = self.coolPhoneTxt.text ?? ""
                    self.user.document_number = self.documentNumber.text ?? ""
                    Mixpanel.mainInstance().track(event: "Usuario Actualizo Datos de Perfil")
                    Mixpanel.mainInstance().people.set(property: "$first_name",
                                                       to: self.user.first_name)
                    Mixpanel.mainInstance().people.set(property: "$last_name",
                                                       to: self.user.last_name)
                    Mixpanel.mainInstance().people.set(property: "Sexo",
                                                       to: self.user.gender ?? "")
                    Mixpanel.mainInstance().people.set(property: "Fecha de Nacimiento" ,
                                                       to: self.user.birth_date!)
                    Mixpanel.mainInstance().people.set(property: "Razon Social",
                                                       to: self.user.invoice_name!)
                    Mixpanel.mainInstance().people.set(property: "RUC",
                                                       to: self.user.invoice_ruc!)
                    Mixpanel.mainInstance().people.set(property: "$phone",
                                                       to: self.user.phone!)
                    Mixpanel.mainInstance().people.set(property: "Numero de Cedula",
                                                       to: self.user.document_number)
                    self.populateFieldsWithUserData()
                    let alert = UIAlertController(title: "Datos actualizados correctamente", message: "", preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                                        alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }else{
                    let alert = UIAlertController(title: "Ha ocurrido un error!", message: json?["data"].string ?? "Error desconocido.", preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }else{
                let alert = UIAlertController(title: "Error de Red", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                let retry = UIAlertAction(title: "Reintentar", style: .Default, handler: { (action) -> Void in
                    self.updateUserData()
                })
                alert.addAction(aceptar)
                alert.addAction(retry)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func volverPressed(sender: UIBarButtonItem) {
        if let navController = self.navigationController {
            navController.dismissViewControllerAnimated(true, completion: nil)
        }else{
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if user.authenticationMethod == .Unregistered {
        }else{
            showChangePasswordAlert()
        }
        return false
    }
    
    func showChangePasswordAlert(){
        let alert = UIAlertController(title: NSLocalizedString("Cambiar contraseña", comment: "Cambiar contraseña"), message:  NSLocalizedString("(Minimo 6 caracteres y al menos uno numerico)", comment: "(Minimo 6 caracteres y al menos uno numerico)"), preferredStyle: .Alert)
        
       
        
        alert.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = NSLocalizedString("Contraseña nueva", comment: "Contraseña nueva")
        }
        
        let cancel = UIAlertAction(title: NSLocalizedString("Cancelar", comment: "Cancelar"), style: .Cancel, handler: nil)
        
        
        let change = UIAlertAction(title: NSLocalizedString("Cambiar", comment: "Cambiar"), style: .Default) { (action) -> Void in
            let new = alert.textFields![0].text
            self.changePassword(new!)
        }
        
        alert.addAction(cancel)
        alert.addAction(change)
        self.presentViewController(alert, animated: true, completion: nil)
    }

    func changePassword(new: String){
        
        self.view.endEditing(true)
        createWaitScreen()
        let parameters = ["password":new]
        api.jsonRequest(.UPDATE_USER, parameters: parameters) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters\(parameters)",json,error)
            }
            self.dismissWaitScreen()
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {

                    self.user.password = new
                    self.passwordTxt.text = new
                    let alert = UIAlertController(title: "Datos actualizados correctamente", message: "", preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json?["data"].string ?? "Error desconocido.", preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }else{
                let alert = UIAlertController(title: "Error de Red", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }

    }
    
}
