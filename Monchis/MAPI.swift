//
//  MAPI.swift
//  Monchis
//
//  Created by jrivarola on 7/30/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
import Alamofire
import Haneke
import SwiftyJSON
import ReachabilitySwift
import TaskQueue
import SCLAlertView

class MAPI {
    
    // Properties
    
    //constants
    
    var requestsQueue = TaskQueue()
    
    //notifications
    let kMAPIResponseSuccesfulNotification = "kMAPIResponseSuccesfulNotification"
    
    //Web Service URL
    
    var newURL: URLStringConvertible {
        return "https://monchisapi.unnaki.net"
    }
    //
    
//    let kMAPIClientsURL:URLStringConvertible = "http://api2.monchis.com.py"
    var kMAPIClientsURL:URLStringConvertible {
        if MUser.sharedInstance.debugMode {
//            return "https://api2monchis.unnaki.net"
            return "http://159.203.252.28"
        }
        return "https://api2.monchis.com.py"
    }
    
    var cacheEnabled: Bool = false
    
    static let SharedInstance = MAPI()
    
    var mgr: Manager!
    var cookies = NSHTTPCookieStorage.sharedHTTPCookieStorage()
    
    var token: String = "" {
        didSet {
            mgr = cfgrManager()
        }
    }
    
    var currentRequest: Request!
    let cache = Cache<SwiftyJSON.JSON>(name: "cacheJSON")
    let imagesCache = Shared.imageCache
    let cacheURIs:[Command] = [.SEARCH_BRANCHES,.LIST_BRANCHES,.LIST_FRANCHISES,.LIST_CATEGORIES,.PRODUCTS_CATEGORIES,.LIST_PRODUCTS,.GET_PRODUCT,.SHOW_PRODUCT,.ORDER_SHOW,.SEARCH_PRODUCT]
    
    //MAPI Commands
    enum Command: String {
        /* user URIs */
        case USER_INFO = "/user"
        case REGISTER_MAIL = "/user/register_email"
        case REGISTER_TWITTER = "/user/register_twitter"
        case REGISTER_FACEBOOK = "/user/register_facebook"
        case USER_RECOVER_PASSWORD = "/user/recover_password"
        // user login
        case LOGIN_EMAIL = "/user/login_email"
        // user updates
        case UPDATE_USER = "/user/update"
        case DELETE_USER = "/user/delete"
        
        /* REGISTER USER DEVICE
         params:
         device_token String El token de identificacion del dispositivo.
         type String Tipo de dispositivo, opciones (1:iOS, 2:android).
         model String Modelo del dispositivo (opcional).
         system String  Sistema Operativo (opcional).
         version String Version del Sistema Operativo (opcional).
         */
        case REGISTER_USER_DEVICE = "/user/device/register"
        // user addresses
        case USER_ADDRESSES = "/user/address"
        case USER_ADDRESSES_CREATE = "/user/address/create"
        case USER_ADDRESSES_UPDATE = "/user/address/update"
        case USER_ADDRESSES_DELETE = "/user/address/delete"
        
        /* Branches */
        /*
         params: time        String Hora en formato hh:mm (opcional).
         day         Integer Dia de la semana, 1 a 7(opcional).
         latitude    String  Latidud opcional
         longitude   String Longitud opcional
         
         */
        case SEARCH_BRANCHES = "/branch/search"
        
        // params: franchise_id Integer
        case LIST_BRANCHES = "/branch"
        
        // FRANCHISES
        case LIST_FRANCHISES = "/franchise"
        
        //params: category_id Integer
        case LIST_CATEGORIES =  "/franchise/category"
        
        //params: franchise_id  Int
        //        value         Int
        case RATE_FRANCHISE = "/franchise/rating/add"
        
        //params: franchise_id  Int
        case LIST_FRANCHISE_RATINGS = "/franchise/rating"
        
        //Busca la branch disponible 
        case SEARCH_NEAR_BRANCH = "/search_near_branch"
        
        // PRODUCTS
        
        //params: branch_id Integer
        case PRODUCTS_CATEGORIES = "/products/categories"
        
        /* params:  branch_id Integer Id del Branch (Opcional).
         category_id Integer Id de la Categoria (Opcional).
         */
        case LIST_PRODUCTS = "/product/category"
        
        //params: product_id Integer Id del Producto.
        case GET_PRODUCT = "/product"
        
        //params:
        case SHOW_PRODUCT = "/product/show"
        
        //params: product_id    Int
        //        branch_id     Int
        case FAVORITE_PRODUCT = "/product/favorite/add"
        
        //params: favorite_id
        case PRODUCT_FAVORITE_INFO = "/product/favorite"
        
        //params: favorite_id
        case DELETE_PRODUCT_FAVORITE = "/product/favorite/delete"
        
        //params: name
        case SEARCH_PRODUCT = "/product/search"
        
        //params: franchise_id int optional
        case GET_PROMOTIONS = "/product/promotions"
        
        case GET_RECOMENDATIONS = "/product/recommendations"
        
        /* CARRITO */
        
        /* params:  product_id  Integer Producto id.
         branch_id   Integer Branch id.
         addons      Array   listado de atributos del producto debe ir en formato arreglo
         [1,2,3,4]' con los id de los atributos.
         quantity    Integer Quantity cantidad de productos a agregar
         
         Agrega un producto en particular al carrito.
         */
        case ADD_TO_CART = "/cart/product/add"
        
        /* params:  order_id            Integer     Order id.
         order_product_id    Integer     Identificador del producto dentro de la orden.
         
         Elimina una orden en particular del carrito.
         */
        case DELETE_PRODUCT_CART = "/cart/product/delete"
        
        /* params:  product_id          Integer     product id.
         order_product_id    Integer     Identificador del producto dentro de la orden.
         quantity            Integer     cantidad de productos a agregar.
         
         */
        case EDIT_PRODUCT_QUANTITY_CART = "/cart/product/editQuantity"
        
        case EDIT_PRODUCT_CART = "/cart/product/edit"
        
        
        /* params:  product_id          Integer     product id.
         order_product_id    Integer     Identificador del producto dentro de la orden.
         quantity            Integer     cantidad de productos a agregar.
         
         Retorna los detalles de una orden en particular del carrito.
         */
        case ORDER_DETAILS_CART = "/cart/order"
        
        /* Elimina todas las ordenes del carrito. */
        case CLEAR_CART = "/cart/clear"
        
        /* params:  delivery_type          String     Tipo de delivery (opcional), valores posibles(1:delivery, 2:pickup, 3:delivery_monchis).
         address_id    String     Direccion id (opcional), la direccion del usuario donde se realizara el delivery. Se utiliza para las busqueda de productos localizada.
         deliver_date            String     Fecha de entrega (opcional), la fecha en formato 'Y-m-d H:m:s' en que se realizara el delivery.
         
         Guardar datos de la orden en el carrito. Al guardar nuevos datos en el carrito, todas las ordenes se elimininan.
         */
        
        case SAVE_CART = "/cart/save"
        
        case SHOW_CART = "/cart"
        
        case CONFIRM_CART = "/cart/confirm"
        
        // HISTORIAL
        
        case ORDER_HISTORY = "/order/history"
        
        // PAYMENT INFO
        
        case ORDER_PAYMENT = "/order/payment_info"
        
        // ORDER INFO
        
        case ORDER_SHOW = "/order/show"
        
        case DELETE_ORDER = "/order/state/cancel"
        
        // DELIVERY INFO
        
        case SET_DELIVERY_INFO = "/order/delivery_info"
        
        
        case REFRESH_TOKEN = "/user/refresh_token"
        
        /* VOUCHERS
         Valida un voucher y devuelve el producto correspondiente
         */
        case VALIDATE_VOUCHER = "/vouchers/validate"
        /*
         Agrega un voucher al carrito
         */
        case REDEEM_VOUCHER = "/vouchers/redeem"
        
        //
        case  GET_REASONS = "/order/feedback/reasons"
        //
        case SAVE_REASONS = "/order/feedback/save"
    }
    
    
    
    
    private func cfgrManager() -> Manager {
        let cfg = NSURLSessionConfiguration.defaultSessionConfiguration()
        cfg.HTTPCookieStorage = cookies
        cfg.HTTPAdditionalHeaders = ["Authorization":"bearer " + token]
        return Manager(configuration: cfg)
    }
    
    init (){
        mgr = cfgrManager()
    }
    
    var requestCounter: Int = 0
    
    
    let retryAttempts = 50
    
    
    func returnCachedData(code: Command,parameters: [String:AnyObject]?, completion: (json: SwiftyJSON.JSON?) -> Void) {
        let str = kMAPIClientsURL.URLString + code.rawValue
        let keyString = generateCacheKeyFrom(str,parameters: parameters)
        
        self.cache.fetch(key: keyString)
            .onSuccess { json in
                // Do something with data
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                print("Returned cache for key", keyString)
                completion(json: json)
            }
            .onFailure({ (error) -> () in
                completion(json: nil)
            })
        
    }
    var showCartRequest: Request!
    
    func networkPostRequest(code: Command,parameters: [String:AnyObject]?,completion: (json: SwiftyJSON.JSON?, error: String?) -> Void){
        let str = kMAPIClientsURL.URLString + code.rawValue
        let keyString = generateCacheKeyFrom(str,parameters: parameters)
        if code == .SHOW_CART {
//            self.showCartRequest?.cancel()
            self.showCartRequest = self.mgr.request(.POST, str, parameters: parameters, encoding: .JSON)
                
                
                .responseSwiftyJSONRenamed { request,response,json,error,errorCode in
                    
                    
                    
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    
                    if response?.statusCode >= 200 && response?.statusCode < 300 {
                        //response OK
                        if json != SwiftyJSON.JSON.null {
                            self.cache.remove(key: keyString)
                            self.cache.set(value: json, key: keyString)
                            completion(json: json, error: nil)
                        }else{
                            if debugModeEnabled {
                                completion(json: nil, error: "JSON NULL")
                            }else{
                                completion(json: nil, error: "Error desconocido")
                            }
                        }
                        if debugModeEnabled {
                            print("Cached uri: \(keyString)")
                        }
                    }else if response?.statusCode == 401 { //Unauthorized
                        if !debugModeEnabled {
                            completion(json: json, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                        }else{
                            completion(json: json, error: error.debugDescription)
                        }
                    }else if response?.statusCode == 400 { //Bad formed json
                        completion(json: json, error: nil)
                    }else{
                        //Not found 404:
                        //Internal server error 500:
                        //Forbidden  403:
                        print("ERROR CODE \(errorCode)")
                        if !debugModeEnabled {
                            completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                        }else{
                            if errorCode == -999 {
                                completion(json: nil, error: self.canceledMessage)
                            }
                            if errorCode == -1001 {
                                completion(json: nil, error: self.timeOutMessage)
                            }
                        }

                        if !debugModeEnabled {
                            completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                        }else{
                            completion(json: nil, error: error.debugDescription)
                        }
                    }
                    
                    
            }
            
            
        }else{
             self.mgr.request(.POST, str, parameters: parameters, encoding: .JSON)
                
                
                .responseSwiftyJSONRenamed { request,response,json,error, errorCode in
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    
                    if response?.statusCode >= 200 && response?.statusCode < 300 {
                        //response OK
                        if json != SwiftyJSON.JSON.null {
                            self.cache.remove(key: keyString)
                            self.cache.set(value: json, key: keyString)
                            completion(json: json, error: nil)
                        }else{
                            if debugModeEnabled {
                                completion(json: nil, error: "JSON NULL")
                            }else{
                                completion(json: nil, error: "Error desconocido")
                            }
                        }
                        if debugModeEnabled {
                            print("Cached uri: \(keyString)")
                        }
                    }else if response?.statusCode == 401 { //Unauthorized
                        if !debugModeEnabled {
                            completion(json: json, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                        }else{
                            completion(json: json, error: error.debugDescription)
                        }
                    }else if response?.statusCode == 400 { //Bad formed json
                        completion(json: json, error: nil)
                    }else{
                        //Not found 404:
                        //Internal server error 500:
                        //Forbidden  403:
                        print("ERROR CODE \(errorCode)")
                        if errorCode == -1001 {
                            completion(json: nil, error: self.timeOutMessage)
                        }
                        if !debugModeEnabled {
                            completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                        }else{
                            if errorCode == -999 {
                                completion(json: nil, error: self.canceledMessage)
                            }
                        }
                    }
                    
                    
            }
            
        }
        
    }
    
    let timeOutMessage = "Tiempo de espera agotado."
    var requestQueue: [TaskQueue] = []
    var needsToShowAlertForCacheBrowsing: Bool = true
    let canceledMessage = "User canceled."
    
    
    
    func postQueueRequest(code: Command,parameters: [String:AnyObject]?,completion: (json: SwiftyJSON.JSON?, error: String?) -> Void){
        
        if connectedToNetwork() {
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            let queue = TaskQueue()
            
            queue.tasks += {result, next in
                
                //network call
                self.networkPostRequest(code, parameters: parameters, completion: { (json, error) in
                    if error != nil {
                        print("error on postqueue \(error)")
                        if self.requestCounter + 1 > self.retryAttempts {
                            self.requestCounter = 0
                            next(nil)
                        } else {
                            if error != self.canceledMessage  {
                                print("Retrying request \(self.requestCounter) for error \(error))")
                                self.requestCounter += 1
                                queue.retry(1)
                            }
                        }
                        completion(json: nil, error: "error")
                    }else{
                        completion(json: json,error: nil)
                    }
                })
            }
            queue.run {result in
                print("====== completions ======")
                print("initial completion: run")
                
            }
            
        }else{
            // HANDLE NO CONNECTIVITY
            if needsToShowAlertForCacheBrowsing {
                SCLAlertView().showWarning("Mmmm...", subTitle: "Al parecer no tienes una conexión a internet, igual puedes navegar por Monchis, guardamos una version local de todo lo que ya visitaste hasta ahora.")
                needsToShowAlertForCacheBrowsing = false
            }
        }
    }
    
    
    var getOrdersRequest: Request?
    var searchRequest: Request?
    
    
    
    
    func jsonRequest(code: Command,parameters: [String:AnyObject]?,cacheRequest: Bool = true,returnCachedDataIfPossible: Bool = true,checkNetworkConnectivity: Bool = true, completion: (json: SwiftyJSON.JSON?, error: String?) -> Void){
        
        var newP: [String:AnyObject]? =  parameters//[:]
        if let userid = MUser.sharedInstance.user_id {
            newP?["user_id"] = userid
        }
        if checkNetworkConnectivity {
            if connectedToNetwork(){
                
                
                
                let str = kMAPIClientsURL.URLString + code.rawValue
                let keyString = generateCacheKeyFrom(str,parameters: parameters)
                
                if cacheRequest {
                    if cacheURIs.contains(code) {
                        if returnCachedDataIfPossible {
                            cache.fetch(key: keyString)
                                
                                .onSuccess { json in
                                    // Do something with data
                                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                    print("Returned cache for key", keyString)
                                    completion(json: json, error: nil)
                                }
                                
                                .onFailure({ (error) -> () in
                                    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                                    
                                    self.currentRequest = self.mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                                        
                                        .responseSwiftyJSONRenamed { request,response,json,error,errorCode in
                                            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                            if response?.statusCode >= 200 && response?.statusCode < 300 {
                                                //response OK
                                                self.cache.set(value: json, key: keyString)
                                                if debugModeEnabled {
                                                    print("Cached uri: \(keyString)")
                                                }
                                                completion(json: json, error: nil)
                                            }else if response?.statusCode == 401 { //Unauthorized
                                                if !debugModeEnabled {
                                                    completion(json: json, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                                }else{
                                                    completion(json: json, error: error.debugDescription)
                                                }
                                            }else if response?.statusCode == 400 { //Bad formed json
                                                completion(json: json, error: nil)
                                            }else{
                                                //Not found 404:
                                                //Internal server error 500:
                                                //Forbidden  403:
                                                if !debugModeEnabled {
                                                    completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                                }else{
                                                  
                                                    completion(json: nil, error: error.debugDescription)
                                                }
                                            }
                                            
                                    }
                                    
                                })
                            
                            
                        }else{ // dont return cached data
                            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                            
                            self.currentRequest = self.mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                                
                                .responseSwiftyJSONRenamed { request,response,json,error,errorCode in
                                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                    
                                    if response?.statusCode >= 200 && response?.statusCode < 300 {
                                        //response OK
                                        self.cache.set(value: json, key: keyString)
                                        if debugModeEnabled {
                                            print("Cached uri: \(keyString)")
                                        }
                                        completion(json: json, error: nil)
                                    }else if response?.statusCode == 401 { //Unauthorized
                                        if !debugModeEnabled {
                                            completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                        }else{
                                            completion(json: json, error: error.debugDescription)
                                        }
                                    }else if response?.statusCode == 400 { //Bad formed json
                                        completion(json: json, error: nil)
                                    }else{
                                        //Not found 404:
                                        //Internal server error 500:
                                        //Forbidden  403:
                                        print("ERROR CODE \(response?.statusCode)")
                                        if !debugModeEnabled {
                                            completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                        }else{
                                            completion(json: nil, error: error.debugDescription)
                                        }
                                    }
                                    
                            }
                        }
                        
                    }else{
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                        
                        self.currentRequest = self.mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                            
                            .responseSwiftyJSONRenamed { request,response,json,error,errorCode in
                                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                
                                if response?.statusCode >= 200 && response?.statusCode < 300 {
                                    //response OK
                                    completion(json: json, error: nil)
                                }else if response?.statusCode == 401 { //Unauthorized
                                    if !debugModeEnabled {
                                        completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                    }else{
                                        completion(json: json, error: error.debugDescription)
                                    }
                                }else if response?.statusCode == 400 { //Bad formed json
                                    completion(json: json, error: nil)
                                }else{
                                    //Not found 404:
                                    //Internal server error 500:
                                    //Forbidden  403:
                                    if !debugModeEnabled {
                                        completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                    }else{
                                        completion(json: nil, error: error.debugDescription)
                                    }
                                }
                                
                        }
                        
                    }
                    
                }else{
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                    
                    currentRequest = mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                        
                        .responseSwiftyJSONRenamed { request,response,json,error,errorCode in

                            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                            
                            if response?.statusCode >= 200 && response?.statusCode < 300 {
                                //response OK
                                completion(json: json, error: nil)
                            }else if response?.statusCode == 401 { //Unauthorized
                                if !debugModeEnabled {
                                    completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                }else{
                                    completion(json: json, error: error.debugDescription)
                                }
                            }else if response?.statusCode == 400 { //Bad formed json
                                completion(json: json, error: nil)
                            }else{
                                //Not found 404:
                                //Internal server error 500:
                                //Forbidden  403:
                                if !debugModeEnabled {
                                    completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                }else{
                                    completion(json: nil, error: error.debugDescription)
                                }
                            }
                            
                    }
                    
                }
            }else{ // no internet connection
                completion(json: nil, error: "No hay conexión a internet.")
                
            }
        }else{
            let str = kMAPIClientsURL.URLString + code.rawValue
            let keyString = generateCacheKeyFrom(str,parameters: parameters)
            
            
            
            
            if cacheRequest {
                if cacheURIs.contains(code) {
                    if returnCachedDataIfPossible {
                        cache.fetch(key: keyString)
                            
                            .onSuccess { json in
                                // Do something with data
                                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                
                                completion(json: json, error: nil)
                            }
                            
                            .onFailure({ (error) -> () in
                                UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                                
                                self.currentRequest = self.mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                                    
                                    .responseSwiftyJSONRenamed { request,response,json,error,errorCode in
                                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                        
                                        if response?.statusCode >= 200 && response?.statusCode < 300 {
                                            //response OK
                                            self.cache.set(value: json, key: keyString)
                                            if debugModeEnabled {
                                                print("Cached uri: \(keyString)")
                                            }
                                            completion(json: json, error: nil)
                                        }else if response?.statusCode == 401 { //Unauthorized
                                            if !debugModeEnabled {
                                                completion(json: json, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                            }else{
                                                completion(json: json, error: error.debugDescription)
                                            }
                                        }else if response?.statusCode == 400 { //Bad formed json
                                            completion(json: json, error: nil)
                                        }else{
                                            //Not found 404:
                                            //Internal server error 500:
                                            //Forbidden  403:
                                            if !debugModeEnabled {
                                                completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                            }else{
                                                completion(json: nil, error: error.debugDescription)
                                            }
                                        }
                                        
                                }
                                
                            })
                        
                        
                    }else{ // dont return cached data
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                        
                        self.currentRequest = self.mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                            
                            .responseSwiftyJSONRenamed { request,response,json,error,errorCode in
                                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                
                                if response?.statusCode >= 200 && response?.statusCode < 300 {
                                    //response OK
                                    self.cache.set(value: json, key: keyString)
                                    if debugModeEnabled {
                                        print("Cached uri: \(keyString)")
                                    }
                                    completion(json: json, error: nil)
                                }else if response?.statusCode == 401 { //Unauthorized
                                    if !debugModeEnabled {
                                        completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                    }else{
                                        completion(json: json, error: error.debugDescription)
                                    }
                                }else if response?.statusCode == 400 { //Bad formed json
                                    completion(json: json, error: nil)
                                }else{
                                    //Not found 404:
                                    //Internal server error 500:
                                    //Forbidden  403:
                                    if !debugModeEnabled {
                                        completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                    }else{
                                        completion(json: nil, error: error.debugDescription)
                                    }
                                }
                                
                        }
                    }
                    
                }else{
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                    
                    self.currentRequest = self.mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                        
                        .responseSwiftyJSONRenamed { request,response,json,error,errorCode in
                            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                            
                            if response?.statusCode >= 200 && response?.statusCode < 300 {
                                //response OK
                                completion(json: json, error: nil)
                            }else if response?.statusCode == 401 { //Unauthorized
                                if !debugModeEnabled {
                                    completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                }else{
                                    completion(json: json, error: error.debugDescription)
                                }
                            }else if response?.statusCode == 400 { //Bad formed json
                                completion(json: json, error: nil)
                            }else{
                                //Not found 404:
                                //Internal server error 500:
                                //Forbidden  403:
                                if !debugModeEnabled {
                                    completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                }else{
                                    completion(json: nil, error: error.debugDescription)
                                }
                            }
                            
                    }
                    
                }
                
            }else{
                UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                
                currentRequest = mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                    
                    .responseSwiftyJSONRenamed { request,response,json,error,errorCode in
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                        
                        if response?.statusCode >= 200 && response?.statusCode < 300 {
                            //response OK
                            completion(json: json, error: nil)
                        }else if response?.statusCode == 401 { //Unauthorized
                            if !debugModeEnabled {
                                completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                            }else{
                                completion(json: json, error: error.debugDescription)
                            }
                        }else if response?.statusCode == 400 { //Bad formed json
                            completion(json: json, error: nil)
                        }else{
                            //Not found 404:
                            //Internal server error 500:
                            //Forbidden  403:
                            if !debugModeEnabled {
                                completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                            }else{
                                completion(json: nil, error: error.debugDescription)
                            }
                        }
                        
                }
                
            }
            
        }
    }
    
    func generateCacheKeyFrom(url: String,parameters: [String:AnyObject]?)->String {
        var keyString = ""
        if let parameters = parameters {
            var parametersString:[String] = []
            for (key,_) in parameters {
                parametersString.append(key)
            }
            parametersString = parametersString.sort({ (key1, key2) -> Bool in
                return key1 < key2
            })
            for parameter in parametersString {
                var strTmp = ""
                if parameter != parametersString.last {
                    strTmp = parameter + "=" + "\(parameters[parameter]!)&"
                    keyString = keyString + strTmp
                }else{
                    strTmp = parameter + "=" + "\(parameters[parameter]!)"
                    keyString = keyString + strTmp
                    
                }
            }
        }
        var returnKey: String = ""
        if parameters?.count > 0 {
            returnKey = "1:" + url + "?" + keyString
            
        }else{
            returnKey = "1:" + url + keyString
            
        }
        return returnKey
    }
    
    //JSON SEARCH REQUEST
    
    func jsonSearchRequest(code: Command,parameters: [String:AnyObject]?,cacheRequest: Bool = true,returnCachedDataIfPossible: Bool = true,checkNetworkConnectivity: Bool = true, completion: (json: SwiftyJSON.JSON?, error: String?) -> Void){
        
        var newP: [String:AnyObject]? =  parameters//[:]
        if let userid = MUser.sharedInstance.user_id {
            newP?["user_id"] = userid
        }
        
        if checkNetworkConnectivity {
            if connectedToNetwork(){
                
                
                
                let str = kMAPIClientsURL.URLString + code.rawValue
                let keyString = generateCacheKeyFrom(str,parameters: parameters)
                
                if cacheRequest {
                    if cacheURIs.contains(code) {
                        if returnCachedDataIfPossible {
                            cache.fetch(key: keyString)
                                
                                .onSuccess { json in
                                    // Do something with data
                                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                    print("Returned cache for key", keyString)
                                    completion(json: json, error: nil)
                                }
                                
                                .onFailure({ (error) -> () in
                                    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                                    
                                    self.searchRequest = self.mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                                        
                                        .responseSwiftyJSONRenamed { request,response,json,error,errorCode in
                                            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                            
                                            if response?.statusCode >= 200 && response?.statusCode < 300 {
                                                //response OK
                                                self.cache.set(value: json, key: keyString)
                                                if debugModeEnabled {
                                                    print("Cached uri: \(keyString)")
                                                }
                                                completion(json: json, error: nil)
                                            }else if response?.statusCode == 401 { //Unauthorized
                                                if !debugModeEnabled {
                                                    completion(json: json, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                                }else{
                                                    completion(json: json, error: error.debugDescription)
                                                }
                                            }else if response?.statusCode == 400 { //Bad formed json
                                                completion(json: json, error: nil)
                                            }else{
                                                //Not found 404:
                                                //Internal server error 500:
                                                //Forbidden  403:
                                                if !debugModeEnabled {
                                                    completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                                }else{
                                                    
                                                    completion(json: nil, error: error.debugDescription)
                                                }
                                            }
                                            
                                    }
                                    
                                })
                            
                            
                        }else{ // dont return cached data
                            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                            
                            self.searchRequest = self.mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                                
                                .responseSwiftyJSONRenamed { request,response,json,error,errorCode in
                                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                    
                                    if response?.statusCode >= 200 && response?.statusCode < 300 {
                                        //response OK
                                        self.cache.set(value: json, key: keyString)
                                        if debugModeEnabled {
                                            print("Cached uri: \(keyString)")
                                        }
                                        completion(json: json, error: nil)
                                    }else if response?.statusCode == 401 { //Unauthorized
                                        if !debugModeEnabled {
                                            completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                        }else{
                                            completion(json: json, error: error.debugDescription)
                                        }
                                    }else if response?.statusCode == 400 { //Bad formed json
                                        completion(json: json, error: nil)
                                    }else{
                                        //Not found 404:
                                        //Internal server error 500:
                                        //Forbidden  403:
                                        if !debugModeEnabled {
                                            completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                        }else{
                                            completion(json: nil, error: error.debugDescription)
                                        }
                                    }
                                    
                            }
                        }
                        
                    }else{
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                        
                        self.searchRequest = self.mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                            
                            .responseSwiftyJSONRenamed { request,response,json,error,errorCode in
                                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                
                                if response?.statusCode >= 200 && response?.statusCode < 300 {
                                    //response OK
                                    completion(json: json, error: nil)
                                }else if response?.statusCode == 401 { //Unauthorized
                                    if !debugModeEnabled {
                                        completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                    }else{
                                        completion(json: json, error: error.debugDescription)
                                    }
                                }else if response?.statusCode == 400 { //Bad formed json
                                    completion(json: json, error: nil)
                                }else{
                                    //Not found 404:
                                    //Internal server error 500:
                                    //Forbidden  403:
                                    if !debugModeEnabled {
                                        completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                    }else{
                                        completion(json: nil, error: error.debugDescription)
                                    }
                                }
                                
                        }
                        
                    }
                    
                }else{
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                    
                    searchRequest = mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                        
                        .responseSwiftyJSONRenamed { request,response,json,error,errorCode in
                            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                            
                            if response?.statusCode >= 200 && response?.statusCode < 300 {
                                //response OK
                                completion(json: json, error: nil)
                            }else if response?.statusCode == 401 { //Unauthorized
                                if !debugModeEnabled {
                                    completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                }else{
                                    completion(json: json, error: error.debugDescription)
                                }
                            }else if response?.statusCode == 400 { //Bad formed json
                                completion(json: json, error: nil)
                            }else{
                                //Not found 404:
                                //Internal server error 500:
                                //Forbidden  403:
                                if !debugModeEnabled {
                                    completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                }else{
                                    completion(json: nil, error: error.debugDescription)
                                }
                            }
                            
                    }
                    
                }
            }else{ // no internet connection
                completion(json: nil, error: "No hay conexión a internet.")
                
            }
        }else{
            let str = kMAPIClientsURL.URLString + code.rawValue
            let keyString = generateCacheKeyFrom(str,parameters: parameters)
            
            
            
            
            if cacheRequest {
                if cacheURIs.contains(code) {
                    if returnCachedDataIfPossible {
                        cache.fetch(key: keyString)
                            
                            .onSuccess { json in
                                // Do something with data
                                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                
                                completion(json: json, error: nil)
                            }
                            
                            .onFailure({ (error) -> () in
                                UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                                
                                self.searchRequest = self.mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                                    
                                    .responseSwiftyJSONRenamed { request,response,json,error,errorCode in
                                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                        
                                        if response?.statusCode >= 200 && response?.statusCode < 300 {
                                            //response OK
                                            self.cache.set(value: json, key: keyString)
                                            if debugModeEnabled {
                                                print("Cached uri: \(keyString)")
                                            }
                                            completion(json: json, error: nil)
                                        }else if response?.statusCode == 401 { //Unauthorized
                                            if !debugModeEnabled {
                                                completion(json: json, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                            }else{
                                                completion(json: json, error: error.debugDescription)
                                            }
                                        }else if response?.statusCode == 400 { //Bad formed json
                                            completion(json: json, error: nil)
                                        }else{
                                            //Not found 404:
                                            //Internal server error 500:
                                            //Forbidden  403:
                                            if !debugModeEnabled {
                                                completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                            }else{
                                                completion(json: nil, error: error.debugDescription)
                                            }
                                        }
                                        
                                }
                                
                            })
                        
                        
                    }else{ // dont return cached data
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                        
                        self.searchRequest = self.mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                            
                            .responseSwiftyJSONRenamed { request,response,json,error, errorCode in
                                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                
                                if response?.statusCode >= 200 && response?.statusCode < 300 {
                                    //response OK
                                    self.cache.set(value: json, key: keyString)
                                    if debugModeEnabled {
                                        print("Cached uri: \(keyString)")
                                    }
                                    completion(json: json, error: nil)
                                }else if response?.statusCode == 401 { //Unauthorized
                                    if !debugModeEnabled {
                                        completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                    }else{
                                        completion(json: json, error: error.debugDescription)
                                    }
                                }else if response?.statusCode == 400 { //Bad formed json
                                    completion(json: json, error: nil)
                                }else{
                                    //Not found 404:
                                    //Internal server error 500:
                                    //Forbidden  403:
                                    if !debugModeEnabled {
                                        completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                    }else{
                                        completion(json: nil, error: error.debugDescription)
                                    }
                                }
                                
                        }
                    }
                    
                }else{
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                    
                    self.searchRequest = self.mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                        
                        .responseSwiftyJSONRenamed { request,response,json,error, errorCode in
                            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                            
                            if response?.statusCode >= 200 && response?.statusCode < 300 {
                                //response OK
                                completion(json: json, error: nil)
                            }else if response?.statusCode == 401 { //Unauthorized
                                if !debugModeEnabled {
                                    completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                }else{
                                    completion(json: json, error: error.debugDescription)
                                }
                            }else if response?.statusCode == 400 { //Bad formed json
                                completion(json: json, error: nil)
                            }else{
                                //Not found 404:
                                //Internal server error 500:
                                //Forbidden  403:
                                if !debugModeEnabled {
                                    completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                                }else{
                                    completion(json: nil, error: error.debugDescription)
                                }
                            }
                            
                    }
                    
                }
                
            }else{
                UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                
                searchRequest = mgr.request(.POST, str, parameters: newP, encoding: .JSON)
                    
                    .responseSwiftyJSONRenamed { request,response,json,error, errorCode in
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                        
                        if response?.statusCode >= 200 && response?.statusCode < 300 {
                            //response OK
                            completion(json: json, error: nil)
                        }else if response?.statusCode == 401 { //Unauthorized
                            if !debugModeEnabled {
                                completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                            }else{
                                completion(json: json, error: error.debugDescription)
                            }
                        }else if response?.statusCode == 400 { //Bad formed json
                            completion(json: json, error: nil)
                        }else{
                            //Not found 404:
                            //Internal server error 500:
                            //Forbidden  403:
                            if !debugModeEnabled {
                                completion(json: nil, error: NSLocalizedString("Ha ocurrido un error!", comment: "Ha ocurrido un error!"))
                            }else{
                                completion(json: nil, error: error.debugDescription)
                            }
                        }
                        
                }
                
            }
            
        }
    }

    
}

