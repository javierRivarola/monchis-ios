//
//  FillMissingDetailsTableViewController.swift
//  Monchis
//
//  Created by jrivarola on 25/2/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD

class FillMissingDetailsTableViewController: UITableViewController {

    let api = MAPI.SharedInstance
    let user = MUser.sharedInstance
    var order: MOrder!
    var cell: CarritoPayMethodTableViewCell!
    var parentController: CarritoViewController!
    @IBOutlet weak var rucTxt: UITextField!
    @IBOutlet weak var razontTxt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: CGRectZero)

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    @IBAction func actualizarPressed(sender: UIButton) {
        updateUserData()
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if (self.user.invoice_name == "" || self.user.invoice_name == nil) && indexPath.row == 1 {
            return UITableViewAutomaticDimension
        }
        
        if (self.user.invoice_ruc == "" || self.user.invoice_ruc == nil) && indexPath.row == 2 {
            return UITableViewAutomaticDimension
        }
        if indexPath.row == 3 || indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }
        return 0
    }
    
    func updateUserData(){
        
        self.view.endEditing(true)
        let hud = MBProgressHUD.showHUDAddedTo(UIApplication.sharedApplication().keyWindow!, animated: true)
        hud.label.text = NSLocalizedString("Actualizando datos..", comment: "Actualizando datos..")
        let parameters = ["invoice_ruc":rucTxt.text ?? "","invoice_name":razontTxt.text ?? ""]
        api.jsonRequest(.UPDATE_USER, parameters: parameters) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters\(parameters)",json,error)
            }
            hud.hideAnimated(true)
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    
                   
                    self.user.invoice_name = self.razontTxt.text ?? ""
                    self.user.invoice_ruc = self.rucTxt.text ?? ""
                    
                    
                    self.cell.wantsTicketBtn.tag = 1
                    self.cell.wantsTicketBtn.setImage(UIImage(named: CarritoViewController.Images.botonFacturaActivo), forState: .Normal)
                    self.order.wantsTicket = true
                    self.order.addInvoiceData()
                    self.parentController.setPaymentInfoForOrder(self.order, paymentMethod: nil)
                    
                    self.performSegueWithIdentifier("unwindFromMissingDetails", sender: self)
                    
                }else{
                    let alert = UIAlertController(title: "Ha ocurrido un error!", message: json?["data"].string ?? "Error desconocido.", preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }else{
                let alert = UIAlertController(title: "Error de Red", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                let retry = UIAlertAction(title: "Reintentar", style: .Default, handler: { (action) -> Void in
                    self.updateUserData()
                })
                alert.addAction(aceptar)
                alert.addAction(retry)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }

    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
