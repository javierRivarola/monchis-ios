//
//  RestaurantMenuTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 9/8/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class RestaurantMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var arrowImgView: UIImageView!
    @IBOutlet weak var gradientImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
