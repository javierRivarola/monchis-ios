//
//  RightSideMenuViewController.swift
//  Monchis
//
//  Created by jrivarola on 7/14/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import Crashlytics

class RightSideMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource {
    
    // MARK: Properties
    
    // Outlets
    @IBOutlet weak var leftConstraintTableView: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var orderTotalLbl: UILabel!
    @IBOutlet weak var histoTableView: UITableView!
    @IBOutlet weak var realizarPedidoBtn: UIButton!
    let cart = MCart.sharedInstance
    @IBOutlet weak var carritoLbl: UILabel!
    @IBOutlet weak var historyLbl: UILabel!
    let user = MUser.sharedInstance
    @IBOutlet weak var totalLbl: UILabel!
    // Constants
    struct ReuseCellID {
        static let orderPreview = "OrderPreviewReuseID"
        static let orderTotalPreview = "PreviewTotalReuseID"
        static let reOrder = "RightMenuReOrderCellID"
        static let orderHistory = "RightMenuReOrderCellID"
        static let subTotal = "SubtotalReuseID"
    }
    
    
    // MARK: LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        registerNotifications()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func registerNotifications(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RightSideMenuViewController.cartUpdated), name: kCartUpdatedNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RightSideMenuViewController.historyDidUpdate), name: kHistoryDidReload, object: nil)
    
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        self.tableView.setNeedsLayout()
//        self.tableView.layoutIfNeeded()
//        self.tableView.reloadData()
        showCartOptions()
        
    }
    func historyDidUpdate(){
        self.histoTableView.reloadData()
    }
    func cartUpdated(){
        showCartOptions()
    }
    
    func setupUI(){
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        histoTableView.rowHeight = UITableViewAutomaticDimension
        histoTableView.estimatedRowHeight = 50
        tableView.estimatedRowHeight = 50
        self.tableView.emptyDataSetDelegate = self
        self.tableView.emptyDataSetDelegate = self
        self.histoTableView.emptyDataSetDelegate = self
        self.histoTableView.emptyDataSetDelegate = self
        showCartOptions()
    }
    
    
    
    func showCartOptions(){
        if MCart.sharedInstance.orders.count > 0 {
            totalLbl.hidden = false
            realizarPedidoBtn.hidden = false
            orderTotalLbl.hidden = false
            orderTotalLbl.text = MCart.sharedInstance.totalComputedCart.asLocaleCurrency
        }else{
            totalLbl.hidden = true
            realizarPedidoBtn.hidden = true
            orderTotalLbl.hidden = true
        }
        
        carritoLbl.text = NSLocalizedString("TU CARRITO (\(MCart.sharedInstance.orders.count))", comment: "Titulo de carrito")
        historyLbl.text =  NSLocalizedString("ULTIMOS PEDIDOS (\(user.orderHistory.count))", comment: "Titulo de historial")
        self.tableView.reloadData()
        self.histoTableView.reloadData()
    }
    
    @IBAction func realizarPedidoPressed(sender: UIButton) {
        self.sideMenuViewController.hideMenuViewController()
        NSNotificationCenter.defaultCenter().postNotificationName(kUserWantsToFinalizeOrdersNotification, object: nil)
    }
    
    @IBAction func verHistorialCompletoPressed(sender: UIButton) {
        Answers.logCustomEventWithName("Usuario presiono Ver historial Desde menu Derecho",
                                       customAttributes:[:])
        self.sideMenuViewController.hideMenuViewController()
        NSNotificationCenter.defaultCenter().postNotificationName("showHistoryOrderTabNotification", object: nil)
    }
    
    var selectedOrder: MOrder!
    func gotoDetailsForOrder(sender: UIButton) {
        Answers.logCustomEventWithName("Usuario vio detalles de orden desde Menu Derecho",
                                       customAttributes:[:])
        let index = sender.tag
        self.sideMenuViewController.hideMenuViewController()
        
        NSNotificationCenter.defaultCenter().postNotificationName("scrollToOrderNotification", object: index)
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
        if segue.identifier == "gotoDetailsSegue" {
            let nav = segue.destinationViewController as! UINavigationController
            let dvc = nav.topViewController as! OrderDetailsViewController
            dvc.order = selectedOrder
            dvc.cameFromSideMenu = true
        }
    }
    
    
    // MARK: UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableView == self.tableView {
            return cart.orders.count > 0 ? cart.orders.count: 0
        }else{
            return user.orderHistory.count > 0 ? 1 : 0
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            if section < cart.orders.count {
                return cart.orders[section].products.count > 0 ? cart.orders[section].products.count : 0
            }else{
                return 1
            }
        }else{
            return user.orderHistory.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView == self.tableView {
            if indexPath.section < cart.orders.count  {

                let cartOrder = cart.orders[indexPath.section]
                if indexPath.row < cartOrder.products.count {
                    let cell = tableView.dequeueReusableCellWithIdentifier(ReuseCellID.orderPreview, forIndexPath: indexPath)
                    cell.textLabel?.text = cartOrder.products[indexPath.row].name
                  
                    cell.layoutIfNeeded()
                    cell.preservesSuperviewLayoutMargins = false
                    cell.separatorInset = UIEdgeInsetsZero
                    cell.layoutMargins = UIEdgeInsetsZero
                    return cell
                }
                
            }else{
                let cell = tableView.dequeueReusableCellWithIdentifier(ReuseCellID.subTotal, forIndexPath: indexPath) as! CartSideSubTotalTableViewCell
                cell.subTotal?.text = cart.totalComputed.asLocaleCurrency
                cell.layoutIfNeeded()
                return cell
            }
        }else{
            
                let cell = tableView.dequeueReusableCellWithIdentifier(ReuseCellID.orderHistory, forIndexPath: indexPath) as! RightMenuOrderAgainTableViewCell
                cell.branchNameLbl.text = user.orderHistory[indexPath.row].branch.franchise?.name
            
            let dateFormatter = NSDateFormatter()

            dateFormatter.locale = user.locale
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if let createdDate = user.orderHistory[indexPath.row].confirmed_at {
                if let date = dateFormatter.dateFromString(createdDate) {
                    dateFormatter.dateFormat = "d MMM yyyy HH:mm"
                    cell.orderDateLbl?.text = dateFormatter.stringFromDate(date)
                }
            }
     
            cell.verDetalleBtn?.addTarget(self, action: #selector(RightSideMenuViewController.gotoDetailsForOrder(_:)), forControlEvents: .TouchUpInside)
            cell.verDetalleBtn?.tag = indexPath.row
            cell.layoutIfNeeded()
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsetsZero
            cell.layoutMargins = UIEdgeInsetsZero
            return cell
            
        }
        return UITableViewCell()
    }
    
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as? UITableViewHeaderFooterView
        header?.textLabel?.textColor = UIColor.whiteColor()
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == self.tableView { // cart section
            if section < cart.orders.count {
                let view = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 55))
                view.backgroundColor = UIColor.whiteColor()
                //let franchiseName = cart.orders[section].first!.franchiseName
                let imgView = UIImageView(frame: CGRectMake(5, 5, 45, 45))
                imgView.layer.cornerRadius = 45/2
                imgView.contentMode = .ScaleAspectFill
                imgView.clipsToBounds = true
                imgView.image = NO_BRANCH_PLACEHOLDER
                view.addSubview(imgView)
                if headerBlurOn {
                    let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light)) as UIVisualEffectView
                    visualEffectView.frame = imgView.bounds
                    imgView.addSubview(visualEffectView)
                }
                
                if cart.orders[section].delivery_type == 1 {
                    if let urlString = cart.orders[section].branch?.franchise?.header_attachment?.imageComputedURL {
                        imgView.hnk_setImageFromURL(urlString,placeholder: NO_BRANCH_PLACEHOLDER)
                    }
                }else{
                    if let urlString = cart.orders[section].branch?.header_attachment?.imageComputedURL {
                        imgView.hnk_setImageFromURL(urlString,placeholder: NO_BRANCH_PLACEHOLDER)
                    }
                }
                
               
                
                let lbl = UILabel(frame: CGRectMake(60, 5,  tableView.frame.size.width - 60,45))
                if let branchName = cart.orders[section].branch?.franchise?.name {
                    lbl.text = branchName.uppercaseString
                }
                lbl.textColor = UIColor.grayColor()
                view.addSubview(lbl)
                lbl.font = UIFont(name: "OpenSans-Regular", size: 15.0) ?? UIFont.systemFontOfSize(15)
                view.layer.shadowColor = UIColor.blackColor().CGColor
                view.layer.shadowOpacity = 1
                view.layer.shadowOffset = CGSizeZero
                view.layer.shadowRadius = 1
                
                return view
            }else{
                return nil
            }
        }else{ // history section
            return nil
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.tableView {
            return 55
        }else{
            return CGFloat.min
        }
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    
  
    
    // MARK: EmptyDataSetSource
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
//        if tableView == (scrollView as? UITableView){

        let image = UIImage(named: "carrito-vacio")
        return image
//        }else{
//            let image = UIImage(named: "historial-vacio")
//            return image
//        }
    }
    
    func imageAnimationForEmptyDataSet(scrollView: UIScrollView!) -> CAAnimation! {
        
        let animation = CABasicAnimation(keyPath: "transform")
        
        animation.fromValue = NSValue(CATransform3D: CATransform3DIdentity)
        animation.toValue = NSValue(CATransform3D:CATransform3DMakeRotation(CGFloat(M_PI_2), 0.0, 0.0, 1.0))
        
        
        
        animation.duration = 0.25
        animation.cumulative = true
        animation.repeatCount = 1
        
        return animation
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
//        if tableView == (scrollView as? UITableView){
            let text = NSMutableAttributedString()
            let emptyCart = NSAttributedString(string: NSLocalizedString("Tu carrito está vacio.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
            
            text.appendAttributedString(emptyCart)
            return text
//        }else{ //history
//            let text = NSMutableAttributedString()
//            let emptyCart = NSAttributedString(string: NSLocalizedString("Tu historial está vacio.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
//            
//            text.appendAttributedString(emptyCart)
//            return text


//        }
       
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
//        if tableView == (scrollView as? UITableView){

        let subTitle = NSAttributedString(string: NSLocalizedString("Seria lindo empezar a llenarlo!", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(14),NSForegroundColorAttributeName:UIColor.whiteColor()])
        return subTitle
//        }else{
//            let subTitle = NSAttributedString(string: NSLocalizedString("Todos los pedidos que realices aparecen aqui.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(15),NSForegroundColorAttributeName:UIColor.whiteColor()])
//            return subTitle
//
//        }
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAnimateImageView(scrollView: UIScrollView!) -> Bool {
        return false
    }
}
