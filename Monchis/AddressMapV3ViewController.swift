//
//  AddressMapViewController.swift
//  Monchis
//
//  Created by Javier Rivarola on 3/21/17.
//  Copyright © 2017 cibersons. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation
import GoogleMaps
import AMPopTip
import SwiftyJSON
import SCLAlertView
import MBProgressHUD
import Alamofire
import DZNEmptyDataSet

class AddressMapV3ViewController: UIViewController, UISearchControllerDelegate, UISearchBarDelegate, AddressDetailsV3Delegate, UIPopoverPresentationControllerDelegate, UITableViewDataSource, UITableViewDelegate,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate, UISearchResultsUpdating  {
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    
    @IBOutlet weak var nextBtn: UIButton!
    var editingMode: Bool = false
    var cameFromCart: Bool = false
    @IBOutlet weak var mapView: GMSMapView!
    //Constants
    let locationManager = CLLocationManager()
    let dataProvider = GoogleDataProvider()
    let baseURLGeocode = "http://maps.google.com/maps/api/geocode/json?"
    let baseAutocompleteURL = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
    var gpsLocation:CLLocation!
    var address: MAddress = MAddress()
    let api = MAPI.SharedInstance
    var tip: AMPopTip!
    let user = MUser.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
    }
    
    struct Constants {
        static let ReuseSearchCellID = "ReuseSearchCellID"
    }
    
    var animatingPin: Bool = false
    func addPinAnimations(){

        animatingPin = true
        pulseAnimation(locationPin, radius: 50)
        UIView.animateWithDuration(0.65, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: [.BeginFromCurrentState, .CurveEaseIn, .Autoreverse,.Repeat], animations: {
            self.locationPin.transform = CGAffineTransformMakeTranslation(0, 2.5)
            }, completion: {fin in
                
        })
    }
    
    func removePinAnimations(){
        
      
        dispatch_async(dispatch_get_main_queue()) {
            self.locationPin.layer.sublayers?.removeAll()
            UIView.animateWithDuration(0.4, delay: 0, options: [.BeginFromCurrentState, .CurveEaseOut], animations: {
                self.locationPin.transform = CGAffineTransformMakeTranslation(0, -2.5)
                }, completion: {fin in
                    self.animatingPin = false
            })
        }
        
    }
    
    var shouldAddOffset: Bool = true
    
    
    
    @IBOutlet weak var locationPin: UIImageView!
    
    func backBtnPressed(){
        searchController?.active = false
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    var results: SearchResultsMapsTableViewController!

    func updateSearchResultsForSearchController(searchController: UISearchController) {
        // Always show the search result controller
        searchController.searchResultsController?.view.hidden = false
    }
    
    
//    func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
//        return UIBarPosition.Top
//    }
    
    @IBOutlet weak var searchContainer: UIView!
    @IBOutlet weak var pressHereToGPSBtn: UIButton!
    func setupUI(){

        nextBtn.hidden = true
        mapView.hidden = true
        locationPin.hidden = true
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.myLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        //        if editingMode {
        //            mapView.camera = GMSCameraPosition(target: address.computedCoordinate, zoom: 16, bearing: 0, viewingAngle: 0)
        //        }else{
        //            mapView.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: -25.33585, longitude: -57.505556), zoom: 13, bearing: 0, viewingAngle: 0)
        //
        //        }
        
        mapView.camera = GMSCameraPosition(target: address.computedCoordinate, zoom: 16, bearing: 0, viewingAngle: 0)
        
        
//        self.navigationController?.navigationBar.translucent = true
        
        // new searchbar
        
        //SearchBar Controller
        results = self.storyboard?.instantiateViewControllerWithIdentifier("SearchResultsMapsTableViewController") as! SearchResultsMapsTableViewController
        results.tableView.emptyDataSetSource = self
        results.tableView.emptyDataSetDelegate = self
        results.view.backgroundColor = UIColor.clearColor()
        results.tableView.backgroundColor = UIColor.clearColor()
        searchController = UISearchController(searchResultsController: results)
        searchController?.hidesNavigationBarDuringPresentation = false
        searchController?.searchResultsUpdater = self
        searchController?.searchBar.searchBarStyle = UISearchBarStyle.Minimal
        searchController?.searchBar.sizeToFit()
        results.tableView.reloadEmptyDataSet()
        let searchBar = searchController?.searchBar
        searchBar?.delegate = self

        searchBar?.placeholder = NSLocalizedString("Ej: Mariscal López casi San Martin", comment: "String que indica el placeholder para buscar direcciones en el mapa")
        searchBar?.tintColor = UIColor.redColor()
        self.extendedLayoutIncludesOpaqueBars = true
        results.tableView.dataSource = self
        results.tableView.delegate = self
        results.tableView.rowHeight = UITableViewAutomaticDimension
        results.tableView.estimatedRowHeight = 70
        results.tableView.tableFooterView = UIView(frame: CGRectZero)
        searchController?.dimsBackgroundDuringPresentation = false
        pressHereToGPSBtn.imageView?.contentMode = .ScaleAspectFit
        self.searchContainer.addSubview(searchBar!)

        //        self.navigationItem.titleView = searchBar
//        self.extendedLayoutIncludesOpaqueBars = true
        
        
        searchController?.delegate = self
        definesPresentationContext = true

                searchController?.searchBar.showsCancelButton = false

        
        
//        
//        resultsViewController = GMSAutocompleteResultsViewController()
//        let filter = GMSAutocompleteFilter()
//        filter.country = "PY"
//        resultsViewController?.autocompleteFilter = filter
//        resultsViewController?.delegate = self
//        
//        searchController = UISearchController(searchResultsController: resultsViewController)
//        searchController?.searchResultsUpdater = resultsViewController
//        searchController?.delegate = self
//        // Put the search bar in the navigation bar.
//        searchController?.searchBar.sizeToFit()
//        searchController?.searchBar.tintColor = UIColor.redColor()
//        navigationItem.titleView = searchController?.searchBar
        
        if self.user.addresses.count > 0 && !cameFromCart {
            let backBtn = UIBarButtonItem(title: "Volver", style: UIBarButtonItemStyle.Done, target: self, action: #selector(AddressMapV3ViewController.backBtnPressed))
            
            navigationItem.leftBarButtonItem = backBtn
        }
        
        
//        searchController?.searchBar.placeholder = "Ej: Mariscal Lopez"
//        searchController?.searchBar.delegate = self
//        // When UISearchController presents the results view, present it in
//        // this view controller, not one further up the chain.
//        definesPresentationContext = true
//        
//        // Prevent the navigation bar from being hidden when searching.
//        searchController?.hidesNavigationBarDuringPresentation = false
        if editingMode {
                addPinAnimations()
            pressHereToGPSBtn.hidden = true
                mapView.alpha = 0
                mapView.hidden = false
                nextBtn.alpha = 0
                nextBtn.hidden = false
                locationPin.alpha = 0
                locationPin.hidden = false
                self.nextBtn.transform = CGAffineTransformMakeTranslation(0, 200)
                UIView.animateWithDuration(0.4, animations: {
                    self.mapView.alpha = 1
                    self.locationPin.alpha = 1
                    self.nextBtn.alpha = 1
                    self.nextBtn.transform = CGAffineTransformIdentity
                    }, completion:  {fin in
                        let delay = 2.5 * Double(NSEC_PER_SEC)
                        self.showTip(7)
                        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                        dispatch_after(time, dispatch_get_main_queue()) {
                            let tip = AMPopTip()
                            tip.popoverColor = UIColor.blackColor()
                            tip.shouldDismissOnTap = true
                            tip.showText("Presiona aqui para llevar el Pin a tu posicion actual", direction: .Left, maxWidth: self.view.bounds.width*0.7, inView: self.view, fromFrame: CGRectMake(self.view.bounds.width - 70, self.view.bounds.height - self.nextBtn.bounds.height - 100, 50, 50))
                        }
                })
        }
    }
    
    
    var placesClient = GMSPlacesClient.sharedClient()
    var googleResults: [JSON] = []
    
    var googleMapsAddressRequest: Request?

    
    
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        searchBarTip?.hide()
        if searchText.characters.count > 0 {
            print("Searching for '\(searchText)'")
            geocodeAddress(searchText, completionHandler: { (success, data) in
                if success == false {
                    print("Autocomplete error for query '\(searchText)'")
                    self.googleResults = []
                    self.results.tableView.reloadData()
                    return
                }else{
                    self.googleResults = []
                    for result in data! {
                        if result["formatted_address"].string != "Paraguay" {
                            self.googleResults.append(result)
                        }
                    }
                    if self.googleResults.count > 0 {
                        self.shouldAddOffset = false
                    }
                    self.results.tableView.reloadData()
                }
                
            })
        }

    }
    var lookupAddressResults: JSON!

    func geocodeAddress(address: String!, completionHandler: ( success: Bool, data: [JSON]?) -> Void) {
        if let lookupAddress = address {
            var geocodeURLString = baseURLGeocode + "address=" + lookupAddress + "&components=country:PY"
            geocodeURLString = geocodeURLString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            
            let geocodeURL = NSURL(string: geocodeURLString)
            
            //
            //            request(.Get, geocodeURL).responseSwiftyJSON(queue: dispatch_get_main_queue()){ request, response, json, error in
            //
            //            }
            
            googleMapsAddressRequest = request(.GET, geocodeURL!, parameters: nil).responseSwiftyJSONRenamed(){ request, response, json, error,errorCode in
                dispatch_async(dispatch_get_main_queue()){
                    
                    if (error != nil) {
                        print(error)
                        completionHandler(success: false, data: nil)
                    }else{
                        let status = json["status"].string!
                        
                        if status == "OK" {
                            let allResults = json["results"].array!
                            completionHandler( success: true, data: allResults)
                        }
                        else {
                            completionHandler( success: false, data: nil)
                        }
                        
                    }
                    
                }
                
            }
        }
    }
    

    
    // MARK: SearchResults Data Source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return googleResults.count > 0 ? 1 : 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return googleResults.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.ReuseSearchCellID, forIndexPath: indexPath)
        if indexPath.row == googleResults.count {
            cell.imageView?.image = UIImage(named: "powered_by_google_on_white")
            cell.imageView?.contentMode = .ScaleAspectFit
            cell.textLabel?.text = ""
            return cell
        }
        cell.imageView?.image = nil
        cell.textLabel?.text = googleResults[indexPath.row]["formatted_address"].string ?? ""
        return cell
        
    }
    
    var selectedRow: Bool = false
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedRow = true
        self.searchController?.active = false
            addPinAnimations()
            mapView.alpha = 0
            mapView.hidden = false
            nextBtn.alpha = 0
            nextBtn.hidden = false
            locationPin.alpha = 0
            locationPin.hidden = false
            self.nextBtn.transform = CGAffineTransformMakeTranslation(0, 200)
            UIView.animateWithDuration(0.4, animations: {
                self.mapView.alpha = 1
                self.locationPin.alpha = 1
                self.nextBtn.alpha = 1
                self.nextBtn.transform = CGAffineTransformIdentity
                }, completion:  {fin in
                    let delay = 1 * Double(NSEC_PER_SEC)
                    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                    dispatch_after(time, dispatch_get_main_queue()) {
                        let tip = AMPopTip()
                        tip.popoverColor = UIColor.darkGrayColor()
                        tip.shouldDismissOnTap = true
                        tip.showText("Presiona aqui para llevar el Pin a tu posicion actual", direction: .Left, maxWidth: self.view.bounds.width*0.7, inView: self.view, fromFrame: CGRectMake(self.view.bounds.width - 70, self.view.bounds.height - self.nextBtn.bounds.height - 100, 50, 50),duration: 4)
                    }
            })
        showTip(7)
        let coordinateSelected = CLLocationCoordinate2D(latitude: googleResults[indexPath.row]["geometry"]["location"]["lat"].double ?? 0, longitude: googleResults[indexPath.row]["geometry"]["location"]["lng"].double ?? 0)
        self.address.coordinate = coordinateSelected
        self.mapView.animateToCameraPosition(GMSCameraPosition(target: coordinateSelected, zoom: 15.5, bearing: 0, viewingAngle: 0))
        self.searchController?.searchBar.text = googleResults[indexPath.row]["formatted_address"].string ?? ""
    }

    
    // MARK: DZNEmptyDataSet
    
//    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
//        let image = UIImage(named: "presionaAqui")
//        return image
//    }
    
    func verticalOffsetForEmptyDataSet(scrollView: UIScrollView!) -> CGFloat {
        if shouldAddOffset {
            return -170
        }else{
            return 0
        }
    }
    
    func customViewForEmptyDataSet(scrollView: UIScrollView!) -> UIView! {
        let imgView = UIImageView()
        imgView.image = UIImage(named: "presionaAqui")
        imgView.contentMode = .ScaleAspectFit
        imgView.frame = CGRectMake(0, 0, 200, 200)
        imgView.center = scrollView.center
        
        return imgView
    }
    
//    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
//        let mut = NSMutableAttributedString()
////        return mut
//        mut.appendAttributedString(NSAttributedString(string: "O PRESIONA AQUÍ\n", attributes: [NSFontAttributeName:UIFont.systemFontOfSize(14), NSForegroundColorAttributeName: UIColor.blackColor()]))
//        mut.appendAttributedString(NSAttributedString(string: "PARA RECIBIR TU PEDIDO DONDE ESTES", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14), NSForegroundColorAttributeName: UIColor.blackColor()]))
//        return mut
//    }
    
  
    
    
//    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
//        
//        let mut = NSMutableAttributedString()
//        mut.appendAttributedString(NSAttributedString(string: "O PRESIONA AQUÍ\n", attributes: [NSFontAttributeName:UIFont.systemFontOfSize(14), NSForegroundColorAttributeName: UIColor.blackColor()]))
//        mut.appendAttributedString(NSAttributedString(string: "PARA RECIBIR TU PEDIDO DONDE ESTES", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14), NSForegroundColorAttributeName: UIColor.blackColor()]))
//        return mut
//    }
    
   
    
    var shouldAnimateToGPSLocation:Bool = false
    func emptyDataSetDidTapView(scrollView: UIScrollView!) {
        //dismiss search and take user to his current location
        self.searchController?.active = false
            addPinAnimations()
            mapView?.alpha = 0
            mapView?.hidden = false
            nextBtn?.alpha = 0
            nextBtn?.hidden = false
            locationPin?.alpha = 0
            locationPin?.hidden = false
            self.nextBtn?.transform = CGAffineTransformMakeTranslation(0, 200)
            UIView.animateWithDuration(0.4, animations: {
                self.mapView?.alpha = 1
                self.locationPin?.alpha = 1
                self.nextBtn?.alpha = 1
                self.nextBtn?.transform = CGAffineTransformIdentity
                }, completion:  {fin in
                    let delay = 1 * Double(NSEC_PER_SEC)
                    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                    dispatch_after(time, dispatch_get_main_queue()) {
                        let tip = AMPopTip()
                        tip.popoverColor = UIColor.darkGrayColor()
                        tip.shouldDismissOnTap = true
                        tip.showText("Presiona aqui para llevar el Pin a tu posicion actual", direction: .Left, maxWidth: self.view.bounds.width*0.7, inView: self.view, fromFrame: CGRectMake(self.view.bounds.width - 70, self.view.bounds.height - (self.nextBtn?.bounds.height ?? 50) - 100, 50, 50))
                    }
            })
        showTip(7)
        if gpsLocation != nil {
            mapView.animateToCameraPosition(GMSCameraPosition(target: gpsLocation.coordinate ?? CLLocationCoordinate2D(latitude: 0,longitude: 0), zoom: 16, bearing: 0, viewingAngle: 0))
        }
    
    }
    
    
    
    
    
    // MARK: UISearchBarDelegate
    
    
    @IBAction func didPressGPSBtn(sender: UIButton) {
        self.searchController?.active = false
        pressHereToGPSBtn.hidden = true
        addPinAnimations()
        mapView?.alpha = 0
        mapView?.hidden = false
        nextBtn?.alpha = 0
        nextBtn?.hidden = false
        locationPin?.alpha = 0
        locationPin?.hidden = false
        self.nextBtn?.transform = CGAffineTransformMakeTranslation(0, 200)
        UIView.animateWithDuration(0.4, animations: {
            self.mapView?.alpha = 1
            self.locationPin?.alpha = 1
            self.nextBtn?.alpha = 1
            self.nextBtn?.transform = CGAffineTransformIdentity
            }, completion:  {fin in
                let delay = 1 * Double(NSEC_PER_SEC)
                let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                dispatch_after(time, dispatch_get_main_queue()) {
                    let tip = AMPopTip()
                    tip.popoverColor = UIColor.darkGrayColor()
                    tip.shouldDismissOnTap = true
                    tip.showText("Presiona aqui para llevar el Pin a tu posicion actual", direction: .Left, maxWidth: self.view.bounds.width*0.7, inView: self.view, fromFrame: CGRectMake(self.view.bounds.width - 70, self.view.bounds.height - (self.nextBtn?.bounds.height ?? 50) - 100, 50, 50))
                }
        })
        showTip(7)
        if gpsLocation != nil {
            mapView.animateToCameraPosition(GMSCameraPosition(target: gpsLocation.coordinate ?? CLLocationCoordinate2D(latitude: 0,longitude: 0), zoom: 16, bearing: 0, viewingAngle: 0))
        }
        

    }

    
    
    func didPresentSearchController(searchController: UISearchController) {
        if !mapView.hidden {
//            searchController.searchBar.showsCancelButton = true
        }else{
//            searchController.searchBar.showsCancelButton = false
        }
//        results.tableView.frame.origin.y = 64 + 41 + 50

    }
    
    func willPresentSearchController(searchController: UISearchController) {
        locationPin.hidden = true
        mapView.hidden = true
        pressHereToGPSBtn.hidden = true
                    searchController.searchBar.showsCancelButton = true
    }
    
    func willDismissSearchController(searchController: UISearchController) {
        if selectedRow {
            mapView.hidden = false
            locationPin.hidden = false
            nextBtn.hidden = false
            pressHereToGPSBtn.hidden = true

        }
        searchController.searchBar.showsCancelButton = false

    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        if !selectedRow {
            mapView.hidden = true
            locationPin.hidden = true
            nextBtn.hidden = true
            pressHereToGPSBtn.hidden = false
        }
    }
    
    var cameFromLoginOrRegister: Bool = false
    
    @IBAction func saveBtnPressed(sender: UIButton) {
//        if !editingMode {
//            address.street1 = searchController?.searchBar.text ?? "Sin Calle Principal"
//        }
        if user.addresses.count > 0 {
            self.performSegueWithIdentifier("showAddressDetailsSegue", sender: self)
        }else{
            
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            self.address.saveBackend(false, completion: { (success, errorMessage) in
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                if success {
                    if self.editingMode {
                        
                    }else{
                        self.performSegueWithIdentifier("showMainViewSegue", sender: self)
                    }
                }else{
                    SCLAlertView().showError("Lo sentimos!", subTitle: errorMessage ?? "Error de conexión")
                }
            })

            
        }
    }
    
    var searchBarTip: AMPopTip!
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        nextBtn.layer.cornerRadius = 5
        nextBtn.layer.borderWidth = 1
        nextBtn.layer.borderColor = UIColor.redColor().CGColor
        nextBtn.clipsToBounds =  true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showAddressDetailsSegue" {
            popupVC = segue.destinationViewController as! AddressDetailsV3TableViewController
            popupVC.delegate = self
            popupVC.address = self.address
            popupVC.cameFromCart = self.cameFromCart
            popupVC.editingMode = self.editingMode
            
            popupVC.modalPresentationStyle = .Popover
            popupVC.popoverPresentationController?.delegate = self
            popupVC.popoverPresentationController?.sourceRect = CGRectMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds),0,0)
            popupVC.popoverPresentationController?.sourceRect.origin.y -= 60
            popupVC.popoverPresentationController?.sourceView = self.view
            popupVC.preferredContentSize = CGSizeMake(self.view.bounds.width*0.9, self.view.bounds.height*0.9)
        }
     }
    var popupVC: AddressDetailsV3TableViewController!
    // MARK: PopOverDelegate
    
    func adaptivePresentationStyleForPresentationController(
        controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    
    func pulseAnimation(forImageView: UIImageView, radius: CGFloat ){
        let pulseEffect = JRPulseAnimation(repeatCount: Float.infinity, radius:radius, position:CGPoint(x: forImageView.bounds.width/2,y: forImageView.bounds.height))
        forImageView.layer.addSublayer(pulseEffect)
        
    }
    
    func didPressEditAddressBtn() {
        
    }
    
    func didPressSaveAddressBtn() {
        
        var shouldReturn: Bool = false
        if popupVC.addressNameTxt.text == "" {
            popupVC.addressNameTxt.addMissingInputAnimation()
            shouldReturn = true
        }
        
        if popupVC.street1Txt.text == "" {
            popupVC.street1Txt.addMissingInputAnimation()
            shouldReturn = true

        }
        
//        if popupVC.street2Txt.text == "" {
//            popupVC.street2Txt.addMissingInputAnimation()
//            shouldReturn = true
//
//        }
//        
//        if popupVC.numberTxt.text == "" {
//            popupVC.numberTxt.addMissingInputAnimation()
//            shouldReturn = true
//
//        }
//        
//        if popupVC.referencesTxt.text == "" {
//            popupVC.referencesTxt.addMissingInputAnimation()
//            shouldReturn = true
//
//        }
        
        if shouldReturn {
            return
        }
        
        self.address.name = popupVC.addressNameTxt.text!
        self.address.street1 = popupVC.street1Txt.text!
        self.address.street2 = popupVC.street2Txt.text!
        self.address.number = popupVC.numberTxt.text!
        self.address.reference = popupVC.referencesTxt.text!

        MBProgressHUD.showHUDAddedTo(UIApplication.sharedApplication().keyWindow!, animated: true)
        address.saveBackend(self.editingMode) { (success, errorMessage) in
            MBProgressHUD.hideHUDForView(UIApplication.sharedApplication().keyWindow!, animated: true)

            if success {
                if self.cameFromCart {
                    self.popupVC?.dismissViewControllerAnimated(true, completion: nil)
                    self.navigationController?.popViewControllerAnimated(true)
                }else{
                    self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
                    self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
}
            }else{
                SCLAlertView().showError("Lo sentimos!", subTitle: errorMessage ?? "Error de conexión")
            }
        }
    }
    
 
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        if googleResults.count > 0 {
            self.searchController?.active = false
                addPinAnimations()
                mapView.alpha = 0
                mapView.hidden = false
                nextBtn.alpha = 0
                nextBtn.hidden = false
                locationPin.alpha = 0
                locationPin.hidden = false
                self.nextBtn.transform = CGAffineTransformMakeTranslation(0, 200)
                UIView.animateWithDuration(0.4, animations: {
                    self.mapView.alpha = 1
                    self.locationPin.alpha = 1
                    self.nextBtn.alpha = 1
                    self.nextBtn.transform = CGAffineTransformIdentity
                    }, completion:  {fin in
                        let delay = 1 * Double(NSEC_PER_SEC)
                        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                        dispatch_after(time, dispatch_get_main_queue()) {
                            let tip = AMPopTip()
                            tip.popoverColor = UIColor.darkGrayColor()
                            tip.shouldDismissOnTap = true
                            tip.showText("Presiona aqui para llevar el Pin a tu posicion actual", direction: .Left, maxWidth: self.view.bounds.width*0.7, inView: self.view, fromFrame: CGRectMake(self.view.bounds.width - 70, self.view.bounds.height - self.nextBtn.bounds.height - 100, 50, 50),duration: 4)
                        }
                })
            showTip(7)
            let coordinateSelected = CLLocationCoordinate2D(latitude: googleResults[0]["geometry"]["location"]["lat"].double ?? 0, longitude: googleResults[0]["geometry"]["location"]["lng"].double ?? 0)
            self.address.coordinate = coordinateSelected
            self.mapView.animateToCameraPosition(GMSCameraPosition(target: coordinateSelected, zoom: 15.5, bearing: 0, viewingAngle: 0))
            self.searchController?.searchBar.text = googleResults[0]["formatted_address"].string ?? ""

        }else{
            return
        }
    }
    
}

// Handle the user's selection.
extension AddressMapV3ViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(resultsController: GMSAutocompleteResultsViewController, didAutocompleteWithPlace place: GMSPlace) {
        searchController?.active = false
        // Do something with the selected place.
        if mapView.hidden {
            addPinAnimations()
            mapView.alpha = 0
            mapView.hidden = false
            nextBtn.alpha = 0
            nextBtn.hidden = false
            locationPin.alpha = 0
            locationPin.hidden = false
            self.nextBtn.transform = CGAffineTransformMakeTranslation(0, 200)
            UIView.animateWithDuration(0.4, animations: {
                self.mapView.alpha = 1
                self.locationPin.alpha = 1
                self.nextBtn.alpha = 1
                self.nextBtn.transform = CGAffineTransformIdentity
                }, completion:  {fin in
                    let delay = 1 * Double(NSEC_PER_SEC)
                    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                    dispatch_after(time, dispatch_get_main_queue()) {
                        let tip = AMPopTip()
                        tip.popoverColor = UIColor.darkGrayColor()
                        tip.shouldDismissOnTap = true
                        tip.showText("Presiona aqui para llevar el Pin a tu posicion actual", direction: .Left, maxWidth: self.view.bounds.width*0.7, inView: self.view, fromFrame: CGRectMake(self.view.bounds.width - 70, self.view.bounds.height - self.nextBtn.bounds.height - 100, 50, 50))
                    }
            })
        }
        showTip(7)
        self.address.coordinate = place.coordinate
        self.mapView.animateToCameraPosition(GMSCameraPosition(target: place.coordinate, zoom: 15.5, bearing: 0, viewingAngle: 0))
        self.searchController?.searchBar.text = place.name
    }
    
    func showTip(duration: NSTimeInterval? ){
        if self.tip != nil {
            return
        }
        self.tip?.hide()
        self.tip = AMPopTip()
        
        self.tip.popoverColor = UIColor.darkGrayColor()
        var frame = self.locationPin.frame
        frame.origin.y -= 15
        if let duration = duration {
             self.tip.showText("Pone el pin en el punto exacto de tu dirección", direction: .Up, maxWidth: self.view.bounds.width*0.8, inView: self.view, fromFrame: frame,duration: duration)
        }else{
             self.tip.showText("Pone el pin en el punto exacto de tu dirección", direction: .Up, maxWidth: self.view.bounds.width*0.8, inView: self.view, fromFrame: frame)
        }
        
    }
    
    func resultsController(resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: NSError){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictionsForResultsController(resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictionsForResultsController(resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        
    }
}

extension AddressMapV3ViewController: GMSMapViewDelegate,CLLocationManagerDelegate {
    // MARK: LocationManagerDelegate
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            locationManager.startUpdatingLocation()
            
        }
        if status == .Denied {
            
        }
    }
    
    
    func mapView(mapView: GMSMapView, willMove gesture: Bool) {
//        locationManager.stopUpdatingLocation()
        removePinAnimations()
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first  {
            gpsLocation = location
            if shouldAnimateToGPSLocation {
                mapView.animateToCameraPosition(GMSCameraPosition(target: gpsLocation.coordinate, zoom: 16, bearing: 0, viewingAngle: 0))
                shouldAnimateToGPSLocation = false
            }
            if !editingMode {
//                mapView.animateToLocation(location.coordinate)
                
                //            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: mapView.camera.zoom + 0.5, bearing: 0, viewingAngle: 0)
                if self.tip != nil {
                    
                }
            }
            
        }
        
    }
    
    
    
    
    
    func mapView(mapView: GMSMapView, idleAtCameraPosition position: GMSCameraPosition) {
        self.address.coordinate = position.target
        addPinAnimations()
        //only for multiple rings
        var delay = 0.1 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(time, dispatch_get_main_queue()) {

        self.pulseAnimation(self.locationPin, radius: 50)
        }
        delay = 0.3 * Double(NSEC_PER_SEC)
        dispatch_after(time, dispatch_get_main_queue()) {
            self.pulseAnimation(self.locationPin, radius: 50)
        }
        if mapView.hidden == false {
            showTip(7)
        }
        
    }
    
    
    
}