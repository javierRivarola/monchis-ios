//
//  MProduct.swift
//  Monchis
//
//  Created by jrivarola on 7/30/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import Foundation
import SwiftyJSON

class MProduct: NSObject {
    
    //just for search
    var franchise: MFranchise!
    var randomString: String?
    var id: Int?
    var loaded: Bool = false
    var category_id: Int?
    var product_id: NSNumber?
    var order_id: NSNumber?
    var name: String?
    var product_description: String?
    var addons: [MAddon] = []
    var order_product_id: NSNumber?
    var price: Float? {
        didSet {
            formattedPrice = "\(price)"
        }
    }
    var formattedPrice: String? {
        didSet{ //format price accordingly
            formattedPrice = formattedPrice?.floatValue.asLocaleCurrency
        }
    }
    var num_flavors: Int?
    var quantity: Int? = 1
    var customizableFields : [MCustomizableField] = []
    var customizableFieldsTotalPrice : Float {
        get {
            var price:Float = 0
                for customization in customizableFields {
                    price = price + customization.addonsTotalPrice
                }
            return price
        }
    }
    var userCustomizationsTotalPrice: Float {
        get {
            var price:Float = 0
            for customization in customizableFields {
                price = price + customization.userSelectionsTotalPrice
            }
            return price
        }
    }
    
    var imageHeight: Double = 0
    var downloadedImageForShare: UIImage!
    var is_combo: Bool?
    var is_promotion: Bool?
    var comments: String?
    var attachments:[MAttachment] = []
    var addonsTotalPrice: Float {
        get {
            var addonsPrice: Float = 0
            if  self.addons.count > 0 {
                for customization in addons {
                    
                    addonsPrice = addonsPrice + (customization.price ?? 0)
                }
                return addonsPrice
            }else{
                    return 0
                }
        }
    }
    
    var addonsDescription:String? {
        get {
            var orderCustomizations = ""
            if  self.addons.count > 0 {
                for customization in addons {
                    if customization != addons.last {
                        orderCustomizations = orderCustomizations  + (customization.etiqueta ?? "" ) +  ", "
                    }else{
                        orderCustomizations = orderCustomizations + (customization.etiqueta ?? "" )
                    }
                }
                return orderCustomizations
            }else{
                return orderCustomizations
            }
        }
    }
    var product_category_id: Int?
    var branch_products: [MRestaurant] = []
    var current_price: String?
    var photo: UIImage!
    var created_at: String?
    init(json: JSON) {
        self.id = json["id"].int
        self.product_description = json["description"].string
        self.is_promotion = json["is_promotion"].bool
        self.quantity = json["quantity"].int ?? 1
        self.current_price = json["current_price"].string
        self.comments = json["comments"].string
        self.name = json["name"].string
        self.product_category_id = json["product_category_id"].int
        self.price = json["price"].float
        if self.price == nil {
            self.price = json["price"].string?.floatValue
        }
        self.is_combo = json["is_combo"].bool
        self.order_product_id = json["order_product_id"].int
        self.product_id = json["id"].int
        self.created_at = json["created_at"].string
        self.voucher_validTo = json["valid_to"].string
        self.voucher_validFrom = json["valid_from"].string
        self.is_voucher = json["is_voucher"].bool
    }
    var voucherCode: String?
    var voucher_validFrom: String?
    var voucher_validTo: String?
    var is_voucher: Bool?
    class func getProductForVoucher(voucherCode:String, completion: (error: String? ,product: MProduct?, branch:  MRestaurant?) -> Void) {
            let parameters = ["voucher":voucherCode]
            MAPI.SharedInstance.jsonRequest(.VALIDATE_VOUCHER, parameters: parameters) { (json, error) in
                if debugModeEnabled {
                    print(#function, "called with parameters", parameters, "response: \(json), error = \(error)")
                }
                if error == nil {
                    if let success = json?["success"].bool {
                        if success {
                            let resto = MRestaurant(data: json!["data"]["branch"])
//                            resto.franchise = MFranchise(json: json!["data"]["franchise"])
                            let product = MProduct(json: json!["data"])
                            product.name = json!["data"]["product"]["name"].string
                            product.product_description = json!["data"]["product"]["description"].string
                            product.price = json!["data"]["product"]["price"].string?.floatValue
                            product.id = json!["data"]["product"]["id"].int
                            if let att = json!["data"]["product"]["attachments"].array?.first {
                                product.attachments = [MAttachment(json: att)]
                            }
                            return  completion(error: nil , product: product, branch: resto)
                        }else{
                            return completion(error: json?["data"].string, product: nil, branch:  nil)
                        }
                    }
                }else{
                    //network error
                    return completion(error: "Error de red.", product: nil, branch: nil)
                }
            }
            

    }
    
    class func redeemVoucher(voucherCode:String, deliveryType: Int,branch_id: Int?, completion: (success: Bool, error: String?) -> Void) {
        var parameters: [String:AnyObject]!
        parameters = ["voucher":voucherCode, "delivery_type": deliveryType]
        if branch_id != nil {
            parameters = ["voucher":voucherCode, "delivery_type": deliveryType, "branch_id": branch_id!]
        }
        MAPI.SharedInstance.jsonRequest(.REDEEM_VOUCHER, parameters: parameters) { (json, error) in
            if debugModeEnabled {
                print(#function, "called with parameters", parameters, "response: \(json), error = \(error)")
            }
            if error == nil {
                if let success = json?["success"].bool {
                    if success {
                        return  completion(success: true, error: nil)
                    }else{
                        return completion(success: false, error: json?["data"].string)
                    }
                }
            }else{
                //network error
                return completion(success: false, error: "Error de red.")
            }
        }
        
        
    }
    
    override init() {
        
    }
    
    class func compareProducts(prod1: [MProduct], prod2: [MProduct]) -> Bool {
        for c in prod1 {
            if !prod2.contains( {cat in
                return cat.id == c.id
            }) {
                return false
            }
        }
        return true
    }
    
    init(fromProduct: MProduct) {
        self.attachments = fromProduct.attachments
        self.id = fromProduct.id
        self.name = fromProduct.name
    }

   
    
    
}

class MBranchProduct: NSObject {
    var product_id: NSNumber?
    var branch_id: NSNumber?
}

func == (lhs: MBranchProduct, rhs: MBranchProduct) -> Bool {
    return lhs.product_id == rhs.product_id && lhs.branch_id == rhs.branch_id
}
