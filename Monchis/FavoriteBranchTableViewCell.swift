//
//  FavoriteBranchTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 12/1/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit

class FavoriteBranchTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
