//
//  FranchiseCollectionViewCell.swift
//  Monchis
//
//  Created by jrivarola on 22/1/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit

class FranchiseCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var franchiseNameLbl: UILabel!
    @IBOutlet weak var franchiseImageView: UIImageView!
}
