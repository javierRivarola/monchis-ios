//
//  LoadingViewController.swift
//  Monchis
//
//  Created by Javier Rivarola on 7/30/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import SCLAlertView
import JRMFloatingAnimation

class LoadingViewController: UIViewController {
    var floatingView: JRMFloatingAnimationView!
    var animationTimer: NSTimer!

    @IBOutlet weak var bgImgView: UIImageView!
    @IBOutlet weak var logoImgView: UIImageView!
    var isInitialScreen: Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
         userShouldBeLoggedIn = NSUserDefaults.standardUserDefaults().boolForKey("userLoggedKey")
        animateLogo()
        floatingView = JRMFloatingAnimationView(startingPoint: view.center)
        floatingView.pop = true
        floatingView.floatingShape = .CurveRight
        floatingView.varyAlpha = true
        floatingView.maxFloatObjectSize = 40
//        animateFood()
//        animationTimer = NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: #selector(LoadingViewController.animateFood), userInfo: nil, repeats: true)
//
        self.view.addSubview(floatingView)
        if userShouldBeLoggedIn{
            MUser.sharedInstance.automaticUserLogin {success in
                if success {
                    self.userLoggedIn()
                }else{
                    self.offlineMode()
                }
            }
        }else{
            showLoginScreen()
        }
 
    }
    
    func registerTemporalUser(){
        
    }
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        animationTimer?.invalidate()
    }
    
    func offlineMode(){
        UIView.animateWithDuration(1, delay: 0,options: [.BeginFromCurrentState,.CurveEaseIn], animations: {
            self.logoImgView.transform = CGAffineTransformIdentity
            }, completion: { (fin) in
                UIView.animateWithDuration(0.45, animations: {
                    self.logoImgView.transform = CGAffineTransformMakeScale(0.0001,0.0001)
                    self.spinner.alpha = 0
                    self.bgImgView.alpha = 0
                    }, completion: { (fin) in
                            self.performSegueWithIdentifier("showApp", sender: self)
                })
        })

    }
    func animateFood(){
        floatingView.addImage(UIImage(named: "pizza1")!)
        floatingView.animate()
        
        floatingView.addImage(UIImage(named: "pizza2")!)
        floatingView.animate()
        
        floatingView.addImage(UIImage(named: "pizza3")!)
        floatingView.animate()
        
        floatingView.addImage(UIImage(named: "pizza4")!)
        floatingView.animate()
        
        floatingView.addImage(UIImage(named: "pizza5")!)
        floatingView.animate()
    }
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var isShowing: Bool = false
    
    func showLoginScreen(){
        MUser.sharedInstance.registerAsTemporalUser { (success, error) in
            if success {
                UIView.animateWithDuration(1, delay: 0,options: [.BeginFromCurrentState,.CurveEaseIn], animations: {
                    self.logoImgView.transform = CGAffineTransformIdentity
                    }, completion: { (fin) in
                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AddressMapV3ViewController") as! AddressMapV3ViewController
                        self.presentViewController(vc, animated: false, completion: nil)
                })
            }else{
                let delay = 1.0 * Double(NSEC_PER_SEC)
                let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                dispatch_after(time, dispatch_get_main_queue()) {
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false
                    )
                    let alert = SCLAlertView(appearance: appearance)
                    alert.addButton("Reintentar") {
                        self.showLoginScreen()
                    }
                    alert.showError("Lo sentimos", subTitle: error ?? "Ocurrio un error inesperado.")
                }
               
            }
        }
       
    }
    
    func animateLogo(){
        self.logoImgView.transform = CGAffineTransformIdentity

        UIView.animateWithDuration(1.5, delay: 0,  options: [.Autoreverse,.Repeat, .CurveEaseInOut], animations: {
            self.logoImgView.transform = CGAffineTransformMakeTranslation(0, -50)
            colorize(self.logoImgView, color: UIColor.redColor().CGColor)
            }) { (fin) in
        }
    }
    
    var userShouldBeLoggedIn: Bool = false

    func userLoggedIn(){
    
            UIView.animateWithDuration(1, delay: 0,options: [.BeginFromCurrentState,.CurveEaseIn], animations: {
                self.logoImgView.transform = CGAffineTransformIdentity
                }, completion: { (fin) in
                    UIView.animateWithDuration(0.45, animations: {
                        self.logoImgView.transform = CGAffineTransformMakeScale(0.0001,0.0001)
                        self.spinner.alpha = 0
                        self.bgImgView.alpha = 0
                        }, completion: { (fin) in
                            print(MUser.sharedInstance.email)
                            print(MUser.sharedInstance.addresses.count)
                            if MUser.sharedInstance.addresses.count == 0 {
                                self.performSegueWithIdentifier("showMapSegue", sender: self)
                            }else {
                                self.performSegueWithIdentifier("showApp", sender: self)
                            }
                    })
            })
            
    }
    

    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
//        if cameFromMap {
//            self.performSegueWithIdentifier("showApp", sender: self)
//            cameFromMap = false
//        }else{
//            if MUser.sharedInstance.logged {
//                userLoggedIn()
//            }
//            
//        }
        if MUser.sharedInstance.logged {
            userLoggedIn()
        }

    }
    
    var cameFromMap: Bool = false


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
