//
//  RestaurantesCollectionViewController.swift
//  Monchis
//
//  Created by jrivarola on 6/22/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import Haneke
import SwiftyJSON
import ReachabilitySwift
import SCLAlertView
import Crashlytics
import ImageOpenTransition
import Mixpanel
import CoreLocation
import MBProgressHUD
import AMPopTip

protocol RestaurantesCollectionViewControllerDelegate {
    func didSelectRestaurant(restaurant: MRestaurant)
}
var filterType: String = "Delivery"
var filterSegmentedControlGlobal: UISegmentedControl!

class RestaurantesCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource, DeliveryPickupDelegate , UINavigationControllerDelegate{
    var customDelegate: RestaurantesCollectionViewControllerDelegate?
    var franchises: [MFranchise] = []
    var openRestaurants: [MRestaurant]!
    var closedRestaurants: [MRestaurant]!
    var openDelAvailableRestaurants: [MRestaurant]!
    var closedDelAvailableRestaurants: [MRestaurant]!
    var restaurants:[MRestaurant] = []
    var refreshControl: UIRefreshControl!
    var franchiseSelected: MFranchise!
    let api = MAPI.SharedInstance
    var loadingBeforeEmptyDataSet: Bool = true
    var fetchingDataFromNetwork: Bool = false
    var imageScalePresentTransition :ImageScaleTransitionDelegate? = nil
    var filterSegmentedControl: UISegmentedControl!

    var category: MCategory! {
        didSet{
            self.title = category?.name
            //loadBranchesForCategory(category.id!)
        }
    }
    var shouldAnimate: Bool = false
    var spinner: UIActivityIndicatorView!
    var netError: Bool = false
    let RESTAURANT_CELL_HEIGHT:CGFloat = 150.0
    var filteredDelivery: [MRestaurant] = []
    var filteredPickup: [MRestaurant] = []
    var shouldShowTooltipsOnFilters: Bool = true
    var pop = AMPopTip()
    
    struct Constants {
        static let reuseIdentifier = "RestaurantesCellReuseID"
        static let reuseHeaderIdentifier = "PestanhaRestaurantesReuseHeader"
        static let RestaurantesClosedCellReuseID = "RestaurantesClosedCellReuseID"
        static let NoCoverRestaurantReuseID = "NoCoverRestaurantReuseID"
        static let headerReuseID = "reuseHeaderID"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.transitioningDelegate = self.imageScalePresentTransition
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RestaurantesCollectionViewController.defaultAddressChanged), name: kUserDidChangeDefaultAddressNotification, object: nil)
        self.collectionView?.emptyDataSetSource = self
        self.collectionView?.emptyDataSetDelegate = self
        configurePullToRefresh()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RestaurantesCollectionViewController.reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
        if let cat = category.id {
            loadBranchesForCategory(cat)
        }else{
            print("cat not found")
        }
    }



    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let reuse = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "RestoHeaders", forIndexPath: indexPath)
        let segmented = reuse.viewWithTag(1) as! UISegmentedControl
        self.filterSegmentedControl = segmented
        segmented.addTarget(self, action: #selector(RestaurantesCollectionViewController.segmentedControlChanged(_:)), forControlEvents: .ValueChanged)
        if filterType == "Delivery" {
            segmented.selectedSegmentIndex = 0
            segmented.setTitle("✔ Delivery (\(self.filteredDelivery.count))", forSegmentAtIndex: 0)
            segmented.setTitle("Pasar a Buscar (\(self.filteredPickup.count))", forSegmentAtIndex: 1)

        }else{
            segmented.setTitle("Delivery (\(self.filteredDelivery.count))", forSegmentAtIndex: 0)
            segmented.selectedSegmentIndex = 1
            segmented.setTitle("✔ Pasar a Buscar (\(self.filteredPickup.count))", forSegmentAtIndex: 1)

        }
        
        if NSUserDefaults.standardUserDefaults().boolForKey("showedFilterTip") != true {
            pop.shouldDismissOnTap = true
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "showedFilterTip")
            NSUserDefaults.standardUserDefaults().synchronize()
        let delay = 1.0 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))

        dispatch_after(time, dispatch_get_main_queue()) {
            if self.shouldShowTooltipsOnFilters {
                self.pop.showText("Ahora puedes filtrar los locales disponibles para Delivery o Pasar a Buscar", direction: .Down, maxWidth: self.view.bounds.width*0.8, inView: self.view, fromFrame: reuse.frame,duration: 7)
            self.shouldShowTooltipsOnFilters = false
            }

        }

        pop.popoverColor = UIColor(hexString: "#1F2329")!
        
        
        }
        
        return reuse
    }
    
    
    func segmentedControlChanged(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            filterType = "Delivery"
            refreshControl.tintColor = UIColor.whiteColor()

        }else{
            filterType = "Pasar a Buscar"
            refreshControl.tintColor = UIColor.redColor()

        }

        self.filterByMode(restaurants) { (filtered) in

            self.restaurants = []
            self.collectionView?.reloadData()
            if sender.selectedSegmentIndex == 0 {
                self.restaurants = self.filteredDelivery
                UIView.animateWithDuration(1.0, animations: {
                    self.collectionView?.backgroundColor = UIColor.clearColor()
                })
            
            }else{
                self.restaurants = self.filteredPickup
                UIView.animateWithDuration(1.0, animations: {
                    self.collectionView?.backgroundColor = UIColor(hexString: "#F0F0F0")
                })
            }
            if self.restaurants.count == 0 {
                self.pop.hide()
            }
            self.loadedIdx = []
            self.collectionView?.reloadData()
            self.collectionView?.reloadEmptyDataSet()
        }
        
    }
    
    func reachabilityChanged(notification: NSNotification) {
        
        if let reachability = notification.object as? Reachability {
            
            if reachability.isReachable() {
                self.collectionView?.reloadEmptyDataSet()
                loadBranchesForCategory(category.id!)
                if reachability.isReachableViaWiFi() {
                    print("Reachable via WiFi")
                } else {
                    print("Reachable via Cellular")
                }
            } else {
                print("Network not reachable")
            }
        }
    }
    
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func deleteCacheData(){
        let url = api.kMAPIClientsURL.URLString + MAPI.Command.SEARCH_BRANCHES.rawValue
        let key = api.generateCacheKeyFrom(url, parameters: lastParameters)
        MAPI.SharedInstance.cache.remove(key: key)
    }
    
    

    
    func filterByPickup(branches: [MRestaurant]) -> [MRestaurant]{
        var filtered:[MRestaurant] = []
        filtered = branches.filter({ (resto) -> Bool in
            return resto.is_pickup_enabled == true
        })
        return filtered
        
    }
    
    func filterByMode(branches: [MRestaurant], completion: (fin: Bool)->Void){
        var tmpDel:[MRestaurant] = []
        var tmpPick:[MRestaurant] = []
        
        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        if (filterType == "Delivery" && filteredDelivery.count == 0)  || (filterType == "Pasar a Buscar" && filteredPickup.count == 0) {
//            MBProgressHUD.showHUDAddedTo(self.collectionView!, animated: true)
            
        }
        dispatch_async(backgroundQueue, {
            
            if self.filteredDelivery.count == 0 {
                
                    tmpDel = branches.sort({ (resto1, resto2) -> Bool in
                        return resto1.franchise?.list_order < resto2.franchise?.list_order
                    })
                    tmpDel = tmpDel.filter({ (resto1) -> Bool in
                        return resto1.hasDelivery()
                    })
                    let tmpDelOpen = tmpDel.filter({ (resto) -> Bool in
                        return resto.isOpen()
                    })
                    let tmpDelClosed = tmpDel.filter({ (resto) -> Bool in
                        return !resto.isOpen() && !resto.isClosedToday()
                    })
                    
                    //first the ones that are open, then closed but open today, then closed today
                    let tmpDelClosedToday  = tmpDel.filter({ (resto) -> Bool in
                        return resto.isClosedToday()
                    })

                    
                    self.filteredDelivery = tmpDelOpen + tmpDelClosed + tmpDelClosedToday
                }else{
                    tmpDel = self.filteredDelivery
                }
                
                if self.filteredPickup.count == 0 {
                    tmpPick = branches.filter({ (resto1) -> Bool in
                        return resto1.franchise?.branches_with_pickup == true && !resto1.isOpen()
                    })
                    
                    var openRestos = branches.filter({ (resto1) -> Bool in
                        return resto1.isOpen() && resto1.franchise?.branches_with_pickup == true
                    })
                    openRestos.sortInPlace({ (resto1, resto2) -> Bool in
                        return resto1.distanceFromMyLocation.floatValue < resto2.distanceFromMyLocation.floatValue
                    })
        
                    
                    
                    self.filteredPickup = openRestos + tmpPick
                }else{
                    tmpPick = self.filteredPickup
                }
            
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                MBProgressHUD.hideHUDForView(self.collectionView!, animated: true)
                self.loadingBeforeEmptyDataSet = false
                completion(fin: true)
                
                
            })
            
        })
    }
    
    func defaultAddressChanged(){
        self.loadingBeforeEmptyDataSet = true
        self.filteredPickup = []
        self.filteredDelivery = []
        self.restaurants = []
        self.collectionView?.reloadData()
        self.collectionView?.reloadEmptyDataSet()
        loadBranchesForCategory(category.id!,useCache: false)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        self.navigationController?.delegate = self.imageScalePresentTransition
//        loadBranchesForCategory(category.id!)
    }
    
    
    
    func configurePullToRefresh(){
        refreshControl = UIRefreshControl()
        var tira = NSAttributedString(string: "Tira para actualizar", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSForegroundColorAttributeName: UIColor.whiteColor()])

        if filterType == "Delivery" {
            refreshControl.tintColor = UIColor.whiteColor()
        }else{
            refreshControl.tintColor = UIColor.redColor()
             tira = NSAttributedString(string: "Tira para actualizar", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSForegroundColorAttributeName: UIColor.redColor()])

        }
        let title = NSMutableAttributedString()
        
        
        title.appendAttributedString(tira)
        refreshControl.attributedTitle = title
        refreshControl.addTarget(self, action: #selector(RestaurantesCollectionViewController.pullToRefresh(_:)), forControlEvents: .ValueChanged)
        self.collectionView?.addSubview(refreshControl)
        self.collectionView?.alwaysBounceVertical = true
    }
    
    func pullToRefresh(sender: UIRefreshControl){
        cachedJSON = nil
        self.filteredPickup = []
        self.filteredDelivery = []
        loadBranchesForCategory(category.id!, useCache: false)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "branchSelectionSegue" {
            let dvc = segue.destinationViewController as! BranchSelectionViewController
            dvc.franchise = selectedFranchise
            
        }
        if segue.identifier == "showBranchSegue" {
            let dvc = segue.destinationViewController as! RestaurantViewController
            dvc.restaurant = selectedBranch
            Answers.logContentViewWithName("Usuario quiere delivery de local \(selectedBranch!.name)",
                                           contentType: "Franquicias",
                                           contentId: "\(selectedBranch!.id!)",
                                           customAttributes: ["Nombre Local":selectedBranch!.name])
            
        }
    }
    
    
    var selectedFranchise: MFranchise?
    // MARK: UICollectionViewDataSource
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return restaurants.count > 0 ? 1 : 0
        //return restaurants != nil ? 1 : 0
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(collectionView.frame.size.width/2, collectionView.frame.size.width/2 + 70)
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        //  selectedRestaurant = categories[selectedPageIndex].restaurants[indexPath.row]
        selectedBranch = restaurants[indexPath.row]
        
        if filterType == "Delivery" {
            selectedBranch.mode = 0
            Mixpanel.mainInstance().track(event: "Usuario Eligio una Franquicia",
                                          properties: ["Franquicia" : selectedBranch.franchise?.name ?? "","Pantalla":"Franquicias por Categoria", "Modo":"Delivery"])
            self.performSegueWithIdentifier("showBranchSegue", sender: self)
        }else{
            //goe directly in pickup
            Mixpanel.mainInstance().track(event: "Usuario Eligio una Franquicia",
                                          properties: ["Franquicia" : selectedBranch.franchise?.name ?? "","Pantalla":"Franquicias por Categoria", "Modo":"Pickup"])
            selectedBranch.mode = 1
            selectedFranchise = selectedBranch?.franchise
            self.performSegueWithIdentifier("branchSelectionSegue", sender: self)
        }
        
//        if selectedBranch.franchise?.branches_with_pickup == true && selectedBranch.is_delivery_enabled! && selectedBranch.delivery_available == true  && selectedBranch.isOpen(){
//            SCLAlertView().showNotice("Atención", subTitle: "Debes elegir si deseas la opción Delivery o Pasar a buscar")
//            Mixpanel.mainInstance().track(event: "Usuario Eligio una Franquicia con Ambas Opciones (Popup)",
//                                          properties: ["Franquicia" : selectedBranch.franchise?.name ?? "","Pantalla":"Franquicias por Categoria"])
//            
//            
//        }else if selectedBranch.franchise?.branches_with_pickup == false && selectedBranch.is_delivery_enabled! && selectedBranch.delivery_available == true && selectedBranch.isOpen(){
//            //go directly in delivery
//            selectedBranch.mode = 0
//            Mixpanel.mainInstance().track(event: "Usuario Eligio una Franquicia",
//                                          properties: ["Franquicia" : selectedBranch.franchise?.name ?? "","Pantalla":"Franquicias por Categoria", "Modo":"Delivery"])
//            self.performSegueWithIdentifier("showBranchSegue", sender: self)
//            //               customTransition(indexPath)
//            
//        }else if selectedBranch.franchise?.branches_with_pickup == true && (!selectedBranch.is_delivery_enabled! || !selectedBranch.delivery_available!) {
//            //goe directly in pickup
//            Mixpanel.mainInstance().track(event: "Usuario Eligio una Franquicia",
//                                          properties: ["Franquicia" : selectedBranch.franchise?.name ?? "","Pantalla":"Franquicias por Categoria", "Modo":"Pickup"])
//            selectedBranch.mode = 1
//            selectedFranchise = selectedBranch?.franchise
//            self.performSegueWithIdentifier("branchSelectionSegue", sender: self)
//            
//        }else {
//            //                customTransition(indexPath)
//            Mixpanel.mainInstance().track(event: "Usuario Eligio una Franquicia",
//                                          properties: ["Franquicia" : selectedBranch.franchise?.name ?? "","Pantalla":"Franquicias por Categoria", "Modo":"Delivery"])
//            self.performSegueWithIdentifier("showBranchSegue", sender: self)
//            
//        }
        
        
        
    }
    
    func customTransition(indexPath: NSIndexPath) {
        let cell = self.collectionView?.cellForItemAtIndexPath(indexPath) as! RestaurantesCollectionViewCell
        
        let restoVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
        restoVC.restaurant = selectedBranch
        restoVC.loadViewIfNeeded()
        let transitionObjectAvatar = ImageScaleTransitionObject(viewToAnimateFrom: cell.imageView,
                                                                viewToAnimateTo: restoVC.restoHeaderImageView,
                                                                duration: 0.4)
        
        self.imageScalePresentTransition = ImageScaleTransitionDelegate(transitionObjects: [transitionObjectAvatar], usingNavigationController: true, duration: 0.4, isProduct: false)
        
        self.navigationController?.delegate = self.imageScalePresentTransition
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            self.navigationController?.delegate = nil
        }
        self.navigationController?.pushViewController(restoVC, animated: true)
        
        CATransaction.commit()
    }
    
    
    
    
    
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return restaurants.count
    }
    
    
    
    let BRANCH_PHOTO_HEIGHT:CGFloat = 300
    let BRANCH_PHOT_WIDTH:CGFloat = 300
    
    let initialColor = UIColor.blackColor().colorWithAlphaComponent(0.15)
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let currentRestaurant = restaurants[indexPath.row]
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Constants.reuseIdentifier, forIndexPath: indexPath) as! RestaurantesCollectionViewCell
        
//        
//        if indexPath.row == 0 {
//            cell.contentView.backgroundColor = initialColor
//        }else{
//            if indexPath.row == 1 {
//                cell.contentView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.21)
//            }else{
//                let prevCell = collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: indexPath.row - 1, inSection: 0)) as? RestaurantesCollectionViewCell
//                let prev2Cell = collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: indexPath.row - 2, inSection: 0)) as? RestaurantesCollectionViewCell
//                if prevCell?.contentView.backgroundColor == initialColor && prev2Cell?.contentView.backgroundColor == initialColor {
//                    cell.contentView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.21)
//                }
//                if prev2Cell?.contentView.backgroundColor == UIColor.blackColor().colorWithAlphaComponent(0.21) && prev2Cell?.contentView.backgroundColor == UIColor.blackColor().colorWithAlphaComponent(0.21) {
//                    cell.contentView.backgroundColor = initialColor
//                }
//                if prev2Cell?.contentView.backgroundColor == initialColor && prevCell?.contentView.backgroundColor == UIColor.blackColor().colorWithAlphaComponent(0.21) {
//                    cell.contentView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.21)
//                }
//                if prev2Cell?.contentView.backgroundColor == UIColor.blackColor().colorWithAlphaComponent(0.21) && prevCell?.contentView.backgroundColor == initialColor {
//                    cell.contentView.backgroundColor = initialColor
//                }
//            }
//            
//        }
        
        
        //configure the cell
        currentRestaurant.attachment?.width = cell.imageView.frame.size.width
        currentRestaurant.attachment?.height = cell.imageView.frame.size.height
        if let imgURL = currentRestaurant.attachment?.resizedImageUrl {
            
            cell.imageView.hnk_setImageFromURL(imgURL, placeholder: NO_BRANCH_PLACEHOLDER)
            
        }else{
            cell.imageView?.image = NO_BRANCH_PLACEHOLDER
        }
        
        
        cell.shadowView.layer.shadowColor = UIColor.blackColor().CGColor
        cell.shadowView.layer.shadowOpacity = 1
        cell.shadowView.layer.shadowOffset = CGSizeZero
        cell.shadowView.layer.shadowRadius = 10
        cell.contView.layer.cornerRadius = 5
        cell.branch = currentRestaurant
        cell.contView.clipsToBounds = true
        

        cell.contView.layer.borderWidth = 1
        cell.contView.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        cell.timeLblBackground.backgroundColor = UIColor(hexString: "#363A40")
        if currentRestaurant.is_pos_enabled! {
            cell.iconoPos.hidden = false
        }else{
            cell.iconoPos.hidden = true
        }
        
//        parallaxEffectOnBackground(cell.imageView)
        
        if filterType == "Delivery" {
            cell.pickupView.hidden = true
            cell.minimumOrderLbl.text = "Pedido Mínimo: \(currentRestaurant.order_minimum.floatValue.asLocaleCurrency)"
            cell.seeMapBtn?.setTitle("Delivery: \(currentRestaurant.delivery_price!.asLocaleCurrency)", forState: .Normal)
            if currentRestaurant.isOpen() {
                cell.timeLabel.text = "Estimado: \(currentRestaurant.delivery_delay) minutos"
            }else{
                cell.timeLabel.attributedText = currentRestaurant.textForScheduleState()
                cell.timeLblBackground.backgroundColor = UIColor.lightGrayColor()
            }
            
            
        }else{
            cell.pickupView.hidden = false
            let attrText = NSMutableAttributedString(attributedString: NSAttributedString(string: "DISTANCIA\n", attributes: [NSForegroundColorAttributeName: UIColor.lightGrayColor(), NSFontAttributeName: UIFont.systemFontOfSize(9)]))
            
            attrText.appendAttributedString(NSAttributedString(string: currentRestaurant.distanceFromMyLocation, attributes: [NSForegroundColorAttributeName: UIColor.blackColor(), NSFontAttributeName: UIFont.boldSystemFontOfSize(14)]))
             attrText.appendAttributedString(NSAttributedString(string: " kms", attributes: [NSForegroundColorAttributeName: UIColor.blackColor(), NSFontAttributeName: UIFont.systemFontOfSize(9)]))
            cell.distanceLbl.attributedText = attrText
            
            cell.seeMapBtnImg.addTarget(self, action: #selector(RestaurantesCollectionViewController.seeBranchOnMap(_:)), forControlEvents: .TouchUpInside)
            cell.seeMapBtnTitle.tag = indexPath.row
            cell.seeMapBtnImg.tag = indexPath.row
            cell.seeMapBgBtn.tag = indexPath.row
            cell.seeMapBgBtn.addTarget(self, action: #selector(RestaurantesCollectionViewController.seeBranchOnMap(_:)), forControlEvents: .TouchUpInside)
            cell.seeMapBtnTitle.addTarget(self, action: #selector(RestaurantesCollectionViewController.seeBranchOnMap(_:)), forControlEvents: .TouchUpInside)
            if currentRestaurant.isOpen() {
                cell.timeLabel.text = "Listo en \(currentRestaurant.delivery_delay - 15) minutos"
            }else{
                cell.timeLabel.attributedText = currentRestaurant.textForScheduleState()
                cell.timeLblBackground.backgroundColor = UIColor.lightGrayColor()
            }
            cell.contView.layer.borderWidth = 1
            cell.contView.layer.borderColor = UIColor.lightGrayColor().CGColor
            
        }
       
        cell.layoutIfNeeded()
        return cell
    }
    
    func seeBranchOnMap(sender: UIButton) {
        let dvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchesOnMapViewController") as! BranchesOnMapViewController
        dvc.branches = [self.restaurants[sender.tag]]
        self.navigationController?.pushViewController(dvc, animated: true)
    }
    
    var loadedIdx: [Int] = []
    
    override func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.row <= 3 && !loadedIdx.contains(indexPath.row)) {
            let cellContent = cell
            let rotationAngleDegrees : Double = -30
            let rotationAngleRadians = rotationAngleDegrees * (M_PI/180)
            let offsetPositioning = CGPoint(x: collectionView.bounds.size.width, y: -20)
            var transform = CATransform3DIdentity
            transform = CATransform3DRotate(transform, CGFloat(rotationAngleRadians), -50, 0, 1)
            transform = CATransform3DTranslate(transform, offsetPositioning.x, offsetPositioning.y, -50)
            
            cellContent.layer.transform = transform
            cellContent.layer.opacity = 0.2
            
            let delay = 0.06 * Double(indexPath.row)
            UIView.animateWithDuration(0.8, delay:delay , usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: .CurveEaseIn, animations: { () -> Void in
                cellContent.layer.transform = CATransform3DIdentity
                cellContent.layer.opacity = 1
            }) { (Bool) -> Void in

            }
            
            loadedIdx.append(indexPath.row)
        }
    
    }
    
//    var firstTimeForHeaderAnimation: Bool = true
//    override func collectionView(collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, atIndexPath indexPath: NSIndexPath) {
////        
////        if elementKind == UICollectionElementKindSectionHeader && firstTimeForHeaderAnimation {
////                firstTimeForHeaderAnimation = false
////            view.layer.opacity = 0.2
////            UIView.animateWithDuration(3, delay:0 , usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: .CurveEaseIn, animations: { () -> Void in
////                view.layer.opacity = 1
////            }) { (Bool) -> Void in
////                
////            }
////            
////        }
//    }
    
    
    func parallaxEffectOnBackground(image: UIImageView) {
        let relativeMotionValue = 8
        let verticalMotionEffect : UIInterpolatingMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y",
                                                                                             type: .TiltAlongVerticalAxis)
        verticalMotionEffect.minimumRelativeValue = -relativeMotionValue
        verticalMotionEffect.maximumRelativeValue = relativeMotionValue
        
        let horizontalMotionEffect : UIInterpolatingMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x",
                                                                                               type: .TiltAlongHorizontalAxis)
        horizontalMotionEffect.minimumRelativeValue = -relativeMotionValue
        horizontalMotionEffect.maximumRelativeValue = relativeMotionValue
        
        let group : UIMotionEffectGroup = UIMotionEffectGroup()
        group.motionEffects = [horizontalMotionEffect, verticalMotionEffect]
        if image.motionEffects.count == 0 {
            image.addMotionEffect(group)
        }
    }
    
    var selectedBranch: MRestaurant!
    
    func didPressPickup(branch: MRestaurant?, cell: RestaurantesCollectionViewCell) {
        selectedFranchise = branch?.franchise
        if branch!.isOpen() && branch!.franchise?.branches_with_pickup == true {
            selectedBranch = branch
            selectedBranch.mode = 1
            Mixpanel.mainInstance().track(event: "Usuario Eligio una Franquicia",
                                          properties: ["Franquicia" : selectedBranch.franchise?.name ?? "","Pantalla":"Franquicias por Categoria", "Modo":"Pickup"])
            self.performSegueWithIdentifier("branchSelectionSegue", sender: self)
        }else{
            cell.contView.shake()
            cell.iconoPos.shake()
        }
    }
    
    
    
    var lastTimeForPopup: NSDate!
    
    func didPressDelivery(branch: MRestaurant?,cell: RestaurantesCollectionViewCell){
        if branch!.isOpen() && branch!.is_delivery_enabled! && branch!.delivery_available! {
            if let lastTimeForPopup = lastTimeForPopup {
                if NSDate().timeIntervalSinceDate(lastTimeForPopup) >= 60*60*60 {
                    
                    self.lastTimeForPopup = NSDate()
                    
                    let alert = SCLAlertView()
                    alert.addButton("Continuar", action: {
                        self.selectedBranch = branch
                        self.selectedBranch.mode = 0
                        //                        self.customTransition((self.collectionView?.indexPathForCell(cell))!)
                        Mixpanel.mainInstance().track(event: "Usuario Eligio una Franquicia",
                            properties: ["Franquicia" : branch?.name ?? "","Pantalla":"Franquicias por Categoria", "Modo":"Delivery"])
                        self.performSegueWithIdentifier("showBranchSegue", sender: self)
                    })
                    alert.addButton("Cambiar dirección", action: {
                        self.presentLeftMenuViewController(self)
                        NSNotificationCenter.defaultCenter().postNotificationName("openAddressBoxNotification", object: nil)
                    })
                    alert.showNotice("Atención", subTitle: "Tu dirección de envio es: \(MUser.sharedInstance.defaultAddress.name), estas seguro que deseas continuar?", closeButtonTitle: "Atras")
                    
                }else{
                    self.selectedBranch = branch
                    self.selectedBranch.mode = 0
                    //                    self.customTransition((self.collectionView?.indexPathForCell(cell))!)
                    
                    self.performSegueWithIdentifier("showBranchSegue", sender: self)
                }
            }else{
                self.lastTimeForPopup = NSDate()
                
                let alert = SCLAlertView()
                alert.addButton("Continuar", action: {
                    self.selectedBranch = branch
                    self.selectedBranch.mode = 0
                    //                    self.customTransition((self.collectionView?.indexPathForCell(cell))!)
                    Mixpanel.mainInstance().track(event: "Usuario Eligio una Franquicia",
                        properties: ["Franquicia" : branch?.name ?? "","Pantalla":"Franquicias por Categoria", "Modo":"Delivery"])
                    self.performSegueWithIdentifier("showBranchSegue", sender: self)
                })
                alert.addButton("Cambiar dirección", action: {
                    self.presentLeftMenuViewController(self)
                    NSNotificationCenter.defaultCenter().postNotificationName("openAddressBoxNotification", object: nil)
                })
                alert.showNotice("Atención", subTitle: "Tu dirección de envio es: \(MUser.sharedInstance.defaultAddress.name), estas seguro que deseas continuar?", closeButtonTitle: "Atras")
                
            }
            
            
        }else{
            cell.iconoPos.shake()
            cell.contView.shake()
        }
    }
    
    
    func createWaitScreen(){
        spinner?.startAnimating()
    }
    
    
    
    func dismissWaitScreen(){
        spinner?.stopAnimating()
        refreshControl?.endRefreshing()
    }
    
    var timer: NSTimer!
    
    var lastParameters:[String:AnyObject]!
    var lastTimeForBranches: NSDate!
    
    
    
    func loadBranchesForCategory(category_id: Int, useCache: Bool = true){
        
        let now = NSDate()
        lastTimeForBranches = now
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let latitude = MUser.sharedInstance.defaultAddress.latitude
        let longitude = MUser.sharedInstance.defaultAddress.longitude
        
        
        
        lastParameters = ["category_id":category_id,
                          "latitude":latitude,
                          "longitude":longitude,
                          "group_by_franchise":true]
        
        fetchingDataFromNetwork = true
        createWaitScreen()
        self.loadingBeforeEmptyDataSet = true
        if useCache {
        api.returnCachedData(.SEARCH_BRANCHES, parameters: lastParameters, completion: { (json) in
            if debugModeEnabled {
                //                print("returnCachedData for branches","called with parameters: nil",json)
            }
            if let jsonArray = json?["data"].array {
                self.updateUIForBranches(jsonArray, isCache:  true)
            }
            
        })
        }
        
        Mixpanel.mainInstance().time(event: "Tiempo de Request: \(MAPI.Command.SEARCH_BRANCHES.rawValue) para \(self.category.name ?? "")")
        api.postQueueRequest(.SEARCH_BRANCHES, parameters: lastParameters) { (json, error) in
            Mixpanel.mainInstance().track(event: "Tiempo de Request: \(MAPI.Command.SEARCH_BRANCHES.rawValue) para \(self.category.name ?? "")", properties: self.lastParameters)
            if debugModeEnabled {
                print(#function,"called with parameters: nil",json,error)
            }
            if error == nil {
                
                if let jsonArray = json?["data"].array {
                    self.updateUIForBranches(jsonArray, isCache: false)
                }
            }else{
                self.refreshControl?.endRefreshing()
            }
            
        }
    }
    
    
    
    func filterBranches (branches: [MRestaurant]) -> [MRestaurant] {
        var filtered: [MRestaurant] = []
        
        self.openRestaurants = branches.filter({ (rest) -> Bool in
            
            return ((!rest.isClosed()) && !(rest.delivery_available ?? false)) ?? false
            
        })
        
        self.openDelAvailableRestaurants = branches.filter({ (rest) -> Bool in
            
            return ((!rest.isClosed()) && (rest.delivery_available ?? false)) ?? false
            
        })
        
        self.closedRestaurants = branches.filter({ (rest) -> Bool in
            return (rest.isClosed() && !(rest.delivery_available ?? false)) ?? false
            
        })
        
        self.closedDelAvailableRestaurants = branches.filter({ (rest) -> Bool in
            
            return (rest.isClosed() && (rest.delivery_available ?? false)) ?? false
            
        })
        
        if self.openDelAvailableRestaurants?.count > 0 {
            filtered +=  self.openDelAvailableRestaurants
        }
        if self.openRestaurants?.count > 0 {
            filtered += self.openRestaurants
        }
        if self.closedDelAvailableRestaurants?.count > 0 {
            filtered += self.closedDelAvailableRestaurants
        }
        if self.closedRestaurants?.count > 0 {
            filtered += self.closedRestaurants
        }
        
        if let forFranchise = self.franchiseSelected {
            let onlyThisFranchise = filtered.filter({$0.franchise?.id == forFranchise.id})
            filtered = onlyThisFranchise
        }
        
        return filtered
    }
    
    var runningBatchUpdate: Bool = false
    var cachedJSON: [SwiftyJSON.JSON]?
    func updateUIForBranches(json: [SwiftyJSON.JSON], isCache: Bool) {
        
       
        if isCache {
            cachedJSON = json
        }else{
            if cachedJSON != nil {
                if cachedJSON! == json {
                    print("same json")
                    return
                }
            }
        }
        
       
        
        
        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            
            var tmpRestos: [MRestaurant] = []
            for data in json {
                let newBranch = MRestaurant(data: data["branch"])
                tmpRestos.append(newBranch)
            }
            
            //        tmpRestos = filterBranches(tmpRestos)
//            if tmpRestos.count == 0 {
                self.loadingBeforeEmptyDataSet = false
//            }
          
            self.fetchingDataFromNetwork = false
            
            
            
//            self.reloadRestoForDateChange() testing comment
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.filterByMode(tmpRestos, completion: { (fin) in
                    self.loadedIdx = []



                    
                    if filterType == "Delivery" {
                        self.restaurants = self.filteredDelivery
                    }else{
                        self.restaurants = self.filteredPickup
                    }
                    self.collectionView?.reloadEmptyDataSet()
                    self.collectionView?.reloadData()
                    self.dismissWaitScreen()
                    self.refreshControl?.endRefreshing()

     
                })
                
                
            })
        })
        
        
        
    }
    
    func reloadRestoForDateChange(){
        let restoForNoti = self.restaurants.sort({ (r1, r2) -> Bool in
            if let  r1r = r1.reloadTime(), r2r = r2.reloadTime() {
                return (r1r.compare(r2r) == NSComparisonResult.OrderedAscending)
            }
            return false
        })
        if let primero = restoForNoti.first {
            if let reloadTime = primero.reloadTime() {
                if reloadTime.timeIntervalSinceDate(getCurrentLocalDate()) > 0 {
                    NSTimer.scheduledTimerWithTimeInterval(reloadTime.timeIntervalSinceDate(getCurrentLocalDate()), target: self, selector: #selector(RestaurantesCollectionViewController.refreshTableAfterDateChange), userInfo: nil, repeats: false)
                    print("Timer set for reloading at \(reloadTime), with interval \(reloadTime.timeIntervalSinceDate(getCurrentLocalDate())), date \(getCurrentLocalDate())")
                    
                }
            }
            
        }
    }
    
    func refreshTableAfterDateChange(){
        self.collectionView?.reloadEmptyDataSet()
        self.collectionView?.reloadData()
        reloadRestoForDateChange()
    }
    
    
    
    
    // MARK: UICollectionViewDelegate
    
    /*
     // Uncomment this method to specify if the specified item should be highlighted during tracking
     override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment this method to specify if the specified item should be selected
     override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
     return false
     }
     
     override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
     return false
     }
     
     override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
     
     }
     */
    
    // MARK: EmptyDataSetSource
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        if connectedToNetwork() {
            if loadingBeforeEmptyDataSet {
                let image = UIImage(named: "loading-resto")
                return image
            }else{
                let image = UIImage(named: "menu-vacio")
                return image
            }
        }else{
            return UIImage(named: "icono-error-red")
        }
        
    }
    
    func imageAnimationForEmptyDataSet(scrollView: UIScrollView!) -> CAAnimation! {
        if connectedToNetwork() {
            let animation = CABasicAnimation(keyPath: "transform")
            animation.fromValue = NSValue(CATransform3D: CATransform3DIdentity)
            animation.toValue = NSValue(CATransform3D:CATransform3DMakeRotation(CGFloat(M_PI_2), 0.0, 0.0, 1.0))
            animation.duration = 0.25
            animation.cumulative = true
            animation.repeatCount = MAXFLOAT
            return animation
        }else{
            return nil
        }
    }
    
    
    let titleColor: UIColor = .whiteColor()
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        if loadingBeforeEmptyDataSet {
            if connectedToNetwork() {
                return nil
            }else{
                let noConection = NSAttributedString(string: NSLocalizedString("No hay conexión.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:titleColor])
                
                return noConection
            }
        }
        if connectedToNetwork() {
            var text: String = ""
            if filterType == "Delivery" {
                text = "No disponemos de locales con Delivery para tu dirección"
            }else{
                text = "No disponemos de locales cercanos para Pasar a Buscar"
            }
            let emptyCart = NSAttributedString(string: text,attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:titleColor])
            
            return emptyCart
        }else{
            let noConection = NSAttributedString(string: NSLocalizedString("No hay conexión.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
            
            return noConection
        }
        
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        if loadingBeforeEmptyDataSet {
            if connectedToNetwork() {
                let gettingData = NSAttributedString(string: NSLocalizedString("Buscando restaurantes...", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:titleColor])
                
                return gettingData
            }else{
                return nil
            }
        }else{
            if connectedToNetwork() {
                var text : String = ""
                if filterType == "Delivery" {
                    text = MUser.sharedInstance.defaultAddress.name.uppercaseString
                }else{
                    text = ""
                }
                let subTitle = NSAttributedString(string: text,attributes: [NSFontAttributeName:UIFont.systemFontOfSize(20),NSForegroundColorAttributeName:titleColor])
                return subTitle
            }else{
                return nil
            }
            
        }
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        if loadingBeforeEmptyDataSet {
            if connectedToNetwork() {
                return nil
            }else{
                let title = NSAttributedString(string: "Volver a Intentar", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(18),NSForegroundColorAttributeName:titleColor])
                return title
            }
        }else{
            var text : String = ""
            if filterType == "Delivery" {
                text = "Ver Locales para Pasar a Buscar"
            }else{
                text = "Ver Locales para Delivery"
            }

            let title = NSAttributedString(string: text, attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(18),NSForegroundColorAttributeName:UIColor.whiteColor()])
            return title
            
        }
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        
        if filterType == "Delivery" {
            self.restaurants = filteredPickup
            filterType = "Pasar a Buscar"
            UIView.animateWithDuration(1.0, animations: {
                self.collectionView?.backgroundColor = UIColor(hexString: "#F0F0F0")
            })
            self.collectionView?.reloadEmptyDataSet()
            self.collectionView?.reloadData()
        }else{
            filterType = "Delivery"
            UIView.animateWithDuration(1.0, animations: {
                self.collectionView?.backgroundColor = UIColor.clearColor()
            })
            self.restaurants = filteredDelivery
            self.collectionView?.reloadEmptyDataSet()
            self.collectionView?.reloadData()
        }
        
    }
    
    func emptyDataSetShouldAnimateImageView(scrollView: UIScrollView!) -> Bool {
        return loadingBeforeEmptyDataSet
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    
    // MARK: EmptyDataSetDelegate
    
}
func getCurrentLocalDate()-> NSDate {
    var now = NSDate()
    let nowComponents = NSDateComponents()
    let calendar = NSCalendar.currentCalendar()
    nowComponents.year = NSCalendar.currentCalendar().component(NSCalendarUnit.Year, fromDate: now)
    nowComponents.month = NSCalendar.currentCalendar().component(NSCalendarUnit.Month, fromDate: now)
    nowComponents.day = NSCalendar.currentCalendar().component(NSCalendarUnit.Day, fromDate: now)
    nowComponents.hour = NSCalendar.currentCalendar().component(NSCalendarUnit.Hour, fromDate: now)
    nowComponents.minute = NSCalendar.currentCalendar().component(NSCalendarUnit.Minute, fromDate: now)
    nowComponents.second = NSCalendar.currentCalendar().component(NSCalendarUnit.Second, fromDate: now)
    nowComponents.timeZone = NSTimeZone(name: "America/Asuncion")
    now = calendar.dateFromComponents(nowComponents)!
    return now
}

