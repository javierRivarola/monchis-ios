//
//  AddressBoxViewController.swift
//  Monchis
//
//  Created by jrivarola on 6/19/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

protocol AddressBoxDelegate {
    func cancelPressed()
    func savePressed()
}

class AddressBoxViewController: UIViewController {
    var delegate: AddressBoxDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
   
    @IBOutlet weak var street1: UITextField!
    @IBOutlet weak var street2: UITextField!
    @IBOutlet weak var number: UITextField!
    @IBOutlet weak var referals: UITextField!
    
    
    @IBAction func cancelPressed(sender: UIButton) {
        delegate?.cancelPressed()
    }
 
    @IBAction func savePressed(sender: UIButton) {
        delegate?.savePressed()
    }
}
