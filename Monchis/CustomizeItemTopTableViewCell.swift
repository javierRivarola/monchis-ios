//
//  CustomizeItemTopTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 9/14/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

protocol ProductFavoriteDelegate{
    func didPressFavoriteBtn()
    func didPressShareBtn()
}

class CustomizeItemTopTableViewCell: UITableViewCell {
    var delegate: ProductFavoriteDelegate?
    @IBOutlet weak var itemImageView: UIImageView! 
    @IBOutlet weak var itemCurrentPrice: UILabel!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var descripcionProducto: UILabel!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet weak var shareBtn: UIButton!

    @IBAction func didPressFavoriteBtn(sender: UIButton) {
        delegate?.didPressFavoriteBtn()
    }
    @IBAction func sharePressed(sender: UIButton) {
        delegate?.didPressShareBtn()
    }
}
