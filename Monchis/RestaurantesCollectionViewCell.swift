//
//  RestaurantesCollectionViewCell.swift
//  Monchis
//
//  Created by jrivarola on 6/22/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

protocol DeliveryPickupDelegate {
    func didPressDelivery(branch: MRestaurant?,cell: RestaurantesCollectionViewCell)
    func didPressPickup(branch: MRestaurant?, cell: RestaurantesCollectionViewCell)
}

class RestaurantesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    var delegate : DeliveryPickupDelegate?
    @IBOutlet weak var contView: UIView!
   
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var seeMapBtnImg: UIButton!
    @IBOutlet weak var seeMapBtnTitle: UIButton!
    @IBOutlet weak var timeLblBackground: UIView!

    @IBOutlet weak var seeMapBgBtn: UIButton!
    @IBOutlet weak var pickupView: UIView!
    @IBOutlet weak var shadowView: UIView!
    var branch: MRestaurant?

    @IBOutlet weak var seeMapBtn: UIButton!
    @IBOutlet weak var minimumOrderLbl: UILabel!
    @IBOutlet weak var iconoPos: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    var indexPath: NSIndexPath?
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.layer.cornerRadius = 10
//        self.clipsToBounds = true
    

    }
    var shadowLayer: CAShapeLayer!
        override var layoutMargins: UIEdgeInsets {
        get { return UIEdgeInsetsZero }
        set(newVal) {}
    }
    
}
