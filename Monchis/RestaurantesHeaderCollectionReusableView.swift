//
//  RestaurantesHeaderCollectionReusableView.swift
//  Monchis
//
//  Created by jrivarola on 17/12/15.
//  Copyright © 2015 cibersons. All rights reserved.
//

import UIKit

class RestaurantesHeaderCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var titulo: UILabel!
}
