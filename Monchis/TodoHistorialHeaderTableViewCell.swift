//
//  TodoHistorialTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 10/5/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class TodoHistorialHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var restoBtn: UIButton!
    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var deliveryAddressLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
