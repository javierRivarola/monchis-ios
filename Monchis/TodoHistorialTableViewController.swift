//
//  TodoHistorialTableViewController.swift
//  Monchis
//
//  Created by jrivarola on 10/5/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import DZNEmptyDataSet
import ReachabilitySwift
import Mixpanel


class TodoHistorialTableViewController: UITableViewController,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource,FavoritesSelectionDelegate,HistorialContentTVCDelegate, BranchSelectionDelegate {
    
    
    // MARK: Properties
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    let api = MAPI.SharedInstance
    let user = MUser.sharedInstance
    let limitResultsTo = 15 //limit the number of results
    var loadingBeforeEmptyDataSet:Bool = true
    
    // MARK: Constants
    
    struct ReuseID {
        static let headerCell = "headerCellHistorialReuseID"
        static let reOrderCell = "reOrderCellHistorialReuseID"
        static let orderDetailsCell = "orderDetailsHistorialReuseID"
        static let historialContentReuseCell = "historialContentReuseCell"
    }
    
    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.emptyDataSetDelegate = self
        tableView.emptyDataSetSource = self
        setupUI()
        configurePullToRefresh()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TodoHistorialTableViewController.scrollToOrder(_:)), name: "scrollToOrderNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TodoHistorialTableViewController.reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TodoHistorialTableViewController.pullToRefresh(_:)), name: kUserLoggedInNotification, object: nil)


    }
    
    func reachabilityChanged(notification: NSNotification) {
        
        let reachability = notification.object as! Reachability
        
        if reachability.isReachable() {
            self.tableView.reloadEmptyDataSet()
            if user.logged {
                self.loadHistory()
            }
            if reachability.isReachableViaWiFi() {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            print("Network not reachable")
            
        }
    }
    
    var cameFromViewWillAppear: Bool = false
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if user.authenticationMethod != .Unregistered && user.logged {
            cameFromViewWillAppear = true
            loadHistory()
        }else{
            loadingBeforeEmptyDataSet = false
            tableView.reloadEmptyDataSet()
        }
        
    }
    
    func scrollToOrder(not: NSNotification){
        if let index = not.object as? Int {
            self.tableView.scrollToRowAtIndexPath(NSIndexPath(forItem: 0, inSection: index), atScrollPosition: .Top, animated: true)
        }
    }
    
    func configurePullToRefresh(){
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = UIColor.whiteColor()
        let title = NSMutableAttributedString()
        let tira = NSAttributedString(string: "Tira para actualizar", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        title.appendAttributedString(tira)
        refreshControl?.attributedTitle = title
        refreshControl?.addTarget(self, action: #selector(TodoHistorialTableViewController.pullToRefresh(_:)), forControlEvents: .ValueChanged)
        self.tableView?.alwaysBounceVertical = true
        refreshControl?.layer.zPosition += 1
    }
    
    func pullToRefresh(sender: UIRefreshControl){
        cameFromViewWillAppear = true
        loadHistory()
    }
    
    
    override func viewWillLayoutSubviews() {
        if let navContr = self.navigationController {
            if navContr.respondsToSelector(Selector("interactivePopGestureRecognizer")) {
                navContr.interactivePopGestureRecognizer?.enabled = false
            }
            
        }
    }
    var headerMonchisImage: UIImageView!
    func setupUI(){
        
        let bg = UIImageView(image: UIImage(named: "background-completoChico"))
        bg.contentMode = .ScaleAspectFill
        bg.clipsToBounds = true
        self.tableView.backgroundView = bg
        
        bg.layer.zPosition = -1
        self.tableView.estimatedRowHeight = 425
        self.tableView.rowHeight = UITableViewAutomaticDimension
        let img = UIImage(named: "texto-monchis-header")
        headerMonchisImage = UIImageView(frame: CGRectMake(0, 0, 50, 25))
        headerMonchisImage.image = img
        headerMonchisImage.contentMode = .ScaleAspectFit
        self.navigationItem.titleView = headerMonchisImage
        
    }
    
    
    func createWaitScreen(){
        
        //        let spinner = MBProgressHUD.showHUDAddedTo(UIApplication.sharedApplication().keyWindow, animated: true)
        //        spinner.mode = MBProgressHUDMode.Indeterminate
        //        spinner.labelText = NSLocalizedString("Obteniendo datos..",comment: "Buscando datos del carrito del usuario")
        //        spinner.labelFont = UIFont(name: "OpenSans-Semibold", size: 14.0)
        spinner.startAnimating()
        
    }
    
    func dismissWaitScreen(){
        // MBProgressHUD.hideAllHUDsForView(UIApplication.sharedApplication().keyWindow, animated: true)
        spinner.stopAnimating()
        refreshControl?.endRefreshing()
    }
    
    
    func loadHistory(){
        loadingBeforeEmptyDataSet = true
        
        createWaitScreen()
        let offset = cameFromViewWillAppear ? 0 : self.user.orderHistory.count
        let parameters =   [
            "limit":limitResultsTo,
            "state": ["delivered","canceled"],
            "offset": offset
        ]
        
        api.jsonRequest(.ORDER_HISTORY, parameters: parameters as? [String : AnyObject], cacheRequest: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                if let json = json {
                    if let success = json["success"].bool {
                        if success {
                            if let jsonOrders = json["data"]["orders"].array {
                                var tmpOrders:[MOrder] = []
                                
                                for order in jsonOrders {
                                    let newOrder:MOrder = MOrder(json: order)
                                    tmpOrders.append(newOrder)
                                }
                                if self.cameFromViewWillAppear  {
                                    self.user.orderHistory = tmpOrders
                                    self.cameFromViewWillAppear = false
                                }else{
                                    self.user.orderHistory += tmpOrders
                                }
                                NSNotificationCenter.defaultCenter().postNotificationName(kHistoryDidReload, object: nil)
                                self.dismissWaitScreen()
                                self.loadingBeforeEmptyDataSet = false
                                self.tableView.reloadData()
                            }
                            
                        }else{
                            self.dismissWaitScreen()
//                            let message = json["data"].string ?? "No error message"
//                            let titleLocalizable = NSLocalizedString("Error",comment: "error")
//                            let alert = UIAlertController(title: titleLocalizable, message: message, preferredStyle: .Alert)
//                            let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
//                            alert.addAction(ok)
//                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
                
            }else{
                self.dismissWaitScreen()
//                let titleLocalizable = NSLocalizedString("Error",comment: "error")
//                let alert = UIAlertController(title: titleLocalizable, message: error, preferredStyle: .Alert)
//                let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
//                alert.addAction(ok)
//                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return user.orderHistory.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 1
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let order = user.orderHistory[indexPath.section]
        let cell = tableView.dequeueReusableCellWithIdentifier(ReuseID.historialContentReuseCell, forIndexPath: indexPath) as! HistorialContentTableViewCell
        cell.delegate = self
        cell.calificarBtn?.imageView?.contentMode = .ScaleAspectFit

        cell.parentController = self
        cell.collectionView?.tag = indexPath.section
        cell.order = order
        if order.status == .CANCELED {
            cell.cancelView?.hidden = false
        }else{
            cell.cancelView?.hidden = true
        }
        if order.feedback_reasons.count > 0 || order.feedback_comments != nil || order.feedback_stars != nil {
            cell.ratingView?.hidden = false
            cell.calificarBtn?.hidden = true
            for i in 1...5 {
                let button = cell.viewWithTag(i*1000) as? UIButton
                button?.setImage(UIImage(named: "estrella-off"), forState: .Normal)
            }
            
            let tag = order.feedback_stars ?? 1
            
            for i in 1...tag {
                let button = cell.viewWithTag(i*1000) as? UIButton
                button?.setImage(UIImage(named: "estrella-on"), forState: .Normal)
            }
        }else{
            cell.calificarBtn?.hidden = false
            cell.ratingView?.hidden = true
        }

        cell.orderTotalLbl?.text = order.total?.asLocaleCurrency
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = user.locale
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let createdDate = order.confirmed_at {
            if let date = dateFormatter.dateFromString(createdDate) {
                dateFormatter.dateFormat = "d MMM yyyy HH:mm"
                cell.orderDateLbl?.text = dateFormatter.stringFromDate(date)
            }
        }
      
        if order.delivery_type == 1 {
            cell.branchNameLbl?.text = order.branch?.franchise?.name
        }else{
            cell.branchNameLbl?.text = order.branch?.name
        }
        cell.collectionView.reloadData()
        return cell
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "seeOrderDetailsSegue" {
            let dvc = segue.destinationViewController as! OrderDetailsViewController
            dvc.order = selectedOrder
        }
        if segue.identifier == "calificarSegue" {
            let dvc = segue.destinationViewController as! CalificarPedidoTableViewController
            dvc.order = selectedOrder
        }
        
    }
    
    
    // MARK: EmptyDataSetSource
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        let image = UIImage(named: "historial-vacio")
        return image
    }
    
    func imageAnimationForEmptyDataSet(scrollView: UIScrollView!) -> CAAnimation! {
        
        let animation = CABasicAnimation(keyPath: "transform")
        
        animation.fromValue = NSValue(CATransform3D: CATransform3DIdentity)
        animation.toValue = NSValue(CATransform3D:CATransform3DMakeRotation(CGFloat(M_PI_2), 0.0, 0.0, 1.0))
        
        animation.duration = 0.25
        animation.cumulative = true
        animation.repeatCount = MAXFLOAT
        return animation
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        if loadingBeforeEmptyDataSet {
            let text = NSAttributedString(string: NSLocalizedString("Buscando historial..", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
            
            return text
        }else{
            let text = NSAttributedString(string: NSLocalizedString("Tu historial está vacio.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
            
            return text
        }
        
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let subTitle = NSAttributedString(string: NSLocalizedString("Todos los pedidos finalizados que realices aparecen aqui.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(15),NSForegroundColorAttributeName:UIColor.whiteColor()])
        return subTitle
    }
    
    func emptyDataSetShouldAnimateImageView(scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    
    
    // MARK: FavoritesDelegate
    
    func didSelectProductFavorite(cell: OrderProductHistoryTableViewCell) {
        let favorite = MFavorite()
        let productIP = tableView.indexPathForCell(cell)!
        let product = user.orderHistory[productIP.section].products[productIP.row - 1]
        favorite.product = product
        favorite.branch = user.orderHistory[productIP.section].branch
        
        if let _ = MUser.sharedInstance.productFavorites.indexOf(product){
            cell.favoriteBtn.setImage(unFavorited, forState: .Normal)
        }else{
            cell.favoriteBtn.setImage(favorited, forState: .Normal)
        }
        
        addFavorite(favorite,cell: cell)
        
        
    }
    
    func addFavorite(favorite: MFavorite, cell:OrderProductHistoryTableViewCell ){
        let parameters = ["product_id":favorite.product.id ?? 0,"branch_id":favorite.branch.id ?? 0]
        api.jsonRequest(.FAVORITE_PRODUCT, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    
                }else{
                    cell.favoriteBtn.setImage(unFavorited, forState: .Normal)
                    let error = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Lo sentimos!", message: error, preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }else{
                cell.favoriteBtn.setImage(unFavorited, forState: .Normal)
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func deleteFavorite(favorite: MFavorite){
        let parameters = ["favorite_id":""]
        api.jsonRequest(.DELETE_PRODUCT_FAVORITE, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    /* handle favorite removal */
                    
                }else{
                    let error = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Lo sentimos!", message: error, preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }else{
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    override func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cellHeight = cell.bounds.height
    }
    
    // MARK: HistorialContentTVCDelegate
    
    func didPressOrderAgainOrder(order: MOrder) {
        
    }
    
    var selectedProduct: MProduct!
    func didPressOrderAgainProduct(product: MProduct, order: MOrder) {
        deliveryOrPickupHistorySearch(order.branch, completion: { (isDelivery, newBranch) in
            Mixpanel.mainInstance().track(event: "Usuario Eligio un Producto", properties: ["Producto" : product.name ?? "","Pantalla":"Historial", "Franquicia": newBranch.franchise?.name ?? ""])
            let productoVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
            
            
            if isDelivery == nil || isDelivery == true {
                
                productoVC.product = product
                
                
                
                
                let branch = newBranch
                productoVC.restaurant = branch
                
                branch.mode = 0
                productoVC.restaurant = branch

                self.navigationController?.pushViewController(productoVC, animated: true)
                
                
            }else{//pickup
//                productoVC.product = product
//
//                let branch = order.branch
//                productoVC.restaurant = branch
//                
//                branch?.mode = 1
//                productoVC.restaurant?.franchise = branch.franchise
//                productoVC.restaurant = branch
//                productoVC.refreshBranch()
//                self.navigationController?.pushViewController(productoVC, animated: true)
                
                
//                
                let branchSelection = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                branchSelection.franchise = newBranch.franchise
                branchSelection.delegate = self
                self.selectedProduct = product
                self.navigationController?.pushViewController(branchSelection, animated: true)

                
            }
        })

    }
    
    var offset: Int = 0
    var totalProducts: Int = 0
    var cellHeight: CGFloat = 0
    // loading on demand
    
    var footerView: UIView = UIView(frame: CGRectZero)
    
    func createFooterViewForLoading(){
        footerView.frame = CGRectMake(0, 0, self.view.bounds.width, 40)
        let activity = UIActivityIndicatorView()
        activity.hidesWhenStopped = true
        activity.color = UIColor.whiteColor()
        activity.center = footerView.center
        activity.tag = 10
        footerView.addSubview(activity)
        activity.startAnimating()
    }
    
    
    override func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let endOfTable: Bool = (scrollView.contentOffset.y >= (CGFloat(self.user.orderHistory.count * Int(cellHeight)) - scrollView.frame.size.height))
            if endOfTable && !scrollView.dragging && !scrollView.decelerating {
                createFooterViewForLoading()
                self.loadHistory()
                self.tableView.tableFooterView = footerView
         
        }else{
            tableView.tableFooterView = UIView(frame: CGRectZero)
        }
        
        
    }
    
    var selectedOrder: MOrder!
    func didPressSeeDetails(order: MOrder) {
        selectedOrder = order
        self.performSegueWithIdentifier("seeOrderDetailsSegue", sender: self)
    }
    
    func didSelectBranch(branch: MRestaurant,order: MOrder?)
    {

            let customizeProductVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
            customizeProductVC.restaurant = branch
            customizeProductVC.product = selectedProduct
            self.navigationController?.pushViewController(customizeProductVC, animated: true)
        
        
    }
    
}
