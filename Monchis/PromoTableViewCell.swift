//
//  PromoTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 8/27/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class PromoTableViewCell: UITableViewCell {

    @IBOutlet weak var carousel: iCarousel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        carousel.tag = 0
        carousel.type = .CoverFlow
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.layoutIfNeeded()
    }
   

}
