//
//  MenuCategoriaViewController.swift
//  Monchis
//
//  Created by jrivarola on 9/9/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit
import MBProgressHUD
import Haneke
import DZNEmptyDataSet
import SwiftyJSON
import ReachabilitySwift
import Crashlytics
import ImageOpenTransition
import Mixpanel

class MenuCategoriaViewController: UIViewController,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource,SearchResultSelectedDelegate, BranchSelectionDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var selectedMenu: MMenu!
    var restaurant: MRestaurant!
    var categories:[MCategory] = []
    var searchController: UISearchController!
    var searchTableViewController:SearchTableViewController!
    var searchBar: UISearchBar!
    var selectedProduct: MProduct!
    let api = MAPI.SharedInstance
    var filteredProducts: [MProduct] = []
    var products: [MProduct] = []
    var headerMonchisImage: UIImageView!
    var loadingBeforeEmptyDataSet: Bool = true
    var selectedScope: Int = 0
    var branchMode: Int = 0
    var selectedProductSearch: MProduct!
    var selectedBranchSearch:MRestaurant!
    
    var refreshControl: UIRefreshControl!
    var localSearchActive: Bool = false
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var menuCategoriaLbl: UILabel!
    @IBOutlet weak var noCoverRestoIconImgView: UIImageView!
    @IBOutlet weak var closedRestoIconImgView: UIImageView!
    @IBOutlet weak var statesSpinner: UIActivityIndicatorView!
    @IBOutlet weak var leadingWidthRestoImgViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoRestoBtn: UIButton!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var restoHeaderImageView: UIImageView!
    struct Constants {
        static let menuCategoriaReuseID = "menuCategoriaReuseID"
        static let ReuseSearchCellID = "ReuseSearchCellID"
        
    }
    
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var pickupDeliveryView: UIView!
    struct Segues {
        static let customizeSelectedItem = "customizeSelectedItemSegue"
        static let infoResto = "infoRestoSegueFromMenu"
    }
    @IBOutlet weak var iconoDeliveryPickup: UIImageView!
    
    @IBAction func editModePressed(sender: UIButton) {
        selectedBranchSearch = nil
        selectedProductSearch = nil
        changeBranchMode(self.restaurant) { (branch) in
            self.branchMode = branch.mode
            if branch.mode == 1 || branch.mode == 2{
                let bsvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                bsvc.delegate = self
                bsvc.franchise = self.restaurant.franchise
                self.navigationController?.pushViewController(bsvc, animated: true)
            }else{
                Mixpanel.mainInstance().track(event: "Usuario Cambio Modo de Local",
                                              properties: ["Pantalla":"Productos de Categoria de Menu de Franquicia", "Franquicia": self.restaurant.franchise?.name ?? "","Antes":self.restaurant.mode,"Despues":branch.mode, ])
                NSNotificationCenter.defaultCenter().postNotificationName("refreshBranchForModeChangeNotification", object: nil)
                self.refreshBranchUI()
                self.refreshBranch()

            }

        }
    }
   
    @IBAction func shareBranchPressed(sender: UIButton) {
        
            var array:[AnyObject] = []
            let text = "Mira este Local en Monchis! - " + self.restaurant.name
            let urlString = "https://admin.monchis.com.py/?q="
            let query = "franchiseid=\(self.restaurant.franchise!.id!)&type=branch"
            let hash = query.toBase64() ?? ""
            
            let link = NSURL(string: urlString + hash)!
            array.append(text)
            array.append(link)
            if let img = self.restoHeaderImageView.image {
                array.insert(img, atIndex: 0)
            }
            let activityViewController = UIActivityViewController(activityItems: array, applicationActivities: nil)
            activityViewController.setValue("Mira este Local en Monchis!", forKey: "subject")
            
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        activityViewController.completionWithItemsHandler = { activityType, success, items, error in
            if !success {
                print("cancelled")
                return
            }
            
            Answers.logShareWithMethod(activityType ?? "",
                                       contentName: self.restaurant.name,
                                       contentType: "Restaurante",
                                       contentId: "\(self.restaurant.id!)",
                                       customAttributes: nil)
         
            Mixpanel.mainInstance().track(event: "Usuario Compartio una Franquicia",
                                          properties: ["Franquicia" : self.restaurant.franchise?.name ?? "","Pantalla":"Categorias de Productos de Menu de Franquicia","Categoria de Menu":self.selectedMenu.name, "Categoria de Menu ID":self.selectedMenu.id ?? 0 , "Metodo": activityType ?? ""])
            
        }
            self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    func didSelectBranch(branch: MRestaurant, order: MOrder?) {
        if selectedProductSearch != nil && selectedBranchSearch == nil {
            let customizeProductVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
            customizeProductVC.restaurant = branch
            customizeProductVC.product = selectedProductSearch
            self.navigationController?.pushViewController(customizeProductVC, animated: true)
        }else if selectedBranchSearch != nil && selectedProductSearch == nil{
            let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
            branch.mode = 1
            restaurantVC.restaurant = branch
            self.navigationController?.pushViewController(restaurantVC, animated: true)
        }else{
            Mixpanel.mainInstance().track(event: "Usuario Cambio Modo de Local",
                                          properties: ["Pantalla":"Productos de Categoria de Menu de Franquicia", "Franquicia": self.restaurant.franchise?.name ?? "","Antes":self.restaurant.mode,"Despues":branch.mode, ])
        self.restaurant = branch
        self.refreshBranchUI()
        }
       
    }
    
    @IBOutlet weak var pickupDeliveryHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblDeliveryPickup: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.transitioningDelegate = self.imageScalePresentTransition

        registerForNotifications()
        configureSearch()
        setupUI()
        animationsForStates()
        //configurePullToRefresh()
        Answers.logContentViewWithName("Usuario vio de menu de Local.",
                                       contentType: "Locales",
                                       contentId: "\(self.selectedMenu!.id)",
                                       customAttributes: ["Local":self.restaurant.id!])
        loadMenuItems()
    }
    
    func setupUI(){
        searchBtn?.imageView?.contentMode = .ScaleAspectFit

        self.branchMode = restaurant.mode
        refreshBranchUIForModeChange()
        restoHeaderImageView.layer.cornerRadius = restoHeaderImageView.bounds.width/2
        restoHeaderImageView.clipsToBounds = true
        self.restaurant?.franchise?.header_attachment?.width = 45
        self.restaurant?.franchise?.header_attachment?.height = 45
        if let imageURL = self.restaurant?.franchise?.header_attachment?.imageComputedURL {
            let format = Format<UIImage>(name: "header")

            restoHeaderImageView.hnk_setImageFromURL(imageURL, format: format)
            if !imageCacheKeys.contains(imageURL.URLString) {
                imageCacheKeys.append(imageURL.URLString)
            }
            cacheFormats[imageURL.URLString] = restoHeaderImageView.hnk_format.name

        }
        shadowView.layer.shadowColor = UIColor.blackColor().CGColor
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shadowOffset = CGSizeZero
        shadowView.layer.shadowRadius = 10
 
        if headerBlurOn { //blur the image
            let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light)) as UIVisualEffectView
            visualEffectView.frame = restoHeaderImageView.bounds
            restoHeaderImageView.addSubview(visualEffectView)
        }
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 400
        // Do any additional setup after loading the view.
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        menuCategoriaLbl.text = self.selectedMenu?.name
        self.title = self.selectedMenu?.name
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        //favorites
        
//        if let _ = MUser.sharedInstance.branchesFavorites.indexOf({$0.id == restaurant.id}) {
//            self.favoriteBtn.setImage(favorited, forState: .Normal)
//        }else{
//            self.favoriteBtn.setImage(unFavorited, forState: .Normal)
//            
//        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

//        if let _ = MUser.sharedInstance.branchesFavorites.indexOf({$0.id == restaurant.id}) {
//            self.favoriteBtn.setImage(favorited, forState: .Normal)
//        }else{
//            self.favoriteBtn.setImage(unFavorited, forState: .Normal)
//            
//        }
        animationsForStates()
//        refreshBranch()
    }
    
    @IBOutlet weak var bgImgView: UIImageView!
    
    
    
    var imageScalePresentTransition :ImageScaleTransitionDelegate? = nil

    
    func refreshBranchUIForModeChange(){
        if restaurant.mode == 0 {
            noCoverRestoIconImgView.hidden = false

            iconoDeliveryPickup.image = UIImage(named: "icono-delivery")
            lblDeliveryPickup.text = "Delivery a: \(MUser.sharedInstance.defaultAddress.name)"
            infoRestoBtn.setTitle(restaurant?.franchise?.name?.uppercaseString, forState: .Normal)
            UIView.animateWithDuration(1.0, animations: {
                self.pickupDeliveryView?.backgroundColor = UIColor(hexString: "#363A40")
                self.bgImgView.alpha = 1
                self.menuCategoriaLbl.textColor = UIColor.whiteColor()

            })
        }else{
            noCoverRestoIconImgView.hidden = true

            infoRestoBtn.setTitle(restaurant?.name.uppercaseString, forState: .Normal)
            iconoDeliveryPickup.image = UIImage(named: "icono-pickup-activo-sobre-activo")
            lblDeliveryPickup.text = "Para buscar de sucursal"
            UIView.animateWithDuration(1.0, animations: {
                self.pickupDeliveryView?.backgroundColor = UIColor(hexString: "#CF1B1B")
                self.bgImgView.alpha = 0
                self.menuCategoriaLbl.textColor = UIColor.darkGrayColor()

                
            })
        }
        self.tableView?.reloadEmptyDataSet()
    }

    func refreshBranchUI() {
        refreshBranchUIForModeChange()
        animationsForStates()
    }

    
    
    func animatonForClosedState() {
        
        closedRestoIconImgView.hidden = false
        UIView.animateWithDuration(0.5, delay: 0, options: [.Autoreverse,.Repeat], animations: { () -> Void in
            self.closedRestoIconImgView.alpha = 0.1
            }, completion: { (fin) -> Void in
        })
      
    }
    
    func animationForOutOfCoverState(){
        if restaurant?.mode == 0 {
            noCoverRestoIconImgView?.hidden = false
        }else{
            noCoverRestoIconImgView?.hidden = true
        }
        UIView.animateWithDuration(0.5, delay: 0, options: [.Autoreverse,.Repeat], animations: { () -> Void in
            self.noCoverRestoIconImgView.alpha = 0.1
            }, completion: { (fin) -> Void in
                
        })
        
    }
    
    func animationsForStates(){
        closedRestoIconImgView.alpha = 1
        noCoverRestoIconImgView.alpha = 1
        noCoverRestoIconImgView.hidden = true
        closedRestoIconImgView.hidden = true

//        if (restaurant.delivery_available!) && (!restaurant.isClosed()) { // open an delivery available
//            leadingWidthRestoImgViewConstraint.constant = 8
//         
//        }
//        
//        if (restaurant.delivery_available!) && (restaurant.isClosed()) {
//            leadingWidthRestoImgViewConstraint.constant = 31
//
//            animatonForClosedState()
//        }
//        if !(restaurant.delivery_available!) && !(restaurant.isClosed()) {
//            if restaurant.mode == 0 {
//                leadingWidthRestoImgViewConstraint?.constant = 31
//            }else{
//                leadingWidthRestoImgViewConstraint?.constant = 8
//            }
//            animationForOutOfCoverState()
//        }
//        
//        if !(restaurant.delivery_available!) && (restaurant.isClosed()) {
//            leadingWidthRestoImgViewConstraint.constant = 31
//
//            animatonForClosedState()
//            animationForOutOfCoverState()
//            
//        }
        if restaurant != nil {
            if restaurant.mode == 0 {
                if restaurant.hasDelivery() && (restaurant.isOpen()) { // open an delivery available
                    leadingWidthRestoImgViewConstraint?.constant = 8
                }
                if restaurant.hasDelivery() &&  (!restaurant.isOpen()) {
                    leadingWidthRestoImgViewConstraint?.constant = 31
                    animatonForClosedState()
                }
                if !restaurant.hasDelivery() && (restaurant.isOpen()) {
                    leadingWidthRestoImgViewConstraint?.constant = 31
                    animationForOutOfCoverState()
                }
                if !restaurant.hasDelivery() && (!restaurant.isOpen()) {
                    leadingWidthRestoImgViewConstraint?.constant = 31
                    animationForOutOfCoverState()
                    animatonForClosedState()
                }
            }else{
                if restaurant.isOpen() {
                    leadingWidthRestoImgViewConstraint?.constant = 8
                }else{
                    leadingWidthRestoImgViewConstraint?.constant = 31
                    animatonForClosedState()
                }
            }
        }

        UIView.animateWithDuration(0.2) {
            self.view.setNeedsLayout()
        }
    }
    
    func configureSearch(){
        //SearchBar Controller
        searchTableViewController = storyboard?.instantiateViewControllerWithIdentifier("SearchTableViewController") as! SearchTableViewController
        searchController = UISearchController(searchResultsController: searchTableViewController)
        
        searchController.hidesNavigationBarDuringPresentation = false
        //searchController.searchBar.searchBarStyle = UISearchBarStyle.Minimal
        
        searchController.searchBar.sizeToFit()
        searchBar = searchController.searchBar
        searchBar.tintColor = UIColor(hexString: "#FF8000")
        
        searchBar.translucent = false
        
        searchBar.delegate = self
        for subView in searchBar.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.attributedPlaceholder =  NSAttributedString(string:"Buscar.. (min 3 caracteres)",
                                                                          attributes:[NSFontAttributeName: UIFont(name: "OpenSans-Semibold", size: 13)!])
                }
            }
        }
        searchBar.scopeButtonTitles = ["Productos","Restaurantes"]

        
        
        searchTableViewController.definesPresentationContext = true
        searchTableViewController.searchDelegate = self
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = true // important
        //self.navigationItem.titleView = searchBar
        searchController.delegate = self
        
       
      

    }
    
    func configurePullToRefresh(){
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.whiteColor()
        let title = NSMutableAttributedString()
        let tira = NSAttributedString(string: "Tira para", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        let refrescar = NSAttributedString(string: " actualizar", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSForegroundColorAttributeName: UIColor(hexString: "#6BBA3B")!])
        title.appendAttributedString(tira)
        title.appendAttributedString(refrescar)
        refreshControl.attributedTitle = title
        refreshControl.addTarget(self, action: #selector(MenuCategoriaViewController.pullToRefresh(_:)), forControlEvents: .ValueChanged)
        self.tableView?.addSubview(refreshControl)
    }
    
    func pullToRefresh(sender: UIRefreshControl){
        loadMenuItems()
    }
    
    
    override func viewWillLayoutSubviews() {
        if let navContr = self.navigationController {
            if navContr.respondsToSelector(Selector("interactivePopGestureRecognizer")) {
                navContr.interactivePopGestureRecognizer?.enabled = false
            }
            
        }
    }
    
    
    func registerForNotifications(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MenuCategoriaViewController.userDefaultAddressChanged), name: kUserDidChangeDefaultAddressNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MenuCategoriaViewController.reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MenuCategoriaViewController.refreshBranchUIForModeChangeNotificationHandler(_:)), name: "refreshBranchForModeChangeNotification", object: nil)

    }
    
    func refreshBranchUIForModeChangeNotificationHandler(notification: NSNotification){
        if let branch = notification.object?["branch"] as? MRestaurant {
            if self.restaurant.franchise?.name == branch.franchise?.name {
                self.restaurant = branch
                refreshBranchUIForModeChange()
            }
        }else{
            self.refreshBranch()
        }
    }
    
    func reachabilityChanged(notification: NSNotification) {
        
        if let reachability = notification.object as? Reachability {
        
        if reachability.isReachable() {
            self.tableView?.reloadEmptyDataSet()
//            if let _ = MUser.sharedInstance.branchesFavorites.indexOf({$0.id == restaurant.id}) {
//                self.favoriteBtn.setImage(favorited, forState: .Normal)
//            }else{
//                self.favoriteBtn.setImage(unFavorited, forState: .Normal)
//                
//            }
            loadMenuItems()

            if branchMode == 0 {
                refreshBranch()
            }else{
                self.refreshBranchUI()
            }
            animationsForStates()
            if reachability.isReachableViaWiFi() {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            print("Network not reachable")
            
        }
        }
    }

    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    func userDefaultAddressChanged(){
        //handle new default address
        refreshBranch()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //    func createWaitScreen(){
    //
    //        let spinner = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    //        spinner.mode = MBProgressHUDMode.Indeterminate
    //        spinner.labelText = NSLocalizedString("Obteniendo Productos..",comment: "Buscando productos para menu restaurante seleccionado")
    //        spinner.labelFont = UIFont(name: "OpenSans-Semibold", size: 14.0)
    //
    //    }
    //
    //    func dismissWaitScreen(){
    //        dispatch_async(dispatch_get_main_queue()) {
    //            MBProgressHUD.hideHUDForView(self.view, animated: true)
    //        }
    //    }
    
    func createWaitScreen(){
        spinner.startAnimating()
        //glowMonchis()
    }
    
    func unGlowMonchis(){
        headerMonchisImage.layer.shadowColor = UIColor.clearColor().CGColor
        headerMonchisImage.layer.shadowRadius = 0
        headerMonchisImage.layer.shadowOpacity = 0
        headerMonchisImage.layer.removeAllAnimations()
    }
    
    func dismissWaitScreen(){
        spinner.stopAnimating()
        refreshControl?.endRefreshing()
//        unGlowMonchis()
    }
    
    
    
    
    func glowMonchis(){
        colorize(self.headerMonchisImage, color:UIColor(hexString: "#950505")!.CGColor)
    }
    
    
    
    func loadMenuItems(){
                loadingBeforeEmptyDataSet = true
                createWaitScreen()
                let parameters = ["franchise_id":self.restaurant?.franchise_id ?? 0,"branch_id":self.restaurant?.id ?? 0,"product_category_id":self.selectedMenu?.id ?? 0]

        api.returnCachedData(.GET_PRODUCT, parameters: parameters, completion: { (json) in
            if debugModeEnabled {
                print("returnCachedData for list products","called with parameters: \(parameters)",json)
            }
            if let jsonArray = json?["data"].array {
                self.updateUIForMenuItems(jsonArray)
            }
            
        })
        
        
        //self.collectionView?.reloadEmptyDataSet()
        
        api.postQueueRequest(.GET_PRODUCT, parameters: parameters) { (json, error) in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                if let jsonArray = json?["data"].array {
                    self.updateUIForMenuItems(jsonArray)
                }
            }else{
                
            }
            
        }
    }
    
    func updateUIForMenuItems(jsonArray: [SwiftyJSON.JSON]){
        self.loadingBeforeEmptyDataSet = false

        var tmp: [MProduct] = []
        for data in jsonArray {
            let newProduct = MProduct(json: data)
            
            for att in data["attachments"].array! {
                let newAtt = MAttachment(json: att)
                newProduct.attachments = [newAtt]
            }
            tmp.append(newProduct)
        }
        self.products = tmp
        self.tableView.reloadEmptyDataSet()
        self.tableView.reloadData()
//            self.tableView.beginUpdates()
//            if self.tableView.numberOfRowsInSection(0) == 0 && tmp.count > 0 {
//                self.products = tmp
//                self.tableView.reloadEmptyDataSet()
//
//                self.tableView.insertSections(NSIndexSet(index: 0), withRowAnimation: .Automatic)
//            }
//            if self.tableView.numberOfRowsInSection(0) > 1 && tmp.count == 0 {
//                self.products = tmp
//                self.tableView.reloadEmptyDataSet()
//
//                self.tableView.reloadData()
//            }
//            if self.tableView.numberOfRowsInSection(0) > 1 && tmp.count > 0 {
//                self.products = tmp
//                self.tableView.reloadEmptyDataSet()
//
//                self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: .Automatic)
//            }
//
//            self.tableView.endUpdates()
        
        self.dismissWaitScreen()
    }
    
    
//    func loadMenuItems(useCache: Bool = true){
//        loadingBeforeEmptyDataSet = true
//        createWaitScreen()
//        let parameters = ["franchise_id":self.restaurant?.franchise_id ?? 0,"branch_id":self.restaurant?.id ?? 0,"product_category_id":self.selectedMenu?.id ?? 0]
//        api.jsonRequest(.GET_PRODUCT, parameters: parameters, returnCachedDataIfPossible: useCache) { (json, error) -> Void in
//            if debugModeEnabled {
//                print(#function, json)
//            }
//            if error == nil{
//                
//                if let json = json {
//                    self.products = []
//                    if let jsonArray = json["data"].array {
//                        for data in jsonArray {
//                            let newProduct = MProduct(json: data)
//                           
//                            for att in data["attachments"].array! {
//                                let newAtt = MAttachment(json: att)
//                                newProduct.attachments = [newAtt]
//                            }
//                            self.products.append(newProduct)
//                        }
//                        self.loadingBeforeEmptyDataSet = false
//                        self.tableView.reloadData()
//                        self.dismissWaitScreen()
//                    }
//                    
//                }else{
//                    self.dismissWaitScreen()
//                    
//                }
//                
//                
//                
//            }else{ // network error
//                self.dismissWaitScreen()
//                let titleLocalizable = NSLocalizedString("Error",comment: "error")
//                let alert = UIAlertController(title: titleLocalizable, message: error, preferredStyle: .Alert)
//                let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
//                alert.addAction(ok)
//                self.presentViewController(alert, animated: true, completion: nil)
//                
//            }
//        }
//    }
//    
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == Segues.customizeSelectedItem {
            let dvc = segue.destinationViewController as! CustomizarProductoViewController
            dvc.product = self.selectedProduct
            dvc.restaurant = self.restaurant
        }
        
        if segue.identifier == Segues.infoResto {
            let dvc = segue.destinationViewController as! InfoRestoViewController
            dvc.restaurant = self.restaurant
        }
        
    }
    
    
}


extension MenuCategoriaViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var product: MProduct!
        if localSearchActive {
            product = filteredProducts[indexPath.row]
        }else{
            product = products[indexPath.row]
        }
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.menuCategoriaReuseID, forIndexPath: indexPath) as! MenuCategoriaTableViewCell
        //cell customization
        if indexPath.row%2 == 0 {
            cell.contentView.backgroundColor = UIColor(hexString: "#f7f2f1")
        }else{
            cell.contentView.backgroundColor = UIColor.whiteColor()
        }
        cell.itemName.text = product.name
        cell.itemDescription.text = product.product_description
        cell.itemPrice.text = product.price?.asLocaleCurrency
        if product.price == 0 {
            cell.itemPrice.hidden = true
            cell.itemFromLabel.text = "Personaliza tu plato"
            cell.itemFromLabel.textColor = UIColor(hexString: "#7ED321")
        }else{
            cell.itemPrice.hidden = false
            cell.itemFromLabel.text = "DESDE"
            cell.itemFromLabel.textColor = UIColor.blackColor()

        }
        if let stringLenght =  cell.itemPrice.text?.characters.count {
            let width = CGFloat(30 + 4 * (stringLenght))
            cell.itemPrice.frame = CGRectMake(cell.itemPrice.frame.origin.x, cell.itemPrice.frame.origin.y,width, cell.itemPrice.frame.height)
        }
       
        
            product.attachments.first?.width = cell.itemImage.bounds.width
            product.attachments.first?.height = cell.itemImage.bounds.height
    
        if let imgUrl = product.attachments.first?.imageComputedURL {
            cell.itemImage.hnk_setImageFromURL(imgUrl, placeholder: NO_ITEM_PLACEHOLDER)
        }else{
            cell.itemImage?.image = NO_ITEM_PLACEHOLDER
        }
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero

        cell.layoutIfNeeded()
        return cell
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if localSearchActive {
            return 1
        }else{
            return products.count
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if localSearchActive {
            return 1
        }else{
            return 1
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
            if localSearchActive {
                
            }else{
                selectedProduct = products[indexPath.row]
//                customTransition(indexPath)
                Mixpanel.mainInstance().track(event: "Usuario Eligio un Producto",
                                              properties: ["Producto" : selectedProduct?.name ?? "","Pantalla":"Productos de Categoria de Menu de Franquicia", "Franquicia": self.restaurant.franchise?.name ?? ""])
                self.performSegueWithIdentifier(Segues.customizeSelectedItem, sender: self)
            }
    }
    
    
    
    func customTransition(indexPath: NSIndexPath) {
        let cell = self.tableView?.cellForRowAtIndexPath(indexPath) as! MenuCategoriaTableViewCell
        
        let restoVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
        restoVC.restaurant = self.restaurant
        restoVC.product = self.selectedProduct
        restoVC.loadViewIfNeeded()
        var transitionObjectAvatar: ImageScaleTransitionObject!
        if restoVC.productImageView != nil {
            transitionObjectAvatar = ImageScaleTransitionObject(viewToAnimateFrom: cell.itemImage,
                                                                    viewToAnimateTo: restoVC.productImageView,
                                                                    duration: 0.4)
        }else{
            transitionObjectAvatar = ImageScaleTransitionObject(viewToAnimateFrom: cell.itemImage,
                                                                viewToAnimateTo: restoVC.restoHeaderImageView,
                                                                duration: 0.4, frameToAnimateTo: CGRectMake(0, 0, 40, 40))
        }
   
        
        
        self.imageScalePresentTransition = ImageScaleTransitionDelegate(transitionObjects: [transitionObjectAvatar], usingNavigationController: true, duration: 0.4, isProduct:  true)
        
        self.navigationController?.delegate = self.imageScalePresentTransition
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            self.navigationController?.delegate = nil
        }
        self.navigationController?.pushViewController(restoVC, animated: true)
        
        CATransaction.commit()
    }
    
    
    
}

extension MenuCategoriaViewController:  UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
    
    // MARK: UISearchBarDelegate
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            if searchBar.selectedScopeButtonIndex == 0 {
                searchTableViewController.lastProductSearched = ""
                searchTableViewController.loadingBeforeEmptyDataSet = false
            }else{
                searchTableViewController.loadingBeforeEmptyDataSet = false
                searchTableViewController.lastFranchiseSearched = ""
            }
        }
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        print(#function)
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        searchTableViewController.scopeIndex = selectedScope
        if selectedScope == 0 {
            searchBar.text = searchTableViewController.lastProductSearched
        }else{
            searchBar.text = searchTableViewController.lastFranchiseSearched
        }
    }
    
    // MARK: UISearchResultsUpdating
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if let text = searchController.searchBar.text {
            if text.characters.count >= 3 {
                self.searchTableViewController.treshold = true
                
                //                let delay = 0.5 * Double(NSEC_PER_SEC)
                //                let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                //                dispatch_after(time, dispatch_get_main_queue()) {
                if searchController.searchBar.selectedScopeButtonIndex == 0 {
                    self.searchTableViewController.searchProductWithText(text)
                }else{
                    self.searchTableViewController.searchFranchisesWithText(text)
                }
                //                }
            }else{
                self.searchTableViewController.loadingBeforeEmptyDataSet = false
                self.searchTableViewController.treshold = false
                self.searchTableViewController.products = []
                self.searchTableViewController.restaurants = []
                self.searchTableViewController.tableView.reloadData()
            }
        }
        print(#function)
    }
    
    // MARK: UISearchControllerDelegate
    
    func willPresentSearchController(searchController: UISearchController) {
        print(#function)
        
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
    }
    
    
    
    func willDismissSearchController(searchController: UISearchController) {
  
        self.tabBarController!.setTabBarVisible(true, animated: true)
    }
    
    
    @IBAction func searchPressed(sender: UIButton) {
        
        if searchController.active {
            self.tabBarController!.setTabBarVisible(true, animated: true)
            self.searchController.dismissViewControllerAnimated(true, completion: nil)
        }else{
            self.tabBarController!.setTabBarVisible(false, animated: true)
            self.presentViewController(self.searchController, animated: true){
                
            }
            
        }
    }
    
    // MARK: EmptyDataSetSource
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        if connectedToNetwork() {
            if restaurant.mode == 0 {
                let image = UIImage(named: "carrito-vacio")
                return image
            }else{
                let image = UIImage(named: "carrito-vacio-rojo")
                return image
            }
        }else{
            if restaurant.mode == 0 {
                
                return UIImage(named: "icono-error-red")
            }else{
                return UIImage(named: "icono-error-red-rojo")
            }
        }
    }
    
    func imageAnimationForEmptyDataSet(scrollView: UIScrollView!) -> CAAnimation! {
        
        let animation = CABasicAnimation(keyPath: "transform")
        
        animation.fromValue = NSValue(CATransform3D: CATransform3DIdentity)
        animation.toValue = NSValue(CATransform3D:CATransform3DMakeRotation(CGFloat(M_PI_2), 0.0, 0.0, 1.0))
        
        animation.duration = 0.25
        animation.cumulative = true
        animation.repeatCount = MAXFLOAT
        return animation
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        var color : UIColor!
        if restaurant.mode == 0 {
            color = UIColor.whiteColor()
        }else{
            color = UIColor(hexString: "#cf1b1b")
        }
        if connectedToNetwork() {
            if loadingBeforeEmptyDataSet {
                
                let text = NSAttributedString(string: NSLocalizedString("Obteniendo Menú...", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(18),NSForegroundColorAttributeName:color])
                
                return text
            }else{
                let text = NSAttributedString(string: NSLocalizedString("Menú en proceso de carga.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(18),NSForegroundColorAttributeName:color])
                
                return text
            }
        }else{
            let text = NSAttributedString(string: NSLocalizedString("No tienes conexión a internet.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(18),NSForegroundColorAttributeName:color])
            
            return text
        }
    }
    
    
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) ->
        NSAttributedString! {
            var color : UIColor!
            if restaurant.mode == 0 {
                color = UIColor.whiteColor()
            }else{
                color = UIColor(hexString: "#cf1b1b")
            }
            if connectedToNetwork() {
                if loadingBeforeEmptyDataSet {
                    return nil
                }else{
                    let subTitle = NSAttributedString(string: NSLocalizedString("< Volver atras", comment: ""),attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(18),NSForegroundColorAttributeName:color])
                    
                    return subTitle
                }
            }else{
                return nil
            }
    }
    func emptyDataSetShouldAnimateImageView(scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    // MARK: SearchResultSelectedDelegate
    func didSelectSearchResult(product: MProduct?, branch: MRestaurant?) {
        selectedProductSearch = nil
        selectedBranchSearch = nil
        if product != nil {
            
            deliveryOrPickupHistorySearch(product!.branch_products.first!, completion: { (isDelivery, newBranch) in
                let productoVC = self.storyboard?.instantiateViewControllerWithIdentifier("CustomizarProductoViewController") as! CustomizarProductoViewController
                if isDelivery == nil || isDelivery == true {
                    productoVC.product = product
                    let branch = newBranch
                    productoVC.restaurant = branch
                    branch.mode = 0
                    productoVC.restaurant?.franchise = product?.franchise
                    self.searchController.active = false
                    self.navigationController?.pushViewController(productoVC, animated: true)
                    
                }else{//pickup
                    self.searchController.active = false
                    self.selectedProductSearch = product
                    let bvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                    bvc.franchise = newBranch.franchise
                    bvc.delegate = self
                    self.navigationController?.pushViewController(bvc, animated: true)
                    
                }
            })
            
            
        }else{
            deliveryOrPickupHistorySearch(branch!, completion: { (isDelivery, newBranch) in
                
                if isDelivery == nil || isDelivery == true {
                    
                    let restaurantVC = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantViewController") as! RestaurantViewController
                    newBranch.mode = 0
                    restaurantVC.restaurant = newBranch
                    
                    //            controllers?.append(categoriasVC!)
                    self.searchController.active = false
                    var controllers = self.navigationController!.viewControllers
                    controllers.removeLast()
                    controllers.removeLast()
                    controllers.append(restaurantVC)
                    self.navigationController?.setViewControllers(controllers, animated: true)
                    
                    
                    
                }else{//pickup
                    self.searchController.active = false
                    self.selectedBranchSearch = newBranch
                    let bvc = self.storyboard?.instantiateViewControllerWithIdentifier("BranchSelectionViewController") as! BranchSelectionViewController
                    bvc.franchise = newBranch.franchise
                    bvc.delegate = self
                    self.navigationController?.pushViewController(bvc, animated: true)
                    
                }
            })
            
            
        }
        
    }
    
    // MARK: Favorites

    @IBAction func favoritePressed(sender: UIButton) {
        if MUser.sharedInstance.branchesFavorites.contains(restaurant){
            self.favoriteBtn.setImage(unFavorited, forState: .Normal)
            deleteFavorite()
            self.favoriteBtn.pop()

        }else{
            self.favoriteBtn.setImage(favorited, forState: .Normal)
            addFavorite()
            self.favoriteBtn.pop()

        }
    }
    
    
    func addFavorite(){
        
        let parameters = ["branch_id":restaurant.id!]
        api.jsonRequest(.FAVORITE_PRODUCT, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    MUser.sharedInstance.branchesFavorites.append(self.restaurant)
                }else{
                    self.favoriteBtn.setImage(unFavorited, forState: .Normal)
                    let error = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Lo sentimos!", message: error, preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }else{
                self.favoriteBtn.setImage(unFavorited, forState: .Normal)
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func deleteFavorite(){
        let parameters = ["branch_id":restaurant.id!]
        api.jsonRequest(.DELETE_PRODUCT_FAVORITE, parameters: parameters, cacheRequest: false, returnCachedDataIfPossible: false) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                let success = json?["success"].bool ?? false
                if success {
                    /* handle favorite removal */
                    
                    if let index = MUser.sharedInstance.branchesFavorites.indexOf({$0.id == self.restaurant.id}) {
                        MUser.sharedInstance.branchesFavorites.removeAtIndex(index)
                        self.favoriteBtn.setImage(unFavorited, forState: .Normal)
                        
                    }
                    
                    
                }else{
                    let error = json?["data"].string ?? "Error desconocido"
                    let alert = UIAlertController(title: "Lo sentimos!", message: error, preferredStyle: .Alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                    alert.addAction(aceptar)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }else{
                let alert = UIAlertController(title: "Error de conexión", message: error, preferredStyle: .Alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: nil)
                alert.addAction(aceptar)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func refreshBranch(){
        self.refreshBranchUI()

        statesSpinner.startAnimating()
        self.leadingWidthRestoImgViewConstraint.constant = 31
        UIView.animateWithDuration(0.2) {
            self.view.setNeedsLayout()
        }
        noCoverRestoIconImgView.hidden = true
        closedRestoIconImgView.hidden = true
        let currentDay = getDayOfWeek()!
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let latitude = MUser.sharedInstance.defaultAddress.latitude ?? ""
        let longitude = MUser.sharedInstance.defaultAddress.longitude ?? ""
        let parameters: [String:AnyObject] = ["franchise_id":self.restaurant.franchise_id!,"day":currentDay,"latitude":latitude,"longitude":longitude,"group_by_franchise":true]
        api.jsonRequest(.SEARCH_BRANCHES, parameters: parameters, returnCachedDataIfPossible: false ) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            self.statesSpinner.stopAnimating()
            if error == nil {
                if let restaurantsArray = json?["data"].array {
                    for data in restaurantsArray {
                        let newBranch = MRestaurant(data: data["branch"])
                        self.restaurant = newBranch
//                        self.restaurant.mode = self.branchMode
                    
                    }
                    self.refreshBranchUI()
                    self.dismissWaitScreen()
                    
                }else{// network error
                    
                    
                }
                
            }
        }
        
    }


    
}

