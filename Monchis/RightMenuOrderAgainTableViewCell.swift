//
//  RightMenuOrderAgainTableViewCell.swift
//  Monchis
//
//  Created by jrivarola on 10/13/15.
//  Copyright (c) 2015 cibersons. All rights reserved.
//

import UIKit

class RightMenuOrderAgainTableViewCell: UITableViewCell {

    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var verDetalleBtn: UIButton!
    @IBOutlet weak var branchNameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
