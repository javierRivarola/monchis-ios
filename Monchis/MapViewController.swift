import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import DynamicBlurView
import GooglePlaces
import Crashlytics
import Mixpanel

class MapViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, AddressBoxDelegate, UITextFieldDelegate, UISearchResultsUpdating, UISearchControllerDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    // MARK: Properties
    
    //Outlets
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var mapCenterPinImage: UIImageView!
    @IBOutlet weak var pinImageVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var popUpHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var mapBotConstraint: NSLayoutConstraint!
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    @IBOutlet weak var ayudaView: UIView!
    @IBOutlet weak var ayudaViewHeightConstraint: NSLayoutConstraint!
    
    //Variables
    var cameFromSearch: Bool = false
    var data:[GMSAutocompletePrediction] = []
    var placesClient = GMSPlacesClient.sharedClient()
    var searchController: UISearchController!
    var addressVC: AddressBoxViewController!
    var popOverActive: Bool = false
    var searchActive : Bool = false
    var results:SearchResultsMapsTableViewController!
    
    var lookupAddressResults: JSON!
    var fetchedFormattedAddress: String!
    var fetchedAddressLongitude: Double!
    var fetchedAddressLatitude: Double!
    var googleMapsAddressRequest: Request?
    var googleMapsAutoCompleteRequest: Request?
    var predictions: [JSON]!
    var saveBtnTag: Int = 0
    var addressToEdit: MAddress?
    var addressBoxHeight: CGFloat = 350
    
    
    
    
    //Constants
    let locationManager = CLLocationManager()
    let dataProvider = GoogleDataProvider()
    let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
    let baseAutocompleteURL = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
    
    //api shared instance
    let api = MAPI.SharedInstance
    let user = MUser.sharedInstance
    
    //Structs
    struct Segues {
        static let AddressBoxSegue = "addressBoxSegue"
        static let MainView = "MainViewFromMaps"
        static let UnwindToSideMenu = "unwindToSideMenu"
        static let VideoTuto = "videoTutoSegue"
    }
    
    struct Constants {
        static let ReuseSearchCellID = "ReuseSearchCellID"
    }
    
    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInterface()
        let showTuto = NSUserDefaults.standardUserDefaults().boolForKey(kShowMapTutorialKey)
        
        if !showTuto {
            let delay = 0.5 * Double(NSEC_PER_SEC)
            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
            dispatch_after(time, dispatch_get_main_queue()) {
                self.showTutorial()

            }
        }

        if addressToEdit != nil {
            setupEditingAddressInterface()
        }
    }
    
    deinit {
        let center = NSNotificationCenter.defaultCenter()
        center.removeObserver(self)
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        addressVC.saveBtn.tag = self.saveBtnTag
       
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func showTutorial(){
       self.performSegueWithIdentifier(Segues.VideoTuto, sender: self)
    }
    
    // UI Setup
    func setupInterface(){
        
        
      
        
        //customize nav bar
        
        let customFont = UIFont(name: "OpenSans-Light", size: 12)
        UINavigationBar.appearance().titleTextAttributes = [ NSFontAttributeName: customFont!]
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: customFont!], forState: UIControlState.Normal)
        
        
        
        mapView.delegate = self
        addressVC.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        //mapCenterPinImage.addGestureRecognizer(tapGesture)
        mapCenterPinImage.hidden = true
        //Register observers
        let center = NSNotificationCenter.defaultCenter()
        
        //Notifications for keyboard
        center.addObserver(self, selector: #selector(MapViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(MapViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        //SearchBar Controller
        results = SearchResultsMapsTableViewController()
        searchController = UISearchController(searchResultsController: results)
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.searchBarStyle = UISearchBarStyle.Default
        searchController.searchBar.sizeToFit()
        let searchBar = searchController.searchBar
        searchBar.delegate = self
        searchBar.placeholder = NSLocalizedString("Buscar..", comment: "String que indica el placeholder para buscar direcciones en el mapa")
        
        results.definesPresentationContext = true
        results.tableView.dataSource = self
        results.tableView.delegate = self
        results.tableView.rowHeight = UITableViewAutomaticDimension
        results.tableView.estimatedRowHeight = 70
        results.tableView.tableFooterView = UIView(frame: CGRectZero)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = true // important
        //self.navigationItem.titleView = searchBar
        searchController.delegate = self
        results.tableView.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: Constants.ReuseSearchCellID)
        
        //
        //        if user.facebookProfile.checkPermission("user_location") {
        //            if let location = user.facebookProfile.locationName {
        //                if let locationID = user.facebookProfile.locationID {
        //                    searchAddress(location)
        //                }
        //            }
        //        }
        
        
    }
    
    func setupEditingAddressInterface(){
        let coordinate = CLLocationCoordinate2D(latitude: Double(self.addressToEdit?.latitude.floatValue ?? 0), longitude: Double(self.addressToEdit?.longitude.floatValue ?? 0))
        addressVC.street1.text = self.addressToEdit?.street1
        addressVC.street2.text = self.addressToEdit?.street2
        addressVC.number.text = self.addressToEdit?.number
        addressVC.referals.text = self.addressToEdit?.reference
        userCoordinateSelected = coordinate
        self.mapView.camera = GMSCameraPosition.cameraWithTarget(coordinate, zoom: 14.0)
        self.ayudaView.fadeOut(0.1)
        self.showPopOver()
    }
    
    
    @IBAction func customMarkerPressed(sender: UITapGestureRecognizer) {
        showPopOver()
    }
    
    @IBAction func searchPressed(sender: UIButton) {
        searchActive = true
        
        self.presentViewController(searchController, animated: true) { () -> Void in
            self.searchController.searchBar.becomeFirstResponder()
        }

        
    }
    
    // MARK: UISearchBarDelegate
    

    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if let currentCoordinate = myCurrentLocation?.coordinate {
            let northEast = CLLocationCoordinate2DMake(currentCoordinate.latitude + 1, currentCoordinate.longitude + 1)
            let southWest = CLLocationCoordinate2DMake(currentCoordinate.latitude - 1, currentCoordinate.longitude - 1)
            let bounds = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            let filter = GMSAutocompleteFilter()
            filter.type = GMSPlacesAutocompleteTypeFilter.Address
            
            if searchText.characters.count > 0 {
                print("Searching for '\(searchText)'")
                placesClient.autocompleteQuery(searchText, bounds: bounds, filter: filter, callback: { (results, error) -> Void in
                    if error != nil {
                        print("Autocomplete error \(error) for query '\(searchText)'")
                        return
                    }
                    
                    self.data = [GMSAutocompletePrediction]()
                    for result in results! {
                        self.data.append(result)
                    }
                    self.results.tableView.reloadData()
                })
            } else {
                self.data = [GMSAutocompletePrediction]()
                self.results.tableView.reloadData()
            }
        }
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        print(#function, terminator: "")
    }
    
    // MARK: UISearchResultsUpdating
    func updateSearchResultsForSearchController(searchController: UISearchController) {
     
        
    }
    
    // MARK: UISearchControllerDelegate
    
    func didPresentSearchController(searchController: UISearchController) {
        searchController.searchBar.becomeFirstResponder()
    }
    
    func willPresentSearchController(searchController: UISearchController) {
        print(#function, terminator: "")
        searchActive = true
        if popOverActive {
            dismissPopOver()
            mapView.userInteractionEnabled = true
            self.mapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0, bottom: 60, right: 0)
            
        }
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        
    }
    
    func willDismissSearchController(searchController: UISearchController) {
        print(#function, terminator: "")
        searchActive = false
        self.navigationItem.setHidesBackButton(false, animated: true)
        
    }
    
    
    // MARK: SearchResults Data Source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return (data.count > 0) ? 1 : 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.ReuseSearchCellID, forIndexPath: indexPath)
        if indexPath.row == data.count {
            
            if let poweredByGoogle = cell.contentView.viewWithTag(10) as? UIImageView {
                poweredByGoogle.hidden = false
                cell.textLabel?.text = ""
            }else{
                cell.textLabel?.text = ""

            let poweredByGoogle = UIImageView(frame: CGRectMake(cell.contentView.frame.width/2, 0, cell.contentView.frame.width/2, cell.contentView.frame.height))
                
                
            poweredByGoogle.tag = 10
            poweredByGoogle.contentMode = .ScaleAspectFit
            poweredByGoogle.image = UIImage(named: "powered_by_google_on_white")
            cell.contentView.addSubview(poweredByGoogle)
            }
            return cell
        }else{
            if let pwrdByGoogle = cell.contentView.viewWithTag(10) as? UIImageView {
                pwrdByGoogle.hidden = true
            }
        }
        
        cell.textLabel?.text = data[indexPath.row].attributedFullText.string
        return cell
        
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row != data.count + 1{
        mapView.clear()
        let placeID = data[indexPath.row].placeID ?? ""
        searchPlaceID(placeID)
            searchActive = false
        }
    }
    
    func searchPlaceID(placeID: String){
        placesClient.lookUpPlaceID(placeID) { place, error in
            if error != nil {
                print("lookup place id query error: \(error!.localizedDescription)", terminator: "")
                return
            }
            if place != nil {
                self.dismissViewControllerAnimated(true, completion: nil)
                self.cameFromSearch = true
                self.mapView(self.mapView, didLongPressAtCoordinate: place!.coordinate)
            } else {
                print("No place details for \(placeID)", terminator: "")
            }
        }
    }
    
    
    // MARK: Keyboards notifications
    
    func keyboardWillShow(notification: NSNotification){

         if popOverActive && !searchActive{
     //   if popOverActive{
            
            mapView.userInteractionEnabled = false
            _ = notification.userInfo
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                _ = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
                UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.5, options: .CurveEaseInOut, animations: {
                    self.mapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0, bottom: 0, right: 0)
                    self.heightConstraint.constant = self.addressBoxHeight + keyboardSize.height
                    self.botomConstrainMapView.constant =  self.heightConstraint.constant
                    self.view.layoutIfNeeded()
                    self.mapView.layoutIfNeeded()
                    self.blurView.layoutIfNeeded()
                    // self.mapCenterPinImage.transform = CGAffineTransformMakeTranslation(0, 30)
                    self.mapCenterPinImage.layoutIfNeeded()
                    }, completion:nil)
            }
        }
        
        if searchActive {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                let contentInsets = UIEdgeInsets(top: results.tableView.contentInset.top, left: 0, bottom: -(keyboardSize.height), right: 0)
                
            results.tableView.contentInset = contentInsets
                
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification){
        if popOverActive {
            //if popOverActive && !searchBarActive {
            mapView.userInteractionEnabled = true
            self.mapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0, bottom: 60, right: 0)
        }
    }
    
    
    @IBOutlet weak var botomConstrainMapView: NSLayoutConstraint!
    
    func showPopOver(){
        popOverActive = true
        addressVC.street1.text = addressLabel.text
        mapCenterPinImage.hidden = false
        mapCenterPinImage.alpha = 0
        mapCenterPinImage.transform = CGAffineTransformMakeTranslation(0, -self.view.bounds.height/2)
        UIView.animateWithDuration(0.7, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.5, options: .CurveEaseInOut, animations: {
            self.mapCenterPinImage.transform = CGAffineTransformMakeTranslation(0, 0)
            self.heightConstraint.constant = self.addressBoxHeight
            self.botomConstrainMapView.constant = self.heightConstraint.constant - 60
            self.view.layoutIfNeeded()
            self.mapView.layoutIfNeeded()
            self.blurView.layoutIfNeeded()
            self.mapCenterPinImage.layoutIfNeeded()
            self.mapCenterPinImage.alpha = 1
            }, completion: {fin in
                
                UIView.animateWithDuration(0.2, delay: 0, options: [.BeginFromCurrentState, .Repeat, .Autoreverse] , animations: {
                    self.mapCenterPinImage.transform = CGAffineTransformMakeTranslation(0, 5)
                    //self.mapCenterPinImage.transform = CGAffineTransformMakeScale(1.1, 1.1)
                    }, completion: {fin in
                })
        })
        
        
        
        
    }
    
    func dismissPopOver(){
        popOverActive = false
        for subview in addressVC.view.subviews {
            if subview.isKindOfClass(UITextField) {
                subview.resignFirstResponder()
            }
        }
        UIView.animateWithDuration(0.5, delay: 0, options: [.CurveEaseInOut, .BeginFromCurrentState] , animations: {
            self.mapCenterPinImage.transform = CGAffineTransformMakeTranslation(0, self.view.bounds.height/2)
            self.heightConstraint.constant = 60
            self.botomConstrainMapView.constant = 0
            self.view.layoutIfNeeded()
            self.blurView.layoutIfNeeded()
            self.mapCenterPinImage.alpha = 0
            self.mapView.layoutIfNeeded()
            self.mapCenterPinImage.layoutIfNeeded()
            self.mapCenterPinImage.transform = CGAffineTransformMakeTranslation(0, -self.view.bounds.height/2)
            }, completion: {fin in
                self.mapCenterPinImage.hidden = true
                self.mapCenterPinImage.alpha = 1
                self.mapCenterPinImage.layer.removeAllAnimations()
                self.mapCenterPinImage.transform = CGAffineTransformMakeTranslation(0, 0)
                self.mapView.userInteractionEnabled = true
                
        })
        
        
//        UIView.animateWithDuration(0.3, delay: 0.3, options: [.CurveEaseOut,.BeginFromCurrentState], animations: { () -> Void in
//            self.mapCenterPinImage.alpha = 0
//
//            }, completion: nil)
        
    }
    
    
    // MARK: AddressBox delegate
    
    func cancelPressed() {
        ayudaView.fadeIn(0.3)
        dismissPopOver()
    }
    
    func savePressed() {
        if addressVC.street1.text != "" && addressVC.street2.text != "" && addressVC.referals.text != "" && addressVC.number.text != "" {
        dismissPopOver()
        let addr = MAddress()
        
        addr.name = NSLocalizedString("Nueva Dirección", comment: "Valor por defecto del nombre de nuevas direcciónes")
        addr.street1 = addressVC.street1.text ?? ""
        addr.street2 = addressVC.street2.text ?? ""
        addr.reference = addressVC.referals.text ?? ""
        addr.number = addressVC.number.text ?? ""
        addr.coordinate = userCoordinateSelected
        addr.latitude = "\(userCoordinateSelected.latitude)"
        addr.longitude = "\(userCoordinateSelected.longitude)"
        
        
        
        let alert = UIAlertController(title: NSLocalizedString("Etiqueta de dirección", comment: "Nickname for the address, could be home, office, girlfriends house.. etc"), message: NSLocalizedString("Elije una etiqueta para tu dirección!",  comment: "description for address nickname"), preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler { (text) -> Void in
            text.placeholder = NSLocalizedString("Ej: Casa, Oficina, etc..", comment: "Placeholder para nickname de direccion")
            if self.addressToEdit != nil {
                text.text = self.addressToEdit!.name
            }
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .Cancel) { (action) -> Void in
            if self.popOverActive {
                self.cancelPressed()
            }

        }
        
        alert.addAction(cancelAction)
        
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar guardar direccion"), style: .Default, handler: { (action) -> Void in
            self.view.endEditing(true)
            //handle save address
            
            let nickNameAddr = alert.textFields?.first
            if nickNameAddr?.text != "" {
                addr.name = nickNameAddr!.text!
            }
            
            //we always make default the address
            addr.is_default = true
            if (self.user.authenticationMethod != .Unregistered) {
                customWaitScreen("Guardando dirección", comment: "Guardando direccion nueva de usuario", view: self.view, blurView: nil)
                var command: MAPI.Command!
                
                if self.addressToEdit != nil {
                    command = .USER_ADDRESSES_UPDATE
                    addr.id = self.addressToEdit!.id
                }else{
                    command = .USER_ADDRESSES_CREATE
                    Answers.logCustomEventWithName("Nueva dirección creada.",
                        customAttributes: ["User ID":self.user.id])
                   
                    
                }
                let addrPrmt = addr.parseAddressForAPI()
                self.api.jsonRequest(command, parameters: addrPrmt) { (json, error) -> Void in
                    if debugModeEnabled {
                        print(#function,"called with parameters: \(addrPrmt)",json, error)

                    }
                    dismissCustomWaitScreen(self.view, blurView: nil)
                    if error == nil {
                        if let json = json {
                            let success = json["success"].bool ?? false
                            if success {
                                 Mixpanel.mainInstance().track(event: "Nueva Direccion Creada", properties: ["Latitud":addr.latitude ?? "", "Longitud":addr.longitude ?? "", "Nombre":addr.name])
                                for  addr in self.user.addresses {
                                    addr.is_default = false
                                }
                                addr.id = json["data"]["id"].int ?? 0
                                if  let addressToDelete = self.addressToEdit {
                                    if let index = self.user.addresses.indexOf(addressToDelete) {
                                        self.user.addresses.removeAtIndex(index)
                                    }else{
                                        //could not delete local address
                                        print("could not delete local address, not found")
                                    }
                                    self.user.addresses.append(addr)
                                }else{
                                    self.user.addresses.append(addr)
                                }
                                self.user.addresses.sortInPlace({ (addr1, addr2) -> Bool in
                                    return addr1.is_default
                                })
                                if self.addressVC.saveBtn.tag == 0 {
                                    self.performSegueWithIdentifier(Segues.MainView, sender: self)
                                }else{ // we come from the menu
                                    self.dismissPopOver()
                                    self.performSegueWithIdentifier(Segues.UnwindToSideMenu, sender: self)
                                }
                            }else{//handle error in request
                                let message = json["data"].string ?? ""
                                self.presentViewController(customAlertWithOK("Error al registrar la dirección", message: message), animated: true, completion: nil)
                            }
                        }else{ //json nil
                            self.presentViewController(customAlertWithOK("Error al registrar la dirección", message: "JSON NIL"), animated: true, completion: nil)
                        }
                    }else{ //mayor error not nil
                        self.presentViewController(customAlertWithOK("Error al registrar la dirección", message: error!), animated: true, completion: nil)
                    }
                }
                
            }else{ // we save the temporal address
                
                
                //dismiss keyboard
             //   dismissCustomWaitScreen(self.view, blurView: nil)
                self.dismissPopOver()
                for  addr in self.user.addresses {
                    addr.is_default = false
                }
                
                if  let addressToDelete = self.addressToEdit {
                    if let index = self.user.addresses.indexOf(addressToDelete) {
                        self.user.addresses.removeAtIndex(index)
                    }else{
                        //could not delete local address
                        print("could not delete local address, not found")
                    }
                    self.user.addresses.append(addr)
                }else{
                    self.user.addresses.append(addr)
                }
                self.user.addresses.sortInPlace({ (addr1, addr2) -> Bool in
                    return addr1.is_default
                })
//
//                self.view.endEditing(true)
//
//                self.mapCenterPinImage.hidden = false
//                self.mapCenterPinImage.transform = CGAffineTransformMakeTranslation(0, -self.view.bounds.height/2)
//                UIView.animateWithDuration(0.7, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.5, options: .CurveEaseInOut, animations: {
//                    self.mapCenterPinImage.transform = CGAffineTransformMakeTranslation(0, 0)
//                    self.heightConstraint.constant = self.addressBoxHeight
//                    self.botomConstrainMapView.constant = self.heightConstraint.constant - 60
//                    self.view.layoutIfNeeded()
//                    self.mapView.layoutIfNeeded()
//                    self.blurView.layoutIfNeeded()
//                    self.mapCenterPinImage.layoutIfNeeded()
//                    }, completion: {fin in
//                        UIView.animateWithDuration(0.2, delay: 0, options: [.BeginFromCurrentState, .Repeat, .Autoreverse] , animations: {
//                            self.mapCenterPinImage.transform = CGAffineTransformMakeTranslation(0, 5)
//                            //self.mapCenterPinImage.transform = CGAffineTransformMakeScale(1.1, 1.1)
//                            }, completion: {fin in
//                        })
//                })
//                
//                
                
                
                if self.addressVC.saveBtn.tag == 0 {
                    self.performSegueWithIdentifier(Segues.MainView, sender: self)
                }else{ // we come from the menu
                    self.performSegueWithIdentifier(Segues.UnwindToSideMenu, sender: self)
                }
                
            }
        }))
        
        
        
        self.presentViewController(alert, animated: true, completion: nil)
        }else{
            self.dismissPopOver()
            let alert = UIAlertController(title: "Debes completar todos los datos!", message: "Por favor completa los campos faltantes para continuar", preferredStyle: .Alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .Cancel, handler: { (act) -> Void in
                self.showPopOver()
            })

           
            alert.addAction(aceptar)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    
    // MARK: Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Segues.AddressBoxSegue {
            addressVC = segue.destinationViewController as! AddressBoxViewController
        }
        if segue.identifier == Segues.VideoTuto {
            videoTutoController = segue.destinationViewController as! VideoTutoMapaViewController
            videoTutoController.mapTuto = true
        }
    }
    
    var videoTutoController: VideoTutoMapaViewController!

    func doneVideoTutoPressed(sender: UIButton) {
        hideTutorial()
    }
    
    func dontShowAgainTutoPressed(sender: UIButton) {
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: kShowMapTutorialKey)
        hideTutorial()
    }
    
    func hideTutorial(){
        videoTutoController.player?.pause()
        videoTutoController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func mapTypeChanged(sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            mapView.mapType = kGMSTypeNormal
        case 1:
            mapView.mapType = kGMSTypeSatellite
        case 2:
            mapView.mapType = kGMSTypeHybrid
        default:
            mapView.mapType = mapView.mapType
        }
    }
    
    // MARK: LocationManagerDelegate
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        // 2
        if status == .AuthorizedWhenInUse {
            
            // 3
            locationManager.startUpdatingLocation()
            
            //4
            mapView.myLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
    var myCurrentLocation:CLLocation!
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first  {
            myCurrentLocation = location
            if self.addressToEdit == nil {
                mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            }
            self.userCoordinateSelected = location.coordinate
            locationManager.stopUpdatingLocation()
        }
        
    }
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
         
            if let address = response?.firstResult() {
                self.userCoordinateSelected = coordinate
                let lines = address.lines!
                self.addressLabel.hidden = false
                self.addressLabel.text = lines.joinWithSeparator("\n")
                if self.popOverActive {
                    self.addressVC.street1.text = self.addressLabel.text
                }
                UIView.animateWithDuration(0.25) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    func mapView(mapView: GMSMapView, idleAtCameraPosition position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
        self.addressLabel.unlock()
        
        
        let labelHeight = self.addressLabel.intrinsicContentSize().height
        self.mapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0, bottom: 60, right: 0)
        
        UIView.animateWithDuration(0.25) {
            self.blurView.alpha = 1
            self.pinImageVerticalConstraint.constant = ((labelHeight - self.topLayoutGuide.length) * 0.5)
            self.view.layoutIfNeeded()
        }
    }
    
    func mapView(mapView: GMSMapView, willMove gesture: Bool) {
        addressLabel.lock()
        
        if (gesture) {
            mapCenterPinImage.fadeIn(0.25)
            mapView.selectedMarker = nil
        }
    }
    
    
    
    var mapRadius: Double {
        get {
            let region = mapView.projection.visibleRegion()
            let verticalDistance = GMSGeometryDistance(region.farLeft, region.nearLeft)
            let horizontalDistance = GMSGeometryDistance(region.farLeft, region.farRight)
            return max(horizontalDistance, verticalDistance)*0.5
        }
    }
    
    
    
    func mapView(mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let customView = UIView()
        let label = UILabel()
        label.text = "customview"
        label.sizeToFit()
        customView.addSubview(label)
        
        return customView
    }
    
    var userCoordinateSelected: CLLocationCoordinate2D!
    
    func mapView(mapView: GMSMapView, didLongPressAtCoordinate coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        
        
        ayudaView.fadeOut(0.3)
        
        
        userCoordinateSelected = coordinate
        var currentZoom = mapView.camera.zoom
        if cameFromSearch {
            currentZoom = 15
            cameFromSearch = false
        }
        
        let currentBearing = mapView.camera.bearing
        let currentViewAngle = mapView.camera.viewingAngle
        mapView.animateToCameraPosition(GMSCameraPosition(target: coordinate, zoom: currentZoom, bearing: currentBearing, viewingAngle: currentViewAngle))
        //        var currentLocation = GMSMarker(position: coordinate)
        //        let locationText = "Direccion 1"
        //        currentLocation.title = locationText
        //        //currentLocation.icon = UIImage(named: "icon_me")
        //        currentLocation.snippet = "Esta direccion se guardara"
        //        currentLocation.appearAnimation = kGMSMarkerAnimationPop
        //
        //        currentLocation.infoWindowAnchor = CGPointMake(0.5, 0.5)
        //        currentLocation.map = mapView;
        if popOverActive{
            UIView.animateWithDuration(0.4, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: [.BeginFromCurrentState, .CurveEaseIn], animations: {
                self.mapCenterPinImage.transform = CGAffineTransformMakeTranslation(0, -self.view.bounds.height/2)
                }, completion: {fin in
                    UIView.animateWithDuration(0.4, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: [.BeginFromCurrentState, .CurveEaseIn], animations: {
                        self.mapCenterPinImage.transform = CGAffineTransformMakeTranslation(0, 0)
                        }, completion: {fin in
                            
                            UIView.animateWithDuration(0.2, delay: 0, options: [.BeginFromCurrentState, .Repeat, .Autoreverse], animations: {
                                self.mapCenterPinImage.transform = CGAffineTransformMakeTranslation(0, 5)
                               // self.mapCenterPinImage.transform = CGAffineTransformMakeScale(1.1, 1.1)
                                }, completion: nil)
                    })
            })
        }else{
            showPopOver()
        }
    }
    
    @IBAction func cancelSaveAddress(sender: UIButton) {
        dismissPopOver()
    }
    
    /*
    func mapView(mapView: GMSMapView!, markerInfoContents marker: GMSMarker!) -> UIView! {
    let placeMarker = marker as! PlaceMarker
    
    if let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView {
    infoView.nameLabel.text = placeMarker.place.name
    
    if let photo = placeMarker.place.photo {
    infoView.placePhoto.image = photo
    } else {
    infoView.placePhoto.image = UIImage(named: "generic")
    }
    
    return infoView
    } else {
    return nil
    }
    }
    */
    
    
    func mapView(mapView: GMSMapView, didTapMarker marker: GMSMarker) -> Bool {
        mapCenterPinImage.fadeOut(0.25)
        return false
    }
    
    func didTapMyLocationButtonForMapView(mapView: GMSMapView) -> Bool {
        mapCenterPinImage.fadeIn(0.25)
        mapView.selectedMarker = nil
        return false
    }
    
    
    /*
    func mapView(mapView: GMSMapView!, didTapInfoWindowOfMarker marker: GMSMarker!) {
    
    
    // 1
    let googleMarker = mapView.selectedMarker as! PlaceMarker
    
    // 2
    dataProvider.fetchDirectionsFrom(mapView.myLocation.coordinate, to: googleMarker.place.coordinate) {optionalRoute in
    if let encodedRoute = optionalRoute {
    // 3
    let path = GMSPath(fromEncodedPath: encodedRoute)
    let line = GMSPolyline(path: path)
    
    // 4
    line.strokeWidth = 4.0
    line.tappable = true
    line.map = self.mapView
    line.strokeColor = self.randomLineColor
    
    // 5
    mapView.selectedMarker = nil
    }
    }
    }
    */
    
    
    var randomLineColor: UIColor {
        get {
            let randomRed = CGFloat(drand48())
            let randomGreen = CGFloat(drand48())
            let randomBlue = CGFloat(drand48())
            return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        }
    }
    
    // MARK: UITextField Delegate
    
    func textFieldDidEndEditing(textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
    }
    
    @IBAction func searchBarTouched(sender: UITextField) {
        searchActive = true
    }
    @IBAction func textChanged(sender: UITextField) {
        
        
    }
    @IBAction func editingAddress(sender: UITextField) {
        googleMapsAddressRequest?.cancel()
        searchAddress(sender.text!)
        
    }
    
    
    func searchAddress(address: String){
        geocodeAddress(address, withCompletionHandler: { (status, success) -> Void in
            if !success {
                print(status, terminator: "")
                
                if status == "ZERO_RESULTS" {
                    print("The location could not be found.", terminator: "")
                }
            }
            else {
                let coordinate = CLLocationCoordinate2D(latitude: self.fetchedAddressLatitude, longitude: self.fetchedAddressLongitude)
                self.mapView.camera = GMSCameraPosition.cameraWithTarget(coordinate, zoom: 14.0)
                self.showPopOver()
                
            }
        })
    }
    
    
    
    func autocompleteAddress(address: String!, completion: ((status: String, success: Bool) -> Void)) {
        if let lookupAddress = address {
            let autoCompleteString = baseAutocompleteURL + "input=" + lookupAddress
            
            let autoCompleteURL = NSURL(string: autoCompleteString)
            
            googleMapsAutoCompleteRequest = request(.GET, autoCompleteURL!, parameters: nil).responseSwiftyJSONRenamed{ request, response, json, error,errorCode in
                dispatch_async(dispatch_get_main_queue()){
                    
                    if (error != nil) {
                        print(error)
                        completion(status: "", success: false)
                    }else{
                        let status = json["status"].string!
                        
                        if status == "OK" {
                            let allResults = json["results"].array!
                            self.predictions = []
                            for result in allResults {
                                self.predictions.append(result)
                            }
                            
                            completion(status: status, success: true)
                        }
                        else {
                            completion(status: status, success: false)
                        }
                        
                    }
                    
                }
                
            }
        }
    }
    
    
    func geocodeAddress(address: String!, withCompletionHandler completionHandler: ((status: String, success: Bool) -> Void)) {
        if let lookupAddress = address {
            var geocodeURLString = baseURLGeocode + "address=" + lookupAddress
            geocodeURLString = geocodeURLString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            
            let geocodeURL = NSURL(string: geocodeURLString)
            
            //
            //            request(.Get, geocodeURL).responseSwiftyJSON(queue: dispatch_get_main_queue()){ request, response, json, error in
            //
            //            }
            
            googleMapsAddressRequest = request(.GET, geocodeURL!, parameters: nil).responseSwiftyJSONRenamed(){ request, response, json, error,errorCode in
                dispatch_async(dispatch_get_main_queue()){
                    
                    if (error != nil) {
                        print(error)
                        completionHandler(status: "", success: false)
                    }else{
                        let status = json["status"].string!
                        
                        if status == "OK" {
                            let allResults = json["results"].array!
                            self.lookupAddressResults = allResults[0]
                            print(self.lookupAddressResults)
                            // Keep the most important values.
                            self.fetchedFormattedAddress = self.lookupAddressResults["formatted_address"].string!
                            let geometry = self.lookupAddressResults["geometry"]
                            self.fetchedAddressLongitude = geometry["location"]["lng"].double
                            self.fetchedAddressLatitude = geometry["location"]["lat"].double
                            
                            
                            
                            completionHandler(status: status, success: true)
                        }
                        else {
                            completionHandler(status: status, success: false)
                        }
                        
                    }
                    
                }
                
            }
        }
    }
    
    @IBAction func atrasBtnPressed(sender: UIButton) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
}