//
//  CarritoPickupTableViewCell.swift
//  Monchis
//
//  Created by Javier Rivarola on 11/7/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
protocol SeeBranchPressed {
    func seeBranchPressed(branch: MRestaurant)
}
class CarritoPickupTableViewCell: UITableViewCell {
    var delegate: SeeBranchPressed?
    @IBOutlet weak var addressLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var branch: MRestaurant!

    @IBOutlet weak var goMapBtn: UIButton!
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func goMapPressed(sender: UIButton) {
        delegate?.seeBranchPressed(branch)
    }

}
