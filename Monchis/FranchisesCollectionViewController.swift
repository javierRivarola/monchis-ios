//
//  FranchisesCollectionViewController.swift
//  Monchis
//
//  Created by jrivarola on 22/1/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class FranchisesCollectionViewController: UICollectionViewController,UICollectionViewDelegateFlowLayout,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource  {
    var franchises: [MFranchise] = []
    let api = MAPI.SharedInstance
    var loadingBeforeEmptyDataSet: Bool = true
    var fetchingDataFromNetwork: Bool = false
    var category: MCategory! {
        didSet{
            self.title = category?.name
            loadFranchisesForCategory(category.id!, useCache:  false)
        }
    }
    var shouldAnimate: Bool = false
    var spinner: UIActivityIndicatorView!
    var netError: Bool = false
    let RESTAURANT_CELL_HEIGHT:CGFloat = 150.0
    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView?.emptyDataSetSource = self
        self.collectionView?.emptyDataSetDelegate = self
        configurePullToRefresh()

    }
    
    func configurePullToRefresh(){
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.whiteColor()
        let title = NSMutableAttributedString()
        let tira = NSAttributedString(string: "Tira para actualizar", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14),NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        title.appendAttributedString(tira)
        refreshControl.attributedTitle = title
        refreshControl.addTarget(self, action: #selector(FranchisesCollectionViewController.pullToRefresh), forControlEvents: .ValueChanged)
        self.collectionView?.addSubview(refreshControl)
        self.collectionView?.alwaysBounceVertical = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //        loadBranchesForCategory(category.id!, useCache: false)
        loadFranchisesForCategory(category.id!, useCache:  false)
        
    }

    func createWaitScreen(){
        spinner?.startAnimating()
    }
    
    func pullToRefresh(){
        loadFranchisesForCategory(category.id!, useCache:  false)
    }
    
    
    
    func dismissWaitScreen(){
        spinner?.stopAnimating()
        refreshControl?.endRefreshing()
    }
    
    
    func loadFranchisesForCategory(category_id: Int, useCache: Bool = true){
        fetchingDataFromNetwork = true
        createWaitScreen()
        self.loadingBeforeEmptyDataSet = true
        self.collectionView?.reloadEmptyDataSet()
        
        let parameters:[String : AnyObject] = ["category_id":category_id]
        
        api.jsonRequest(.LIST_FRANCHISES, parameters: parameters, returnCachedDataIfPossible: useCache ) { (json, error) -> Void in
            if debugModeEnabled {
                print(#function,"called with parameters: \(parameters)",json,error)
            }
            if error == nil {
                var tempFranchises:[MFranchise] = []
                if let franchiseArray = json?["data"].array {
                    for data in franchiseArray {
                        let newFranchise = MFranchise(json: data)
                        tempFranchises.append(newFranchise)
                    }
                    self.franchises = tempFranchises
                    
                }
                if self.franchises.count == 0 {
                    self.loadingBeforeEmptyDataSet = false
                }
                self.fetchingDataFromNetwork = false
                self.dismissWaitScreen()
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.collectionView?.reloadData()
                    
                })
            }else{// network error
                self.dismissWaitScreen()
                self.fetchingDataFromNetwork = false
                self.collectionView?.reloadEmptyDataSet()
                if self.navigationController?.visibleViewController == self {
                    let titleLocalizable = NSLocalizedString("Error",comment: "error")
                    let alert = UIAlertController(title: titleLocalizable, message: error, preferredStyle: .Alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Aceptar", comment: "Aceptar"), style: .Default, handler: nil)
                    alert.addAction(ok)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                
            }
            
        }
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return franchises.count > 0 ? 1 : 0
        //return restaurants != nil ? 1 : 0
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(collectionView.frame.size.width/2, RESTAURANT_CELL_HEIGHT)
    }
    
    
    /* franchises data source */
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return franchises.count > 0 ? franchises.count : 0
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("FranchiseReuseID", forIndexPath: indexPath) as! FranchiseCollectionViewCell
        let franchise = franchises[indexPath.row]
        if let imgUrl = franchise.icon_attachment?.imageComputedURL {
            cell.franchiseImageView.hnk_setImageFromURL(imgUrl, placeholder: PLACE_HOLDER_IMAGE)
        }
        cell.franchiseNameLbl.text = franchise.name
        return cell
        
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */
    
    // MARK: EmptyDataSetSource
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        if connectedToNetwork() {
            let image = UIImage(named: "menu-vacio")
            return image
        }else{
            let image = UIImage(named: "icono-error-red")
            return image
        }
    }
    
    func imageAnimationForEmptyDataSet(scrollView: UIScrollView!) -> CAAnimation! {
        
        let animation = CABasicAnimation(keyPath: "transform")
        
        animation.fromValue = NSValue(CATransform3D: CATransform3DIdentity)
        animation.toValue = NSValue(CATransform3D:CATransform3DMakeRotation(CGFloat(M_PI_2), 0.0, 0.0, 1.0))
        
        animation.duration = 0.25
        animation.cumulative = true
        animation.repeatCount = MAXFLOAT
        return animation
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        if loadingBeforeEmptyDataSet {
            if connectedToNetwork() {
                return nil
            }else{
                let noConection = NSAttributedString(string: NSLocalizedString("No hay conexión.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
                
                return noConection
            }
        }
        if connectedToNetwork() {
            let emptyCart = NSAttributedString(string: NSLocalizedString("No hay restaurantes.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
            
            return emptyCart
        }else{
            let noConection = NSAttributedString(string: NSLocalizedString("No hay conexión.", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
            
            return noConection
        }
        
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        if loadingBeforeEmptyDataSet {
            if connectedToNetwork() {
                let gettingData = NSAttributedString(string: NSLocalizedString("Buscando restaurantes...", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17),NSForegroundColorAttributeName:UIColor.whiteColor()])
                
                return gettingData
            }else{
                return nil
            }
        }else{
            if connectedToNetwork() {
                let subTitle = NSAttributedString(string: NSLocalizedString("Prueba con otra categoria!", comment: ""),attributes: [NSFontAttributeName:UIFont.systemFontOfSize(15),NSForegroundColorAttributeName:UIColor.whiteColor()])
                return subTitle
            }else{
                return nil
            }
            
        }
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        if loadingBeforeEmptyDataSet {
            if connectedToNetwork() {
                return nil
            }else{
                let title = NSAttributedString(string: "Volver a Intentar", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(18),NSForegroundColorAttributeName:UIColor.whiteColor()])
                return title
            }
        }else{
            
            let title = NSAttributedString(string: "Volver a Intentar", attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(18),NSForegroundColorAttributeName:UIColor.whiteColor()])
            return title
            
        }
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        loadFranchisesForCategory(category.id!, useCache:  false)
        
    }
    
    func emptyDataSetShouldAnimateImageView(scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return true
    }

}
