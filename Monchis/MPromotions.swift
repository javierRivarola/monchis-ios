//
//  MPromotion.swift
//  Monchis
//
//  Created by Javier Rivarola on 7/29/16.
//  Copyright © 2016 cibersons. All rights reserved.
//

import Foundation
import SwiftyJSON

class MPromotions {
    static let SharedInstance = MPromotions()
    var products: [MProduct] = []
    
    init(){
        
    }
    
    
    
    
   
    
    
    
    
    func getPromotions(){
        print("GET PROMOTIONS CALLED")
        MAPI.SharedInstance.returnCachedData(.GET_PROMOTIONS, parameters: nil) { (json) in
            if debugModeEnabled {
                print("returnCachedData for promotions","called with parameters: nil",json)
            }
            if let jsonArray = json?["data"].array {
                self.updateUIForPromotions(jsonArray)
            }
            
        }
         MAPI.SharedInstance.postQueueRequest(.GET_PROMOTIONS, parameters: nil) { (json, error) in
            
            if debugModeEnabled {
                print(#function,"called with parameters: nil",json,error)
            }
            if error == nil {
                if let jsonArray = json?["data"].array {
                    self.updateUIForPromotions(jsonArray)
                }
            }else{
                
            }
            
        }
        

    }
    
    func updateUIForPromotions(json: [SwiftyJSON.JSON]) {
        var tmp:[MProduct] = []
        for data in json {
            let newProduct = MProduct(json: data)
            if let attachmentsArray = data["attachments"].array {
                for att in attachmentsArray {
                    let newAtt = MAttachment(json: att)
                    newProduct.attachments = [newAtt]
                }
                
            }
            
            if let branchProducts = data["branch_products"].array {
                var tmpBranches:[MRestaurant] = []
                for bProduct in branchProducts {
                    let newBranch = MRestaurant(data: bProduct["branch"])
                    
                    tmpBranches.append(newBranch)
                }
                newProduct.branch_products = tmpBranches
                
            }
            
            tmp.append(newProduct)
        }
        self.products = tmp
        
    }

}