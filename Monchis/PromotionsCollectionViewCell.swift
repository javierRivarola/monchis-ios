//
//  PromotionsCollectionViewCell.swift
//  Monchis
//
//  Created by Monchis Mac on 7/10/17.
//  Copyright © 2017 cibersons. All rights reserved.
//

import UIKit

class PromotionsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var productImgView: UIImageView!
    
    override func applyLayoutAttributes(layoutAttributes: UICollectionViewLayoutAttributes) {
        super.applyLayoutAttributes(layoutAttributes)
        if let attributes = layoutAttributes as? PromotionsLayoutAttributes {
            imageViewHeightLayoutConstraint.constant = attributes.photoHeight
        }
    }
}
